/* Delete Unnecessary Script! */
var showtime = true;
var unload = 0;
var theurl;
var klik = false;
function fctrim(str){
	str = str.replace(/^\s+/, '');
	for(var i = str.length - 1; i >= 0; i--){
		if(/\S/.test(str.charAt(i))){
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}

$(document).ready(function(){
	shownotif();
/*
	$("textarea.chk").redactor({
		buttons: ['bold', 'italic','|','unorderedlist', 'orderedlist', 'outdent', 'indent','|' ,'image'],
		removeStyles: true,
		cleanUp: true,
		autoformat: true,
		imageUpload: isUrl+'index.php/upload/set_img'
	});
*/	

	$("#star_").click(function(){
		unload = 12;
		$("#inotif_").slideUp(500);
	});
	$(".menunya").mouseover(function(){
		$(this).addClass('mousedown');
		$(this).children('.submenunya').stop().show().animate({height:$(this).attr('t')+'px'});
	});
	$(".menunya").mouseout(function(){
		$(this).removeClass('mousedown');
		$(this).children('.submenunya').stop().animate({height:'0px'});
	});
	$('a.minibutton, input, textarea').bind({mousedown: function(){
		$(this).addClass('mousedown');
	},focus: function(){
		$(this).addClass('mousedown');
	},click: function(){
		$(this).addClass('mousedown');
	},blur: function(){
		$(this).removeClass('mousedown');
	},mouseup: function(){
		$(this).removeClass('mousedown');
	}});
	$("input, textarea, select").focus(function(){
		if($(this).attr('wajib')=="yes"){
			$(".msgtitle_").fadeOut('slow');
			$(this).removeClass('wajib');
		}
	});
	$(".tampil").click(function(){
		theurl = $(this).parent().children().val();
		window.open(theurl, '_blank');
		return false;
	});
	$(".hapus").click(function(){
		if(confirm('Hapus File Terpilih sekarang?')){
			theurl = $(this).parent().children().val();
			theurl = theurl.split('/');
			theurl = theurl[theurl.length-1];
			$("#freg_").attr("action", $("#freg_").attr("action") + '/delete/' + theurl);
			$("#freg_").submit();
		}
		return false;
	});
	$("#filenya").change(function(){
		if(confirm('Upload File Pendukung Anda sekarang?')){
			$("#freg_").attr("action", $("#freg_").attr("action") + '/upload');
			$("#freg_").submit();
		}
		return false;
	});
	$("#manual").change(function(){
		if(confirm('Upload File Petunjuk Penggunaan sekarang?')){
			$("#fmanual_").attr("action", $("#fmanual_").attr("action") + '/upload');
			$("#fmanual_").submit();
		}
		return false;
	});
	$("#satuan").autocomplete($("#satuan").attr('url'), {width: 226, selectFirst: false});
	$("#satuan").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#detailsatuan").html(data[2]);
		}
	});
	$("#kelompok").autocomplete($("#kelompok").attr('url'), {width: 226, selectFirst: false});
	$("#kelompok").result(function(event, data, formatted){
	});
	$("#negara").autocomplete($("#negara").attr('url'), {width: 226, selectFirst: false});
	$("#negara").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#detailnegara").html(data[2]);
		}
	});
	$("#jenis_pangan").autocomplete(($("#jenis_pangan").attr('url') + $("#jnspgn").val()), {width: 226, selectFirst: false});
	$("#jenis_pangan").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#nama_jenis").val(data[2]);
		}
	});
	$("#kode_bahan").autocomplete($("#kode_bahan").attr('url'), {width: 226, selectFirst: false});
	$("#kode_bahan").result(function(event, data, formatted){
		if(data){
			$(this).val(data[2]);
			$("#bahan_id").val(data[3]);
			$("#bahan").html('<br> &bull; ' + data[4]);
			$("#latin").html('<br> &bull; ' + data[5]);
			$("#sumber").val(data[6]);
		}
	});
	$("#nama").autocomplete($("#nama").attr('url'), {width: 226, selectFirst: false});
	$("#nama").result(function(event, data, formatted){
		if(data){
			$(this).val(data[2]);
			$("#user_id").val(data[1]);
			$("#jumlah").focus();
		}
	});
	$("#lap_trader").autocomplete($("#lap_trader").attr('url'), {width: 226, selectFirst: false});
	$("#lap_trader").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
		}
	});
	$("#lap_pabrik").autocomplete($("#lap_pabrik").attr('url'), {width: 226, selectFirst: false});
	$("#lap_pabrik").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
		}
	});
});

function ShowDP(obj_date){
	$('#'+obj_date).DatePicker({
		date: $('#'+obj_date).val(),
		current: $('#'+obj_date).val(),
		starts: 1,
		position: 'bottom',
		onBeforeShow: function(){
			var n = $('#'+obj_date).val();
			var now = new Date();
			if(n==""){
				$('#'+obj_date).DatePickerSetDate(now, true);	
			}else{
				$('#'+obj_date).DatePickerSetDate($('#'+obj_date).val(), true);
			}
		},
		onChange: function(formated, dates){
			$('#'+obj_date).val(formated);
		}
	});
}

function save_post(formid){
	$(".msgtitle_").hide();
	$(".msgtitle_").css('color', 'blue');
	$(".msgtitle_").html('Verifikasi Data..');
	$(".msgtitle_").fadeIn('slow');
	var notvalid = 0;
	if(klik) return false;
	$.each($("input:visible, select:visible, textarea:visible"), function(){
		if($(this).attr('wajib')){
			if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
				$(this).addClass('wajib');
				notvalid++;
			}
		}
	});
	if(notvalid==0 && (formid=='#fpassword_' || formid=='#fnewuser_' || formid=='#fnewpendaftar_')){
		if($('#konfirmasi').val()!=$('#pwd').val()){
			$('#konfirmasi').addClass('wajib');
			notvalid = -1;
		}
	}
	if(notvalid==-1){
		$(".msgtitle_").css('color', 'red');
		$(".msgtitle_").html('Konfirmasi Password Harus Sama');
		$(".msgtitle_").fadeIn('slow');
		return false;
	}else if(notvalid>0){
		$(".msgtitle_").css('color', 'red');
		$(".msgtitle_").html('Ada ' + notvalid + ' Kolom Yang Harus Diisi');
		$(".msgtitle_").fadeIn('slow');
		return false;
	}
	klik = true;
	$.ajax({
		type: 'POST',
		url: $(formid).attr('action') + '/ajax',
		data: $(formid).serialize(),
		success: function(data){ 
			//alert(data);return false;
			if(data.search("MSG")>=0){
				arrdata = data.split('#');
				if(arrdata[1]=="OK"){
					$(".msgtitle_").css('color', 'green');
					$(".msgtitle_").html(arrdata[2]);
				}else{
					$(".msgtitle_").css('color', 'red');
					$(".msgtitle_").html(arrdata[2]);
				}
				if(arrdata.length>3){
					setTimeout(function(){location.href = arrdata[3];}, 2000);
					return false;
				}
			}else{
				$(".msgtitle_").css('color', 'red');
				$(".msgtitle_").html('Proses Gagal.');
			}
			klik = false;
		}
	});
	return false;
}

function show_detail(id, url){
	$(id).html('&nbsp;Loading..');
	$(id).load(url);
	return false;
}

function show_input2(status,url){
	$.get(url + status, function(hasil){
	});

}

function show_input(status, allowed, url){
	$("#load").html('&nbsp;Loading..');
	var boleh = allowed.search(status);
	$(".newtrproses").remove();
	if(boleh>=0 && status!=""){
		$.get(url + status, function(hasil){
			if(hasil!=''){
				$("#trproses").after(hasil);
				$("#actproses").val(status);
				$("#load").html('');
				if($("#jml").val()>0){
					alert("Data yang Diupload Belum Lengkap");
					$("#trcatatan").hide();
					$("#pros").hide();
				}else{
					$("#trcatatan").show();
					$("#pros").show();
				}
			}
		});
	}else{
		if(boleh=="0"){
			$(".btp0").hide();
			$(".judulbtp").hide();
			$("#trcatatan").hide();
			$("#pros").hide();
			$("#load").html('');
		}else{
			$("#load").html('');
			$("#actproses").val("");
			$(".newtrproses").remove();
			if($("#jml").val()>0){
				alert("Data yang Diupload Belum Lengkap");
				$("#trcatatan").hide();
				$("#pros").hide();
			}else{
				$("#trcatatan").show();
				$("#pros").show();
			}
		}
	}
	return false;
}

function seldeputi(deputi){
	$('#hasilcb').load($('#deputiaju').attr('url') + deputi);
	return false;
}

function selstat(stat){
	$("#21").hide();
	$("#22").hide();
	$("#23").hide();
	$("#24").hide();
	$("#menu").hide();
	$("#" + stat).show();
	$("#menu").show();
}

function seljenis(id){
	if(id=='203'){
		$(".trjenis").hide();
		$('#jenisstatus').val("202");
		$("#pabrik").attr('usaha', '21');
		$("#tdprodusen").html('Penerima Lisensi *');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$(".trpabrik").hide();
		$(".trprodusenpabrik").show();
		$("#tdprodusenpabrik").html('Pemberi Lisensi *');
	}else{ 
		$(".trjenis").show();}
}

function selstatus(id, subid, jns){
	var status = $("#" + id).val();
	var jenis = $("#" + subid).val();
	var stts = $("#" + jns).val();
	$(".trprodusenpabrik").hide();
	$("#tdprodusen").html('Produsen *');
	$("#tdnmprodusen").html('Nama Produsen *');
	$("#tdalmprodusen").html('Alamat Produsen *');
	$(".trkategori").hide();
	if(status=='201' || status=='202'){
		if(stts=='201'){
			$("#produsen").attr('usaha', '21');
			$("#pabrik").removeAttr('onchange');
			setnextcb($("#stusaha"), 'usaha');
			$("#tdprodusen").html('Pemberi Kontrak *');
			$(".trprodusen").show();
			$("#trkontrak").hide();
			$(".trpabrik").hide();
			$(".trprodusenpabrik").show();
			$("#tdprodusenpabrik").html('Penerima Kontrak *');
			$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
		}else if(stts=='203'){
			$(".trprodusen").show();
			$(".trpabrik").hide();
			$("#pabrik").attr('usaha', '21');		
			$(".trprodusenpabrik").show();			
			$("#tdprodusenpabrik").html('Pengemas Kembali *');
			$("#trkontrak").hide();					
		}else{
			$("#tdprodusen").html('Pabrik *');
			$("#pabrik").attr('usaha', '21');
			setnextcb($("#stusaha"), 'usaha');
			$(".trprodusen").show();
			$("#trkontrak").hide();
			$(".trpabrik").hide();
			$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
		}
		if(jenis=='210'){
			$(".trkategori").show();
		}
	}else if(status=='202'){
		$("#pabrik").attr('usaha', '23');
		setnextcb($("#stusaha"), 'usaha');
		if(stts=='202'){
			$(".trprodusen").show();
			$("#tdprodusen").html('Pabrik *');
			$("#trkontrak").hide();
			document.getElementById("trkontrak").value='';
			$("#kontrak").attr("checked", false);
			document.getElementById("produsen").selectedIndex = 0;
		}		
		else if(stts=='203'){
			$(".trpabrik").show();
			$(".trprodusen").show();		
			$(".trprodusenpabrik").show();			
			$("#tdprodusenpabrik").html('Pengemas Kembali *');
			$("#trkontrak").hide();
			
		}else{
			$("#produsen").attr('usaha', '23');
			$("#pabrik").removeAttr('onchange');
			setnextcb($("#stusaha"), 'usaha');
			$("#tdprodusen").html('Pemberi Kontrak *');
			$(".trprodusen").show();
			$("#trkontrak").hide();
			$(".trpabrik").hide();
			$(".trprodusenpabrik").show();			
			$("#tdprodusenpabrik").html('Penerima Kontrak *');
			$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
		}
	}else if(status=='203'){
		$("#tdprodusen").html('Penerima Lisensi *');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$("#tdnmprodusen").html('Nama Pemberi Lisensi *');
		$("#tdalmprodusen").html('Alamat Pemberi Lisensi *');
		$(".trpabrik input, textarea").val('');
		$(".trpabrik").show();
		$("#pabrik").attr('usaha', '21');
		$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
	}else if(status=='204'){
		$("#pabrik").attr('usaha', '21');
		setnextcb($("#stusaha"), 'usaha');
		$("#tdprodusen").html('Pemberi Kontrak *');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$(".trpabrik").hide();
		$(".trprodusenpabrik").show();
		$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
	}else if(status=='205'){
		$("#pabrik").attr('usaha', '22');
		$("#tdprodusen").html('Pengemas Kembali *');
		setnextcb($("#stusaha"), 'usaha');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$(".trpabrik input, textarea").val('');
		$(".trpabrik").show();
	}else if(status=='206'){
		$("#tdprodusen").html('Pabrik *');
		$("#pabrik").attr('usaha', '21');
		setnextcb($("#stusaha"), 'usaha');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$(".trpabrik").hide();
		$(".trkategori").hide();
		$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
	}
}

function set_post(obj, frm, url){
	$.ajax({
		type: 'POST',
		url: url,
		data: frm.serialize(),
		success: function(data){
			if(data!="") obj.val($.trim(data));
		}
	});
	return false;
}

function batal(){
	location.href = self.location;
	return false;
};

function kembali(url){
	location.href = url;
}

function setnextcb(obj, attr){
	var next = obj.parent().parent().next().children().children().last();
	if(obj.attr('id')=='pabrik'){
		var next = $('#sediaan');
		$("#produsen").html(obj.html());
		$("#produsen").attr("usaha", $("#pabrik").attr("usaha"));
	}else if(obj.attr('id')=='produsen'){
		var next = $('#sediaan');
	}else if(obj.attr('id')=='penerima_kontrak'){
		var next = $('#sediaan');
	}else{
		var next = obj.parent().parent().next().children().children().last();
	}
	if(attr!=null) next.attr(attr, obj.val());
	$.get(obj.attr('url') + obj.val(), function(hasil){
		next.html(hasil);
	});
}

function showfile(obj){
	theurl = obj.parent().children().val();
	arrurl = theurl.split('/');
	//alert(arrurl);
	url = site + '/download/data/' + arrurl[5] + '/' + arrurl[6];
	window.open(url, '_blank');
	return false;
};

function setpabrik(obj){
	$(obj).autocomplete(obj.attr('url'), {width: 226, selectFirst: false});
	$(obj).result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			var alamat = obj.parent().parent().next().children().children().last();
			alamat.text(data[2]);
			var prop = alamat.parent().parent().next().children().children().last();
			prop.val(data[3]);
			if(prop.attr('name').search('PROVINSI')>=0){
				var kota = prop.parent().parent().next().children().children().last();
				$.get(prop.attr('url') + prop.val(), function(hasil){
					kota.html(hasil);
					kota.val(data[4]);
				});
			}
		}
	});
}

function setbtp(obj){
	$(obj).autocomplete(obj.attr('url'), {width: 226, selectFirst: false});
	$(obj).result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			var golongan = obj.parent().parent().next().children().children();
			golongan.val(data[2]);
		}
	});
}


function shownotif(){
	if(unload==0){
		unload = 12;
		$.get($('#inotif_').attr('url'), function(hasil){
			var jum = hasil.length;
			if(jum>3){
				$('#inotif_ div.msg').html(hasil);
				$("#inotif_").slideDown(1500);
				$('#inotif_ div.msg span').css({opacity: 0.0});
				$('#inotif_ div.msg span:first').addClass('show');
				$('#inotif_ div.msg span:first').css({opacity: 1.0});
				if(showtime){
					setInterval('gallery()', 5000);
					showtime = false;
				}
			}else{
				$("#inotif_").slideUp(500);
			}
		});
	}
	unload--;
	setTimeout('shownotif()', 5000);
}

function gallery(){
	var current = ($('#inotif_ div.msg span.show')?  $('#inotif_ div.msg span.show') : $('#inotif_ div.msg span:first'));
	var next = ((current.next().length)?current.next():$('#inotif_ div.msg span:first'));	
	next.css({opacity: 0.0}).addClass('show').animate({opacity: 1.0}, 1500);
	current.animate({opacity: 0.0}, 700).removeClass('show');
}

function freport(objform){
	$.ajax({
	   type: "POST",
	   url: $(objform).attr('action'),
	   data: $(objform).serialize(),
	   success: function(){
		   $(objform).submit();
	   }
	});
}