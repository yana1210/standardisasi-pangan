$(document).ready(function(){
	var checked = false;
	$("#tb_menu").change(function(){
		isi = $(this).val();
		chk = $(".tb_chk:checked").length;
		$("#tb_menu option").each(function(){
			if($(this).text()==isi){
				url = $(this).attr('url');
				jml = $(this).attr('jml');
				met = $(this).attr('met');
			}
		});
		if(url=="") return false;
		if(chk==0 && jml!='0'){
			alert('Maaf, Data belum dipilih.');
			$(this).val(0);
			return false;
		}
		if(jml=='1' && chk > 1){
			alert('Maaf, Data yang bisa diproses hanya 1 (satu).');
			$(this).val(0);
			return false;
		}
		if(jml!='0'){
			if(!confirm('Proses data terpilih sekarang?')){
				$(this).val(0);
				return false;
			}
		}
		if(met=="GET"){
			if(jml=='0')
				location.href = url;
			else
				location.href = url + '/' + $(".tb_chk:checked").val();
		}else if(met=="GETNEW"){
			if(jml=='0'){
				window.open(url, '_blank')
			}else{
				var arr = new Array();
				$.each($(".tb_chk:checked"), function(){
					arr.push($(this).val());
				});
				window.open(url + '/' + arr.join(), '_blank');
			}
		}else if(met=="POST"){
			$('#labelload').fadeIn('Slow');
			$.ajax({
				type: 'POST',
				url: url,
				data: $("#tb_form").serialize(),
				success: function(data){
					//alert(data); return false;
					if(data.search("MSG")>=0){
						arrdata = data.split('MSG#');
						arrdata = arrdata[1].split('#');
						alert(arrdata[0]);
						if(arrdata.length>1) location.href = arrdata[1];
					}else{
						alert('Proses Gagal.');
					}
					$('#labelload').fadeOut('Slow');
				}
			});
		}
		$(this).val(0);
		return false;
	});
	$(".tabelajax tr").mouseover(function(){
    	$(this).addClass("hilite");
   	});
	$(".tabelajax tr").mouseout(function(){
		$(this).removeClass("hilite");
	});
	$(".tb_chk").click(function(){
		checked = true;
		if(!this.checked){
			$(this).parent().parent().removeClass("selected");
			$("#tb_chkall").attr('checked', this.checked);
		}else{
			$(this).parent().parent().addClass("selected");
			if($(".tabelajax input:checkbox.tb_chk:checked").length == $(".tabelajax input:checkbox:not(#tb_chkall)").length) $("#tb_chkall").attr('checked', this.checked);
		}
	});
	$(".tabelajax td").click(function(btn){
		if($("#tb_chkall").attr('id')){
			if(checked){
				checked = false;
				return true;
			}
			if(!btn.ctrlKey){
				$(".tabelajax tr").removeClass("selected");
				$(".tabelajax input:checkbox").attr('checked', false);
			}
			$(this).parent().addClass("selected");
			$(this).parent().children().children().attr('checked', true);
			if($(".tabelajax input:checkbox.tb_chk:checked").length == $(".tabelajax input:checkbox:not(#tb_chkall)").length) $("#tb_chkall").attr('checked', true);
		}else{
			if(checked){
				checked = false;
				return true;
			}
			$(".tabelajax tr").removeClass("selected");
			$(".tabelajax input:checkbox").attr('checked', false);
			$(this).parent().addClass("selected");
			$(this).parent().children().children().attr('checked', true);
			if($(".tabelajax input:checkbox.tb_chk:checked").length == $(".tabelajax input:checkbox:not(#tb_chkall)").length) $("#tb_chkall").attr('checked', true);
			if($(this).parent().attr('urldetil')){
				if(fctrim($(this).parent().attr('urldetil'))!=""){
					$('#newtr').remove();
					var jmltd = $('td', $(this).parent()).length;
					var addtd = '';
					if($(".tabelajax input:checkbox").length > 0){
						addtd = '<td></td>';
						jmltd--;
					}
					if($(this).parent().children().first().attr('class')){
						$(this).parent().after('<tr id="newtr">' + addtd + '<td id="filltd" colspan="' + jmltd + '" class="' + $(this).parent().children().first().attr('class') + '"></td></tr>');
					}else{
						$(this).parent().after('<tr id="newtr">' + addtd + '<td id="filltd" colspan="' + jmltd + '"></td></tr>');
					}
					$('#filltd').html('Loading..');
					$('#filltd').load($('#urldtl').val() + fctrim($(this).parent().attr('urldetil')));
				}
			}
		}
		return true;
	});
	$(".tabelajax td").bind("contextmenu",function(kode){
		if($("#tb_chkall").attr('id')){
			if(checked){
				checked = false;
				return true;
			}
			if($(this).parent().attr('urldetil')){
				if(fctrim($(this).parent().attr('urldetil'))!=''){
					$('#newtr').remove();
					var jmltd = $('td', $(this).parent()).length;
					var addtd = '';
					var cls = '';
					if($(this).parent().children().first().attr('class')) cls = ' class="' + $(this).parent().children().first().attr('class') + '"';
					$(this).parent().after('<tr id="newtr">' + addtd + '<td id="filltd" colspan="' + jmltd + '"' + cls + '></td></tr>');
					$('#filltd').html('Loading..');
					$('#filltd').load($('#urldtl').val() + fctrim($(this).parent().attr('urldetil')));
				}
			}
		}
		return false;
	});
	$("#tb_chkall").click(function(){
		$(".tabelajax").find(':checkbox').attr('checked', this.checked);
		$('#newtr').remove();
		if(!this.checked){
			$(".tabelajax input:checkbox:not(#tb_chkall)").parent().parent().removeClass("selected");
		}else{
			$(".tabelajax input:checkbox:not(#tb_chkall)").parent().parent().addClass("selected");
		}
	});
	$("#tb_hal").bind("contextmenu",function(kode){
		newhal = $(this).val();
		if(newhal>1)
			newhal--;
		else
			return false;
		redirect_url(newhal);
		return false;
	});
	$("span.nav").click(function(){
		total = $("#tb_total").html();
		total = parseInt(total);
		newhal = $("#tb_hal").val();
		newhal = parseInt(newhal);
		if(escape($(this).html())=='%BB' && newhal<total){
			newhal++;
			redirect_url(newhal);
		}else if(escape($(this).html())=='%AB' && newhal>1){
			newhal--;
			redirect_url(newhal);
		}
		return false;
	});
	$(".order").click(function(){ 
		$("#orderby").val($(this).attr("orderby"));
		$("#sortby").val($(this).attr("sortby"));
		redirect_url($("#tb_hal").val());
		return false;
	});
	$("#tb_view, #tb_hal, #tb_cari").bind('keypress', function(kode){
		if((kode.keyCode==13) || (((kode.keyCode==38) || (kode.keyCode==40)) && ($(this).attr("id")=="tb_hal"))){
			newhal = $("#tb_hal").val();
			if($(this).attr("id")=="tb_hal"){
				if(kode.keyCode==38){
					newhal++;
				}else if(kode.keyCode==40){
					if(newhal>1)
						newhal--;
					else
						return false;
				}
			}
			redirect_url(newhal);
		}
	});
});

function redirect_url(newhal){
	$('#labelload').fadeIn('Slow');
	newlocation = 'row/' + $("#tb_view").val() + '/page/' + newhal + '/order/' + $("#orderby").val() + '/' + $("#sortby").val();
	if($("#tb_cari").val()!=null) if($("#tb_cari").val()!="") newlocation +=  '/search/' + $("#tb_keycari").val() + '/' + $("#tb_cari").val().replace('/', '');
	if($('#useajax').val()=="TRUE"){
		$("#tb_hal").val(newhal);
		newlocation = fctrim('ajax/' + newlocation);
		if(window.location.hash!=newlocation) window.location.hash = newlocation;
		newlocation = $("#tb_form").attr('action') + '/' + newlocation;
		$('#divtabelajax').load(newlocation);
		$('#labelload').fadeOut('Slow');
	}else{
		newlocation = fctrim($("#tb_form").attr('action') + '/' + newlocation);
		location.href = newlocation;
	}
}

function fctrim(str){
	str = str.replace(/^\s+/, '');
	for(var i = str.length - 1; i >= 0; i--){
		if(/\S/.test(str.charAt(i))){
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}