$().ready(function(){
	var matched, browser;

jQuery.uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
        /(msie) ([\w.]+)/.exec( ua ) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
        [];

    return {
        browser: match[ 1 ] || "",
        version: match[ 2 ] || "0"
    };
};

matched = jQuery.uaMatch( navigator.userAgent );
browser = {};

if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if ( browser.chrome ) {
    browser.webkit = true;
} else if ( browser.webkit ) {
    browser.safari = true;
}

jQuery.browser = browser;
	$(".num").keydown(function (e) {
		if(e.keyCode == 188){
			$(this).val($(this).val() + '.');
			e.preventDefault();
		}
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		   (e.keyCode == 65 && e.ctrlKey === true) || 
		   (e.keyCode >= 35 && e.keyCode <= 39)) {
				return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	$(".telp").keydown(function (e) {
		if(e.keyCode == 188){
			$(this).val($(this).val() + '.');
			e.preventDefault();
		}
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		   (e.keyCode == 65 && e.ctrlKey === true) || 
		   (e.keyCode == 189 ) || 
		   (event.shiftKey && e.keyCode == 57 ) || 
		   (event.shiftKey && e.keyCode == 48 ) || 
		   (e.keyCode >= 35 && e.keyCode <= 39)) {
				return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$(".tanggal").click(function() { datepicker($(this)) });
	$(".tanggal").focus(function(){
		$(this).mask("9999-99-99");
	});
	$(".day").focus(function(){
		$(this).mask("99");
	});
	$(".kodepos").focus(function(){
		$(this).mask("99999");
	});
	$("#input-npwp").focus(function(){
		$(this).mask("99.999.999.9-999.999");
	});
	$("#keycode").focus(function(){
		$(this).mask("999999"); 
	});
	$("#product").autocomplete($("#product").attr('url'), {width: 210, selectFirst: false});
	$("#product").result(function(event, data, formatted){
		if(data){
			$('#product_id').val(data[1]);
			$(this).val(data[2]);
			$('#produk').val(data[2]);
			$('#stok').val(data[3]);
		}
	});
	$("#industry").autocomplete($("#industry").attr('url'), {width: 210, selectFirst: false});
	$("#industry").result(function(event, data, formatted){
		if(data){
			$('#industry_id').val(data[1]);
			$(this).val(data[2]);
			$('#industri_farmasi').val(data[2]);
		}
	});
	$("#contenta").autocomplete($("#contenta").attr('url'), {width: 210, selectFirst: false});
	$("#contenta").result(function(event, data, formatted){
		if(data){
			$('#content_id').val(data[1]);
			$(this).val(data[2]);
			$('#bahan_baku_obat').val(data[2]);
		}
	});
	$("#group").autocomplete($("#group").attr('url'), {width: 210, selectFirst: false});
	$("#group").result(function(event, data, formatted){
		if(data){
			$('#group_id').val(data[1]);
			$(this).val(data[2]);
			$('#golongan_obat').val(data[2]);
		}
	});
	$("#unit").autocomplete($("#unit").attr('url'), {width: 210, selectFirst: false});
	$("#unit").result(function(event, data, formatted){
		if(data){
			$('#unit_id').val(data[1]);
			$(this).val(data[2]);
			$('#satuan').val(data[2]);
		}
	});
	$("#package").autocomplete($("#package").attr('url'), {width: 210, selectFirst: false});
	$("#package").result(function(event, data, formatted){
		if(data){
			$('#package_id').val(data[1]);
			$(this).val(data[2]);
			$('#kemasan').val(data[2]);
		}
	});
	$("#form").autocomplete($("#form").attr('url'), {width: 210, selectFirst: false});
	$("#form").result(function(event, data, formatted){
		if(data){
			$('#form_id').val(data[1]);
			$(this).val(data[2]);
			$('#bentuk_sediaan').val(data[2]);
		}
	});
	$("#pharma").autocomplete($("#pharma").attr('url'), {width: 400, selectFirst: false});
	$("#pharma").result(function(event, data, formatted){
		if(data){
			$('#pharma_id').val(data[1]);
			$(this).val(data[2]);
			$('#farmakoterapi').val(data[2]);
		}
	});
	$("#sediaan").autocomplete($("#sediaan").attr('url'), {width: 226, selectFirst: false});
	$("#sediaan").result(function(event, data, formatted){
		if(data){
			$("#kode").val(data[1]);
			$("#comodity").val(data[3]);
			$(this).val(data[2]);
		} 
	});
	$(".kecamatan").autocomplete($(".kecamatan").attr('url'), {width: 226, selectFirst: false});
	$(".kecamatan").result(function(event, data, formatted){
		if(data){
			$("#region_id"+$(this).attr('autoid')).val(data[1]);
			$(this).val(data[2]);
			$("#kecamatan_name"+$(this).attr('autoid')).val(data[2]);
		}
	});
	$("#tradercity").autocomplete($("#tradercity").attr('url'), {width: 226, selectFirst: false});
	$("#tradercity").result(function(event, data, formatted){
		if(data){
			$("#traderid").val(data[1]);
			$(this).val(data[2]);
			$("#kotatrader").val(data[2]);
		}
	});
	$("#tradername").autocomplete($("#tradername").attr('url'), {width: 226, selectFirst: false});
	$("#tradername").result(function(event, data, formatted){
		if(data){
			$("#traderid").val(data[1]);
			$(this).val(data[2]);
			$("#namatrader").val(data[2]);
		}
	});
	$("#ingredients_code").autocomplete($("#ingredients_code").attr('url'), {width: 226, selectFirst: false});
	$("#ingredients_code").result(function(event, data, formatted){
		if(data){
			$(this).val(data[2]);
			$("#ingredients_id").val(data[3]);
			$("#ingredients_name").html('<br> &bull; ' + data[4]);
			$("#ingredients_name_latin").html('<br> &bull; ' + data[5]);
			$("#ingredients_source").val(data[6]);
		}
	});
	$("#pbk_code").autocomplete($("#pbk_code").attr('url'), {width: 375, selectFirst: false});
	$("#pbk_code").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#pbk_id").val(data[5]);
		}
	});
	$("#pbk_code2").autocomplete($("#pbk_code2").attr('url'), {width: 375, selectFirst: false});
	$("#pbk_code2").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#pbk_id2").val(data[5]);
		}
	});
	$("#penerima_kontrak").autocomplete($("#penerima_kontrak").attr('url'), {width: 375, selectFirst: false});
	$("#penerima_kontrak").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#pbk_id3").val(data[5]);
		}
	});
	$("#pbk_code4").autocomplete($("#pbk_code4").attr('url'), {width: 375, selectFirst: false});
	$("#pbk_code4").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#pbk_id4").val(data[5]);
		}
	});
	$("#produsen").autocomplete($("#produsen").attr('url'), {width: 375, selectFirst: false});
	$("#produsen").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#pbk_id5").val(data[5]);
		}
	});
	$("#pbk_code6").autocomplete($("#pbk_code6").attr('url'), {width: 375, selectFirst: false});
	$("#pbk_code6").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#pbk_id6").val(data[5]);
		}
	});
	$("#penerima_lisensi").autocomplete($("#penerima_lisensi").attr('url'), {width: 375, selectFirst: false});
	$("#penerima_lisensi").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#pbk_id7").val(data[5]);
		}
	});
	$("#pbk_code8").autocomplete($("#pbk_code8").attr('url'), {width: 375, selectFirst: false});
	$("#pbk_code8").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#pbk_id8").val(data[5]);
		}
	});

	$("#ingredients_name").autocomplete($("#ingredients_name").attr('url'), {width: 375, selectFirst: false});
	$("#ingredients_name").result(function(event, data, formatted){
		if(data){
			$(this).val(data[4]);
//			$("#ingredients_name").val(data[2]);
		}
	});
	$("#ingredients_name1").autocomplete($("#ingredients_name1").attr('url'), {width: 375, selectFirst: false});
	$("#ingredients_name1").result(function(event, data, formatted){
		if(data){
			$(this).val(data[4]);
		}
	});
	$("#ingredients_name2").autocomplete($("#ingredients_name2").attr('url'), {width: 375, selectFirst: false});
	$("#ingredients_name2").result(function(event, data, formatted){
		if(data){
			$(this).val(data[4]);
		}
	});
	$("#ingredients_name3").autocomplete($("#ingredients_name3").attr('url'), {width: 375, selectFirst: false});
	$("#ingredients_name3").result(function(event, data, formatted){
		if(data){
			$(this).val(data[4]);
		}
	});
	$("#pbk_code22").autocomplete($("#pbk_code22").attr('url'), {width: 375, selectFirst: false});
	$("#pbk_code22").result(function(event, data, formatted){
		if(data){
			$(this).val(data[1]);
			$("#pbk_id22").val(data[5]);
			$("#address").val(data[2]).attr('readonly', 'readonly');
			$("#province").val(data[3]).attr('disabled', 'disabled');
			$("#city").val(data[4]).attr('disabled', 'disabled');
			$("#province1").val(data[3]);
			$("#city1").val(data[4]);
		}
	});


});

function show_input(status, allowed, url){
	$("#load").html('&nbsp;Loading..');
	var boleh = allowed.search(status);
	$(".newtrproses").remove();
	//if($("#jmlkomposisi").val()==0){
	//	alert("Komposisi belum diisi");
	//	location.href = $("#jmlkomposisi").attr('urlkom');
	//	return false;
	//} 
	if(boleh>=0 && status!=""){
		$.get(url + status, function(hasil){
			if(hasil!=''){
				$("#trproses").after(hasil);
				$("#actproses").val(status);
				$("#load").html('');
				if($("#jml").val()>0){
					alert("Data yang Diupload Belum Lengkap");
					$("#trcatatan").hide();
					$("#pros").hide();
				}else{
					$("#trcatatan").show();
					$("#pros").show();
				}
			}
		});
	}else{
		if(boleh=="0"){
			$(".btp0").hide();
			$(".judulbtp").hide();
			$("#trcatatan").hide();
			$("#pros").hide();
			$("#load").html('');
		}else{
			$("#load").html('');
			$("#actproses").val("");
			$(".newtrproses").remove();
			if($("#jml").val()>0){
				alert("Data yang Diupload Belum Lengkap");
				$("#trcatatan").hide();
				$("#pros").hide();
			}else{
				$("#trcatatan").show();
				$("#pros").show();
			}
		}
	}
	return false;
}

function setnextcb(obj, attr, target){
	var next = obj.parent().parent().next().children().children().last();
    if(obj.attr('id')=='negara'){
        var next = $('#propinsi');
    }else if(obj.attr('id')=='propinsi'){
		var next = $('#kota');
	}else if(obj.attr('id')=='kota'){
		var next = $('#kecamatan');
	}else if(obj.attr('id')=='type'){
		var next = $('#propinsi');
	}else{
		var next = obj.parent().parent().next().children().children().last();
	}
	if(attr!=null) next.attr(attr, obj.val());
	$.get(obj.attr('url') + obj.val(), function(hasil){
		next.html(hasil);
	});
}

function setnextcb2(obj, attr, target){
	var next = obj.parent().parent().next().children().children().last();
    if(obj.attr('id')=='propinsi'){
		var next = $('#kota');
	}else if(obj.attr('id')=='kota'){
		var next = $('#kecamatan');
	}else if(obj.attr('id')=='type'){
		var next = $('#propinsi');
	}else{
		var next = obj.parent().parent().next().children().children().last();
	}
	if(attr!=null) next.attr(attr, obj.val());
	if(obj.attr('id')=='propinsi'){
		$.get(obj.attr('url') + obj.val()+'/'+$("#type").val(), function(hasil){
			next.html(hasil);
		});
	}else{
		$.get(obj.attr('url') + obj.val(), function(hasil){
			next.html(hasil);
		});
	}
}

function save_post(form){
	var notvalid = 0;
	var data;
	$(form + " div.notification").fadeOut(500);
	$(form + " div.notification").removeClass('success').removeClass('error').removeClass('attention').removeClass('hidden').removeClass('information');
	$(form + " div.notification").addClass('information');
	$(form + " div.notification div").html('Verifikasi data..');
	$(form + " div.notification").fadeIn(1000);
	$.each($(form + " input:visible,"+ form + " select:visible,"+ form + " textarea:visible"), function(){
		if($(this).attr('required')){
			if($(this).attr('required') && ($(this).val()=="" || $(this).val()==null )){
				//var test = $(this).attr('id');
				//alert(test.toString());
				notvalid++;
			}
		}
	});
	if(notvalid>0){
		$(form + " div.notification").removeClass('success').removeClass('error').removeClass('attention').removeClass('hidden').removeClass('information');
		$(form + " div.notification").addClass('attention');
		$(form + " div.notification div").html('Ada ' + notvalid + ' kolom yang belum diisi/dipilih, mohon periksa kembali isian Anda pada kolom bertanda <b class="red">*</b>)');
	}else{
		if($("#pwd").length>0 && $("#cfrmpwd").length>0){
			if($("#pwd").val() != $("#cfrmpwd").val()){
				$(form + " div.notification").removeClass('success').removeClass('error').removeClass('attention').removeClass('hidden').removeClass('information');
				$(form + " div.notification").addClass('attention');
				$(form + " div.notification div").html('Password & Konfirmasi Password yang Anda masukkan tidak sama');
				return false;
			}
		}
		if(form == '#fdirect'){
			$(form).submit();
		}else{
			$.ajax({
			type: 'POST',
			url: $(form).attr('action') + '/ajax',
			data: $(form).serialize(),
			success: function(data){
				if(data.search("MSG")>=0 || data.search("msg")>=0){ 
					arrdata = data.split('#');
					$(form + " div.notification").addClass(arrdata[1]);
					$(form + " div.notification div").html(arrdata[2]);
					if(arrdata.length>3){
						setTimeout(function(){location.href = arrdata[3];}, 2000);
					}
				}else{
					$(form + " div.notification").addClass('error');
					$(form + " div.notification div").html('Proses gagal. Silahkan ulangi beberapa saat lagi.');
				}
				}
			});
		}
	}
	return false;
}



function setnextcb(obj, attr){
	var next = obj.parent().parent().next().children().children().last();
	var checktot = "";
	if(obj.attr('id')=='province'){
		var next = $('#city');
		next.show();
	}else if(obj.attr('id')=='pabrik'){
		var next = $('#sediaan');
	}else if(obj.attr('id')=='produsen' || obj.attr('id')=='penerima_kontrak'|| obj.attr('id')=='penerima_lisensi'){
		var next = $('#sediaan');
	}else if(obj.attr('id')=='kelompok'){
		$('#kel2').show();
		var next = $('#kelompok2');
		var checktot = "chk";
	}else if(obj.attr('id')=='kelompok2'){
		$('#kel3').show();
		var next = $('#kelompok3');
		var checktot = "chk";
	}else{
		var next = obj.parent().parent().next().children().children().last();
	}
	next.html('<option value="">Loading...</option>');
	if(attr!=null) next.attr(attr, obj.val());
	$.get(obj.attr('url') + obj.val(), function(hasil){
		next.show(300);
		if(checktot == "chk" && hasil == ' <option value="">&nbsp;</option>'){
			next.hide(300);
		}else{
			next.html(hasil);
		}
	});
}


function datepicker(id){
	thn = new Date();
	$(id).datepicker({ 
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		yearRange: "-100:+5",
	});
	$(id).datepicker('show');
}

function show_detail(id, url){
	$(id).html('&nbsp;Loading..');
	$(id).load(url);
	return false;
}

function show_input2(status,url){
	$.get(url + status, function(hasil){
	});

}
function seldeputi(deputi){
	$('#hasilcb').load($('#deputiaju').attr('url') + deputi);
	return false;
}

function selstat(stat){
	$("#21").hide();
	$("#22").hide();
	$("#23").hide();
	$("#24").hide();
	$("#menu").hide();
	$("#" + stat).show();
	$("#menu").show();
}

function seljenis(id){
	if(id=='203'){
		$(".trjenis").hide();
		$('#jenisstatus').val("202");
		$("#pabrik").attr('usaha', '21');
		$("#tdprodusen").html('Penerima Lisensi *');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$(".trpabrik").hide();
		$(".trprodusenpabrik").show();
		$("#tdprodusenpabrik").html('Pemberi Lisensi *');
	}else{ 
		$(".trjenis").show();}
}

function selstatus(id, subid, jns){
	var status = $("#" + id).val();
	var jenis = $("#" + subid).val();
	var stts = $("#" + jns).val();
	$(".trprodusenpabrik").hide();
	$("#tdprodusen").html('Produsen *');
	$("#tdnmprodusen").html('Nama Produsen *');
	$("#tdalmprodusen").html('Alamat Produsen *');
	$(".trkategori").hide();
	if(status=='201' || status=='202'){
		if(stts=='201'){
			$("#produsen").attr('usaha', '21');
			$("#pabrik").removeAttr('onchange');
			setnextcb($("#stusaha"), 'usaha');
			$("#tdprodusen").html('Pemberi Kontrak *');
			$(".trprodusen").show();
			$("#trkontrak").hide();
			$(".trpabrik").hide();
			$(".trprodusenpabrik").show();
			$("#tdprodusenpabrik").html('Penerima Kontrak *');
			$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
		}else if(stts=='203'){
			$(".trprodusen").show();
			$(".trpabrik").hide();
			$("#pabrik").attr('usaha', '21');		
			$(".trprodusenpabrik").show();			
			$("#tdprodusenpabrik").html('Pengemas Kembali *');
			$("#trkontrak").hide();					
		}else{
			$("#tdprodusen").html('Pabrik *');
			$("#pabrik").attr('usaha', '21');
			setnextcb($("#stusaha"), 'usaha');
			$(".trprodusen").show();
			$("#trkontrak").hide();
			$(".trpabrik").hide();
			$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
		}
		if(jenis=='210'){
			$(".trkategori").show();
		}
	}else if(status=='202'){
		$("#pabrik").attr('usaha', '23');
		setnextcb($("#stusaha"), 'usaha');
		if(stts=='202'){
			$(".trprodusen").show();
			$("#tdprodusen").html('Pabrik *');
			$("#trkontrak").hide();
			document.getElementById("trkontrak").value='';
			$("#kontrak").attr("checked", false);
			document.getElementById("produsen").selectedIndex = 0;
		}		
		else if(stts=='203'){
			$(".trpabrik").show();
			$(".trprodusen").show();		
			$(".trprodusenpabrik").show();			
			$("#tdprodusenpabrik").html('Pengemas Kembali *');
			$("#trkontrak").hide();
			
		}else{
			$("#produsen").attr('usaha', '23');
			$("#pabrik").removeAttr('onchange');
			setnextcb($("#stusaha"), 'usaha');
			$("#tdprodusen").html('Pemberi Kontrak *');
			$(".trprodusen").show();
			$("#trkontrak").hide();
			$(".trpabrik").hide();
			$(".trprodusenpabrik").show();			
			$("#tdprodusenpabrik").html('Penerima Kontrak *');
			$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
		}
	}else if(status=='203'){
		$("#tdprodusen").html('Penerima Lisensi *');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$("#tdnmprodusen").html('Nama Pemberi Lisensi *');
		$("#tdalmprodusen").html('Alamat Pemberi Lisensi *');
		$(".trpabrik input, textarea").val('');
		$(".trpabrik").show();
		$("#pabrik").attr('usaha', '21');
		$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
	}else if(status=='204'){
		$("#pabrik").attr('usaha', '21');
		setnextcb($("#stusaha"), 'usaha');
		$("#tdprodusen").html('Pemberi Kontrak *');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$(".trpabrik").hide();
		$(".trprodusenpabrik").show();
		$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
	}else if(status=='205'){
		$("#pabrik").attr('usaha', '22');
		$("#tdprodusen").html('Pengemas Kembali *');
		setnextcb($("#stusaha"), 'usaha');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$(".trpabrik input, textarea").val('');
		$(".trpabrik").show();
	}else if(status=='206'){
		$("#tdprodusen").html('Pabrik *');
		$("#pabrik").attr('usaha', '21');
		setnextcb($("#stusaha"), 'usaha');
		$(".trprodusen").show();
		$("#trkontrak").hide();
		$(".trpabrik").hide();
		$(".trkategori").hide();
		$("#ipabrik").attr('url',site+'/autocomplete/pabrik/1/MD/');
	}
}
