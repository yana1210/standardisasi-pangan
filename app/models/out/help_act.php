<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Help_act extends Model{ 

	function set_help($sMenu="", $sSubmenu="", $iIdsalah=""){
		$esp = get_instance();
		$esp->load->model("main","main", true);
		//$iId = $this->input->post('id');
		if($sMenu=='frm-reset'){ 
            $arrLogin = $this->input->post('reset');
			if($arrLogin['USER_MAIL']!='' ){ //&& $code!='' 
				$query2= "SELECT user_pic_mail FROM tm_user_pic where user_pic_mail ='".$arrLogin['USER_MAIL']."'";
				$arrData2 = $this->db->query($query2)->row_array(); 
				if($arrData2['user_pic_mail'] == ''){
					$msg = "MSG#OK#Email Belum Terdaftar";
				}else{

			        $to = $arrLogin['USER_MAIL'];
					$USER2 = $this->db->query("SELECT USER_ID  FROM tm_user_pic WHERE USER_PIC_MAIL = '$to'")->row_array();
					$USER = $USER2['USER_ID'];
			        $subject = "Reset Password";
			        $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial;  width:580px;"><tr><td>

									<p>Yth. Bapak/Ibu Pengguna Aplikasi e-Standardisasi Pangan BPOM</p><br />
									<p>Terima kasih atas partisipasi Anda dalam menggunakan aplikasi e-Standardisasi Pangan</p><br />
									<p>Jangan Lupa Mengganti Password Anda Secara Berkala.</p>
									<p>Klik link berikut untuk reset password :<span style="margin-left: 100px;"><a style="color:white;" href="'. site_url().'help/reset/'.(hash("sha256", $USER)).'/'.$USER.'" target="_blank">Klik disini</a></span>
									</p><br /><br />
					Terima Kasih</td></tr></table>';
					$esp->main->sendMailLocal('register', $to, $isi, $subject);
					// $esp->main->send_mail('register', $to, $isi, $subject);
					$msg = "MSG#OK#Email Berhasil Terkirim#".site_url();
				}
			}
			return $msg;

		}else if($sMenu=='frm-contact'){ 
			$arrLogin = $this->input->post('contact'); 
			$this->db->insert('tm_our_contact', $arrLogin); 

			$to = $arrLogin['EMAIL'];
			$subject = 'Kontak Kami - Badan POM Indonesia';
            $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial;  width:580px;"><tr><td>

					<p>Yth. Bapak/Ibu Pengguna Aplikasi e-Standardisasi Pangan BPOM</p><br />
					<p>Terima kasih atas partisipasi Anda dalam menggunakan aplikasi e-Standardisasi Pangan</p><br />
					<p>Nama Lengkap : '.$arrLogin['NAME'].'</p>
					<p style="color:white">Email        : '.$to.'</p>
					<p>Pertanyaan   : '.$arrLogin['QUESTION'].'</p><br /><br />
						</p><br /><br />
					Terima Kasih</td></tr></table>';
			$esp->main->sendMailLocal('', $to, $isi, $subject);
			// $esp->main->send_mail('', $to, $isi, $subject);
			$msg = "MSG#OK#Proses berhasil dilakukan. Terima kasih telah menghubungi kontak kami#".site_url();
			// return $msg;

		}else if($sMenu=='frm-password'){
			$iCounTol = $esp->main->get_bol_trans_begin();
			$NEW_PASS = $this->input->post('NEW_PASS');
			$CONF_PASS = $this->input->post('CONF_PASS');
			$id = $this->input->post('id');
			$kd=$this->newsession->userdata('captcha2'); 
			$code=$this->input->post('KODE'); 
			if($NEW_PASS <> $CONF_PASS){
				return "MSG#OK#Konfirmasi Password Baru Tidak Sesuai";
			}else{
				$PASSWORD['USER_PASSWORD'] = hash("sha256", $NEW_PASS);
				$this->db->where('USER_ID', $id); //$_SESSION['USER_ID']
				$this->db->update('tm_user', $PASSWORD);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$iRqsId = $iId;
				if($esp->main->get_bol_trans_status($iCounTol)){
					return "MSG#OK#Proses Ubah Password Berhasil#".site_url();
				}else{
					return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
				}
			}
			// if($code != ''){
			// 	if ($kd==$code){
			// 	}else{
			// 		return "MSG#ERR#Kode Keamanan Tidak Sesuai! $kd";
			// 	}
			// }else{
			// 	return "MSG#ERR#Silahkan Lengkapi Data! ";
			// }

		}
	}	



	function get_help($sMenu="", $iId=""){ 
		$esp = get_instance();
		$esp->load->model("main","main", true);
		if($sMenu == "frm-reset"){ 

			$arrData['act'] = site_url('help/post/frm-reset/edit');
			
		} else if($sMenu == "frm-contact"){ 

			$arrData['act'] = site_url('help/post/frm-contact/edit');
			
		} else if($sMenu == "frm-manual") {

			$arrData['act'] = site_url('help/post/frm-manual/edit');
			$sqlProfile = "SELECT A.USER_ID, A.USER_NAME, A.USER_TYPE_ID, B.STATUS_DETAIL AS 'STATUS', A.USER_NPWP, A.USER_ADDR, 
						A.REGION_ID, C.REGION_NAME AS 'KOTA', A.USER_ZIP, A.USER_TELP, A.USER_FAX, A.USER_MAIL

						FROM tm_user A

						LEFT JOIN tr_status B ON B.STATUS_ID = A.USER_TYPE_ID 
						LEFT JOIN tm_region C ON C.REGION_ID = A.REGION_ID

						WHERE A.USER_ID ='".$_SESSION['USER_ID']."'" ;
			$dataProfile = $esp->main->get_arr_result($sqlProfile);
			// if($dataProfile){
			// 	foreach($sqlProfile->result_array() as $rowProfile){
			// 		$arrdata['dtprofile'] = $rowProfile;
			// 	}
			// }
		}

		
		$arrData = array(
            "iId" => $iId,
            "action" => $action,
            "arrUser" => $arrUser,
            "DATA_DOC" => $DATA_DOC,
            "USER_TYPE" => $USER_TYPE,
            "ID_TYPE" => $ID_TYPE,
            "COMPANY_SCALE" => $COMPANY_SCALE,
            "COMPANY_STATUS" => $COMPANY_STATUS
        );	
		return $arrdata;
	}
	

	function forget_password($iId){
		$datasess['_LOGGED'] = TRUE; 
		echo $_SESSION['_LOGGED'];
		$this->newsession->set_userdata($datasess);
		if(!$_SESSION['_LOGGED'] = TRUE){
			$sRet = '<script>location.href="'.site_url().'help/reset/'.$iId.'"</script>';
		}else{
			$sRet = "MSG#ERR#Login Gagal" . site_url();
		}
		 echo $sRet;
	}
	
}
?>