<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');
class Register_act extends Model{

	var $arrext = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.pdf', ".PDF", ".png", ".PNG");

	function set_register($sMenu="", $sSubmenu="", $iIdsalah=""){
		$esp = get_instance();
		$esp->load->model("main","main", true);
		$iId = $this->input->post('id');
		if($sMenu=='register'){
            $iCounTol = $esp->main->get_bol_trans_begin();
			$arrTmUser = $this->input->post('REG');
			$arrTmUserCom = $this->input->post('REGCOM');
			$arrTmUserPrsn = $this->input->post('REGPRSN');
			if($sSubmenu == 'save'){
				$this->db->insert('tm_user', $arrTmUser);
		        $iUserId = $this->db->insert_id();

				if($arrTmUserCom['USER_COM_STATUS_ID'] != ''){
					$arrTmUserCom['USER_ID'] = $iUserId;
					$this->db->insert('tm_user_com', $arrTmUserCom);
		            $esp->main->get_bol_trans_check($iCounTol);
				}
				if($arrTmUserPrsn['USER_PRSN_IDTYPE'] != ''){
					$arrTmUserPrsn['USER_ID'] = $iUserId;
					$this->db->insert('tm_user_prsn', $arrTmUserPrsn);
		            $esp->main->get_bol_trans_check($iCounTol);
				}
				session_start();
				$arrData['USER_ID'] = $iUserId;
				$this->newsession->set_userdata($arrData);
			}else if($sSubmenu == 'update'){
				// die('as');
				$arrTmUser['USER_ADDR'] = trim($arrTmUser['USER_ADDR']);
				$this->db->where('USER_ID', $iId);
				$this->db->update('tm_user', $arrTmUser);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$this->db->where('USER_ID', $iId);
				$this->db->delete('tm_user_com');
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$this->db->where('USER_ID', $iId);
				$this->db->delete('tm_user_prsn');
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				if($arrTmUserCom['USER_COM_STATUS_ID'] != ''){
					$arrTmUserCom['USER_ID'] = $iId;
					$this->db->insert('tm_user_com', $arrTmUserCom);
		            $esp->main->get_bol_trans_check($iCounTol);
				}
				if($arrTmUserPrsn['USER_PRSN_IDTYPE'] != ''){
					$arrTmUserPrsn['USER_ID'] = $iId;
					$this->db->insert('tm_user_prsn', $arrTmUserPrsn);
		            $esp->main->get_bol_trans_check($iCounTol);
				}
				$iUserId = $iId;
			}			
			//print_r($this->db->last_query());
			if($esp->main->get_bol_trans_status($iCounTol)){
				return "MSG#OK#Data Berhasil Disimpan#".site_url()."register/new_reg/frm-reg-pic/".$iUserId;
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}

		}else if($sMenu=='pic'){ 
            $iCounTol = $esp->main->get_bol_trans_begin();
			$arrTmUserPic = $this->input->post('REG');
			if($sSubmenu == 'save'){
				$this->db->insert('tm_user_pic', $arrTmUserPic);
            	$esp->main->get_bol_trans_check($iCounTol); echo "$sSubmenu";
				$iUserId = $arrTmUserPic['USER_ID'];
			}else if($sSubmenu == 'update'){
				$arrTmUserPic['USER_PIC_ADDR'] = trim($arrTmUserPic['USER_PIC_ADDR']);
				$this->db->where('USER_ID', $iId);
				$this->db->update('tm_user_pic', $arrTmUserPic);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);			
				$iUserId = $iId;
			}
			//print_r($arrTmUserPic); die();

			if($esp->main->get_bol_trans_status($iCounTol)){
				return "MSG#OK#Data Berhasil Disimpan#".site_url()."register/new_reg/frm-reg-doc/".$iUserId;
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}
		}else if($sMenu=='doc'){
            $iCounTol = $esp->main->get_bol_trans_begin();
			$arrTmUserDoc = $this->input->post('DOC');
			$iUserId = $iId;
            $arrData['USER_DOC_STATUS_ID'] = 'UPS06';
            $where = "USER_ID = $iUserId AND DOCUMENT_ID in ('01','02','03','04') ";
            $this->db->where($where);
            $this->db->update('tm_user_doc', $arrData);
            $esp->main->get_bol_trans_check($iCounTol,FALSE);			
            /*$arrDokHapus = $this->db->query("SELECT USER_DOC_PATH FROM TM_USER_DOC WHERE USER_ID = $iUserId AND DOCUMENT_ID IN ('01','02','03','04') AND USER_DOC_STATUS_ID = 'UPS06' ")->result_array();
            foreach ($arrDokHapus as $key1 => $value1) {
            	$aa = $value1['USER_DOC_PATH'];
            	unlink($aa);
            }*/
            foreach ($arrTmUserDoc as $key => $value) {
				$this->db->insert('tm_user_doc', $value);
            	$esp->main->get_bol_trans_check($iCounTol);
            }

			if($esp->main->get_bol_trans_status($iCounTol)){
				return "MSG#OK#Data Berhasil Disimpan#".site_url()."register/new_reg/frm-reg-user/".$iUserId;
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}

		}else if($sMenu=='user'){
            $iCounTol = $esp->main->get_bol_trans_begin();
			$arrTmUser = $this->input->post('REG');
			$usr = $arrTmUser['USER_USERNAME'];
			$pass = $arrTmUser['USER_PASSWORD'];
			$arrTmUser['USER_STATUS_ID'] = "US01"; 
			$arrTmUser['USER_PASSWORD'] = hash("sha256",$arrTmUser['USER_PASSWORD']);

			$this->db->where('USER_ID', $iId);
			$this->db->update('tm_user', $arrTmUser);

			$arrTsUserRole['USER_ID'] = $iId; 
			$arrTsUserRole['ROLE_ID'] = "601"; 
			$arrTsUserRole['USER_ROLE_STATUS'] = "1"; 

			$this->db->where('USER_ID', $iId);
			$this->db->delete('TS_USER_ROLE');
            $esp->main->get_bol_trans_check($iCounTol,FALSE);

			$this->db->insert('TS_USER_ROLE', $arrTsUserRole);
        	$esp->main->get_bol_trans_check($iCounTol);

			if (!empty($_SERVER['HTTP_CLIENT_IP'])){
				 $ip=$_SERVER['HTTP_CLIENT_IP'];
				//Is it a proxy address
			}else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
			}else{
				 $ip=$_SERVER['REMOTE_ADDR'];
			}
			$arrLog['USER_ID']= $iId;
			$arrLog['USER_LOG_DESC_ID']= "200";
			$arrLog['USER_LOG_IP']= $_SERVER['REMOTE_ADDR'];
			$this->db->insert('tl_user_log',$arrLog);	
            $esp->main->get_bol_trans_check($iCounTol);

            $arrUser = $this->db->query("select * from tm_user_pic where user_id='$iId' ")->row_array();
            $to = $arrUser['USER_PIC_MAIL'];
            $subject = "Registrasi Akun";
            $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial;  width:580px;"><tr><td>REGISTRASI BERHASIL<br><br>

Selamat, Anda telah melakukan proses Pendaftaran Akun di Sistem e-Standardisasi Pangan yang diselenggarakan oleh BPOM (Badan Pengawas Obat dan Makanan) Republik Indonesia. Berikut username dan password Anda : <br> Username : '.$usr.'<br> Password : '.$pass.'<br> Silakan login dengan akun Anda. <br><br>
Terima Kasih</td></tr></table>';
			// $esp->main->send_mail('register', $to, $isi, $subject);
			$esp->main->sendMailLocal('register', $to, $isi, $subject);
			if($esp->main->get_bol_trans_status($iCounTol)){
				return "MSG#OK#Registrasi berhasil dilakukan. Silakan login dengan akun Anda#".site_url();
			}else{
				return "MSG#ERR#Proses registrasi gagal. Silakan Ulangi Kembali";
			}

		}
	}					

	function get_registrasi($sMenu="",$iId=""){
		$esp = get_instance();
		$esp->load->model("main","main", true);

		if($sMenu=="frm-reg"){
			$PROVINCE = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) = '00'", "REGION_ID", "REGION_NAME", TRUE);
	        $USER_TYPE = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_ID IN ('UTY01','UTY02')  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
	        $ID_TYPE = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE  STATUS_TYPE = 'ID_TYPE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
	        $COMPANY_STATUS = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'COMPANY_STATUS'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
	        $COMPANY_SCALE = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'COMPANY_SCALE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
	        if($iId != ''){
	            $arrUser = $this->db->query("SELECT a.*, b.`USER_COM_STATUS_ID`, b.`USER_COM_SCALE_ID`, b.`USER_COM_LDR_NAME`, b.`USER_COM_TLDR_NAME`, c.`USER_PRSN_IDTYPE`, c.`USER_PRSN_IDNO`, c.USER_TYPE_ETC FROM tm_user a LEFT JOIN tm_user_com b ON a.`USER_ID`=b.`USER_ID` LEFT JOIN tm_user_prsn c ON a.`USER_ID`=c.`USER_ID` WHERE a.`USER_ID`= $iId ")->row_array();
	            $CITY = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) <> '00' AND LEFT(REGION_ID, 2) = LEFT('$arrUser[REGION_ID]', 2)" , "REGION_ID", "REGION_NAME", TRUE);
	            $action = "update";
	        }else{
	            $action = "save";
	        }

		}else if($sMenu=="frm-reg-pic"){
			$PROVINCE = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) = '00'", "REGION_ID", "REGION_NAME", TRUE);
            $arrUser = $this->db->query("SELECT a.* FROM tm_user_pic a  WHERE a.`USER_ID`= '$iId' ")->row_array();
	        if($arrUser['USER_ID'] != ''){
	            $CITY = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) <> '00' AND LEFT(REGION_ID, 2) = LEFT('$arrUser[REGION_ID]', 2)" , "REGION_ID", "REGION_NAME", TRUE);
	            $action = "update";
	        }else{
	            $action = "save";
	        }

	        $ID_TYPE = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE  STATUS_TYPE = 'ID_TYPE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);

		}else if($sMenu=="frm-reg-doc"){
            $doc_var = $this->db->query("select * from TM_DOCUMENTS where DOCUMENT_ID in ('01','02','03','04') ORDER BY ABS(DOCUMENT_ID) ASC")->result_array();
            foreach ($doc_var as $key => $value) {
                $nomor = $key + 1;
                $DATA_DOC[$value['DOCUMENT_ID']][DOCUMENT_ID] = $value['DOCUMENT_ID'];
                if ($DATA_DOC[$value['DOCUMENT_ID']][DOCUMENT_ID] == 68) {
                    $im = "(bila ada)";
                } else {
                    $im = "<span class='red'>*</span>";
                }
                $DATA_DOC[$value['DOCUMENT_ID']][TITLE] = "<table><tr><td style='width:23px' valign = 'top'><b>" . $nomor . ".</b></td><td> " . $value['DOCUMENT_STATUS'] . "  " . $im . " :</td></tr></table>";
            }
            $arrDoc = $this->db->query("select * from tm_user_doc where user_id = $iId and USER_DOC_STATUS_ID = 'UPS02' ORDER BY ABS(DOCUMENT_ID) ASC")->result_array();
			if(count($arrDoc) > 0){
	            foreach ($arrDoc as $key => $value) {
	                $DATA_DOC[$value['DOCUMENT_ID']][USER_DOC_PATH] = $value['USER_DOC_PATH'];
	                $DATA_DOC[$value['DOCUMENT_ID']][USER_DOC_NAME] = $value['USER_DOC_NAME'];
	                $DATA_DOC[$value['DOCUMENT_ID']][USER_DOC_EXT] = $value['USER_DOC_EXT'];
	                $DATA_DOC[$value['DOCUMENT_ID']][USER_DOC_SIZE] = $value['USER_DOC_SIZE'];
	            }

			}

					
		}
        $arrData = array(
        	'PROVINCE' => $PROVINCE,
        	'CITY' => $CITY,
            "iId" => $iId,
            "action" => $action,
            "arrUser" => $arrUser,
            "DATA_DOC" => $DATA_DOC,
            "USER_TYPE" => $USER_TYPE,
            "ID_TYPE" => $ID_TYPE,
            "COMPANY_SCALE" => $COMPANY_SCALE,
            "COMPANY_STATUS" => $COMPANY_STATUS
        );

    	return $arrData;		
		
	}
	


	
	function set_lupa(){
		$USER = $this->main->post_to_query($this->input->post('USER'));
		$this->db->trans_begin();
		$this->db->insert('tm_user', $USER);
		if($this->db->affected_rows() > 0){
				$ret = "MSG||YES||Data berhasil disimpan||";
				$response = TRUE;
		}else{
			$ret = 'MSG||NO||Data Gagal Disimpan';
			$response = FALSE;
		}
		if($this->db->trans_status() === FALSE || !$response)
		{
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}
		return $ret;
	}
}
?>