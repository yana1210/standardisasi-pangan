<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends Model{

	function get_arr_result(&$query, $db=""){
		if($db=="") $db = $this->db;
		$data = $db->query($query);
		if($data->num_rows() > 0){
			$query = $data;
		}else{
			return false;
		}
		return true;
	}
	function get_arr_uraian($query, $select){
		$data = $this->db->query($query);
		if($data->num_rows() > 0){
			$row = $data->row();
			return $row->$select;
		}else{
			return "";
		}
		return 1;
	}
	function get_int_online_user(&$guest){

        $sessions = array();
		$path = realpath(session_save_path());
		$i = 0;
		$guest = 0;
        foreach(glob($path . '/sess*') as $file){
            if(filesize($file)>900) $i++;
			else $guest++;
        }
        
        return $i;
    }
	function get_str_tr_product($sId, $sField){
		$sField = $this->get_arr_uraian("SELECT $sField FROM TX_PRODUCT WHERE PRODUCT_ID = '$sId'", $sField); 
		return $sField;
	}
	function get_str_tr_status($sId, $sType){
		$sStatusDetail = $this->get_arr_uraian("SELECT STATUS_DETAIL FROM TR_STATUS WHERE STATUS_ID = '$sId' AND STATUS_TYPE = '$sType'", "STATUS_DETAIL"); 
		return $sStatusDetail;
	}
	function get_note_tr_status($sId, $sType){
		$sStatusDetail = $this->get_arr_uraian("SELECT STATUS_NOTE FROM TR_STATUS WHERE STATUS_ID = '$sId' AND STATUS_TYPE = '$sType'", "STATUS_NOTE"); 
		return $sStatusDetail;
	}
	function tes(){
		return 0;
	}
	function set_tl_trader($sTraderId, $sStatusId, $sTraderNote, $sTraderDetail){
		$this->db->query("INSERT INTO TL_TRADER (TRADER_ID, STATUS_ID_PAST, STATUS_ID, TRADER_NOTE, TRADER_DETAIL, USER_ID, ROLE_ID) VALUES('$sTraderId', (SELECT TRADER_STATUS FROM TM_TRADER WHERE TRADER_ID = '$sTraderId'), '$sStatusId', '$sTraderNote', '$sTraderDetail', '".$this->newsession->userdata('USER_ID')."', '".$this->newsession->userdata('USER_ROLE')."')");
		return 1;
	}
    function set_tl_product($sProductId, $sStatusId, $sProductNote, $sProductDetail) {
        $iSLAStatus = 0;
        $arrProductLogBefore = $this->db->query("SELECT * FROM TL_PRODUCT WHERE PRODUCT_ID = '$sProductId' ORDER BY PRODUCT_PROCESS_DATE DESC LIMIT 1")->row_array();
        if ($dateProductProcessSLA == "")
            $dateProductProcessSLA = "0000-00-00 00:00:00";
        if ($arrProductLogBefore[PRODUCT_PROCESS_DATE] == "")
            $arrProductLogBefore[PRODUCT_PROCESS_DATE] = "0000-00-00 00:00:00";
        if ($arrProductLogBefore[PRODUCT_PROCESS_SLA] == "")
            $arrProductLogBefore[PRODUCT_PROCESS_SLA] = "0000-00-00 00:00:00";
        $role = $this->newsession->userdata('ROLE_ID');

        if(($role=='RL09' || $role=='RL10')||($sStatusId=='9999')){
         $this->db->query("INSERT INTO TL_PRODUCT (PRODUCT_ID, STATUS_ID, PRODUCT_NOTE, PRODUCT_DETAIL, USER_ID, PRODUCT_PROCESS_DATE_BEFORE, PRODUCT_PROCESS_SLA) VALUES('$sProductId', '$sStatusId', '$sProductNote', '$sProductDetail', '" . $this->newsession->userdata('USER_ID') . "', '$arrProductLogBefore[PRODUCT_PROCESS_DATE]', '0')");
        }else{//die('as');
        $this->db->query("INSERT INTO TL_PRODUCT (PRODUCT_ID, STATUS_ID, PRODUCT_NOTE, PRODUCT_DETAIL, USER_ID, PRODUCT_PROCESS_DATE_BEFORE, PRODUCT_PROCESS_SLA, PRODUCT_PROCESS_SLA_BEFORE) VALUES('$sProductId', '$sStatusId', '$sProductNote', '$sProductDetail', '" . $this->newsession->userdata('USER_ID') . "', '$arrProductLogBefore[PRODUCT_PROCESS_DATE]', GET_SLA_WAKTU('$sProductId','$arrProductLogBefore[PRODUCT_PROCESS_DATE]',NOW()), '$arrProductLogBefore[PRODUCT_PROCESS_DATE]')");}
        return 1;
    }
	function get_bol_trans_begin(){
		$this->db->trans_begin();
		return 0;
	}
	function get_bol_trans_check(&$iCounTol, $status=TRUE){
		
		if($this->db->affected_rows() <= 0 && $status == TRUE){
			$iCounTol++;
			return 0;
		}
		return 1;
	}
	function get_bol_trans_status($iCounTol){
		if ($this->db->trans_status() === FALSE || $iCounTol > 0){
		    $this->db->trans_rollback();
		    return 0;
		}else{
		    $this->db->trans_commit();
		    return 1;
		}
	}
	function get_bol_trans_statusV2($iCounTol){
		//tanpa trans_begin
		if ($iCounTol > 0){
		    $this->db->trans_rollback();
		    return 0;
		}else{
		    $this->db->trans_commit();
		    return 1;
		}
	}
	function get_arr_combobox($query, $key, $value, $empty = FALSE, &$disable = ""){//die('ko');
		$combobox = array();
		$data = $this->db->query($query);
		if($empty) $combobox[""] = "&nbsp;";
		if($data->num_rows() > 0){
			$kodedis = "";
			$arrdis = array();
			foreach($data->result_array() as $row){
				if(is_array($disable)){
					if($kodedis==$row[$disable[0]]){
						if(!array_key_exists($row[$key], $combobox)) $combobox[$row[$key]] = str_replace("'", "\'", "&nbsp; &nbsp;&nbsp;".$row[$value]);
					}else{
						if(!array_key_exists($row[$disable[0]], $combobox)) $combobox[$row[$disable[0]]] = $row[$disable[1]];
						if(!array_key_exists($row[$key], $combobox)) $combobox[$row[$key]] = str_replace("'", "\'", "&nbsp; &nbsp;&nbsp;".$row[$value]);
					}
					$kodedis = $row[$disable[0]];
					if(!in_array($kodedis, $arrdis)) $arrdis[] = $kodedis;
				}else{
					$combobox[$row[$key]] = str_replace("'", "\'", $row[$value]);
				}
			}
			$disable = $arrdis;
		}
		return $combobox;
	}
    function konversi_hari(){
        switch(date("l"))
        {
            case 'Monday':$nmh="Senin";break; 
            case 'Tuesday':$nmh="Selasa";break; 
            case 'Wednesday':$nmh="Rabu";break; 
            case 'Thursday':$nmh="Kamis";break; 
            case 'Friday':$nmh="Jum'at";break; 
            case 'Saturday':$nmh="Sabtu";break; 
            case 'Sunday':$nmh="Minggu";break; 
        }
        echo '<span class="hidden-sm hidden-xs" style="font-size:12px;">'.$nmh.'</span>';
    }

	function sendMailLocal($sType="", $email="", $isi="",$subject=""){ 
		// $this->load->library('PHPMailerAutoload');
		require_once(APPPATH.'libraries/PHPMailerAutoload.php');
		
        $mail = new PHPMailer();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        
        // set smtp
        $mail->IsSMTP();
        $mail->Host = 'ssl://smtp.gmail.com';
        $mail->Port = '465';
		$mail->SMTPSecure = 'ssl';
        $mail->SMTPAuth = true;
        // $mail->Username = 'rekomendasihubla@gmail.com';
        // $mail->Password = 'hubla123';	
        $mail->Username = 'yana1994.taryana@gmail.com';
        $mail->Password = 'yana12101994';	
        $mail->WordWrap = 150;
        $mail->SetFrom('yanataryana545@gmail.com', 'Notifikasi Standardisasi Pangan');
		// $mail->addBCC('widarahmawatiefandi@gmail.com');
		
		if($sType=='reset_pass'){
			$USER = $this->main->get_arr_uraian("SELECT USER_ID AS USER FROM tm_user WHERE USER_MAIL = '".$email."'", "USER");
			$subject = '[no-reply] Reset Account Password - Badan POM Indonesia';
			$message = '<div style="padding: 1cm 2cm 3cm 2cm;">
							<p>Yth. Bapak/Ibu Pengguna Aplikasi e-Standardisasi Pangan BPOM</p><br />
							<p>Terima kasih atas partisipasi Anda dalam menggunakan aplikasi e-Standardisasi Pangan</p><br />
							<p>Jangan Lupa Mengganti Password Anda Secara Berkala.</p>
							<p>Klik link berikut untuk reset password :  <br /><span style="margin-left: 100px;"><a href="'. site_url().'help/reset/'.(hash("sha256", $USER)).'" target="_blank">Klik disini</a></span>
							</p><br /><br />
							<span style="margin-left: 140px;">Hormat Kami</span><br /> <b>Inspektorat - Badan Pengawas Obat dan Makanan Republik Indonesia</b>
						</div>';

		} else if($sType=='question'){
			$NAMA = $this->main->get_arr_uraian("SELECT NAME AS NAMA FROM tm_our_contact WHERE EMAIL = '".$email."'", "NAMA"); 
			$subject = '[no-reply] Kontak Kami - Badan POM Indonesia';
			$message = '<div style="padding: 1cm 2cm 3cm 2cm;">
							<p>Yth. Bapak/Ibu Pengguna Aplikasi e-Standardisasi Pangan BPOM</p><br />
							<p>Terima kasih atas partisipasi Anda dalam menggunakan aplikasi e-Standardisasi Pangan</p><br />
							<p>Nama Lengkap : '.$NAMA.'</p>
							<p>Email        : '.$email.'</p>
							<p>Pertanyaan   : '.$isi.'</p><br /><br />
							<span style="margin-left: 140px;">Hormat Kami</span><br /> <b>Inspektorat - Badan Pengawas Obat dan Makanan Republik Indonesia</b>
						</div>';

		} else if($sType=='mailuser'){
			$query = "SELECT A.USER_ID, A.USER_USERNAME, A.USER_TYPE_ID, A.USER_NAME, A.USER_NPWP, 
					A.USER_ADDR, A.REGION_ID, A.USER_ZIP, A.USER_TELP, A.USER_FAX, A.USER_MAIL, 
					A.USER_IP, A.USER_STATUS_ID

					FROM tm_user A

					LEFT JOIN tr_status B ON A.USER_TYPE_ID = B.STATUS_ID
					LEFT JOIN tm_region C ON A.REGION_ID = C.REGION_ID
					LEFT JOIN tr_status D ON A.USER_STATUS_ID = D.STATUS_ID 
					WHERE A.USER_ID = '".$iId."'";
			$data = $esp->main->get_result_array($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrData['data'] = $row;
				}
			}	 
			$subject = '[no-reply] Kontak Kami - Badan POM Indonesia';
			$message = '<div style="padding: 1cm 2cm 3cm 2cm;">
							<table class="table">
								<tr>
									<td colspan="3" style="width:100%; text-align:center;">
										<p>Yth. Bapak/Ibu Pengguna Aplikasi e-Standardisasi Pangan BPOM</p><br />
										<p>Terima kasih atas partisipasi Anda dalam menggunakan aplikasi e-Standardisasi Pangan</p><br />
									</td>
								</tr>
								<tr>
									<td> Username </td>
									<td> : </td
									<td> '.$arrData['data']['USER_USERNAME'].'</td>
								</tr>
								<tr>
									<td> </td>
									<td> </td
									<td> </td>
								</tr>
							</table>
						</div>';

		}else {
			$subject = ''.$subject;
			$message = '<html><body style="background: #ffffff; color: #000000; font-family: arial; font-size: 13px; margin: 20px; color: #363636;"><table style="margin-bottom: 2px"><tr style="font-size: 13px; color: #0b1d90; font-weight: 700; font-family: arial;"><td width="41" style="margin: 0 0 6px 10px;"><img src="' . base_url() . 'img/_logomail.gif" style="vertical-align: middle;"/></td><td style="font-family: arial; vertical-align: middle; color: #153f6f;">' . $subject . '<br/><span style="color: #858585; font-size: 10px; text-decoration: none;">e-Standardisasi Pangan</span></td></tr></table><div style="background-color: #D4FFD4; margin-top: 4px; margin-bottom: 10px; padding: 5px; font-family: Verdana; font-size: 11px; width:600px; text-align:justify;">' . $isi . '</div><div style="border-top: 1px solid #dcdcdc; clear: both; font-size: 11px; margin-top: 10px; padding-top: 5px;"><div style="font-family: arial; font-size: 10px; color: #a7aaab;">Direktorat Standard Pangan<br>Badan Pengawas Obat dan Makanan - Republik Indonesia</div><a style="text-decoration: none; font-family: arial; font-size: 10px; font-weight: normal;" href="http://www.pom.go.id/">Website BPOM </a></div></body></html>';
		}

		$mail->AddAddress($email);
        $mail->Subject = $subject;

		$body = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><!--[if IE]><html xmlns="http://www.w3.org/1999/xhtml" class="ie"><![endif]--><!--[if !IE]><!--><html style="margin: 0;padding: 0;" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]--><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge" /><!--<![endif]-->
    <meta name="viewport" content="width=device-width" /><style type="text/css">
@media only screen and (min-width: 620px){.wrapper{min-width:600px !important}.wrapper h1{}.wrapper h1{font-size:64px !important;line-height:63px !important}.wrapper h2{}.wrapper h2{font-size:30px !important;line-height:38px !important}.wrapper h3{}.wrapper h3{font-size:22px !important;line-height:31px !important}.column{}.wrapper .size-8{font-size:8px !important;line-height:14px !important}.wrapper .size-9{font-size:9px !important;line-height:16px !important}.wrapper .size-10{font-size:10px !important;line-height:18px !important}.wrapper .size-11{font-size:11px !important;line-height:19px !important}.wrapper .size-12{font-size:12px !important;line-height:19px !important}.wrapper .size-13{font-size:13px !important;line-height:21px !important}.wrapper .size-14{font-size:14px !important;line-height:21px !important}.wrapper .size-15{font-size:15px !important;line-height:23px
!important}.wrapper .size-16{font-size:16px !important;line-height:24px !important}.wrapper .size-17{font-size:17px !important;line-height:26px !important}.wrapper .size-18{font-size:18px !important;line-height:26px !important}.wrapper .size-20{font-size:20px !important;line-height:28px !important}.wrapper .size-22{font-size:22px !important;line-height:31px !important}.wrapper .size-24{font-size:24px !important;line-height:32px !important}.wrapper .size-26{font-size:26px !important;line-height:34px !important}.wrapper .size-28{font-size:28px !important;line-height:50px !important}.wrapper .size-30{font-size:30px !important;line-height:38px !important}.wrapper .size-32{font-size:32px !important;line-height:40px !important}.wrapper .size-34{font-size:34px !important;line-height:43px !important}.wrapper .size-36{font-size:36px !important;line-height:43px !important}.wrapper
.size-40{font-size:40px !important;line-height:47px !important}.wrapper .size-44{font-size:44px !important;line-height:50px !important}.wrapper .size-48{font-size:48px !important;line-height:54px !important}.wrapper .size-56{font-size:56px !important;line-height:60px !important}.wrapper .size-64{font-size:64px !important;line-height:63px !important}}
</style>
    <style type="text/css">
body {
  margin: 0;
  padding: 0;
}
table {
  border-collapse: collapse;
  table-layout: fixed;
}
* {
  line-height: inherit;
}
[x-apple-data-detectors],
[href^="tel"],
[href^="sms"] {
  color: inherit !important;
  text-decoration: none !important;
}
.wrapper .footer__share-button a:hover,
.wrapper .footer__share-button a:focus {
  color: #ffffff !important;
}
.btn a:hover,
.btn a:focus,
.footer__share-button a:hover,
.footer__share-button a:focus,
.email-footer__links a:hover,
.email-footer__links a:focus {
  opacity: 0.8;
}
.preheader,
.header,
.layout,
.column {
  transition: width 0.25s ease-in-out, max-width 0.25s ease-in-out;
}
.layout,
div.header {
  max-width: 400px !important;
  -fallback-width: 95% !important;
  width: calc(100% - 20px) !important;
}
div.preheader {
  max-width: 360px !important;
  -fallback-width: 90% !important;
  width: calc(100% - 60px) !important;
}
.snippet,
.webversion {
  Float: none !important;
}
.column {
  max-width: 400px !important;
  width: 100% !important;
}
.fixed-width.has-border {
  max-width: 402px !important;
}
.fixed-width.has-border .layout__inner {
  box-sizing: border-box;
}
.snippet,
.webversion {
  width: 50% !important;
}
.ie .btn {
  width: 100%;
}
[owa] .column div,
[owa] .column button {
  display: block !important;
}
.ie .column,
[owa] .column,
.ie .gutter,
[owa] .gutter {
  display: table-cell;
  float: none !important;
  vertical-align: top;
}
.ie div.preheader,
[owa] div.preheader,
.ie .email-footer,
[owa] .email-footer {
  max-width: 560px !important;
  width: 560px !important;
}
.ie .snippet,
[owa] .snippet,
.ie .webversion,
[owa] .webversion {
  width: 280px !important;
}
.ie div.header,
[owa] div.header,
.ie .layout,
[owa] .layout,
.ie .one-col .column,
[owa] .one-col .column {
  max-width: 600px !important;
  width: 600px !important;
}
.ie .fixed-width.has-border,
[owa] .fixed-width.has-border,
.ie .has-gutter.has-border,
[owa] .has-gutter.has-border {
  max-width: 602px !important;
  width: 602px !important;
}
.ie .two-col .column,
[owa] .two-col .column {
  max-width: 300px !important;
  width: 300px !important;
}
.ie .three-col .column,
[owa] .three-col .column,
.ie .narrow,
[owa] .narrow {
  max-width: 200px !important;
  width: 200px !important;
}
.ie .wide,
[owa] .wide {
  width: 400px !important;
}
.ie .two-col.has-gutter .column,
[owa] .two-col.x_has-gutter .column {
  max-width: 290px !important;
  width: 290px !important;
}
.ie .three-col.has-gutter .column,
[owa] .three-col.x_has-gutter .column,
.ie .has-gutter .narrow,
[owa] .has-gutter .narrow {
  max-width: 188px !important;
  width: 188px !important;
}
.ie .has-gutter .wide,
[owa] .has-gutter .wide {
  max-width: 394px !important;
  width: 394px !important;
}
.ie .two-col.has-gutter.has-border .column,
[owa] .two-col.x_has-gutter.x_has-border .column {
  max-width: 292px !important;
  width: 292px !important;
}
.ie .three-col.has-gutter.has-border .column,
[owa] .three-col.x_has-gutter.x_has-border .column,
.ie .has-gutter.has-border .narrow,
[owa] .has-gutter.x_has-border .narrow {
  max-width: 190px !important;
  width: 190px !important;
}
.ie .has-gutter.has-border .wide,
[owa] .has-gutter.x_has-border .wide {
  max-width: 396px !important;
  width: 396px !important;
}
.ie .fixed-width .layout__inner {
  border-left: 0 none white !important;
  border-right: 0 none white !important;
}
.ie .layout__edges {
  display: none;
}
.mso .layout__edges {
  font-size: 0;
}
.layout-fixed-width,
.mso .layout-full-width {
  background-color: #ffffff;
}
@media only screen and (min-width: 620px) {
  .column,
  .gutter {
    display: table-cell;
    Float: none !important;
    vertical-align: top;
  }
  div.preheader,
  .email-footer {
    max-width: 560px !important;
    width: 560px !important;
  }
  .snippet,
  .webversion {
    width: 280px !important;
  }
  div.header,
  .layout,
  .one-col .column {
    max-width: 600px !important;
    width: 600px !important;
  }
  .fixed-width.has-border,
  .fixed-width.ecxhas-border,
  .has-gutter.has-border,
  .has-gutter.ecxhas-border {
    max-width: 602px !important;
    width: 602px !important;
  }
  .two-col .column {
    max-width: 300px !important;
    width: 300px !important;
  }
  .three-col .column,
  .column.narrow {
    max-width: 200px !important;
    width: 200px !important;
  }
  .column.wide {
    width: 400px !important;
  }
  .two-col.has-gutter .column,
  .two-col.ecxhas-gutter .column {
    max-width: 290px !important;
    width: 290px !important;
  }
  .three-col.has-gutter .column,
  .three-col.ecxhas-gutter .column,
  .has-gutter .narrow {
    max-width: 188px !important;
    width: 188px !important;
  }
  .has-gutter .wide {
    max-width: 394px !important;
    width: 394px !important;
  }
  .two-col.has-gutter.has-border .column,
  .two-col.ecxhas-gutter.ecxhas-border .column {
    max-width: 292px !important;
    width: 292px !important;
  }
  .three-col.has-gutter.has-border .column,
  .three-col.ecxhas-gutter.ecxhas-border .column,
  .has-gutter.has-border .narrow,
  .has-gutter.ecxhas-border .narrow {
    max-width: 190px !important;
    width: 190px !important;
  }
  .has-gutter.has-border .wide,
  .has-gutter.ecxhas-border .wide {
    max-width: 396px !important;
    width: 396px !important;
  }
}
@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
  .fblike {
    background-image: url(https://i7.createsend1.com/static/eb/master/13-the-blueprint-3/images/fblike@2x.png) !important;
  }
  .tweet {
    background-image: url(https://i8.createsend1.com/static/eb/master/13-the-blueprint-3/images/tweet@2x.png) !important;
  }
  .linkedinshare {
    background-image: url(https://i10.createsend1.com/static/eb/master/13-the-blueprint-3/images/lishare@2x.png) !important;
  }
  .forwardtoafriend {
    background-image: url(https://i9.createsend1.com/static/eb/master/13-the-blueprint-3/images/forward@2x.png) !important;
  }
}
@media (max-width: 321px) {
  .fixed-width.has-border .layout__inner {
    border-width: 1px 0 !important;
  }
  .layout,
  .column {
    min-width: 320px !important;
    width: 320px !important;
  }
  .border {
    display: none;
  }
}
.mso div {
  border: 0 none white !important;
}
.mso .w560 .divider {
  Margin-left: 260px !important;
  Margin-right: 260px !important;
}
.mso .w360 .divider {
  Margin-left: 160px !important;
  Margin-right: 160px !important;
}
.mso .w260 .divider {
  Margin-left: 110px !important;
  Margin-right: 110px !important;
}
.mso .w160 .divider {
  Margin-left: 60px !important;
  Margin-right: 60px !important;
}
.mso .w354 .divider {
  Margin-left: 157px !important;
  Margin-right: 157px !important;
}
.mso .w250 .divider {
  Margin-left: 105px !important;
  Margin-right: 105px !important;
}
.mso .w148 .divider {
  Margin-left: 54px !important;
  Margin-right: 54px !important;
}
.mso .size-8,
.ie .size-8 {
  font-size: 8px !important;
  line-height: 14px !important;
}
.mso .size-9,
.ie .size-9 {
  font-size: 9px !important;
  line-height: 16px !important;
}
.mso .size-10,
.ie .size-10 {
  font-size: 10px !important;
  line-height: 18px !important;
}
.mso .size-11,
.ie .size-11 {
  font-size: 11px !important;
  line-height: 19px !important;
}
.mso .size-12,
.ie .size-12 {
  font-size: 12px !important;
  line-height: 19px !important;
}
.mso .size-13,
.ie .size-13 {
  font-size: 13px !important;
  line-height: 21px !important;
}
.mso .size-14,
.ie .size-14 {
  font-size: 14px !important;
  line-height: 21px !important;
}
.mso .size-15,
.ie .size-15 {
  font-size: 15px !important;
  line-height: 23px !important;
}
.mso .size-16,
.ie .size-16 {
  font-size: 16px !important;
  line-height: 24px !important;
}
.mso .size-17,
.ie .size-17 {
  font-size: 17px !important;
  line-height: 26px !important;
}
.mso .size-18,
.ie .size-18 {
  font-size: 18px !important;
  line-height: 26px !important;
}
.mso .size-20,
.ie .size-20 {
  font-size: 20px !important;
  line-height: 28px !important;
}
.mso .size-22,
.ie .size-22 {
  font-size: 22px !important;
  line-height: 31px !important;
}
.mso .size-24,
.ie .size-24 {
  font-size: 24px !important;
  line-height: 32px !important;
}
.mso .size-26,
.ie .size-26 {
  font-size: 26px !important;
  line-height: 34px !important;
}
.mso .size-28,
.ie .size-28 {
  font-size: 28px !important;
  line-height: 36px !important;
}
.mso .size-30,
.ie .size-30 {
  font-size: 30px !important;
  line-height: 38px !important;
}
.mso .size-32,
.ie .size-32 {
  font-size: 32px !important;
  line-height: 40px !important;
}
.mso .size-34,
.ie .size-34 {
  font-size: 34px !important;
  line-height: 43px !important;
}
.mso .size-36,
.ie .size-36 {
  font-size: 36px !important;
  line-height: 43px !important;
}
.mso .size-40,
.ie .size-40 {
  font-size: 40px !important;
  line-height: 47px !important;
}
.mso .size-44,
.ie .size-44 {
  font-size: 44px !important;
  line-height: 50px !important;
}
.mso .size-48,
.ie .size-48 {
  font-size: 48px !important;
  line-height: 54px !important;
}
.mso .size-56,
.ie .size-56 {
  font-size: 56px !important;
  line-height: 60px !important;
}
.mso .size-64,
.ie .size-64 {
  font-size: 64px !important;
  line-height: 63px !important;
}
</style>

  <!--[if !mso]><!--><style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic);
</style><link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic" rel="stylesheet" type="text/css" /><!--<![endif]--><style type="text/css">
body{background-color:#fff}.logo a:hover,.logo a:focus{color:#859bb1 !important}.mso .layout-has-border{border-top:1px solid #ccc;border-bottom:1px solid #ccc}.mso .layout-has-bottom-border{border-bottom:1px solid #ccc}.mso .border,.ie .border{background-color:#ccc}.mso h1,.ie h1{}.mso h1,.ie h1{font-size:64px !important;line-height:63px !important}.mso h2,.ie h2{}.mso h2,.ie h2{font-size:30px !important;line-height:38px !important}.mso h3,.ie h3{}.mso h3,.ie h3{font-size:22px !important;line-height:31px !important}.mso .layout__inner,.ie .layout__inner{}.mso .footer__share-button p{}.mso .footer__share-button p{font-family:sans-serif}
</style><meta name="robots" content="noindex,nofollow" />
<meta property="og:title" content="My First Campaign" />
</head>
<!--[if mso]>
  <body class="mso">
<![endif]-->
<!--[if !mso]><!-->
  <body class="half-padding" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;">
<!--<![endif]-->
    <table class="wrapper" style="border-collapse: collapse;table-layout: fixed;min-width: 320px;width: 100%;background-color: #f0f0f0;" cellpadding="0" cellspacing="0" role="presentation"><tbody><tr><td>

      <div role="section">
      <div style="background-color: #f3f3f3;">
        <div class="layout one-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #f3f3f3;"><td class="layout__edges">&nbsp;</td><td style="width: 600px" class="w560"><![endif]-->
            <div class="column" style="max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">
          <a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #61ba61;" href="http://standarpangan.pom.go.id/"><img style="border: 0;display: block;height: auto;width: 100%;max-width: 534px;" alt="Direktorat Standardisasi Produk Pangan" width="534" src="http://standarpangan.pom.go.id/images/feature/dspp_header.png" /></a>
        </div>

            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>

      <div style="background-color: #4b5462;background-position: 0px 0px;background-image: url(http://standarpangan.pom.go.id/images/feature/blue_mail.jpg);background-repeat: repeat;">
        <div class="layout one-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #4b5462;background-position: 0px 0px;background-image: url(http://standarpangan.pom.go.id/images/feature/blue_mail.jpg);background-repeat: repeat;"><td class="layout__edges">&nbsp;</td><td style="width: 600px" class="w560"><![endif]-->
            <div class="column" style="max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">
      <div style="line-height:50px;font-size:1px">&nbsp;</div>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;">
      <p class="size-30" style="Margin-top: 0;Margin-bottom: 20px;font-family: ubuntu,sans-serif;font-size: 26px;line-height: 34px;text-align: center;" lang="x-size-30"><span class="font-ubuntu"><span style="color:#fff"><strong>'.$subject.'</strong></span></span></p>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;">
      <div class="divider" style="display: block;font-size: 2px;line-height: 2px;Margin-left: auto;Margin-right: auto;width: 40px;background-color: #ccc;Margin-bottom: 20px;">&nbsp;</div>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;">
      <h1 class="size-20" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #000;font-size: 17px;line-height: 26px;font-family: ubuntu,sans-serif;text-align: left;" lang="x-size-20"><span class="font-ubuntu"><span style="color:#fff">'.$isi.'</span></span></h1><h1 class="size-16" style="Margin-top: 20px;Margin-bottom: 20px;font-style: normal;font-weight: normal;color: #000;font-size: 16px;line-height: 24px;font-family: ubuntu,sans-serif;text-align: center;" lang="x-size-16"><span class="font-ubuntu"><span style="color:#fff"></span></span></h1>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;">
      <div style="line-height:30px;font-size:1px">&nbsp;</div>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">
      <div style="line-height:1px;font-size:1px">&nbsp;</div>
    </div>

            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>

      <div style="line-height:10px;font-size:10px;">&nbsp;</div>

      <div class="layout one-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #f0f0f0;">
        <!--[if (mso)|(IE)]><table align="center" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-fixed-width" style="background-color: #f0f0f0;"><td style="width: 600px" class="w560"><![endif]-->
          <div class="column" style="text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">

            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;Margin-bottom: 12px;">
      <div class="divider" style="display: block;font-size: 2px;line-height: 2px;Margin-left: auto;Margin-right: auto;width: 40px;background-color: #ccc;">&nbsp;</div>
    </div>

          </div>
        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
        </div>
      </div>

      <div style="line-height:10px;font-size:10px;">&nbsp;</div>

      <div style="background-color: #234269;">
        <div class="layout three-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #234269;"><td class="layout__edges">&nbsp;</td><td style="width: 200px" valign="top" class="w160"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">
        <div style="font-size: 12px;font-style: normal;font-weight: normal;Margin-bottom: 20px;" align="center">
          <img style="border: 0;display: block;height: auto;width: 100%;max-width: 30px;" alt="Kunjungi Kami" width="30" src="http://standarpangan.pom.go.id/images/feature/kantor.png" />
        </div>
      </div>

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">
      <p class="size-12" style="Margin-top: 0;Margin-bottom: 0;font-size: 12px;line-height: 19px;text-align: center;" lang="x-size-12"><span style="color:#fff">KUNJUNGI KAMI<br />
<br />
Jl. Percetakan Negara No.23,&nbsp;<br />
Jakarta Pusat, 10560</span><br />
<br />
<a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #61ba61;" href="https://goo.gl/maps/QHUBbvCgRXm">Get Direction</a></p>
    </div>

            </div>
          <!--[if (mso)|(IE)]></td><td style="width: 200px" valign="top" class="w160"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">
        <div style="font-size: 12px;font-style: normal;font-weight: normal;Margin-bottom: 20px;" align="center">
          <img style="border: 0;display: block;height: auto;width: 100%;max-width: 30px;" alt="Hubungi Kami" width="30" src="http://standarpangan.pom.go.id/images/feature/telp.png" />
        </div>
      </div>

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">
      <p class="size-12" style="Margin-top: 0;Margin-bottom: 0;font-size: 12px;line-height: 19px;text-align: center;" lang="x-size-12"><span style="color:#ffffff">HUBUNGI KAMI<br />
<br />
Telp: 021-42875584<br />
Fax: 021-42875780<br />
<br />
&nbsp;Email:&nbsp;</span><br />
<a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #61ba61;" href="mailto:standarpangan@pom.go.id">standarpangan@pom.go.id</a></p>
    </div>

            </div>
          <!--[if (mso)|(IE)]></td><td style="width: 200px" valign="top" class="w160"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">
        <div style="font-size: 12px;font-style: normal;font-weight: normal;Margin-bottom: 20px;" align="center">
          <img style="border: 0;display: block;height: auto;width: 100%;max-width: 30px;" alt="Waktu Konsultasi" width="30" src="http://standarpangan.pom.go.id/images/feature/kalender.png" />
        </div>
      </div>

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">
      <p class="size-12" style="Margin-top: 0;Margin-bottom: 0;font-size: 12px;line-height: 19px;text-align: center;" lang="x-size-12"><span style="color:#ffffff">WAKTU KONSULTASI<br />
<br />
Senin - Kamis: 09.00 - 15.00</span></p>
    </div>

            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>

      <div style="line-height:15px;font-size:15px;">&nbsp;</div>

      <div class="layout one-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #f0f0f0;">
        <!--[if (mso)|(IE)]><table align="center" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-fixed-width" style="background-color: #f0f0f0;"><td style="width: 600px" class="w560"><![endif]-->
          <div class="column" style="text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">

            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;Margin-bottom: 12px;">
      <p style="Margin-top: 0;Margin-bottom: 0;text-align: center;">&#169;&nbsp;Direktorat Standardisasi Produk Pangan</p>
    </div>

          </div>
        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
        </div>
      </div>

      <div style="line-height:20px;font-size:20px;">&nbsp;</div>

<!--      <div style="line-height:40px;font-size:40px;">&nbsp;</div> -->
    </div></td></tr></tbody></table>

</body></html>
';
        $mail->Body = $body;
		$mail->IsHTML(true); 
		if($mail->Send()){
			// echo "gmail sukses";
			return TRUE;
		}else{
			// echo $email;
			return $email->ErrorInfo;	
		}

	}
	public function sendMailSMTP($sType="", $to = "", $isi = "", $subject = "")
	{
		//Load email library
		$this->load->library('email');

		//SMTP & mail configuration
		$config = array(
		    'protocol'  => 'smtp',
		    'smtp_host' => 'smtp.gmail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'yanataryana545@gmail.com',
		    'smtp_pass' => 'yana!@!)1994ai',
		    'mailtype'  => 'html',
		    'charset'   => 'utf-8',
		    'priority'	=> 1,
		    'newline'	=> '\r\n'
		);
		$this->email->initialize($config);

		$this->email->to($to);
		$this->email->from('yanataryana545@gmail.com', 'nama');
		$this->email->subject($subject);
		$this->email->message($isi);

		//Send email
		if ($this->email->send()) {
		    echo 'Sukses! email berhasil dikirim.';
		} else {
			echo $this->email->print_debugger();
		    echo 'Error! email tidak dapat dikirim.';
		}
	}

	function send_mail($sType="", $to = "", $isi = "", $subject = ""){
//	function send_mail($to = "", $nama = "", $subject = "", $isi = ""){
/*
        $this->load->library('email');
        $mailconfig = $this->config->config;
        $mailconfig = $mailconfig['mail_config'];
        $this->email->set_newline("\r\n");
        $this->email->from($mailconfig['FROM'], $mailconfig['NAME']);
        $email = str_replace(';', ',', $to);
        $this->email->to($email);
        $this->email->reply_to('ereg_otsm@pom.go.id', 'Administrator e-Registration');
        $bcc = str_replace(';', ',', $mailconfig['BCC']);
        $this->email->bcc($bcc);
        $this->email->subject($subject);
*/
		$this->load->library('email');
		$mailconfig = $this->config->config;
		$mailconfig = $mailconfig['mail_config'];
		$this->email->set_newline("\r\n");


$config['protocol']    = 'smtp';

$config['smtp_host']    = '172.16.1.81';


$config['smtp_user']    = 'standarpangan';

$config['smtp_pass']    = 'tikbpom';

$config['charset']    = 'utf-8';
/*
$config['newline']    = "\r\n";

$config['mailtype'] = 'text'; // or html
*/
$config['validation'] = TRUE; // bool whether to validate email or not      

$this->email->initialize($config);

		$this->email->from($mailconfig['FROM'], $mailconfig['NAME']);
		$email = str_replace(';', ',', $to);
		$this->email->to($email);
		$this->email->reply_to('standarpangan@pom.go.id', 'Administrator e-Standardisasi Pangan');
		$bcc = str_replace(';', ',', $mailconfig['BCC']);
		$this->email->bcc($bcc);
		$this->email->subject($subject);
		$body = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><!--[if IE]><html xmlns="http://www.w3.org/1999/xhtml" class="ie"><![endif]--><!--[if !IE]><!--><html style="margin: 0;padding: 0;" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]--><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge" /><!--<![endif]-->
    <meta name="viewport" content="width=device-width" /><style type="text/css">
@media only screen and (min-width: 620px){.wrapper{min-width:600px !important}.wrapper h1{}.wrapper h1{font-size:64px !important;line-height:63px !important}.wrapper h2{}.wrapper h2{font-size:30px !important;line-height:38px !important}.wrapper h3{}.wrapper h3{font-size:22px !important;line-height:31px !important}.column{}.wrapper .size-8{font-size:8px !important;line-height:14px !important}.wrapper .size-9{font-size:9px !important;line-height:16px !important}.wrapper .size-10{font-size:10px !important;line-height:18px !important}.wrapper .size-11{font-size:11px !important;line-height:19px !important}.wrapper .size-12{font-size:12px !important;line-height:19px !important}.wrapper .size-13{font-size:13px !important;line-height:21px !important}.wrapper .size-14{font-size:14px !important;line-height:21px !important}.wrapper .size-15{font-size:15px !important;line-height:23px
!important}.wrapper .size-16{font-size:16px !important;line-height:24px !important}.wrapper .size-17{font-size:17px !important;line-height:26px !important}.wrapper .size-18{font-size:18px !important;line-height:26px !important}.wrapper .size-20{font-size:20px !important;line-height:28px !important}.wrapper .size-22{font-size:22px !important;line-height:31px !important}.wrapper .size-24{font-size:24px !important;line-height:32px !important}.wrapper .size-26{font-size:26px !important;line-height:34px !important}.wrapper .size-28{font-size:28px !important;line-height:50px !important}.wrapper .size-30{font-size:30px !important;line-height:38px !important}.wrapper .size-32{font-size:32px !important;line-height:40px !important}.wrapper .size-34{font-size:34px !important;line-height:43px !important}.wrapper .size-36{font-size:36px !important;line-height:43px !important}.wrapper
.size-40{font-size:40px !important;line-height:47px !important}.wrapper .size-44{font-size:44px !important;line-height:50px !important}.wrapper .size-48{font-size:48px !important;line-height:54px !important}.wrapper .size-56{font-size:56px !important;line-height:60px !important}.wrapper .size-64{font-size:64px !important;line-height:63px !important}}
</style>
    <style type="text/css">
body {
  margin: 0;
  padding: 0;
}
table {
  border-collapse: collapse;
  table-layout: fixed;
}
* {
  line-height: inherit;
}
[x-apple-data-detectors],
[href^="tel"],
[href^="sms"] {
  color: inherit !important;
  text-decoration: none !important;
}
.wrapper .footer__share-button a:hover,
.wrapper .footer__share-button a:focus {
  color: #ffffff !important;
}
.btn a:hover,
.btn a:focus,
.footer__share-button a:hover,
.footer__share-button a:focus,
.email-footer__links a:hover,
.email-footer__links a:focus {
  opacity: 0.8;
}
.preheader,
.header,
.layout,
.column {
  transition: width 0.25s ease-in-out, max-width 0.25s ease-in-out;
}
.layout,
div.header {
  max-width: 400px !important;
  -fallback-width: 95% !important;
  width: calc(100% - 20px) !important;
}
div.preheader {
  max-width: 360px !important;
  -fallback-width: 90% !important;
  width: calc(100% - 60px) !important;
}
.snippet,
.webversion {
  Float: none !important;
}
.column {
  max-width: 400px !important;
  width: 100% !important;
}
.fixed-width.has-border {
  max-width: 402px !important;
}
.fixed-width.has-border .layout__inner {
  box-sizing: border-box;
}
.snippet,
.webversion {
  width: 50% !important;
}
.ie .btn {
  width: 100%;
}
[owa] .column div,
[owa] .column button {
  display: block !important;
}
.ie .column,
[owa] .column,
.ie .gutter,
[owa] .gutter {
  display: table-cell;
  float: none !important;
  vertical-align: top;
}
.ie div.preheader,
[owa] div.preheader,
.ie .email-footer,
[owa] .email-footer {
  max-width: 560px !important;
  width: 560px !important;
}
.ie .snippet,
[owa] .snippet,
.ie .webversion,
[owa] .webversion {
  width: 280px !important;
}
.ie div.header,
[owa] div.header,
.ie .layout,
[owa] .layout,
.ie .one-col .column,
[owa] .one-col .column {
  max-width: 600px !important;
  width: 600px !important;
}
.ie .fixed-width.has-border,
[owa] .fixed-width.has-border,
.ie .has-gutter.has-border,
[owa] .has-gutter.has-border {
  max-width: 602px !important;
  width: 602px !important;
}
.ie .two-col .column,
[owa] .two-col .column {
  max-width: 300px !important;
  width: 300px !important;
}
.ie .three-col .column,
[owa] .three-col .column,
.ie .narrow,
[owa] .narrow {
  max-width: 200px !important;
  width: 200px !important;
}
.ie .wide,
[owa] .wide {
  width: 400px !important;
}
.ie .two-col.has-gutter .column,
[owa] .two-col.x_has-gutter .column {
  max-width: 290px !important;
  width: 290px !important;
}
.ie .three-col.has-gutter .column,
[owa] .three-col.x_has-gutter .column,
.ie .has-gutter .narrow,
[owa] .has-gutter .narrow {
  max-width: 188px !important;
  width: 188px !important;
}
.ie .has-gutter .wide,
[owa] .has-gutter .wide {
  max-width: 394px !important;
  width: 394px !important;
}
.ie .two-col.has-gutter.has-border .column,
[owa] .two-col.x_has-gutter.x_has-border .column {
  max-width: 292px !important;
  width: 292px !important;
}
.ie .three-col.has-gutter.has-border .column,
[owa] .three-col.x_has-gutter.x_has-border .column,
.ie .has-gutter.has-border .narrow,
[owa] .has-gutter.x_has-border .narrow {
  max-width: 190px !important;
  width: 190px !important;
}
.ie .has-gutter.has-border .wide,
[owa] .has-gutter.x_has-border .wide {
  max-width: 396px !important;
  width: 396px !important;
}
.ie .fixed-width .layout__inner {
  border-left: 0 none white !important;
  border-right: 0 none white !important;
}
.ie .layout__edges {
  display: none;
}
.mso .layout__edges {
  font-size: 0;
}
.layout-fixed-width,
.mso .layout-full-width {
  background-color: #ffffff;
}
@media only screen and (min-width: 620px) {
  .column,
  .gutter {
    display: table-cell;
    Float: none !important;
    vertical-align: top;
  }
  div.preheader,
  .email-footer {
    max-width: 560px !important;
    width: 560px !important;
  }
  .snippet,
  .webversion {
    width: 280px !important;
  }
  div.header,
  .layout,
  .one-col .column {
    max-width: 600px !important;
    width: 600px !important;
  }
  .fixed-width.has-border,
  .fixed-width.ecxhas-border,
  .has-gutter.has-border,
  .has-gutter.ecxhas-border {
    max-width: 602px !important;
    width: 602px !important;
  }
  .two-col .column {
    max-width: 300px !important;
    width: 300px !important;
  }
  .three-col .column,
  .column.narrow {
    max-width: 200px !important;
    width: 200px !important;
  }
  .column.wide {
    width: 400px !important;
  }
  .two-col.has-gutter .column,
  .two-col.ecxhas-gutter .column {
    max-width: 290px !important;
    width: 290px !important;
  }
  .three-col.has-gutter .column,
  .three-col.ecxhas-gutter .column,
  .has-gutter .narrow {
    max-width: 188px !important;
    width: 188px !important;
  }
  .has-gutter .wide {
    max-width: 394px !important;
    width: 394px !important;
  }
  .two-col.has-gutter.has-border .column,
  .two-col.ecxhas-gutter.ecxhas-border .column {
    max-width: 292px !important;
    width: 292px !important;
  }
  .three-col.has-gutter.has-border .column,
  .three-col.ecxhas-gutter.ecxhas-border .column,
  .has-gutter.has-border .narrow,
  .has-gutter.ecxhas-border .narrow {
    max-width: 190px !important;
    width: 190px !important;
  }
  .has-gutter.has-border .wide,
  .has-gutter.ecxhas-border .wide {
    max-width: 396px !important;
    width: 396px !important;
  }
}
@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
  .fblike {
    background-image: url(https://i7.createsend1.com/static/eb/master/13-the-blueprint-3/images/fblike@2x.png) !important;
  }
  .tweet {
    background-image: url(https://i8.createsend1.com/static/eb/master/13-the-blueprint-3/images/tweet@2x.png) !important;
  }
  .linkedinshare {
    background-image: url(https://i10.createsend1.com/static/eb/master/13-the-blueprint-3/images/lishare@2x.png) !important;
  }
  .forwardtoafriend {
    background-image: url(https://i9.createsend1.com/static/eb/master/13-the-blueprint-3/images/forward@2x.png) !important;
  }
}
@media (max-width: 321px) {
  .fixed-width.has-border .layout__inner {
    border-width: 1px 0 !important;
  }
  .layout,
  .column {
    min-width: 320px !important;
    width: 320px !important;
  }
  .border {
    display: none;
  }
}
.mso div {
  border: 0 none white !important;
}
.mso .w560 .divider {
  Margin-left: 260px !important;
  Margin-right: 260px !important;
}
.mso .w360 .divider {
  Margin-left: 160px !important;
  Margin-right: 160px !important;
}
.mso .w260 .divider {
  Margin-left: 110px !important;
  Margin-right: 110px !important;
}
.mso .w160 .divider {
  Margin-left: 60px !important;
  Margin-right: 60px !important;
}
.mso .w354 .divider {
  Margin-left: 157px !important;
  Margin-right: 157px !important;
}
.mso .w250 .divider {
  Margin-left: 105px !important;
  Margin-right: 105px !important;
}
.mso .w148 .divider {
  Margin-left: 54px !important;
  Margin-right: 54px !important;
}
.mso .size-8,
.ie .size-8 {
  font-size: 8px !important;
  line-height: 14px !important;
}
.mso .size-9,
.ie .size-9 {
  font-size: 9px !important;
  line-height: 16px !important;
}
.mso .size-10,
.ie .size-10 {
  font-size: 10px !important;
  line-height: 18px !important;
}
.mso .size-11,
.ie .size-11 {
  font-size: 11px !important;
  line-height: 19px !important;
}
.mso .size-12,
.ie .size-12 {
  font-size: 12px !important;
  line-height: 19px !important;
}
.mso .size-13,
.ie .size-13 {
  font-size: 13px !important;
  line-height: 21px !important;
}
.mso .size-14,
.ie .size-14 {
  font-size: 14px !important;
  line-height: 21px !important;
}
.mso .size-15,
.ie .size-15 {
  font-size: 15px !important;
  line-height: 23px !important;
}
.mso .size-16,
.ie .size-16 {
  font-size: 16px !important;
  line-height: 24px !important;
}
.mso .size-17,
.ie .size-17 {
  font-size: 17px !important;
  line-height: 26px !important;
}
.mso .size-18,
.ie .size-18 {
  font-size: 18px !important;
  line-height: 26px !important;
}
.mso .size-20,
.ie .size-20 {
  font-size: 20px !important;
  line-height: 28px !important;
}
.mso .size-22,
.ie .size-22 {
  font-size: 22px !important;
  line-height: 31px !important;
}
.mso .size-24,
.ie .size-24 {
  font-size: 24px !important;
  line-height: 32px !important;
}
.mso .size-26,
.ie .size-26 {
  font-size: 26px !important;
  line-height: 34px !important;
}
.mso .size-28,
.ie .size-28 {
  font-size: 28px !important;
  line-height: 36px !important;
}
.mso .size-30,
.ie .size-30 {
  font-size: 30px !important;
  line-height: 38px !important;
}
.mso .size-32,
.ie .size-32 {
  font-size: 32px !important;
  line-height: 40px !important;
}
.mso .size-34,
.ie .size-34 {
  font-size: 34px !important;
  line-height: 43px !important;
}
.mso .size-36,
.ie .size-36 {
  font-size: 36px !important;
  line-height: 43px !important;
}
.mso .size-40,
.ie .size-40 {
  font-size: 40px !important;
  line-height: 47px !important;
}
.mso .size-44,
.ie .size-44 {
  font-size: 44px !important;
  line-height: 50px !important;
}
.mso .size-48,
.ie .size-48 {
  font-size: 48px !important;
  line-height: 54px !important;
}
.mso .size-56,
.ie .size-56 {
  font-size: 56px !important;
  line-height: 60px !important;
}
.mso .size-64,
.ie .size-64 {
  font-size: 64px !important;
  line-height: 63px !important;
}
</style>

  <!--[if !mso]><!--><style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic);
</style><link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic" rel="stylesheet" type="text/css" /><!--<![endif]--><style type="text/css">
body{background-color:#fff}.logo a:hover,.logo a:focus{color:#859bb1 !important}.mso .layout-has-border{border-top:1px solid #ccc;border-bottom:1px solid #ccc}.mso .layout-has-bottom-border{border-bottom:1px solid #ccc}.mso .border,.ie .border{background-color:#ccc}.mso h1,.ie h1{}.mso h1,.ie h1{font-size:64px !important;line-height:63px !important}.mso h2,.ie h2{}.mso h2,.ie h2{font-size:30px !important;line-height:38px !important}.mso h3,.ie h3{}.mso h3,.ie h3{font-size:22px !important;line-height:31px !important}.mso .layout__inner,.ie .layout__inner{}.mso .footer__share-button p{}.mso .footer__share-button p{font-family:sans-serif}
</style><meta name="robots" content="noindex,nofollow" />
<meta property="og:title" content="My First Campaign" />
</head>
<!--[if mso]>
  <body class="mso">
<![endif]-->
<!--[if !mso]><!-->
  <body class="half-padding" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;">
<!--<![endif]-->
    <table class="wrapper" style="border-collapse: collapse;table-layout: fixed;min-width: 320px;width: 100%;background-color: #f0f0f0;" cellpadding="0" cellspacing="0" role="presentation"><tbody><tr><td>

      <div role="section">
      <div style="background-color: #f3f3f3;">
        <div class="layout one-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #f3f3f3;"><td class="layout__edges">&nbsp;</td><td style="width: 600px" class="w560"><![endif]-->
            <div class="column" style="max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">
          <a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #61ba61;" href="http://standarpangan.pom.go.id/"><img style="border: 0;display: block;height: auto;width: 100%;max-width: 534px;" alt="Direktorat Standardisasi Produk Pangan" width="534" src="http://standarpangan.pom.go.id/images/feature/dspp_header.png" /></a>
        </div>

            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>

      <div style="background-color: #4b5462;background-position: 0px 0px;background-image: url(http://standarpangan.pom.go.id/images/feature/blue_mail.jpg);background-repeat: repeat;">
        <div class="layout one-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #4b5462;background-position: 0px 0px;background-image: url(http://standarpangan.pom.go.id/images/feature/blue_mail.jpg);background-repeat: repeat;"><td class="layout__edges">&nbsp;</td><td style="width: 600px" class="w560"><![endif]-->
            <div class="column" style="max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">
      <div style="line-height:50px;font-size:1px">&nbsp;</div>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;">
      <p class="size-30" style="Margin-top: 0;Margin-bottom: 20px;font-family: ubuntu,sans-serif;font-size: 26px;line-height: 34px;text-align: center;" lang="x-size-30"><span class="font-ubuntu"><span style="color:#fff"><strong>'.$subject.'</strong></span></span></p>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;">
      <div class="divider" style="display: block;font-size: 2px;line-height: 2px;Margin-left: auto;Margin-right: auto;width: 40px;background-color: #ccc;Margin-bottom: 20px;">&nbsp;</div>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;">
      <h1 class="size-20" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #000;font-size: 17px;line-height: 26px;font-family: ubuntu,sans-serif;text-align: left;" lang="x-size-20"><span class="font-ubuntu"><span style="color:#fff">'.$isi.'</span></span></h1><h1 class="size-16" style="Margin-top: 20px;Margin-bottom: 20px;font-style: normal;font-weight: normal;color: #000;font-size: 16px;line-height: 24px;font-family: ubuntu,sans-serif;text-align: center;" lang="x-size-16"><span class="font-ubuntu"><span style="color:#fff"></span></span></h1>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;">
      <div style="line-height:30px;font-size:1px">&nbsp;</div>
    </div>

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">
      <div style="line-height:1px;font-size:1px">&nbsp;</div>
    </div>

            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>

      <div style="line-height:10px;font-size:10px;">&nbsp;</div>

      <div class="layout one-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #f0f0f0;">
        <!--[if (mso)|(IE)]><table align="center" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-fixed-width" style="background-color: #f0f0f0;"><td style="width: 600px" class="w560"><![endif]-->
          <div class="column" style="text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">

            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;Margin-bottom: 12px;">
      <div class="divider" style="display: block;font-size: 2px;line-height: 2px;Margin-left: auto;Margin-right: auto;width: 40px;background-color: #ccc;">&nbsp;</div>
    </div>

          </div>
        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
        </div>
      </div>

      <div style="line-height:10px;font-size:10px;">&nbsp;</div>

      <div style="background-color: #234269;">
        <div class="layout three-col" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
          <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-full-width" style="background-color: #234269;"><td class="layout__edges">&nbsp;</td><td style="width: 200px" valign="top" class="w160"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">
        <div style="font-size: 12px;font-style: normal;font-weight: normal;Margin-bottom: 20px;" align="center">
          <img style="border: 0;display: block;height: auto;width: 100%;max-width: 30px;" alt="Kunjungi Kami" width="30" src="http://standarpangan.pom.go.id/images/feature/kantor.png" />
        </div>
      </div>

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">
      <p class="size-12" style="Margin-top: 0;Margin-bottom: 0;font-size: 12px;line-height: 19px;text-align: center;" lang="x-size-12"><span style="color:#fff">KUNJUNGI KAMI<br />
<br />
Jl. Percetakan Negara No.23,&nbsp;<br />
Jakarta Pusat, 10560</span><br />
<br />
<a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #61ba61;" href="https://goo.gl/maps/QHUBbvCgRXm">Get Direction</a></p>
    </div>

            </div>
          <!--[if (mso)|(IE)]></td><td style="width: 200px" valign="top" class="w160"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">
        <div style="font-size: 12px;font-style: normal;font-weight: normal;Margin-bottom: 20px;" align="center">
          <img style="border: 0;display: block;height: auto;width: 100%;max-width: 30px;" alt="Hubungi Kami" width="30" src="http://standarpangan.pom.go.id/images/feature/telp.png" />
        </div>
      </div>

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">
      <p class="size-12" style="Margin-top: 0;Margin-bottom: 0;font-size: 12px;line-height: 19px;text-align: center;" lang="x-size-12"><span style="color:#ffffff">HUBUNGI KAMI<br />
<br />
Telp: 021-42875584<br />
Fax: 021-42875780<br />
<br />
&nbsp;Email:&nbsp;</span><br />
<a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #61ba61;" href="mailto:standarpangan@pom.go.id">standarpangan@pom.go.id</a></p>
    </div>

            </div>
          <!--[if (mso)|(IE)]></td><td style="width: 200px" valign="top" class="w160"><![endif]-->
            <div class="column" style="Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">
        <div style="font-size: 12px;font-style: normal;font-weight: normal;Margin-bottom: 20px;" align="center">
          <img style="border: 0;display: block;height: auto;width: 100%;max-width: 30px;" alt="Waktu Konsultasi" width="30" src="http://standarpangan.pom.go.id/images/feature/kalender.png" />
        </div>
      </div>

              <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">
      <p class="size-12" style="Margin-top: 0;Margin-bottom: 0;font-size: 12px;line-height: 19px;text-align: center;" lang="x-size-12"><span style="color:#ffffff">WAKTU KONSULTASI<br />
<br />
Senin - Kamis: 09.00 - 15.00</span></p>
    </div>

            </div>
          <!--[if (mso)|(IE)]></td><td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
          </div>
        </div>
      </div>

      <div style="line-height:15px;font-size:15px;">&nbsp;</div>

      <div class="layout one-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #f0f0f0;">
        <!--[if (mso)|(IE)]><table align="center" cellpadding="0" cellspacing="0" role="presentation"><tr class="layout-fixed-width" style="background-color: #f0f0f0;"><td style="width: 600px" class="w560"><![endif]-->
          <div class="column" style="text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">

            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;Margin-bottom: 12px;">
      <p style="Margin-top: 0;Margin-bottom: 0;text-align: center;">&#169;&nbsp;Direktorat Standardisasi Produk Pangan</p>
    </div>

          </div>
        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
        </div>
      </div>

      <div style="line-height:20px;font-size:20px;">&nbsp;</div>

<!--      <div style="line-height:40px;font-size:40px;">&nbsp;</div> -->
    </div></td></tr></tbody></table>

</body></html>
';
		$this->email->message($body);
		$hasil=$this->email->send();

//echo $this->email->print_debugger();
/*if ( ! $this->email->send())
{
			echo $mailconfig['FROM']."gagal";
//        die('gagal');
}else{
			echo $mailconfig['FROM']."sukses";
//	die('berhasil');
}
*/
$this->email->clear();
//        return $hasil;
/*
if (  $this->email->send())
{
		return $this->email->send();
//		return $hasil;
}else{
        // Generate error
		echo $this->email->print_debugger();
}
*/
		/*$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.pom.go.id',
			'smtp_port' => 25,
			'smtp_user' => 'bidang_ti',
			'smtp_pass' => 'p455w0rd',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1',
			'start_tls' => true
		);*/
		/*$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.pom.go.id',
			'smtp_port' => 25,
			'smtp_user' => 'ereg_otsm', 
			'smtp_pass' => 'tikbpom',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1',
			'start_tls' => true
		);
		$this->load->library('email', $config);*/
	}


}
?>