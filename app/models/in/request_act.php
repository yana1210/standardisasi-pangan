<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//error_reporting(E_ERROR);
class Request_act extends Model{

	function request_get($sMenu, $iId){
		$esp = get_instance();
		$esp->load->model("main","main", true);
			if($sMenu=="new"){

				// $typeRqs = "SELECT USER_ID, USER_TYPE_ID FROM tm_user WHERE USER_TYPE_ID = '".$_SESSION($iId)."'";
				// $datatypeRqs = $esp->main->get_arr_result($typeRqs);
				// if($datatypeRqs){
				// 	foreach($typeRqs->result_array() as $typeRqs){
				// 		$arrdata['rowData']= $typeRqs;
				// 	}
				// }

				$arrdata['apptype'] = array(''=>'','1'=>'Perorangan','2'=>'Perusahaan');
				//$arrdata['pengguna'] = $this->db->get("TM_USER_PIC")->result_array(); 
				$arrdata['noisn'] = $esp->main->get_arr_combobox("SELECT A.BTP_TYPE_NAME_ID AS NOMOR, A.BTP_TYPE_NAME AS NAMA 
					FROM TR_BTP_TYPE_NAME A 
					LEFT JOIN TR_BTP_TYPE_NO B ON B.BTP_TYPE_NO_ID = A.BTP_TYPE_NAME_ID","NOMOR", "NOMOR", TRUE);
				$arrdata['foodtype'] = $esp->main->get_arr_combobox("SELECT FOOD_TYPE_NO AS NO, FOOD_TYPE_NAME AS NAMA FROM TR_FOOD_TYPE","NO","NAMA", TRUE);

				$pengguna = "SELECT USER_ID, USER_PIC_NAME, USER_PIC_TELP, USER_PIC_FAX, USER_PIC_MAIL 
							FROM tm_user_pic ORDER BY USER_PIC_NAME";
				$dataPengguna = $esp->main->get_arr_result($pengguna);
				if($dataPengguna){
					foreach($pengguna->result_array() as $rowPengguna){
						$arrdata['rowData'][] = $rowPengguna;
					}
				}

				if($id==""){  
					$arrdata['act'] = site_url('request/post/request/new');
				}else{
					$arrdata['act'] = site_url('request/post/request/edit');
				}

			}else if($sMenu=="list"){
				$sTitle = "Daftar Pemohon Terdaftar";
				$sStatus_id = "TR00";
			}
			// $this->load->library('newtable');
			// $this->newtable->search(array(array('APPLICANT_NAME', 'NAMA PEMOHON'), array('APPLICANT_ADDRESS', 'ALAMAT')));
			// $this->newtable->action(site_url()."applicant/applicant_list/$menu");
			// $this->newtable->cidb($this->db);
			// $this->newtable->ciuri($this->uri->segment_array());
			// $this->newtable->orderby(1);
			// $this->newtable->keys("APPLICANT_ID");
			// $this->newtable->hiddens(array("APPLICANT_ID"));
			// $prosesnya = array('Preview' => array('GET', site_url()."applicant/preview", '1'));
			// $this->newtable->menu($prosesnya);
			// $query = "SELECT APPLICANT_ID, APPLICANT_NAME AS 'NAMA PEMOHON', APPLICANT_IDENTITY_NUMBER AS 'NPWP/NIK', APPLICANT_ADDRESS AS 'ALAMAT', APPLICANT_PHONE AS TELEPON, (CASE WHEN APPLICANT_TYPE = '01' THEN 'Perorangan' WHEN APPLICANT_TYPE = '02' THEN 'Badan Usaha' ELSE '-' END) AS 'JENIS PEMOHON' FROM TM_APPLICANT WHERE STATUS_ID ='$sStatus_id' ";
			// $this->newtable->columns(array("APPLICANT_ID", "APPLICANT_NAME", "APPLICANT_IDENTITY_NUMBER", "APPLICANT_ADDRESS", "APPLICANT_PHONE", "APPLICANT_TYPE", "STATUS_ID"));
			// $this->newtable->use_ajax(TRUE);
			// $this->newtable->js_file('<script type="text/javascript" src="'.base_url().'js/newtable.js"></script>');
			// $tabel = $this->newtable->generate($query);
			// $arrdata = array("sTitle" => $sTitle,
			// 				 "sTable" => $tabel,
			// 				 "");
			return $arrdata;
		// }else{
		// 	redirect(site_url(), refresh);
		// }
	}

	function applicant_list($menu=""){
//		if(array_key_exists('01', $this->newsession->userdata('ROLE')) && (array_key_exists('0', $this->newsession->userdata('DEPUTI')) || array_key_exists('2', $this->newsession->userdata('DEPUTI')))){ #Administrator, Administrator Deputi 3

			if($menu=="new"){
				$sTitle = "Daftar Pemohon Baru";
				$sStatus_id = "TR99";
			}else if($menu=="registered"){
				$sTitle = "Daftar Pemohon Terdaftar";
				$sStatus_id = "TR00";
			}
			$this->load->library('newtable');
			$this->newtable->search(array(array('APPLICANT_NAME', 'NAMA PEMOHON'), array('APPLICANT_ADDRESS', 'ALAMAT')));
			$this->newtable->action(site_url()."applicant/applicant_list/$menu");
			$this->newtable->cidb($this->db);
			$this->newtable->ciuri($this->uri->segment_array());
			$this->newtable->orderby(1);
			$this->newtable->keys("APPLICANT_ID");
			$this->newtable->hiddens(array("APPLICANT_ID"));
			$prosesnya = array('Preview' => array('GET', site_url()."applicant/preview", '1'));
			$this->newtable->menu($prosesnya);
			$query = "SELECT APPLICANT_ID, APPLICANT_NAME AS 'NAMA PEMOHON', APPLICANT_IDENTITY_NUMBER AS 'NPWP/NIK', APPLICANT_ADDRESS AS 'ALAMAT', APPLICANT_PHONE AS TELEPON, (CASE WHEN APPLICANT_TYPE = '01' THEN 'Perorangan' WHEN APPLICANT_TYPE = '02' THEN 'Badan Usaha' ELSE '-' END) AS 'JENIS PEMOHON' FROM TM_APPLICANT WHERE STATUS_ID ='$sStatus_id' ";
			$this->newtable->columns(array("APPLICANT_ID", "APPLICANT_NAME", "APPLICANT_IDENTITY_NUMBER", "APPLICANT_ADDRESS", "APPLICANT_PHONE", "APPLICANT_TYPE", "STATUS_ID"));
			$this->newtable->use_ajax(TRUE);
			$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'js/newtable.js"></script>');
			$tabel = $this->newtable->generate($query);
			$arrdata = array("sTitle" => $sTitle,
							 "sTable" => $tabel);
			return $arrdata;
/*		}else{
			redirect(base_url());
			exit();
		}
*/	}

	function set_applicant($sMenu){ 
		$asrot = get_instance();
		$asrot->load->model("main","main", true);
		
		if($sMenu == "form"){
			$iId = $this->input->post('INGREDIENTS_ID');
			$arrApplicant = $this->input->post('INGREDIENTS');
			$arrData = array();
			foreach($arrApplicant as $c => $d){
				$arrData[$c] = $d;
			}
			if($iId == ""){
				$iCounTol = $asrot->main->get_bol_trans_begin();
				$execTrader = $this->db->insert('TM_INGREDIENTS', $arrData);
				$asrot->main->get_bol_trans_check($iCounTol, true);

				if($asrot->main->get_bol_trans_status($iCounTol)){
					return "MSG#OK#Proses Tambah Bahan Baku Berhasil#".site_url()."applicant/applicant_list";
				}else{
					return "MSG#ERR#Proses Tambah Bahan Baku Gagal. Silakan Ulangi Kembali";
				}

			}else{
				$iCounTol = $asrot->main->get_bol_trans_begin();
				$this->db->where('INGREDIENTS_ID', $iId);
				$execTrader = $this->db->update('TM_INGREDIENTS', $arrData);
				$asrot->main->get_bol_trans_check($iCounTol, true);

				if($asrot->main->get_bol_trans_status($iCounTol)){
					return "MSG#OK#Proses Ubah Bahan Baku Berhasil#".site_url()."applicant/applicant_list";
				}else{
					return "MSG#ERR#Proses Ubah Bahan Baku Gagal. Silakan Ulangi Kembali";
				}
			}
		}else if($sMenu=="delete"){ #Delete
			$iCounTol = $asrot->main->get_bol_trans_begin();
			$sTableId = JOIN($this->input->post('tb_chk'), "', '");
			$this->db->query("UPDATE TM_INGREDIENTS SET STATUS_ID = 'IN00' WHERE INGREDIENTS_ID IN('$sTableId')");
			$asrot->main->get_bol_trans_check($iCounTol, true);

			if($asrot->main->get_bol_trans_status($iCounTol)){
				return "MSG#Proses Hapus Bahan Baku Berhasil#".site_url()."applicant/applicant_list";
			}else{
				return "MSG#Proses Hapus Bahan Baku Gagal. Silakan Ulangi Kembali";
			}
		}


	}
        
    function get_arr_form($iId){
		$asrot = get_instance();
		$asrot->load->model("main","main", true);
    	$arrApplicant = $this->db->query("SELECT A.* FROM TM_INGREDIENTS A  WHERE A.INGREDIENTS_ID = '$iId' ")->row_array();
		$sTitle = "Ubah Data Bahan Baku";
    	$arrData = array("sTitle" => $sTitle,'arrApplicant' => $arrApplicant);
    	return $arrData;
    }


}
?>