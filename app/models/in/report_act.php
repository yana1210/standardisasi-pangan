<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//error_reporting(E_ERROR);
class Report_act extends Model{

    function get_arr_list_report($sType) {
        //if ($this->newsession->userdata('ROLE_ID') == "RL01" || $this->newsession->userdata('ROLE_ID') == "RL03" || $this->newsession->userdata('ROLE_ID') == "RL02" || $this->newsession->userdata('ROLE_ID') == "RL15") {//die($sType);
            $ROLE_ID = $this->newsession->userdata('ROLE_ID');
            $USER_ID = $this->newsession->userdata('USER_ID');
	        if($ROLE_ID == "603"){
	            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/ass/draft-rec', '1'));
	            $sWhere = " "; //WHERE a.`RQS_STATUS_ID` in ('RS02') AND a.`RQS_PROCESS_ID` in ('PR13') 
	            $sTitle = 'REPORT >> <span style="color: blue"> MONTHLY REPORT </span>';
	            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/ass/draft-rec/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</B>','</a>','<div>','</div>') AS 'NO AJU', f.USER_NAME as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN',    j.`STATUS_DETAIL` AS 'STATUS BERKAS' FROM tx_rqs a 
	LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
	LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
	LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
	LEFT JOIN TR_STATUS j ON a.`RQS_STATUS_ID` = j.`STATUS_ID` 
	LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere"; //d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN',a.`RQS_BTP_MERK` AS 'MERK DAGANG',i.`BTP_FUNCTION_NAME` AS 'GOLONGAN BTP', LEFT JOIN tr_btp_function i ON a.`RQS_BTP_FUNCTION_ID`=i.`BTP_FUNCTION_ID`
	            $search = array(array('RQS_NO_AJU', 'NO AJU'), array('f.USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('BTP_FUNCTION_NAME', 'GOLONGAN BTP') );
	            $action = site_url() . "report/list_report/" . $sType;
            }else {
	            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/ass/draft-rec', '1'));
	            $sWhere = " "; //WHERE a.`RQS_STATUS_ID` in ('RS05','RS06') AND a.`RQS_PROCESS_ID` in ('PR13') 
	            $sTitle = 'REPORT >> <span style="color: blue"> MONTHLY REPORT </span>';
	            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/ass/draft-rec/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</B>','</a>','<div>','</div>') AS 'NO AJU', a.RQS_ASS_DATE as 'TANGGAL ASSESSMENT', f.USER_NAME as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', j.`STATUS_DETAIL` AS 'STATUS BERKAS' FROM tx_rqs a 
	LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
	LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
	LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
	LEFT JOIN TR_STATUS j ON a.`RQS_STATUS_ID` = j.`STATUS_ID` 
	LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere"; //d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN',a.`RQS_BTP_MERK` AS 'MERK DAGANG',i.`BTP_FUNCTION_NAME` AS 'GOLONGAN BTP', LEFT JOIN tr_btp_function i ON a.`RQS_BTP_FUNCTION_ID`=i.`BTP_FUNCTION_ID`
	            $search = array(array('RQS_NO_AJU', 'NO AJU'), array('f.USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('BTP_FUNCTION_NAME', 'GOLONGAN BTP') );
                $action = site_url() . "report/list_report/" . $sType;
            }


//echo $sQuery; die();
            $this->load->library('newtable');
            $this->newtable->hiddens(array('RQS_ID'));
            $this->newtable->search($search);
            $this->newtable->action($action);
            $this->newtable->cidb($this->db);
            $this->newtable->ciuri($this->uri->segment_array());
            $this->newtable->orderby(1);
            $this->newtable->sortby("ASC");
            $this->newtable->keys("RQS_ID");
            $this->newtable->menu($prosesnya);
            $this->newtable->use_ajax(TRUE);
            $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
            $sTable = $this->newtable->generate($sQuery);
            $arrData = array("sTitle" => $sTitle,
                "sTable" => $sTable,
                "arrProcess" => $arrProcess);
            return $arrData;
    }


	function list_log($iId = "", $ispreview = FALSE){
        $query = "SELECT A.RQS_ID,A.RQS_LOG_DATE AS TANGGAL, CONCAT('<b>',B.USER_NAME,'</b><br>',A.RQS_LOG_DATE) AS 'OLEH',CONCAT('<b>',C.LOG_DESC,'</b><br>',A.RQS_LOG_NOTE) AS 'KEGIATAN'  
FROM TL_RQS_LOG A 
LEFT JOIN TM_USER B ON A.USER_ID = B.USER_ID 
LEFT JOIN TR_LOG_DESC C ON A.RQS_LOG_DESC_ID=C.LOG_DESC_ID

WHERE A.RQS_ID  = '$iId' "; //LEFT JOIN TR_STATUS E ON D.RQS_STATUS_ID=E.STATUS_ID
        $jumlah = $this->db->query($query)->num_rows();
        $this->load->library('newtable');
        $this->newtable->hiddens(array('TANGGAL', 'RQS_ID'));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        $this->newtable->orderby(1);
        $this->newtable->sortby('DESC');
        $this->newtable->keys(array("RQS_ID"));
        if ($ispreview) {
            $this->newtable->rowcount("ALL");
            $this->newtable->show_chk(FALSE);
            $this->newtable->show_search(FALSE);
        }
        $this->newtable->columns(array("OLEH", "KEGIATAN"));
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');

        $tabel = $this->newtable->generate($query);

        if ($ispreview) {
            $tabel = "<script type=\"text/javascript\" src=\"" . base_url() . "js/newtable.js\"></script>" . $tabel;
            $arrdata = $tabel;
        } else {
            $arrdata = array("htmltable" => $tabel,
                "jmltable" => $jumlah,
                "sTitle" => $sTitle);
        }
        return $arrdata;
	}
    function download($sMenu = "", $sSubMenu = "", $sSubMenu2 = "") { //print_r($sMenu);die();
        $esp = get_instance();
        $esp->load->model("main", "main", true);
            $tgl1 = $this->input->post('tglaju1');
            $tgl2 = $this->input->post('tglaju2');
            $tgl2 = date('Y-m-d', strtotime('+1 days', strtotime($tgl2))); // harus ditambah 1 hari
                $sWhere = "WHERE a.`RQS_STATUS_ID` IN ('RS05','RS06') AND a.`RQS_PROCESS_ID` IS NOT NULL AND a.RQS_CREATE_DATE BETWEEN '$tgl1' AND '$tgl2'"; 
                $sQuery = "SELECT a.*, f.USER_NAME, f.USER_ADDR, f.USER_TELP, f.USER_FAX, f.USER_MAIL, c.BTP_TYPE_NO, c.BTP_TYPE_NAME,e.FOOD_TYPE_NO, e.FOOD_TYPE_NAME, b.`USER_NAME` AS 'VERIFIKATOR',d.`USER_NAME` AS 'ASESOR', j.`STATUS_DETAIL` AS 'POSISI', k.`STATUS_DETAIL` AS 'STATUS',
                    l.S_LETTER_NUMBER,l.S_NUMBER_APPROVED,l.S_MAX_LIMIT_APPROVED,l.S_DATE,l.T_DATE, datediff(l.S_DATE, a.RQS_VER_DATE) as selisih_s, datediff(l.T_DATE, a.RQS_VER_DATE) as selisih_t, m.`STATUS_DETAIL` AS 'status_p', n.`STATUS_DETAIL` AS 'skala_p', g.TRADER_TYPE
                 FROM tx_rqs a 
    LEFT JOIN tm_user b ON a.`RQS_VER_BY`=b.`USER_ID`
    LEFT JOIN tr_btp_type c ON a.`RQS_BTP_TYPE_ID`=c.`BTP_TYPE_ID`
    LEFT JOIN tr_food_type e ON a.`RQS_FOOD_TYPE_ID`=e.`FOOD_TYPE_ID`
    LEFT JOIN tm_user d ON a.`RQS_ASS_BY`=d.`USER_ID`
    LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
    LEFT JOIN TR_STATUS j ON a.`RQS_PROCESS_ID` = j.`STATUS_ID` 
    LEFT JOIN TR_STATUS k ON a.`RQS_STATUS_ID` = k.`STATUS_ID` 
    LEFT JOIN tx_rqs_letter l ON a.`RQS_ID` = l.`RQS_ID` 
    LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID`
    LEFT JOIN TR_STATUS m ON g.`USER_COM_STATUS_ID` = m.`STATUS_ID` 
    LEFT JOIN TR_STATUS n ON g.`USER_COM_SCALE_ID` = n.`STATUS_ID` 

     $sWhere";
    //echo $sQuery; die();
            $query = $this->db->query($sQuery)->result_array();

//  print_r($query);die();
            return $query;
        }


}
?>