 <?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Email_act extends CI_Model {
	function kirim_email($sType,$email,$id,$pass){
		//echo $pass; die();
		$this->load->library('PHPMailerAutoload');
		
        $mail = new PHPMailer();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        
        // set smtp
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = '587';
		$mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = 'rekomendasihubla@gmail.com';
        $mail->Password = 'hubla123';	
        $mail->WordWrap = 150;
        $mail->setFrom('rekomendasihubla@gmail.com', 'Notifikasi e-Standar Pangan');
		$mail->addBCC('widarahmawatiefandi@gmail.com');
		
		if($type=='lupapassword'){		
			$subject = '[no_reply]Lupa Password';
			$message = '<html><head><title>Pendaftaran</title></head><body>';
			$message  = '<p style="font-color:#000;">Anda Telah Melakukan Lupa Password.</p>';
			$message .= '<p><br>Jangan Lupa Mengganti Password Anda Secara Berkala.</p>';
			$message .= '<p>Klik link berikut untuk reset password :  <a href="'.site_url().'/lupa/set/'.$id.'" target="_blank">Klik disini</a></p>';
			$message .= '<p><br>Terima kasih</p>';
			$message .= '</body></html>';
		}else if($type=='rekomendasi'){		
			$query ="SELECT A.HDR_NOAJU AS NOAJU, B.REK_NO AS NOREK, B.MMSI_NO AS MMSI, DATE_FORMAT(C.PROS_TGL,'%d %M %Y') AS TGL FROM TX_HUBHDR A  
	LEFT JOIN TX_REKOMENDASI B ON A.HDR_ID=B.HDR_ID 
	LEFT JOIN TX_HUBPROSES C ON A.HDR_ID=C.HDR_ID WHERE A.HDR_ID='".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['file'] = $row;
				}
			}
			
			if($izin == '3'){
			
			$subject = '[no_reply]Rekomendasi Surat';
			$message = '<html><head><title>Rekomendasi</title></head><body>';
			$message  = '<p style="font-color:#000;">Selamat ! Pengajuan Anda Telah Diproses Dan Disetujui Pada '.$arrdata['file']['TGL'].'</p>';
			$message .= '<p><br>No. Aju : '.$arrdata['file']['NOAJU'].'</p>';
			$message .= '<p><br>No. MMSI : '.$arrdata['file']['MMSI'].'</p>';
			$message .= '<p><br>Silahkan Mendatangi Petugas Dan Menunjukkan E-mail ini Atau Menyebutkan No. Aju Untuk Pencetakan Surat</p>';
			$message .= '<p><br>Terima Kasih</p>';
			$message .= '</body></html>';
			}else{
				$subject = '[no_reply]Rekomendasi Surat';
			$message = '<html><head><title>Rekomendasi</title></head><body>';
			$message  = '<p style="font-color:#000;">Selamat ! Pengajuan Anda Telah Diproses Dan Disetujui Pada '.$arrdata['file']['TGL'].'</p>';
			$message .= '<p><br>No. Aju : '.$arrdata['file']['NOAJU'].'</p>';
			$message .= '<p><br>No. Rekomendasi : '.$arrdata['file']['NOREK'].'</p>';
			$message .= '<p><br>Silahkan Mendatangi Petugas Dan Menunjukkan E-mail ini Atau Menyebutkan No. Aju Untuk Pencetakan Surat</p>';
			$message .= '<p><br>Terima Kasih</p>';
			$message .= '</body></html>';
			
		} else if($type=='approval'){
		
		$query = "SELECT A.FAC_REG_NO, E.REFF_NAME AS JENIS, A.FAC_NPWP AS NPWP, A.FAC_ID, A.FAC_NAME, A.FAC_ADDR, C.STATE_NAME AS PROVINSI,
							D.STATE_NAME AS KOTA, A.FAC_ZIP, A.FAC_TELP, A.FAC_FACTORY_ADDR, F.STATE_NAME AS PROV_P, G.STATE_NAME AS KOTA_P,
							A.FAC_FACTORY_ZIP, A.FAC_FACTORY_TELP, B.USER_FULLNAME, B.USER_ID, B.USER_NAME AS USERNAME, B.USER_POSITION,
							B.USER_PHONE, B.USER_EMAIL 
							FROM TM_FAC A
							LEFT JOIN TM_USER B ON A.FAC_ID = B.FAC_ID 
							LEFT JOIN TB_STATE C ON A.FAC_PROV = C.STATE_CODE AND C.STATE_TYPE='1' 
							LEFT JOIN TB_STATE D ON A.FAC_CITY = D.STATE_CODE AND D.STATE_TYPE='2'
							LEFT JOIN TB_STATE F ON A.FAC_FACTORY_PROV = F.STATE_CODE AND F.STATE_TYPE='1' 
							LEFT JOIN TB_STATE G ON A.FAC_FACTORY_CITY = G.STATE_CODE AND G.STATE_TYPE='2'
							LEFT JOIN TB_REFF E ON A.FAC_DESC = E.REFF_CODE AND E.REFF_TAB='0001'   
							WHERE B.USER_ID = '".$id."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['data'] = $row;
					}
				}	
				
			$subject = 'Approval Pengguna';
			$message = '<html><head><title>Selamat ! Data Registrasi Anda :</title></head><body>';
				$message .= '<p style="font-size:14px">Selamat ! Data Registrasi Anda :</p>';							
				$message .= '<table>';
				$message .= '<p><br>Data Perusahaan</p></br>';
				$message .= '<tr>';
				$message .= '<td>No. Regisrasi</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_REG_NO'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Jenis Pabrik</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['JENIS'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No. NPWP</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['NPWP'].'</td>';
				$message .= '</tr>';		
				$message .= '<tr>';
				$message .= '<td>Nama Kantor</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_NAME'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Alamat</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_ADDR'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Provinsi</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['PROVINSI'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kota</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['KOTA'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kode Pos</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_ZIP'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No Telp</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_TELP'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Alamat Pabrik</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_FACTORY_ADDR'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Provinsi</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['PROV_P'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kota</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['KOTA_P'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kode Pos</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_FACTORY_ZIP'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No Telp Pabrik</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_FACTORY_TELP'].'</td>';
				$message .= '</tr></br>';
				$message .= '<p><br>Data Pemegang Akses</p></br>';
				$message .= '<tr>';
				$message .= '<td>Nama </td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_FULLNAME'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Jabatan </td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_POSITION'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No Telp</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_PHONE'].'</td>';
				$message .= '</tr>';
				$message .= '<td>Email</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_EMAIL'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>User Name</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USERNAME'].'</td>';
				$message .= '</tr>';
				$message .= '</table></br>';
				$message .= '<p style="font-size:14px">Telah DiApprove. Berikut Data Login Anda :</p>';
				$message .= '<br><p style="font-size:14px">Username 	: '.$arrdata['data']['USERNAME'].' </p>';
				$message .= '<br><p style="font-size:14px">Password 	: '.$pass.' </p>';
				$message .= '<br><p style="font-size:14px">Silahkan Melakukan Proses Login Aplikasi Dengan Menggunakan Data Login Anda.</p>';
				$message .= '<br><p>Terima kasih</p>';
				$message .= '</body></html>';
		}else if($type=='registrasi'){
			$query = "SELECT A.FAC_REG_NO, E.REFF_NAME AS JENIS, A.FAC_NPWP AS NPWP, A.FAC_ID, A.FAC_NAME, A.FAC_ADDR, C.STATE_NAME AS PROVINSI,
							D.STATE_NAME AS KOTA, A.FAC_ZIP, A.FAC_TELP, A.FAC_FACTORY_ADDR, F.STATE_NAME AS PROV_P, G.STATE_NAME AS KOTA_P,
							A.FAC_FACTORY_ZIP, A.FAC_FACTORY_TELP, B.USER_FULLNAME, B.USER_ID, B.USER_NAME AS USERNAME, B.USER_POSITION,
							B.USER_PHONE, B.USER_EMAIL 
							FROM TM_FAC A
							LEFT JOIN TM_USER B ON A.FAC_ID = B.FAC_ID 
							LEFT JOIN TB_STATE C ON A.FAC_PROV = C.STATE_CODE AND C.STATE_TYPE='1' 
							LEFT JOIN TB_STATE D ON A.FAC_CITY = D.STATE_CODE AND D.STATE_TYPE='2'
							LEFT JOIN TB_STATE F ON A.FAC_FACTORY_PROV = F.STATE_CODE AND F.STATE_TYPE='1' 
							LEFT JOIN TB_STATE G ON A.FAC_FACTORY_CITY = G.STATE_CODE AND G.STATE_TYPE='2'
							LEFT JOIN TB_REFF E ON A.FAC_DESC = E.REFF_CODE AND E.REFF_TAB='0003'   
							WHERE B.USER_ID = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['data'] = $row;
				}
			}
			$subject = '[no_reply]Registrasi Pengguna APP CPOB';
			$message = '<html><head><title>Selamat !</title></head><body>';
				$message  = '<p style="font-color:#000;">Selamat ! Registrasi Anda Berhasil, Berikut Data Registrasi Anda.</p>';
				$message .= '<table>';
				$message .= '<p><br>Data Perusahaan</p></br>';
				$message .= '<tr>';
				$message .= '<td>No. Regisrasi</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_REG_NO'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Jenis Pabrik</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['JENIS'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No. NPWP</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['NPWP'].'</td>';
				$message .= '</tr>';	
				$message .= '<tr>';
				$message .= '<td>Nama Kantor</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_NAME'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Alamat</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_ADDR'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Provinsi</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['PROVINSI'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kota</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['KOTA'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kode Pos</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_ZIP'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No Telp</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_TELP'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Alamat Pabrik</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_FACTORY_ADDR'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Provinsi</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['PROV_P'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kota</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['KOTA_P'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kode Pos</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_FACTORY_ZIP'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No Telp Pabrik</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_FACTORY_TELP'].'</td>';
				$message .= '</tr></br>';
				$message .= '<p><br>Data Pemegang Akses</p></br>';
				$message .= '<tr>';
				$message .= '<td>Nama </td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_FULLNAME'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Jabatan </td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_POSITION'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No Telp</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_PHONE'].'</td>';
				$message .= '</tr>';
				$message .= '<td>Email</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_EMAIL'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>User Name</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USERNAME'].'</td>';
				$message .= '</tr>';
				$message .= '</table></br>';
				$message .= '<p><br>Silahkan Menunggu Email Konfirmasi Selanjutnya Untuk Password Penggunaan Aplikasi.</p>';
				$message .= '<p><br>Terima kasih</p>';
				$message .= '</body></html>';
		}else if($type=='regulang'){
			$query = "SELECT A.FAC_REG_NO, E.REFF_NAME AS JENIS, A.FAC_NPWP AS NPWP, A.FAC_ID, A.FAC_NAME, A.FAC_ADDR, C.STATE_NAME AS PROVINSI,
							D.STATE_NAME AS KOTA, A.FAC_ZIP, A.FAC_TELP, A.FAC_FACTORY_ADDR, F.STATE_NAME AS PROV_P, G.STATE_NAME AS KOTA_P,
							A.FAC_FACTORY_ZIP, A.FAC_FACTORY_TELP, B.USER_FULLNAME, B.USER_ID, B.USER_NAME AS USERNAME, B.USER_POSITION,
							B.USER_PHONE, B.USER_EMAIL 
							FROM TM_FAC A
							LEFT JOIN TM_USER B ON A.FAC_ID = B.FAC_ID 
							LEFT JOIN TB_STATE C ON A.FAC_PROV = C.STATE_CODE AND C.STATE_TYPE='1' 
							LEFT JOIN TB_STATE D ON A.FAC_CITY = D.STATE_CODE AND D.STATE_TYPE='2'
							LEFT JOIN TB_STATE F ON A.FAC_FACTORY_PROV = F.STATE_CODE AND F.STATE_TYPE='1' 
							LEFT JOIN TB_STATE G ON A.FAC_FACTORY_CITY = G.STATE_CODE AND G.STATE_TYPE='2'
							LEFT JOIN TB_REFF E ON A.FAC_DESC = E.REFF_CODE AND E.REFF_TAB='0001'   
							WHERE B.USER_ID = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['data'] = $row;
				}
			}
			$subject = '[no_reply]Registrasi Ulang Pengguna APP CPOB';
			$message = '<html><head><title>Selamat !</title></head><body>';
				$message  = '<p style="font-color:#000;">Selamat ! Registrasi Ulang Anda Berhasil, Berikut Data Registrasi Anda.</p>';
				$message .= '<table>';
				$message .= '<p><br>Data Perusahaan</p></br>';
				$message .= '<tr>';
				$message .= '<td>No. Regisrasi</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_REG_NO'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Jenis Pabrik</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['JENIS'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No. NPWP</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['NPWP'].'</td>';
				$message .= '</tr>';		
				$message .= '<tr>';
				$message .= '<td>Nama Kantor</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_NAME'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Alamat</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_ADDR'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Provinsi</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['PROVINSI'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kota</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['KOTA'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kode Pos</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_ZIP'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No Telp</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_TELP'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Alamat Pabrik</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_FACTORY_ADDR'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Provinsi</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['PROV_P'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kota</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['KOTA_P'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Kode Pos</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_FACTORY_ZIP'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No Telp Pabrik</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['FAC_FACTORY_TELP'].'</td>';
				$message .= '</tr></br>';
				$message .= '<p><br>Data Pemegang Akses</p></br>';
				$message .= '<tr>';
				$message .= '<td>Nama </td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_FULLNAME'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>Jabatan </td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_POSITION'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>No Telp</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_PHONE'].'</td>';
				$message .= '</tr>';
				$message .= '<td>Email</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USER_EMAIL'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td>User Name</td>';
				$message .= '<td>:</td>';
				$message .= '<td>'.$arrdata['data']['USERNAME'].'</td>';
				$message .= '</tr>';
				$message .= '</table></br>';
				$message .= '<p><br>Silahkan Menunggu Email Konfirmasi Selanjutnya Untuk Password Penggunaan Aplikasi.</p>';
				$message .= '<p><br>Terima kasih</p>';
				$message .= '</body></html>';
		}else if($type=='reject'){
			$query = "SELECT A.FAC_REG_NO, E.REFF_NAME AS JENIS, H.REFF_NAME AS STATUS, A.FAC_NPWP AS NPWP, A.FAC_ID, A.FAC_NAME, A.FAC_ADDR, C.STATE_NAME AS PROVINSI,
							D.STATE_NAME AS KOTA, A.FAC_ZIP, A.FAC_TELP, A.FAC_FACTORY_ADDR, F.STATE_NAME AS PROV_P, G.STATE_NAME AS KOTA_P,
							A.FAC_FACTORY_ZIP, A.FAC_FACTORY_TELP, B.USER_FULLNAME, B.USER_NAME AS USERNAME, B.USER_POSITION, B.USER_PHONE, B.USER_EMAIL,
							B.USER_PASS AS PASSWORD
							FROM TM_FAC A
							LEFT JOIN TM_USER B ON A.FAC_ID = B.FAC_ID 
							LEFT JOIN TB_STATE C ON A.FAC_PROV = C.STATE_CODE AND C.STATE_TYPE='1' 
							LEFT JOIN TB_STATE D ON A.FAC_CITY = D.STATE_CODE AND D.STATE_TYPE='2'
							LEFT JOIN TB_STATE F ON A.FAC_FACTORY_PROV = F.STATE_CODE AND F.STATE_TYPE='1' 
							LEFT JOIN TB_STATE G ON A.FAC_FACTORY_CITY = G.STATE_CODE AND G.STATE_TYPE='2'
							LEFT JOIN TB_REFF E ON A.FAC_DESC = E.REFF_CODE 
							LEFT JOIN TB_REFF H ON A.FAC_STATUS=H.REFF_CODE AND H.REFF_TAB='0001'  
							WHERE B.USER_ID = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['data'] = $row;
				}
			}
			$subject = '[no_reply]Reject Pengguna';
			$message = '<html><head><title>Pendaftaran</title></head><body>';
			$message .= '<p style="font-size:14px">Selamat ! Data Registrasi Anda :</p>';							
			$message .= '<table>';
			$message .= '<p><br>Data Perusahaan</p></br>';
			$message .= '<tr>';
			$message .= '<td>No. Regisrasi</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['FAC_REG_NO'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Jenis Pabrik</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['JENIS'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>No. NPWP</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['NPWP'].'</td>';
			$message .= '</tr>';		
			$message .= '<tr>';
			$message .= '<td>Nama Kantor</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['FAC_NAME'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Alamat</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['FAC_ADDR'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Provinsi</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['PROVINSI'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Kota</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['KOTA'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Kode Pos</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['FAC_ZIP'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>No Telp</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['FAC_TELP'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Alamat Pabrik</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['FAC_FACTORY_ADDR'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Provinsi</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['PROV_P'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Kota</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['KOTA_P'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Kode Pos</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['FAC_FACTORY_ZIP'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>No Telp Pabrik</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['FAC_FACTORY_TELP'].'</td>';
			$message .= '</tr></br>';
			$message .= '<p><br>Data Pemegang Akses</p></br>';
			$message .= '<tr>';
			$message .= '<td>Nama </td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_FULLNAME'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Jabatan </td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_POSITION'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>No Telp</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_PHONE'].'</td>';
			$message .= '</tr>';
			$message .= '<td>Email</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_EMAIL'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>User Name</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USERNAME'].'</td>';
			$message .= '</tr>';
			$message .= '</table></br>';
			$message .= '<p style="font-size:14px">Tidak Dapat Diproses Atau Direject. Silahkan Melakukan Registrasi Ulang.</p>';
			$message .= '<br><p>Terima kasih</p>';
			$message .= '</body></html>';
		}else if($type=='newpetugas'){
			$query = "SELECT A.USER_ID, A.USER_FULLNAME, B.GROUP_DESC AS ROLE, A.USER_POSITION, A.USER_NUMBER, A.USER_PHONE,
						A.USER_EMAIL, A.USER_NAME AS USERNAME, A.USER_PASS AS PASSWORD, A.USER_STATUS
						FROM TM_USER A
						LEFT JOIN TB_GROUP_USER B ON A.GROUP_ID = B.GROUP_ID
						WHERE A.USER_ID = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['data'] = $row;
				}
			}
			$subject = '[no_reply]Petugas Baru';
			$message = '<html><head><title>Selamat !</title></head><body>';
			$message .= '<p style="font-size:14px">Selamat ! Data Petugas Anda :</p>';							
			$message .= '<table>';
			$message .= '<tr>';
			$message .= '<td>Nama Petugas</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_FULLNAME'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Role</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['ROLE'].'</td>';
			$message .= '</tr>';	
			$message .= '<tr>';
			$message .= '<td>Jabatan</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_POSITION'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>NIP</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_NUMBER'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>No. Telepon</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_PHONE'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Email</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_EMAIL'].'</td>';
			$message .= '</tr>';
			$message .= '</table></br>';
			$message .= '<p style="font-size:14px">Telah Tersimpan. Berikut Data Login Anda :</p>';
			$message .= '<br><p style="font-size:14px">Username 	: '.$arrdata['data']['USERNAME'].' </p>';
			$message .= '<br><p style="font-size:14px">Password 	: '.$arrdata['data']['PASSWORD'].' </p>';
			$message .= '<br><p style="font-size:14px">Silahkan Melakukan Proses Login Aplikasi Dengan Menggunakan Data Login Anda.</p>';
			$message .= '<br><p>Terima kasih</p>';
			$message .= '</body></html>';
		}else if($type=='editpetugas'){
			$query = "SELECT A.USER_ID, A.USER_FULLNAME, B.GROUP_DESC AS ROLE, A.USER_POSITION, A.USER_NUMBER, A.USER_PHONE,
						A.USER_EMAIL, A.USER_NAME AS USERNAME, A.USER_PASS AS PASSWORD, A.USER_STATUS
						FROM TM_USER A
						LEFT JOIN TB_GROUP_USER B ON A.GROUP_ID = B.GROUP_ID
						WHERE A.USER_ID = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['data'] = $row;
				}
			}
			$subject = '[no_reply]Petugas Edit';
			$message = '<html><head><title>Selamat !</title></head><body>';
			$message .= '<p style="font-size:14px">Selamat ! Data Petugas Anda :</p>';							
			$message .= '<table>';
			$message .= '<tr>';
			$message .= '<td>Nama Operator</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_FULLNAME'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Role</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['ROLE'].'</td>';
			$message .= '</tr>';	
			$message .= '<tr>';
			$message .= '<td>Jabatan</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_POSITION'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>NIP</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_NUMBER'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>No. Telepon</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_PHONE'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Email</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_EMAIL'].'</td>';
			$message .= '</tr>';
			$message .= '</table></br>';
			$message .= '<p style="font-size:14px">Telah Tersimpan. Berikut Data Login Anda :</p>';
			$message .= '<br><p style="font-size:14px">Username 	: '.$arrdata['data']['USERNAME'].' </p>';
			$message .= '<br><p style="font-size:14px">Password 	: '.$pass.' </p>';
			$message .= '<br><p style="font-size:14px">Silahkan Melakukan Proses Login Aplikasi Dengan Menggunakan Data Login Anda.</p>';
			$message .= '<br><p>Terima kasih</p>';
			$message .= '</body></html>';
		}else if($type=='newoperator'){
			$query = "SELECT A.USER_ID, A.USER_FULLNAME, B.GROUP_DESC AS ROLE, A.USER_POSITION, A.USER_NUMBER, A.USER_PHONE,
						A.USER_EMAIL, A.USER_NAME AS USERNAME, A.USER_PASS AS PASSWORD, A.USER_STATUS
						FROM TM_USER A
						LEFT JOIN TB_GROUP_USER B ON A.GROUP_ID = B.GROUP_ID
						WHERE A.USER_ID = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['data'] = $row;
				}
			}
			$subject = '[no_reply]Operator Baru';
			$message = '<html><head><title>Selamat !</title></head><body>';
			$message .= '<p style="font-size:14px">Selamat ! Data Petugas Anda :</p>';							
			$message .= '<table>';
			$message .= '<tr>';
			$message .= '<td>Nama Operator</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_FULLNAME'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Role</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['ROLE'].'</td>';
			$message .= '</tr>';	
			$message .= '<tr>';
			$message .= '<td>Jabatan</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_POSITION'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>NIP</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_NUMBER'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>No. Telepon</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_PHONE'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Email</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_EMAIL'].'</td>';
			$message .= '</tr>';
			$message .= '</table></br>';
			$message .= '<p style="font-size:14px">Telah Tersimpan. Berikut Data Login Anda :</p>';
			$message .= '<br><p style="font-size:14px">Username 	: '.$arrdata['data']['USERNAME'].' </p>';
			$message .= '<br><p style="font-size:14px">Password 	: '.$pass.' </p>';
			$message .= '<br><p style="font-size:14px">Silahkan Melakukan Proses Login Aplikasi Dengan Menggunakan Data Login Anda.</p>';
			$message .= '<br><p>Terima kasih</p>';
			$message .= '</body></html>';
		}else if($type=='editoperator'){
			$query = "SELECT A.USER_ID, A.USER_FULLNAME, B.GROUP_DESC AS ROLE, A.USER_POSITION, A.USER_NUMBER, A.USER_PHONE,
						A.USER_EMAIL, A.USER_NAME AS USERNAME, A.USER_PASS AS PASSWORD, A.USER_STATUS
						FROM TM_USER A
						LEFT JOIN TB_GROUP_USER B ON A.GROUP_ID = B.GROUP_ID
						WHERE A.USER_ID = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['data'] = $row;
				}
			}
			$subject = '[no_reply]Operator Edit';
			$message = '<html><head><title>Selamat !</title></head><body>';
			$message .= '<p style="font-size:14px">Selamat ! Data Operator Anda :</p>';							
			$message .= '<table>';
			$message .= '<p><br>Data Perusahaan</p></br>';
			$message .= '<tr>';
			$message .= '<td>Nama Operator</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_FULLNAME'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Role</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['ROLE'].'</td>';
			$message .= '</tr>';	
			$message .= '<tr>';
			$message .= '<td>Jabatan</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_POSITION'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>NIP</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_NUMBER'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>No. Telepon</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_PHONE'].'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td>Email</td>';
			$message .= '<td>:</td>';
			$message .= '<td>'.$arrdata['data']['USER_EMAIL'].'</td>';
			$message .= '</tr>';
			$message .= '</table></br>';
			$message .= '<p style="font-size:14px">Telah Tersimpan. Berikut Data Login Anda :</p>';
			$message .= '<br><p style="font-size:14px">Username 	: '.$arrdata['data']['USERNAME'].' </p>';
			$message .= '<br><p style="font-size:14px">Password 	: '.$pass.' </p>';
			$message .= '<br><p style="font-size:14px">Silahkan Melakukan Proses Login Aplikasi Dengan Menggunakan Data Login Anda.</p>';
			$message .= '<br><p>Terima kasih</p>';
			$message .= '</body></html>';
		}else if($type=='lupa'){		
			$subject = '[no_reply]Lupa Password';
			$message = '<html><head><title>Pendaftaran</title></head><body>';
			$message  = '<p style="font-color:#000;">Anda Telah Melakukan Lupa Password.</p>';
			$message .= '<p><br>Jangan Lupa Mengganti Password Anda Secara Berkala.</p>';
			$message .= '<p>Klik link berikut untuk reset password :  <a href="'.site_url().'/lupa/set/'.$id.'" target="_blank">Klik disini</a></p>';
			$message .= '<p><br>Terima kasih</p>';
			$message .= '</body></html>';
		}else if($type=='rekomendasi'){		
			$query ="SELECT A.HDR_NOAJU AS NOAJU, B.REK_NO AS NOREK, B.MMSI_NO AS MMSI, DATE_FORMAT(C.PROS_TGL,'%d %M %Y') AS TGL FROM TX_HUBHDR A  
	LEFT JOIN TX_REKOMENDASI B ON A.HDR_ID=B.HDR_ID 
	LEFT JOIN TX_HUBPROSES C ON A.HDR_ID=C.HDR_ID WHERE A.HDR_ID='".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['file'] = $row;
				}
			}
			
			if($izin == '3'){
			
			$subject = '[no_reply]Rekomendasi Surat';
			$message = '<html><head><title>Rekomendasi</title></head><body>';
			$message  = '<p style="font-color:#000;">Selamat ! Pengajuan Anda Telah Diproses Dan Disetujui Pada '.$arrdata['file']['TGL'].'</p>';
			$message .= '<p><br>No. Aju : '.$arrdata['file']['NOAJU'].'</p>';
			$message .= '<p><br>No. MMSI : '.$arrdata['file']['MMSI'].'</p>';
			$message .= '<p><br>Silahkan Mendatangi Petugas Dan Menunjukkan E-mail ini Atau Menyebutkan No. Aju Untuk Pencetakan Surat</p>';
			$message .= '<p><br>Terima Kasih</p>';
			$message .= '</body></html>';
			}else{
				$subject = '[no_reply]Rekomendasi Surat';
			$message = '<html><head><title>Rekomendasi</title></head><body>';
			$message  = '<p style="font-color:#000;">Selamat ! Pengajuan Anda Telah Diproses Dan Disetujui Pada '.$arrdata['file']['TGL'].'</p>';
			$message .= '<p><br>No. Aju : '.$arrdata['file']['NOAJU'].'</p>';
			$message .= '<p><br>No. Rekomendasi : '.$arrdata['file']['NOREK'].'</p>';
			$message .= '<p><br>Silahkan Mendatangi Petugas Dan Menunjukkan E-mail ini Atau Menyebutkan No. Aju Untuk Pencetakan Surat</p>';
			$message .= '<p><br>Terima Kasih</p>';
			$message .= '</body></html>';
			
			}
			
		}else if($type=='ditolak'){		
			$query = "SELECT  E.HDR_NOAJU AS NOAJU,F.REK_NO AS NOREK,G.PROS_KET AS KET, A.REFF_NAMA AS JENIS FROM  TB_REFF A 
			LEFT JOIN TM_PEMOHON B ON A.REFF_KODE=B.PEMOHON_TIPE 
			LEFT JOIN TM_DAERAH C ON B.PEMOHON_PROP=C.DAERAH_KODE 
			LEFT JOIN TM_USER D ON B.USER_ID=D.USER_ID 
			LEFT JOIN TX_HUBHDR E ON E.PEMOHON_ID=B.PEMOHON_ID
			LEFT JOIN TX_REKOMENDASI F ON F.HDR_ID=E.HDR_ID
			LEFT JOIN TX_HUBPROSES G ON G.HDR_ID=E.HDR_ID
			WHERE REFF_TAB='0001' AND E.HDR_ID ='".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['file'] = $row;
				}
			}
			$subject = '[no_reply]Penolakan Pengajuan Surat';
			$message = '<html><head><title>Penolakan</title></head><body>';
			$message  = '<p style="font-color:#000;">Maaf, Pengajuan Anda Dengan Nomor Aju '.$arrdata['file']['NOAJU'].'</p>';
			$message .= '<p><br>Ditolak Atau Tidak Dapat Diproses dengan catatan : </p>';
			$message .= '<p><br><br><b> '.$arrdata['file']['KET'].'</b></p>';
			$message .= '<p><br>Silahkan Perbaiki Atau Melakukan Pengajuan Kembali.	</p>';
			$message .= '<p><br>Terima Kasih</p>';
			$message .= '</body></html>';
			
		}else if($type=='billing'){
			$query = "SELECT NOAJU, CERT_BILL_BANK FROM TX_CERT_HDR WHERE HDR_ID = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['file'] = $row;
				}
			}
			$subject = '[no_reply]Penerimaan Pengajuan';
			$message = '<html><head><title>Penerimaan</title></head><body>';
			$message  = '<p style="font-color:#000;">Pengajuan Anda Dengan Nomor Aju '.$arrdata['file']['NOAJU'].' akan diproses. Segera lakukan pembayaran dengan Billing ID <b>'.$arrdata['file']['CERT_BILL_BANK'].'</b></p>';
			$message .= '<p><br>Terima Kasih</p>';
			$message .= '</body></html>';
		}
		
        $mail->addAddress($email);
        $mail->Subject = $subject;
        $mail->Body = $message;
		$mail->IsHTML(true); 
		if($mail->Send()){
			return TRUE;
		}else{
			return $mail->ErrorInfo;
		}
	}
}

?>