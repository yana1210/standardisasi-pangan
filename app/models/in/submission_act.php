<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//error_reporting(E_ERROR);
class Submission_act extends Model{

    function get_arr_list_rqs($sType) {
        $ROLE_ID = $this->newsession->userdata('ROLE_ID');
        $USER_ID = $this->newsession->userdata('USER_ID');
        $sTitle = 'SUBMISSION >> VIEW SUBMISSION >> <span style="color: blue"> DAFTAR SUBMISSION </span>';
        if ($ROLE_ID == "601") {
        	if($sType == '00'){
                $sWhere = "WHERE (a.`RQS_STATUS_ID` = '' OR a.`RQS_STATUS_ID` IS NULL) and a.`USER_ID` = '$USER_ID' ";
                $prosesnya = array('Ubah Data' => array('GET', site_url() . 'submission/new_rqs/frm-adm', '1'),'Lihat Detail' => array('GET', site_url() . 'submission/new_rqs/detail', '1'));
        	}else{
                $sWhere = "WHERE a.`RQS_STATUS_ID` = '$sType' and a.`USER_ID` = '$USER_ID' ";
                $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'submission/new_rqs/detail', '1'));
        	}
        } else {
        	if($sType == '00'){
                $sWhere = "WHERE (a.`RQS_STATUS_ID` = '' OR a.`RQS_STATUS_ID` IS NULL) ";
	            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'submission/new_rqs/detail', '1'));
        	}else{
                $sWhere = "WHERE a.`RQS_STATUS_ID` = '$sType'  ";
                $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'submission/new_rqs/detail', '1'));
        	}
        }
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a href= \"" . site_url() . "submission/new_rqs/detail/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', b.`BTP_TYPE_NO` as 'NO INS',b.`BTP_TYPE_NAME` AS 'NAMA JENIS BTP', d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN', d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', a.`RQS_PIC_NAME` AS 'PENANGGUNG JAWAB' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID` $sWhere"; //LEFT JOIN tr_status e ON a.`RQS_STATUS_ID`=e.`STATUS_ID` , e.`STATUS_DETAIL` AS 'STATUS'
        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('RQS_ID'));

        $this->newtable->search(array(array('RQS_NO_AJU', 'NOMOR PERMOHONAN'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'NAMA JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('RQS_PIC_NAME', 'PENANGGUNG JAWAB') ));

        $this->newtable->action(site_url() . "submission/list_rqs/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("DESC");
        $this->newtable->keys("RQS_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        //print_r($arrData);die();
        return $arrData;
    }
    function get_arr_list_ass($sType) {
        $ROLE_ID = $this->newsession->userdata('ROLE_ID');
        $USER_ID = $this->newsession->userdata('USER_ID');
        if ($ROLE_ID == "601") {
            $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02') and a.`USER_ID` = '$USER_ID' ";
            $sTitle = 'SUBMISSION >> VIEW ASSIGNMENT >> <span style="color: blue"> DAFTAR ASSIGNMENT </span>';
        } else if($ROLE_ID == "602" || $ROLE_ID == "604"){
            $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02')  AND a.`RQS_PROCESS_ID` in ('PR02') AND a.RQS_VER_BY = '$USER_ID' "; //baru atau revisi berkas
            $sTitle = 'SUBMISSION >> ASSIGNMENT >> <span style="color: blue"> DAFTAR ASSIGNMENT </span>';
        } else if($ROLE_ID == "698"){
            $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02')  AND a.`RQS_PROCESS_ID` in ('PR02') "; //baru atau revisi berkas
            $sTitle = 'DAFTAR SUBMISSION';
        } else {
            $sTitle = 'BELUM DISET';
        }
        if($sType == 'ass'){ // menu assessment di assesor
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/assignment/detail/',a.`RQS_ID`,'\">',a.`RQS_VER_DATE`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'TANGGAL VERIFIKASI', a.RQS_NO_AJU as 'NO AJU', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP', d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN', d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', a.`RQS_BTP_MERK` AS 'MERK DAGANG', i.`BTP_FUNCTION_NAME` AS 'GOLONGAN BTP', h.`USER_NAME` AS 'VERIFIED BY' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN tm_user h ON a.`RQS_VER_BY`=h.`USER_ID` 
LEFT JOIN tr_btp_function i ON a.`RQS_BTP_FUNCTION_ID`=i.`BTP_FUNCTION_ID`
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere";
            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/assignment/detail', '1'));
            $search = array(array('RQS_VER_DATE', 'TANGGAL VERIFIKASI'),array('RQS_NO_AJU', 'NO AJU'), array('f.USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('BTP_FUNCTION_NAME', 'GOLONGAN BTP'), array('h.USER_NAME', 'VERIFIED BY') );
            $action = site_url() . "assessment/list_assignment/" . $sType;

        }else{ // menu submission di verifikator
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "submission/assignment/detail/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NO PERMOHONAN', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', a.`RQS_BTP_MERK` AS 'MERK DAGANG', a.`RQS_PIC_NAME` AS 'PJ' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` 
$sWhere";
            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'submission/assignment/detail', '1'));
            $search = array( array('USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'),  array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('RQS_PIC_NAME', 'PENANGGUNG JAWAB') );
            $action = site_url() . "submission/list_assignment/" . $sType;
        }


        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('RQS_ID'));

        $this->newtable->search($search);

        $this->newtable->action($action);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("RQS_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        //print_r($arrData);die();
        return $arrData;
    }


	function set_submission($sMenu="", $sSubmenu="", $iIdsalah=""){
		$esp = get_instance();
		$esp->load->model("main","main", true);
		$iId = $this->input->post('id');
		$sUserId = $this->newsession->userdata('USER_ID');
        $now = date('Y-m-d H:i:s');

		if($sMenu=="adm"){ //print_r($_POST); die();
            $iCounTol = $esp->main->get_bol_trans_begin();
			$arrTxRqs = $this->input->post('REQUEST');
			$arrTxRqs['RQS_CREATE_DATE'] = $now;

			if($sSubmenu == 'new'){
				$arrTxRqs['RQS_STATUS_ID'] = 'RS01';
				$this->db->insert('tx_rqs', $arrTxRqs);
		        $iRqsId = $this->db->insert_id();

			}else if($sSubmenu == 'edit'){
				$this->db->where('RQS_ID', $iId);
				$this->db->update('tx_rqs', $arrTxRqs);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$iRqsId = $iId;
			}			
			//print_r($this->db->last_query());
			if($esp->main->get_bol_trans_status($iCounTol)){
				return "MSG#OK#Data Berhasil Disimpan#".site_url()."submission/new_rqs/frm-btp/".$iRqsId;
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}

		}else if($sMenu=='btp'){
            $iCounTol = $esp->main->get_bol_trans_begin();
			$arrTxRqs = $this->input->post('REQUEST');
			$arrTxRqs['RQS_CREATE_DATE'] = $now;
			$arrTxRqsDln = $this->input->post('REQUEST_DLN');
			$arrTxRqsLcns = $this->input->post('REQUEST_LCNS');
			$arrTxRqsImpr = $this->input->post('REQUEST_IMPR');
			$arrTxRqsPckg = $this->input->post('REQUEST_PCKG');

			$DLN = $this->input->post('DLN');
			$PCKG = $this->input->post('PCKG');
			$LCNS = $this->input->post('LCNS');
			$IMPR = $this->input->post('IMPR');

			if($sSubmenu == 'edit'){
				$this->db->where('RQS_ID', $iId);
				$this->db->update('tx_rqs', $arrTxRqs);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				if(count($arrTxRqsDln) > 1){ 
					$this->db->where('RQS_LCNS_ID', $LCNS);
					$this->db->delete('tx_rqs_lcns');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);

					$this->db->where('RQS_IMPR_ID', $IMPR);
					$this->db->delete('tx_rqs_impr');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);

					$this->db->where('RQS_PCKG_ID', $PCKG);
					$this->db->delete('tx_rqs_pckg');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);

					if($DLN != ''){ //edit
						$this->db->where('RQS_DLN_ID', $DLN);
						$this->db->update('tx_rqs_dln', $arrTxRqsDln);
			            $esp->main->get_bol_trans_check($iCounTol,FALSE);
					}else{ //save
						$arrTxRqsDln['RQS_ID'] = $iId;
						$this->db->insert('tx_rqs_dln', $arrTxRqsDln);
			            $esp->main->get_bol_trans_check($iCounTol);
					}
				} else if(count($arrTxRqsLcns) > 1){
					$this->db->where('RQS_DLN_ID', $DLN);
					$this->db->delete('tx_rqs_dln');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);

					$this->db->where('RQS_IMPR_ID', $IMPR);
					$this->db->delete('tx_rqs_impr');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);

					$this->db->where('RQS_PCKG_ID', $PCKG);
					$this->db->delete('tx_rqs_pckg');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);
					if($LCNS != ''){ //edit
						$this->db->where('RQS_LCNS_ID', $LCNS);
						$this->db->update('tx_rqs_lcns', $arrTxRqsLcns);
			            $esp->main->get_bol_trans_check($iCounTol,FALSE);
					}else{ //save
						$arrTxRqsLcns['RQS_ID'] = $iId;
						$this->db->insert('tx_rqs_lcns', $arrTxRqsLcns);
			            $esp->main->get_bol_trans_check($iCounTol);
					}
				} else if(count($arrTxRqsImpr) > 1){
					$this->db->where('RQS_DLN_ID', $DLN);
					$this->db->delete('tx_rqs_dln');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);

					$this->db->where('RQS_LCNS_ID', $LCNS);
					$this->db->delete('tx_rqs_lcns');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);

					$this->db->where('RQS_PCKG_ID', $PCKG);
					$this->db->delete('tx_rqs_pckg');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);
					if($IMPRs != ''){ //edit
						$this->db->where('RQS_IMPR_ID', $IMPR);
						$this->db->update('tx_rqs_impr', $arrTxRqsImpr);
			            $esp->main->get_bol_trans_check($iCounTol,FALSE);
					}else{ //save
						$arrTxRqsImpr['RQS_ID'] = $iId;
						$this->db->insert('tx_rqs_impr', $arrTxRqsImpr);
			            $esp->main->get_bol_trans_check($iCounTol);
					}
				} else if(count($arrTxRqsPckg) > 1){
					$this->db->where('RQS_DLN_ID', $DLN);
					$this->db->delete('tx_rqs_dln');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);

					$this->db->where('RQS_LCNS_ID', $LCNS);
					$this->db->delete('tx_rqs_lcns');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);

					$this->db->where('RQS_IMPR_ID', $IMPR);
					$this->db->delete('tx_rqs_impr');
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);
					if($PCKG != ''){ //edit
						$this->db->where('RQS_PCKG_ID', $PCKG);
						$this->db->update('tx_rqs_pckg', $arrTxRqsPckg);
			            $esp->main->get_bol_trans_check($iCounTol,FALSE);
					}else{ //save
						$arrTxRqsPckg['RQS_ID'] = $iId;
						$this->db->insert('tx_rqs_pckg', $arrTxRqsPckg);
			            $esp->main->get_bol_trans_check($iCounTol);
					}
				}
		        $iRqsId = $iId;
			}			
			//print_r($this->db->last_query());
			if($esp->main->get_bol_trans_status($iCounTol)){
				return "MSG#OK#Data Berhasil Disimpan#".site_url()."submission/new_rqs/frm-tech/".$iRqsId;
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}

		}else if($sMenu=="tech"){
			// print_r($this->input->post('TX_RQS_BTP_COMP')); die();
            $iCounTol = 0;
			$TX_RQS = $this->input->post('TX_RQS');
			$TX_RQS['RQS_CREATE_DATE'] = $now;
			$TX_RQS_PRD_REG_DOC = $this->input->post('TX_RQS_PRD_REG_DOC');
			$TX_RQS_FOOD_COMP = $this->input->post('TX_RQS_FOOD_COMP');
			$TX_RQS_BTP_COMP = $this->input->post('TX_RQS_BTP_COMP');
			$TX_RQS_DOC = $this->input->post('DOC');
			$RQS_NO_AJU = $this->input->post('RQS_NO_AJU');
			$RQS_PIC_MAIL = $this->input->post('RQS_PIC_MAIL');

			$btp_id = $this->input->post('btp_id');
			$food_id = $this->input->post('food_id');
			$function_id = $TX_RQS['RQS_BTP_FUNCTION_ID'];
/*
					$sql_p = $this->db->query("SELECT a.RQS_VER_BY,a.RQS_ASS_BY,c.USER_ID_2 FROM tx_rqs a LEFT JOIN tm_user c ON a.RQS_VER_BY=c.user_id  WHERE DATE_FORMAT(a.rqs_create_date, '%Y-%m-%d') = CURDATE() AND a.RQS_BTP_TYPE_ID = $btp_id AND a.RQS_FOOD_TYPE_ID = $food_id AND a.RQS_BTP_FUNCTION_ID = $function_id limit 1 ")->row_array();
					if($sql_p['RQS_VER_BY'] != ''){ // jika permohonan pernah diajukan maka
						$TX_RQS['RQS_VER_BY'] = $sql_p['RQS_VER_BY'];
						$v = $sql_p['RQS_VER_BY'];
						$a = $sql_p['RQS_ASS_BY'];
						echo "1.<br>";							
					}else{ // jika permohonan belum pernah diajukan maka
						$sql_p2 = $this->db->query("SELECT c.USER_ID,c.USER_ID_2 FROM tr_date a 
													LEFT JOIN ts_partition b ON a.day_name=b.partition_day 
													LEFT JOIN tm_user c ON b.user_id=c.user_id 
													WHERE a.date = CURDATE() AND b.level = 'v' AND a.status_id = 1 AND c.user_status_id = 'US01' 
													ORDER BY b.PARTITION_COUNTER ASC LIMIT 1")->row_array();
						$TX_RQS['RQS_VER_BY'] = $sql_p2['USER_ID'];
						$v = $TX_RQS['RQS_VER_BY'];
						if($sql_p2['USER_ID_2'] != ''){ // jika 1 orang memiliki akun verifikator dan asesor maka
							$TX_RQS['RQS_ASS_BY'] = $sql_p2['USER_ID_2'];
							$a = $TX_RQS['RQS_ASS_BY'];							
						echo "2.<br>";							
						}else{ // jika verifikator dan asesor beda orang maka
							$sql_p3 = $this->db->query("SELECT c.USER_ID,c.USER_ID_2 FROM tr_date a 
														LEFT JOIN ts_partition b ON a.day_name=b.partition_day 
														LEFT JOIN tm_user c ON b.user_id=c.user_id 
														WHERE a.date = CURDATE() AND b.level = 'a' AND a.status_id = 1 AND c.user_status_id = 'US01' 
														ORDER BY b.PARTITION_COUNTER ASC LIMIT 1")->row_array();

							$TX_RQS['RQS_ASS_BY'] = $sql_p3['USER_ID'];							
							$a = $TX_RQS['RQS_ASS_BY'];							
						echo "3.<br>";							
						}
					}
					$sql_p3 = $this->db->query("SELECT day_name FROM tr_date WHERE date = CURDATE() ")->row_array();
					$hari = $sql_p3['day_name'];
					$this->db->query("UPDATE TS_PARTITION   SET PARTITION_COUNTER = PARTITION_COUNTER + 1 WHERE USER_ID = $v and level = 'v' and PARTITION_DAY = '$hari' ");
					$this->db->query("UPDATE TS_PARTITION   SET PARTITION_COUNTER = PARTITION_COUNTER + 1 WHERE USER_ID = $a and level = 'a' and PARTITION_DAY = '$hari' ");
echo $TX_RQS['RQS_VER_BY']."<br>";
echo $TX_RQS['RQS_ASS_BY'];

			die();
*/
            if($TX_RQS['RQS_STATUS_ID'] == 'RS02'){
            	if($RQS_NO_AJU == ''){
			        $this->db->query("update tm_user set user_last_aju = user_last_aju + 1 where user_id = '$sUserId'");
			        $lastaju = $this->db->query("select user_last_aju from tm_user where user_id = '$sUserId'")->row_array();
			        $aju1 = $lastaju['user_last_aju'];
			        $aju = str_pad($aju1, 5, '0', STR_PAD_LEFT);
			        //$userid = str_pad($sUserId, 5, '0', STR_PAD_LEFT);
			        $userid = substr($sUserId,5,5);
			        $aju = 'ESTD3' . $userid . date('Y') . $aju;

					$TX_RQS['RQS_NO_AJU'] = $aju;

/*
					$sql_p = $this->db->query("SELECT a.RQS_VER_BY,a.RQS_ASS_BY,c.USER_ID_2 FROM tx_rqs a LEFT JOIN tm_user c ON a.RQS_VER_BY=c.user_id  WHERE DATE_FORMAT(a.rqs_create_date, '%Y-%m-%d') = CURDATE() AND a.RQS_BTP_TYPE_ID = '$btp_id' AND a.RQS_FOOD_TYPE_ID = '$food_id' AND a.RQS_BTP_FUNCTION_ID = '$function_id' limit 1 ")->row_array();
					if($sql_p['RQS_VER_BY'] != ''){ // jika permohonan pernah diajukan maka
						$TX_RQS['RQS_VER_BY'] = $sql_p['RQS_VER_BY'];
						$v = $sql_p['RQS_VER_BY'];
						$a = $sql_p['RQS_ASS_BY'];
					}else{ // jika permohonan belum pernah diajukan maka
						$sql_p2 = $this->db->query("SELECT c.USER_ID,c.USER_ID_2 FROM tr_date a 
													LEFT JOIN ts_partition b ON a.day_name=b.partition_day 
													LEFT JOIN tm_user c ON b.user_id=c.user_id 
													WHERE a.date = CURDATE() AND b.level = 'v' AND a.status_id = 1 AND c.user_status_id = 'US01' 
													ORDER BY b.PARTITION_COUNTER,b.PARTITION_ID ASC LIMIT 1")->row_array();
						$TX_RQS['RQS_VER_BY'] = $sql_p2['USER_ID'];
						$v = $TX_RQS['RQS_VER_BY'];
						if($sql_p2['USER_ID_2'] != ''){ // jika 1 orang memiliki akun verifikator dan asesor maka
							$TX_RQS['RQS_ASS_BY'] = $sql_p2['USER_ID_2'];
							$a = $TX_RQS['RQS_ASS_BY'];							
						}else{ // jika verifikator dan asesor beda orang maka
							$sql_p3 = $this->db->query("SELECT c.USER_ID,c.USER_ID_2 FROM tr_date a 
														LEFT JOIN ts_partition b ON a.day_name=b.partition_day 
														LEFT JOIN tm_user c ON b.user_id=c.user_id 
														WHERE a.date = CURDATE() AND b.level = 'a' AND a.status_id = 1 AND c.user_status_id = 'US01' 
														ORDER BY b.PARTITION_COUNTER,b.PARTITION_ID ASC LIMIT 1")->row_array();

							$TX_RQS['RQS_ASS_BY'] = $sql_p3['USER_ID'];							
							$a = $TX_RQS['RQS_ASS_BY'];							
						}
					}
					$sql_p3 = $this->db->query("SELECT day_name FROM tr_date WHERE date = CURDATE() ")->row_array();
					$hari = $sql_p3['day_name'];
					$this->db->query("UPDATE TS_PARTITION   SET PARTITION_COUNTER = PARTITION_COUNTER + 1 WHERE USER_ID = $v and level = 'v' and PARTITION_DAY = '$hari' ");
					$this->db->query("UPDATE TS_PARTITION   SET PARTITION_COUNTER = PARTITION_COUNTER + 1 WHERE USER_ID = $a and level = 'a' and PARTITION_DAY = '$hari' ");
*/					
            	}

					$sql_p3 = $this->db->query("SELECT day_name FROM tr_date WHERE date = CURDATE() ")->row_array();
					$hari = $sql_p3['day_name'] != "" ? $sql_p3['day_name'] : "Senin" ;

					$sql_p2 = $this->db->query("SELECT c.USER_ID,c.USER_ID_2 FROM tr_date a 
												LEFT JOIN ts_partition b ON a.day_name=b.partition_day 
												LEFT JOIN tm_user c ON b.user_id=c.user_id 
												WHERE a.date = CURDATE() AND b.level = 'v' AND a.status_id = 1 AND c.user_status_id = 'US01' 
												ORDER BY b.PARTITION_COUNTER,b.PARTITION_ID ASC LIMIT 1")->row_array();
					$TX_RQS['RQS_VER_BY'] = $sql_p2['USER_ID'];
					$v = $TX_RQS['RQS_VER_BY'];
					$this->db->query("UPDATE TS_PARTITION   SET PARTITION_COUNTER = PARTITION_COUNTER + 1 WHERE USER_ID = $v and level = 'v' and PARTITION_DAY = '$hari' ");

				$arrTlRqsLog['RQS_ID'] = $iId;
				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = '301';
				$this->db->insert('tl_rqs_log', $arrTlRqsLog);
	            $esp->main->get_bol_trans_check($iCounTol);
	            $message = "Pengajuan berhasil disubmit. Anda akan mendapatkan pemberitahuan di email Anda";

	            $subject = "Pengajuan Penggunaan BTP";
	            $to = $RQS_PIC_MAIL;
	            $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial; width:580px;"><tr><td>PENGAJUAN BERHASIL DISUBMIT<br><br>

	Pengajuan Penggunaan BTP akan dievaluasi terlebih dahulu. Notifikasi pemberitahuan hasil evaluasi akan dikirim via email. <br><br>
	Terima Kasih</td></tr></table>';
				$esp->main->sendMailLocal('register', $to, $isi, $subject);
				// $esp->main->send_mail('register', $to, $isi, $subject);

            }else if($TX_RQS['RQS_STATUS_ID'] == 'RS01'){
				$arrTlRqsLog['RQS_ID'] = $iId;
				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = '300';
				$this->db->insert('tl_rqs_log', $arrTlRqsLog);
	            $esp->main->get_bol_trans_check($iCounTol);
	            $message = "Draft Berhasil disimpan. Draft akan disimpan selama 3 hari, setelah itu akan dihapus secara otomatis.";
            }

			$TX_RQS['RQS_PROCESS_ID'] = 'PR02';
			$this->db->where('RQS_ID', $iId);
			$this->db->update('tx_rqs', $TX_RQS);
            $esp->main->get_bol_trans_check($iCounTol,FALSE);

			$this->db->where('RQS_ID', $iId);
			$this->db->delete('TX_RQS_BTP_COMP');
            $esp->main->get_bol_trans_check($iCounTol,FALSE);
			$this->insert_banyak('TX_RQS_BTP_COMP', $TX_RQS_BTP_COMP);			
            $esp->main->get_bol_trans_check($iCounTol);

			$this->db->where('RQS_ID', $iId);
			$this->db->delete('TX_RQS_FOOD_COMP');
            $esp->main->get_bol_trans_check($iCounTol,FALSE);
			$this->insert_banyak('TX_RQS_FOOD_COMP', $TX_RQS_FOOD_COMP);			
            $esp->main->get_bol_trans_check($iCounTol);

			$this->db->where('RQS_ID', $iId);
			$this->db->delete('TX_RQS_DOC');
            $esp->main->get_bol_trans_check($iCounTol,FALSE);
			$this->insert_banyak('TX_RQS_DOC', $TX_RQS_DOC);			

			$this->db->where('RQS_ID', $iId);
			$this->db->delete('TX_RQS_PRD_REG_DOC');
            $esp->main->get_bol_trans_check($iCounTol,FALSE);
			$this->insert_banyak('TX_RQS_PRD_REG_DOC', $TX_RQS_PRD_REG_DOC);			
            $esp->main->get_bol_trans_check($iCounTol);

			if($esp->main->get_bol_trans_statusV2($iCounTol)){ //tanpa trans_begin()
//				return "MSG#OK#Proses Berhasil#".site_url()."submission/new_rqs/frm-tech/".$iId;
//				return "MSG#OK#Proses Berhasil#".site_url()."submission/list_rqs/all";
//				return "MSG#OK#".$message."#".site_url()."submission/new_rqs/detail/".$iId;
				return "MSG#OK#".$message."#".site_url();
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}
		}else if($sMenu=="detail"){
            $iCounTol = 0;
			$RQS_NOTE = $this->input->post('RQS_NOTE');
			$RQS_LEVEL = $this->input->post('RQS_LEVEL');
			$TX_RQS3['RQS_LEVEL'] = $RQS_LEVEL;
			$TX_RQS3['RQS_NOTE'] = $RQS_NOTE;
			$TxRqsDoc = $this->input->post('TxRqsDoc');
			$TX_RQS_PRD_REG_DOC = $this->input->post('RQS_PRD_REG_DOC');
			$menu = $this->input->post('menu');
			if($menu == 'verify'){
				$TX_RQS3['RQS_PROCESS_ID'] = "PR03";
				$TX_RQS3['RQS_VER_DATE'] = $now;
				$TX_RQS3['RQS_VER_BY'] = $sUserId;				
				$sql_p2 = $this->db->query("SELECT b.USER_ID FROM  ts_partition b 
											LEFT JOIN tm_user c ON b.user_id=c.user_id 
											WHERE b.level = 'a' AND c.user_status_id = 'US01' 
											ORDER BY b.PARTITION_COUNTER,b.PARTITION_ID ASC LIMIT 1")->row_array();
				$TX_RQS3['RQS_ASS_BY'] = $sql_p2['USER_ID'];
				$a = $TX_RQS3['RQS_ASS_BY'];
				$this->db->query("UPDATE TS_PARTITION   SET PARTITION_COUNTER = PARTITION_COUNTER + 1 WHERE USER_ID = '$a' and level = 'a'  ");
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$this->db->where('RQS_ID', $iId);
				$this->db->update('tx_rqs', $TX_RQS3);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$arrTlRqsLog['RQS_ID'] = $iId;
				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_NOTE'] = $RQS_NOTE;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = '302';
				$this->db->insert('tl_rqs_log', $arrTlRqsLog);
	            $esp->main->get_bol_trans_check($iCounTol);

			}else if($menu == 'revisi'){
		    	if(!empty($TxRqsDoc)){
					$value = "(";
			    	foreach($TxRqsDoc as $datas) {
			    		$value .= "'" . $datas . "',";
			    	}
			    	$value = substr($value, 0, -1).")";

					$where = "RQS_DOC_ID in $value";
					$TX_RQS['RQS_DOC_STATUS_ID'] = "UPS07";
					$this->db->where($where);
					$this->db->update('TX_RQS_DOC', $TX_RQS);
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);
		    	}

		    	if(!empty($TX_RQS_PRD_REG_DOC)){
					$value2 = "(";
			    	foreach($TX_RQS_PRD_REG_DOC as $datas2) {
			    		$value2 .= "'" . $datas2 . "',";
			    	}
			    	$value2 = substr($value2, 0, -1).")";
					$where2 = "RQS_PRD_REG_ID in $value2";
					$TX_RQS2['RQS_PRD_REG_DOC_STATUS_ID'] = "UPS07";
					$this->db->where($where2);
					$this->db->update('tx_rqs_prd_reg_doc', $TX_RQS2);
		            $esp->main->get_bol_trans_check($iCounTol,FALSE);
		    	}
				$arrTlRqsLog['RQS_ID'] = $iId;
				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_NOTE'] = $RQS_NOTE;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = '302';
				$this->db->insert('tl_rqs_log', $arrTlRqsLog);
	            $esp->main->get_bol_trans_check($iCounTol);

				$TX_RQS3['RQS_PROCESS_ID'] = "PR05";
				$TX_RQS3['RQS_STATUS_ID'] = "RS04";
				$this->db->where('RQS_ID', $iId);
				$this->db->update('tx_rqs', $TX_RQS3);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

	            $subject = "Pengajuan Penggunaan BTP Perlu Perbaikan";
                $rqs = $this->db->query("select * from tx_rqs where rqs_id = $iId")->row_array();
	            $to = $rqs['RQS_PIC_MAIL'];
	            $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial; font-size:12px; font-weight: 700; width:580px;"><tr><td>Pengajuan Penggunaan BTP Perlu Perbaikan<br><br>

	Pengajuan penggunaan BTP perlu perbaikan. Batas waktu perbaikan 5 hari. Setelah 5 hari permohonan akan dibatalkan secara otomatis. Mohon perbaiki pengajuan Anda melalui aplikasi e-Standardisasi Pangan. <br><br>
	Terima Kasih</td></tr></table>';
				$esp->main->sendMailLocal('register', $to, $isi, $subject);
				// $esp->main->send_mail('register', $to, $isi, $subject);

			}else if($menu == 'setujui'){ //print_r($_POST); die('fsdkjfskd');
				$iId = $this->input->post('id');
				$sUserId = $this->newsession->userdata('USER_ID');

                $this->db->query("update tx_rqs set RQS_STATUS_ID = 'RS05', RQS_PROCESS_ID = 'PR17' WHERE RQS_ID = $iId ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);

				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = '311';
				$arrTlRqsLog['RQS_ID'] = $iId;
				$this->db->insert('tl_rqs_log', $arrTlRqsLog);

                $rqs = $this->db->query("select * from tx_rqs where rqs_id = $iId")->row_array();
                $subject = "Pengajuan Penggunaan BTP";
                $to = $rqs["RQS_PIC_MAIL"];
                $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial;  width:580px;"><tr><td>Pengajuan Penggunaan BTP Disetujui<br><br>
				Berkas Permohonan Nomor '.$rqs["RQS_NO_AJU"].' sudah selesai, surat jawaban harap diambil ke Direktorat Standardisasi Produk Pangan
				<br><br>
				Terima Kasih</td></tr></table>';
				$esp->main->sendMailLocal('register', $to, $isi, $subject);
                // $esp->main->send_mail('register', $to, $isi, $subject);

                return "MSG#OK#Proses Berhasil#" . site_url() . "assessment/list_assignment/ass";
			}else if($menu == 'tolak'){
				$iId = $this->input->post('id');
				$sUserId = $this->newsession->userdata('USER_ID');

                $this->db->query("update tx_rqs set RQS_STATUS_ID = 'RS06', RQS_PROCESS_ID = 'PR17' WHERE RQS_ID = $iId ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);

				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = '311';
				$arrTlRqsLog['RQS_ID'] = $iId;
				$this->db->insert('tl_rqs_log', $arrTlRqsLog);

                $rqs = $this->db->query("select * from tx_rqs where rqs_id = $iId")->row_array();
                $subject = "Pengajuan Penggunaan BTP";
                $to = $rqs["RQS_PIC_MAIL"];
                $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial;  width:580px;"><tr><td>Pengajuan Penggunaan BTP Ditolak<br><br>
		        Berkas Permohonan Nomor '.$rqs["RQS_NO_AJU"].' sudah selesai, surat jawaban harap diambil ke Direktorat Standardisasi Produk Pangan
		        <br><br>
		        Terima Kasih</td></tr></table>';
				$esp->main->sendMailLocal('register', $to, $isi, $subject);
                // $esp->main->send_mail('register', $to, $isi, $subject);

                return "MSG#OK#Proses Berhasil#" . site_url() . "assessment/list_assignment/ass";
			}else if($menu == 'cek_btp_kp'){
				die('cek_btp_kp');
			}

			if($esp->main->get_bol_trans_statusV2($iCounTol)){ //tanpa trans_begin()
				return "MSG#OK#Proses Berhasil.#".site_url()."submission/list_assignment/all";
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}
		}else if($sMenu=='cancel'){
            $iCounTol = 0;
			$TX_RQS3['RQS_STATUS_ID'] = "RS03";
			$this->db->where('RQS_ID', $iId);
			$this->db->update('tx_rqs', $TX_RQS3);
            $esp->main->get_bol_trans_check($iCounTol,FALSE);

			if($esp->main->get_bol_trans_statusV2($iCounTol)){ //tanpa trans_begin()
				return "MSG#OK#Permohonan Berhasil Dibatalkan.#".site_url()."home";
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}
		}
	}	

	public function insert_banyak($tabel="",$data="") {
    	//field
    	$temp = join(",",array_keys($data[1]));
    	$field = "(" . $temp . ")";
    	$sql = "INSERT INTO $tabel $field VALUES ";

    	//value
    	foreach($data as $datas) {
            $bersih = str_replace("'", "\'", $datas);
    		$test = join("','",$bersih);
    		$value .= "('" . $test . "'),";
    	}

    	$sql .= rtrim($value,",");
    	
    	$exec = $this->db->query($sql);
    	return $exec; //affected_row / jml row
    }

	function submission_get($sMenu, $iId){
		$esp = get_instance();
		$esp->load->model("main","main", true);
		$sUserId = $this->newsession->userdata('USER_ID');
        $ROLE_ID = $this->newsession->userdata('ROLE_ID');
		if($sMenu=="frm-adm"){

			$arrdata['DENOMINATION'] = $esp->main->get_arr_combobox("SELECT DENOMINATION_ID,DENOMINATION_NAME FROM TR_DENOMINATION   ORDER BY DENOMINATION_ID", "DENOMINATION_ID", "DENOMINATION_NAME", TRUE);
			$arrdata['USER_TYPE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_ID IN ('UTY01','UTY02')  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);

			$arrdata['noisn'] = $esp->main->get_arr_combobox("SELECT A.BTP_TYPE_ID AS ID, concat(A.BTP_TYPE_NO,' - ',A.BTP_TYPE_NAME) AS NO FROM TR_BTP_TYPE A","ID", "NO", TRUE);
			$arrdata['foodtype'] = $esp->main->get_arr_combobox("SELECT FOOD_TYPE_ID AS ID, FOOD_TYPE_NAME AS NAMA FROM TR_FOOD_TYPE","ID","NAMA", TRUE);
			if($iId==""){  
				$arrdata['arrTxRqs'] = $this->db->query("SELECT USER_ID, USER_PIC_NAME, USER_PIC_TELP, USER_PIC_FAX, USER_PIC_MAIL FROM tm_user_pic where USER_ID = '$sUserId'")->row_array();
				$arrdata['arrTxRqs']['RQS_PIC_NAME'] = $arrdata['arrTxRqs']['USER_PIC_NAME'];
				$arrdata['arrTxRqs']['RQS_PIC_TELP'] = $arrdata['arrTxRqs']['USER_PIC_TELP'];
				$arrdata['arrTxRqs']['RQS_PIC_FAX'] = $arrdata['arrTxRqs']['USER_PIC_FAX'];
				$arrdata['arrTxRqs']['RQS_PIC_MAIL'] = $arrdata['arrTxRqs']['USER_PIC_MAIL'];
				
				$arrdata['act'] = site_url('submission/post/adm/new');
			}else{
				$arrdata['arrTxRqs'] = $this->db->query("SELECT c.`FOOD_TYPE_NAME`,b.`BTP_TYPE_NAME`,b.`BTP_TYPE_NO`,a.* FROM tx_rqs a LEFT JOIN tr_btp_type b ON a.RQS_BTP_TYPE_ID = b.`BTP_TYPE_ID` LEFT JOIN tr_food_type c ON a.RQS_FOOD_TYPE_ID = c.`FOOD_TYPE_ID` WHERE a.`RQS_ID` = $iId ")->row_array();
				$arrdata['act'] = site_url('submission/post/adm/edit');
			}
		}else if($sMenu=="detail"){
            if ($ROLE_ID == "601") {
	            $arrdata['sTitle'] = 'SUBMISSION >> VIEW ASSIGNMENT >> DAFTAR ASSIGNMENT >> <span style="color: blue"> DETAIL ASSIGNMENT </span>';
	            $arrdata['back'] = "submission/list_assignment/all";
            } else if($ROLE_ID == "602" || $ROLE_ID == "604"){
	            $arrdata['sTitle'] = 'SUBMISSION >> ASSIGNMENT >> DAFTAR ASSIGNMENT >> <span style="color: blue"> DETAIL ASSIGNMENT </span>';
	            $arrdata['back'] = "submission/list_assignment/all";
            } else {
	            $arrdata['sTitle'] = 'BELUM DISET';
	            $arrdata['back'] = "assessment";
            }
			$arrdata['arrTxRqs'] = $this->db->query("SELECT m.TRADER_TYPE,l.USER_NAME as ASS_BY, k.USER_NAME as VER_BY ,g.USER_NAME,g.USER_ADDR,j.`STATUS_DETAIL` as 'STAT',i.`STATUS_DETAIL` as 'CERTIFICATION_STATUS',h.`STATUS_DETAIL` as 'JENIS_P',f.`STATUS_DETAIL`,c.`BTP_TYPE_NO`,d.`FOOD_TYPE_NAME`, d.`FOOD_TYPE_NO`,c.`BTP_TYPE_NAME`,b.`BTP_FUNCTION_NAME`,a.* FROM tx_rqs a 
LEFT JOIN tr_btp_function b ON a.`RQS_BTP_FUNCTION_ID` = b.`BTP_FUNCTION_ID` 
LEFT JOIN tr_btp_type c ON a.`RQS_BTP_TYPE_ID`=c.`BTP_TYPE_ID`
LEFT JOIN TR_FOOD_TYPE d ON a.`RQS_FOOD_TYPE_ID` = d.`FOOD_TYPE_ID` 
LEFT JOIN TM_USER g ON a.`USER_ID` = G.`USER_ID` 
LEFT JOIN TR_STATUS f ON G.`USER_TYPE_ID` = f.`STATUS_ID` 
LEFT JOIN TR_STATUS h ON a.`RQS_FAC_TYPE_ID` = h.`STATUS_ID` 
LEFT JOIN TR_STATUS i ON a.`RQS_CTF_STATUS` = i.`STATUS_ID` 
LEFT JOIN TR_STATUS j ON a.`RQS_STATUS_ID` = j.`STATUS_ID` 
LEFT JOIN tm_user k ON a.`RQS_VER_BY`=k.`USER_ID` 
LEFT JOIN tm_user l ON a.`RQS_ASS_BY`=l.`USER_ID` 
LEFT JOIN tm_user_com m ON a.`USER_ID`=m.`USER_ID` 
WHERE a.`RQS_ID` = $iId ")->row_array();

			if($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY01'){
				$arrdata['arrTxRqsDln'] = $this->db->query("SELECT a.* FROM tx_rqs_dln a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY02'){
				$arrdata['arrTxRqsPckg'] = $this->db->query("SELECT a.* FROM tx_rqs_pckg a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY03'){
				$arrdata['arrTxRqsLcns'] = $this->db->query("SELECT a.* FROM tx_rqs_lcns a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY04'){
				$arrdata['arrTxRqsImpr'] = $this->db->query("SELECT a.* FROM tx_rqs_impr a WHERE a.`RQS_ID` = $iId ")->row_array();
			}

			$arrdata['arrTxRqsBtpComp'] = $this->db->query("SELECT a.* FROM TX_RQS_BTP_COMP a WHERE a.`RQS_ID` = $iId and a.rqs_btp_comp_status_id = 'COS1' ")->result_array();
			$arrdata['arrTxRqsFoodComp'] = $this->db->query("SELECT a.* FROM TX_RQS_FOOD_COMP a WHERE a.`RQS_ID` = $iId and a.rqs_food_comp_status_id = 'COS1' ")->result_array();
			$arrdata['arrTxRqsPrdRegDoc'] = $this->db->query("SELECT d.`STATUS_DETAIL`,a.* FROM tx_rqs_prd_reg_doc a left join tr_status d on a.RQS_PRD_REG_DOC_STATUS_ID=d.STATUS_ID WHERE a.`RQS_ID` = $iId and a.rqs_prd_reg_doc_status_id = 'UPS02' ")->result_array();
			
			$arrdata['arrTxRqsDoc05'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='05'")->row_array();
			$arrdata['arrTxRqsDoc06'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='06'")->row_array();
			$arrdata['arrTxRqsDoc07'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='07'")->row_array();
			$arrdata['arrTxRqsDoc08'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='08'")->row_array();
			$arrdata['arrTxRqsDoc09'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='09'")->row_array();
			$arrdata['arrTxRqsDoc11'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='11'")->row_array();

		    $countlog = "SELECT RQS_LOG_DESC_ID FROM tl_rqs_log WHERE RQS_ID  = '$iId' ";
		    $arrdata['jumlahlog'] = $this->db->query($countlog)->num_rows();

		}else if($sMenu=="frm-btp"){
			$arrdata['DENOMINATION'] = $esp->main->get_arr_combobox("SELECT DENOMINATION_ID,DENOMINATION_NAME FROM TR_DENOMINATION   ORDER BY DENOMINATION_ID", "DENOMINATION_ID", "DENOMINATION_NAME", TRUE);
			$arrdata['FACTORY_TYPE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'FACTORY_TYPE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);

			$arrdata['arrTxRqs'] = $this->db->query("SELECT b.`BTP_TYPE_NAME`,a.* FROM tx_rqs a LEFT JOIN TR_BTP_TYPE b ON a.RQS_BTP_TYPE_ID = b.`BTP_TYPE_ID` WHERE a.`RQS_ID` = $iId ")->row_array();

			if($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY01'){
				$arrdata['arrTxRqsDln'] = $this->db->query("SELECT a.* FROM tx_rqs_dln a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY02'){
				$arrdata['arrTxRqsPckg'] = $this->db->query("SELECT a.* FROM tx_rqs_pckg a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY03'){
				$arrdata['arrTxRqsLcns'] = $this->db->query("SELECT a.* FROM tx_rqs_lcns a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY04'){
				$arrdata['arrTxRqsImpr'] = $this->db->query("SELECT a.* FROM tx_rqs_impr a WHERE a.`RQS_ID` = $iId ")->row_array();
			}

			$arrdata['act'] = site_url('submission/post/btp/edit');
		}else if($sMenu=="frm-tech"){
			$arrdata['arrTxRqs'] = $this->db->query("SELECT a.*,b.`BTP_FUNCTION_NAME`,c.BTP_TYPE_NAME,c.BTP_TYPE_NO FROM tx_rqs a LEFT JOIN tr_btp_function b ON a.RQS_BTP_FUNCTION_ID = b.`BTP_FUNCTION_ID` LEFT JOIN tr_btp_type c ON a.RQS_BTP_TYPE_ID = c.`BTP_TYPE_ID` WHERE a.`RQS_ID` = $iId ")->row_array(); //, 
			$btp_id = $arrdata['arrTxRqs']['RQS_BTP_TYPE_ID'];
			$arrdata['arrTxRqsBtpComp'] = $this->db->query("SELECT a.* FROM TX_RQS_BTP_COMP a WHERE a.`RQS_ID` = $iId and a.rqs_btp_comp_status_id = 'COS1' ")->result_array();
			$arrdata['arrTxRqsFoodComp'] = $this->db->query("SELECT a.* FROM TX_RQS_FOOD_COMP a WHERE a.`RQS_ID` = $iId and a.rqs_food_comp_status_id = 'COS1' ")->result_array();
			$arrdata['arrTxRqsPrdRegDoc'] = $this->db->query("SELECT a.* FROM tx_rqs_prd_reg_doc a WHERE a.`RQS_ID` = $iId and a.rqs_prd_reg_doc_status_id = 'UPS02' ")->result_array();
			
			$arrdata['arrTxRqsDoc05'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='05'")->row_array();
			$arrdata['arrTxRqsDoc06'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='06'")->row_array();
			$arrdata['arrTxRqsDoc07'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='07'")->row_array();
			$arrdata['arrTxRqsDoc08'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='08'")->row_array();
			$arrdata['arrTxRqsDoc09'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='09'")->row_array();
			$arrdata['arrTxRqsDoc11'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='11'")->row_array();

			$arrdata['DENOMINATION'] = $esp->main->get_arr_combobox("SELECT DENOMINATION_ID,DENOMINATION_NAME FROM TR_DENOMINATION   ORDER BY DENOMINATION_ID", "DENOMINATION_ID", "DENOMINATION_NAME", TRUE);
			$arrdata['FACTORY_TYPE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'FACTORY_TYPE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
			$arrdata['RQS_CTF_STATUS'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'CERTIFICATION_STATUS'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
			$arrdata['RQS_BTP_FUNCTION_ID'] = $esp->main->get_arr_combobox("SELECT b.BTP_FUNCTION_ID,b.BTP_FUNCTION_NAME FROM  TR_BTP_FUNCTION b INNER JOIN tr_permenkes a ON a.`BTP_FUNCTION_ID`=b.`BTP_FUNCTION_ID` WHERE a.`BTP_TYPE_ID`='$btp_id' ORDER BY b.BTP_FUNCTION_NAME", "BTP_FUNCTION_ID", "BTP_FUNCTION_NAME", TRUE);

			$arrdata['act'] = site_url('submission/post/tech/edit');
		}else if($sMenu=="list"){
			$sTitle = "Daftar Pemohon Terdaftar";
			$sStatus_id = "TR00";
		}
		return $arrdata;
	}

}
?>