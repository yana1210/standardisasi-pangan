<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//error_reporting(E_ERROR);
class Master_act extends Model{

    function get_list_user($sType) {
        // $ROLE_ID = $this->newsession->userdata('ROLE_ID');
        // $USER_ID = $this->newsession->userdata('USER_ID');
        $sTitle = '<span style="color: blue"> DAFTAR USER </span>';
        $sQuery = "SELECT A.USER_ID, A.USER_USERNAME AS 'USERNAME', F.ROLE_NAME AS 'ROLE', B.STATUS_DETAIL AS 'TIPE USER', A.USER_NAME AS 'NAMA USER',
                    A.USER_MAIL AS 'EMAIL', D.STATUS_DETAIL AS 'STATUS'

                    FROM tm_user A

                    LEFT JOIN tr_status B ON A.USER_TYPE_ID = B.STATUS_ID
                    LEFT JOIN tm_region C ON A.REGION_ID = C.REGION_ID
                    LEFT JOIN tr_status D ON A.USER_STATUS_ID = D.STATUS_ID  
                    LEFT JOIN ts_user_role E ON A.USER_ID = E.USER_ID 
                     LEFT JOIN tr_role F ON E.ROLE_ID = F.ROLE_ID WHERE A.USER_STATUS_ID NOT IN ('US02')"; //print_r($sQuery);die();
            
                    //$sWhere";
        //$prosesnya = array('Lihat Detail' => array('GET', site_url() . 'master/new_user/frm-user', '1'));
        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_user/frm-user', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_user/frm-user', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-user/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('USER_ID'));

        $this->newtable->search(array(array('USER_USERNAME', 'USERNAME'), array('STATUS_DETAIL', 'TIPE USER'), array('USER_NAME', 'NAMA USER')));

        $this->newtable->action(site_url() . "master/list_user/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("USER_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_pegawai($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR PEGAWAI </span>';
        $sQuery = "SELECT A.USER_ID, A.USER_USERNAME AS 'USERNAME', F.ROLE_NAME AS 'ROLE', B.STATUS_DETAIL AS 'TIPE USER', A.USER_NAME AS 'NAMA USER',
                    A.USER_MAIL AS 'EMAIL', D.STATUS_DETAIL AS 'STATUS'

                    FROM tm_user A

                    LEFT JOIN tr_status B ON A.USER_TYPE_ID = B.STATUS_ID
                    LEFT JOIN tm_region C ON A.REGION_ID = C.REGION_ID
                    LEFT JOIN tr_status D ON A.USER_STATUS_ID = D.STATUS_ID  
                    LEFT JOIN ts_user_role E ON A.USER_ID = E.USER_ID 
                     LEFT JOIN tr_role F ON E.ROLE_ID = F.ROLE_ID 
                    WHERE A.USER_TYPE_ID = 'UTY03' "; //AND A.USER_STATUS_ID NOT IN ('US02')
            
                    //$sWhere";
        //$prosesnya = array('Lihat Detail' => array('GET', site_url() . 'master/new_user/frm-user', '1'));
        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_pegawai/frm-pegawai', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_pegawai/frm-pegawai', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-pegawai/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('USER_ID'));

        $this->newtable->search(array(array('USER_USERNAME', 'USERNAME'), array('STATUS_DETAIL', 'TIPE USER'), array('USER_NAME', 'NAMA USER')));

        $this->newtable->action(site_url() . "master/list_pegawai/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("USER_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_pemohon($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR PEMOHON </span>';
        $sQuery = "SELECT A.USER_ID, A.USER_USERNAME AS 'USERNAME', B.STATUS_DETAIL AS 'TIPE USER', A.USER_NAME AS 'NAMA USER',
                    A.USER_MAIL AS 'EMAIL', D.STATUS_DETAIL AS 'STATUS'

                    FROM tm_user A

                    LEFT JOIN tr_status B ON A.USER_TYPE_ID = B.STATUS_ID
                    LEFT JOIN tm_region C ON A.REGION_ID = C.REGION_ID
                    LEFT JOIN tr_status D ON A.USER_STATUS_ID = D.STATUS_ID WHERE A.USER_TYPE_ID IN ('UTY01', 'UTY02')";// AND A.USER_STATUS_ID NOT IN ('US02')
            
                    //$sWhere";
        //$prosesnya = array('Lihat Detail' => array('GET', site_url() . 'master/new_user/frm-user', '1'));
        $prosesnya = array(
                           'Edit' => array('GET', site_url().'master/new_pemohon/frm-pemohon', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-pemohon/delete', 'N'));
//'Tambah' => array('GET', site_url().'master/new_pemohon/frm-pemohon', '0', 'home'),
        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('USER_ID'));

        $this->newtable->search(array(array('USER_USERNAME', 'USERNAME'), array('STATUS_DETAIL', 'TIPE USER'), array('USER_NAME', 'NAMA USER')));

        $this->newtable->action(site_url() . "master/list_pemohon/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("USER_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_btp($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR BTP </span>';
        $sQuery = "SELECT BTP_TYPE_ID, BTP_TYPE_NO AS 'NO. INS', BTP_TYPE_NAME AS 'NAMA JENIS BTP' FROM tr_btp_type";
            
                    //$sWhere";
        //$prosesnya = array('Lihat Detail' => array('GET', site_url() . 'master/new_user/frm-user', '1'));
        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_btp/frm-btp', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_btp/frm-btp', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-btp/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('BTP_TYPE_ID'));

        $this->newtable->search(array(array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'NAMA JENIS BTP')));

        $this->newtable->action(site_url() . "master/list_btp/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("BTP_TYPE_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_btp_function($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR FUNGSI BTP </span>';
        $sQuery = "SELECT BTP_FUNCTION_ID, BTP_FUNCTION_NAME AS 'FUNGSI BTP', BTP_FUNCTION_NOMOR_PERATURAN AS 'NOMOR PERATURAN', BTP_FUNCTION_YEAR AS 'TAHUN' FROM tr_btp_function";
            
                    //$sWhere";
        //$prosesnya = array('Lihat Detail' => array('GET', site_url() . 'master/new_user/frm-user', '1'));
        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_btp/frm-btp-function', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_btp/frm-btp-function', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-btp-function/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('BTP_FUNCTION_ID'));

        $this->newtable->search(array(array('BTP_FUNCTION_NAME', 'FUNGSI BTP'), array('BTP_FUNCTION_NOMOR_PERATURAN', 'NOMOR PERATURAN'), array('BTP_FUNCTION_YEAR', 'TAHUN')));

        $this->newtable->action(site_url() . "master/list_btp_function/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("BTP_FUNCTION_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_food_type($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR JENIS PANGAN </span>';
        $sQuery = "SELECT FOOD_TYPE_ID, FOOD_TYPE_NO AS 'NO JENIS PANGAN', FOOD_TYPE_NAME AS 'NAMA JENIS PANGAN' FROM tr_food_type";
            
                    //$sWhere";
        //$prosesnya = array('Lihat Detail' => array('GET', site_url() . 'master/new_user/frm-user', '1'));
        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_btp/frm-food-type', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_btp/frm-food-type', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-food-type/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('FOOD_TYPE_ID'));

        $this->newtable->search(array(array('FOOD_TYPE_NAME', 'NAMA JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO JENIS PANGAN')));

        $this->newtable->action(site_url() . "master/list_food_type/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("FOOD_TYPE_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_piket($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR PIKET </span>';
        $sQuery = "SELECT a.PARTITION_ID, b.USER_NAME AS NAMA, a.PARTITION_DAY AS HARI, CASE
    WHEN a.LEVEL = 'v' THEN 'verifikator'
    WHEN a.LEVEL = 'a' THEN 'assesor'
    ELSE '-' 
    END AS LEVEL 
    FROM ts_partition a 
        LEFT JOIN tm_user b ON a.USER_ID = b.USER_ID";
            
                    //$sWhere";
        //$prosesnya = array('Lihat Detail' => array('GET', site_url() . 'master/new_user/frm-user', '1'));
        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_btp/frm-piket', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_btp/frm-piket', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-piket/delete', 'N'));

        $this->load->library('newtable');
        $this->newtable->hiddens(array('PARTITION_ID'));

        $this->newtable->search(array(array('USER_NAME', 'NAMA'), array('PARTITION_DAY', 'HARI')));

        $this->newtable->action(site_url() . "master/list_piket/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("PARTITION_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable);
        return $arrData;
     
    }

    function get_list_hari_kerja($sType) {
        $sTitle = '<span style="color: blue"> DAFTAR HARI KERJA </span>';
        $sQuery = "SELECT a.DATE, a.DAY_NAME AS HARI, CASE
    WHEN a.STATUS_ID = 1 THEN 'hari kerja'
    WHEN a.STATUS_ID = 0 THEN 'hari libur'
    ELSE '-' 
    END AS STATUS 
    FROM tr_date a ";
            
                    //$sWhere";
        //$prosesnya = array('Lihat Detail' => array('GET', site_url() . 'master/new_user/frm-user', '1'));
        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_btp/frm-hari-kerja', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_btp/frm-hari-kerja', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-hari-kerja/delete', 'N'));

        $this->load->library('newtable');
        $this->newtable->hiddens(array('PARTITION_ID'));

        $this->newtable->search(array(array('DATE', 'TANGGAL'), array('DAY_NAME', 'HARI')));

        $this->newtable->action(site_url() . "master/list_hari_kerja/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("DESC");
        $this->newtable->keys("DATE");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable);
        return $arrData;
     
    }

    function get_list_permenkes($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR PERMENKES </span>';
        $sQuery = "SELECT A.PERMENKES_ID, B.BTP_TYPE_NO AS 'NO. INS', B.BTP_TYPE_NAME AS 'NAMA JENIS BTP',  CONCAT(C.BTP_FUNCTION_ID,' - ',C.BTP_FUNCTION_NAME) AS 'FUNGSI BTP'
                    FROM tr_permenkes A
                    LEFT JOIN tr_btp_type B ON A.BTP_TYPE_ID = B.BTP_TYPE_ID
                    LEFT JOIN tr_btp_function C ON A.BTP_FUNCTION_ID = C.BTP_FUNCTION_ID";
            
        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_permenkes/frm-permenkes', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_permenkes/frm-permenkes', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-permenkes/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('PERMENKES_ID'));

        $this->newtable->search(array(array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'NAMA JENIS BTP'), array('BTP_FUNCTION_NAME', 'FUNGSI BTP')));

        $this->newtable->action(site_url() . "master/list_permenkes/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("PERMENKES_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_codex1($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR TABEL CODEX ONE </span>';
        $sQuery = "SELECT A.CODEX_ONE_ID, B.BTP_TYPE_NO AS 'NO. INS',  B.BTP_TYPE_NAME AS 'NAMA JENIS BTP',  CONCAT(C.FOOD_TYPE_NO,' - ',C.FOOD_TYPE_NAME) AS 'NO. KATEGORI PANGAN/JENIS PANGAN', A.MAX_LIMIT AS 'BATAS MAKSIMAL', A.UNIT,  A.CODEX_ONE_NOTE_ID AS 'NOTE'
                    FROM tr_codex_one A
                    LEFT JOIN tr_btp_type B ON A.BTP_TYPE_ID = B.BTP_TYPE_ID
                    LEFT JOIN tr_food_type C ON A.FOOD_TYPE_ID = C.FOOD_TYPE_ID";
            
        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_codex1/frm-codex1', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_codex1/frm-codex1', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-codex1/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('CODEX_ONE_ID'));

        $this->newtable->search(array(array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'NAMA JENIS BTP'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('FOOD_TYPE_NAME', 'JENIS PANGAN')));

        $this->newtable->action(site_url() . "master/list_codex1/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("CODEX_ONE_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_codex3($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR TABEL CODEX THREE </span>';
        $sQuery = "SELECT A.CODEX_THREE_ID, B.BTP_TYPE_NO AS 'NO. INS',  B.BTP_TYPE_NAME AS 'NAMA JENIS BTP', A.BTP_FUNCTION_NAME AS 'FUNGSI BTP', A.CODEX_YEAR AS 'TAHUN'
                    FROM tr_codex_three A
                    LEFT JOIN tr_btp_type B ON A.BTP_TYPE_ID = B.BTP_TYPE_ID";

        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_codex3/frm-codex3', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_codex3/frm-codex3', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-codex3/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('CODEX_THREE_ID'));

        $this->newtable->search(array(array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'NAMA JENIS BTP')));

        $this->newtable->action(site_url() . "master/list_codex3/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("CODEX_THREE_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_annex3($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR PENGECUALIAN CODEX THREE </span>';

        $sQuery = "SELECT A.CODEX_ANNEX_THREE_ID, B.FOOD_TYPE_NO AS 'NO KATEGORI PANGAN', B.FOOD_TYPE_NAME AS 'JENIS PANGAN'
                    FROM tr_codex_annex_three A
                    LEFT JOIN tr_food_type B ON A.FOOD_TYPE_ID = B.FOOD_TYPE_ID";

        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_annex3/frm-annex3', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_annex3/frm-annex3', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-annex3/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('CODEX_ANNEX_THREE_ID'));

        $this->newtable->search(array(array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('FOOD_TYPE_NAME', 'JENIS PANGAN')));

        $this->newtable->action(site_url() . "master/list_annex3/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("CODEX_ANNEX_THREE_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_perka($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR PERKA & IZIN KHUSUS </span>';

        $sQuery = "SELECT A.PERKAJINSUS_ID, B.BTP_TYPE_NO AS 'NO. INS', B.BTP_TYPE_NAME AS 'NAMA JENIS BTP', CONCAT(C.FOOD_TYPE_NO,' - ',C.FOOD_TYPE_NAME) AS 'NO. KATEGORI PANGAN/JENIS PANGAN', A.MAX_LIMIT 'BATAS MAKSIMAL', A.UNIT, A.NOTE AS 'CATATAN'  
                    FROM tr_perkajinsus A
                    LEFT JOIN tr_btp_type B ON A.BTP_TYPE_ID = B.BTP_TYPE_ID
			        LEFT JOIN tr_food_type C ON A.FOOD_TYPE_ID = C.FOOD_TYPE_ID";

        $prosesnya = array('Tambah' => array('GET', site_url().'master/new_perka/frm-perka', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_perka/frm-perka', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-perka/delete', 'N'));

        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        $this->load->library('newtable');
        $this->newtable->hiddens(array('PERKAJINSUS_ID'));

        $this->newtable->search(array(array('BTP_TYPE_NO', 'NO INS'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN')));

        $this->newtable->action(site_url() . "master/list_perka/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("PERKAJINSUS_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_our_contact($sType) {

        $sTitle = '<span style="color: blue"> DAFTAR KONTAK KAMI </span>';

        $sQuery = "SELECT a.QUESTION_ID,a.NAME AS NAMA, a.EMAIL AS EMAIL, a.QUESTION AS PERTANYAAN, a.CREATE_DATE AS TANGGAL FROM tm_our_contact a";

        $prosesnya = array('');
        /*$prosesnya = array('Tambah' => array('GET', site_url().'master/new_perka/frm-perka', '0', 'home'),
                           'Edit' => array('GET', site_url().'master/new_perka/frm-perka', '1'),
                           'Hapus' => array('POST', site_url().'master/post/frm-perka/delete', 'N'));
        $arrProcess = array('TR00' => "Setujui",
            'TR04' => "Perlu Perbaikan",
            'TR03' => "Tolak");
        */
        $this->load->library('newtable');
        $this->newtable->hiddens(array('QUESTION_ID'));

        $this->newtable->search(array(array('NAME', 'NAMA'), array('EMAIL', 'EMAIL'), array('QUESTION', 'PERTANYAAN')));

        $this->newtable->action(site_url() . "master/list_our_contact/" . $sType);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("QUESTION_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        return $arrData;
     
    }

    function get_list_request($sType) {

            $ROLE_ID = $this->newsession->userdata('ROLE_ID');
            $USER_ID = $this->newsession->userdata('USER_ID');
            if ($ROLE_ID == "601") {
                $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02') and a.`USER_ID` = '$USER_ID' ";
                $sTitle = 'SUBMISSION >> VIEW ASSIGNMENT >> <span style="color: blue"> DAFTAR ASSIGNMENT </span>';
            } else if($ROLE_ID == "602"){
                $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02')  AND a.`RQS_PROCESS_ID` in ('PR02','PR03','PR04','PR05')";
                $sTitle = 'SUBMISSION >> ASSIGNMENT >> <span style="color: blue"> DAFTAR ASSIGNMENT </span>';
            } else if($ROLE_ID == "603"){
                $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02') AND a.`RQS_PROCESS_ID` in ('PR03','PR06')";
                $sTitle = 'ASSESSMENT >> ASSIGNMENT >> <span style="color: blue"> DAFTAR ASSIGNMENT </span>';
            } else {
                $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02')";
                $sTitle = 'BELUM DISET';
            }
            if($sType == 'ass'){ // menu assessment di assesor
                $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/assignment/detail/',a.`RQS_ID`,'\">',a.`RQS_VER_DATE`,'</B>','</a>','<div>','</div>') AS 'TANGGAL VERIFIKASI', a.RQS_NO_AJU as 'NO AJU', f.USER_NAME as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',c.`BTP_TYPE_NAME` AS 'JENIS BTP', d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN', d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', a.`RQS_BTP_MERK` AS 'MERK DAGANG', i.`BTP_FUNCTION_NAME` AS 'GOLONGAN BTP', h.`USER_NAME` AS 'VERIFIED BY' FROM tx_rqs a 
LEFT JOIN tr_btp_type_no b ON a.`RQS_BTP_TYPE_NO_ID`=b.`BTP_TYPE_NO_ID`
LEFT JOIN tr_btp_type_name c ON a.`RQS_BTP_TYPE_NAME_ID`=c.`BTP_TYPE_NAME_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN tm_user h ON a.`RQS_VER_BY`=h.`USER_ID` 
LEFT JOIN tr_btp_function i ON a.`RQS_BTP_FUNCTION_ID`=i.`BTP_FUNCTION_ID`
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere";
                $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/assignment/detail', '1'));
                $search = array(array('RQS_VER_DATE', 'TANGGAL VERIFIKASI'),array('RQS_NO_AJU', 'NO AJU'), array('f.USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('BTP_FUNCTION_NAME', 'GOLONGAN BTP'), array('h.USER_NAME', 'VERIFIED BY') );
                $action = site_url() . "assessment/list_assignment/" . $sType;

            }else{ // menu submission di verifikator
                $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:8px\" href= \"" . site_url() . "submission/assignment/detail/',a.`RQS_ID`,'\">',a.`RQS_CREATE_DATE`,'</B>','</a>','<div>','</div>') AS 'TANGGAL MASUK', f.USER_NAME as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',c.`BTP_TYPE_NAME` AS 'JENIS BTP', d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN', d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', a.`RQS_BTP_MERK` AS 'MERK DAGANG', a.`RQS_PIC_NAME` AS 'PJ', h.`STATUS_DETAIL` AS 'JENIS PERUSAHAAN' FROM tx_rqs a 
LEFT JOIN tr_btp_type_no b ON a.`RQS_BTP_TYPE_NO_ID`=b.`BTP_TYPE_NO_ID`
LEFT JOIN tr_btp_type_name c ON a.`RQS_BTP_TYPE_NAME_ID`=c.`BTP_TYPE_NAME_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` 
LEFT JOIN tr_status h ON g.`USER_COM_SCALE_ID`=h.`STATUS_ID`  $sWhere";
                $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'submission/assignment/detail', '1'));
                $search = array(array('RQS_CREATE_DATE', 'TANGGAL MASUK'), array('USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('RQS_PIC_NAME', 'PENANGGUNG JAWAB'), array('STATUS_DETAIL', 'JENIS PERUSAHAAN') );
                $action = site_url() . "submission/list_assignment/" . $sType;
            }


            $arrProcess = array('TR00' => "Setujui",
                'TR04' => "Perlu Perbaikan",
                'TR03' => "Tolak");
            $this->load->library('newtable');
            $this->newtable->hiddens(array('RQS_ID'));

            $this->newtable->search($search);

            $this->newtable->action($action);
            $this->newtable->cidb($this->db);
            $this->newtable->ciuri($this->uri->segment_array());
            //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
            $this->newtable->orderby(1);
            $this->newtable->sortby("ASC");
            $this->newtable->keys("RQS_ID");
            $this->newtable->menu($prosesnya);
            $this->newtable->use_ajax(TRUE);
            $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
            $sTable = $this->newtable->generate($sQuery);
            $arrData = array("sTitle" => $sTitle,
                "sTable" => $sTable,
                "arrProcess" => $arrProcess);
            //print_r($arrData);die();
            return $arrData;
        /*} else {
            redirect(base_url());
            exit();
        }*/
    }


    function set_master($sMenu="", $sSubmenu="", $iIdsalah=""){ 
        $esp = get_instance();
        $esp->load->model("main","main", true);
        $iId = $this->input->post('id');
        $now = date('Y-m-d H:i:s');

        if($sMenu=='frm-user'){

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrTmUser = $this->input->post('REG');
            $arrTmUserCom = $this->input->post('REGCOM');
            $arrTmUserPrsn = $this->input->post('REGPRSN');

            if($sSubmenu == 'new'){
                $this->db->insert('tm_user', $arrTmUser);
                $iUserId = $this->db->insert_id();

                if($arrTmUserCom['USER_COM_STATUS_ID'] != ''){
                    $arrTmUserCom['USER_ID'] = $iUserId;
                    $this->db->insert('tm_user_com', $arrTmUserCom);
                    $esp->main->get_bol_trans_check($iCounTol);
                }
                if($arrTmUserPrsn['USER_PRSN_IDTYPE'] != ''){
                    $arrTmUserPrsn['USER_ID'] = $iUserId;
                    $this->db->insert('tm_user_prsn', $arrTmUserPrsn);
                    $esp->main->get_bol_trans_check($iCounTol);
                }
                if($arrTmUserRole['ROLE_ID'] == ''){
                    $arrTmUserRole['USER_ID'] = $iUserId;
                    $arrTmUserRole['USER_ROLE_STATUS'] = '1';
                    $this->db->insert('ts_user_role', $arrTmUserRole);
                    $esp->main->get_bol_trans_check($iCounTol);
                }

            }else if($sSubmenu == 'edit'){ 
                $arrTmUser['USER_ADDR'] = trim($arrTmUser['USER_ADDR']);
                $this->db->where('USER_ID', $iId);
                $this->db->update('tm_user', $arrTmUser);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                $this->db->where('USER_ID', $iId);
                $this->db->delete('tm_user_com');
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                $this->db->where('USER_ID', $iId);
                $this->db->delete('tm_user_prsn');
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                $this->db->where('USER_ID', $iId);
                $this->db->delete('ts_user_role');
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                if($arrTmUserCom['USER_COM_STATUS_ID'] != ''){
                    $arrTmUserCom['USER_ID'] = $iId;
                    $this->db->insert('tm_user_com', $arrTmUserCom);
                    $esp->main->get_bol_trans_check($iCounTol);
                }
                if($arrTmUserPrsn['USER_PRSN_IDTYPE'] != ''){
                    $arrTmUserPrsn['USER_ID'] = $iId;
                    $this->db->insert('tm_user_prsn', $arrTmUserPrsn);
                    //print_r($_POST);die('ooo');
                    $esp->main->get_bol_trans_check($iCounTol);
                }
                if($arrTmUserRole['ROLE_ID'] != ''){ 
                    $arrTmUserRole['USER_ID'] = $iId;
                    $arrTmUserRole['USER_ROLE_STATUS'] = '1';
                    $this->db->insert('ts_user_role', $arrTmUserRole);
                    $esp->main->get_bol_trans_check($iCounTol);
                }

                $iUserId = $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("update tm_user set USER_STATUS_ID = 'US02' WHERE USER_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_user/mst-user";

            }             

            if($esp->main->get_bol_trans_status($iCounTol)){
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_user/mst-user";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }


        }if($sMenu=='frm-pegawai'){ //print_r($sMenu.'-'.$sSubmenu);die('--'.'oke');

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrTmUser = $this->input->post('USER');
            //$arrTmUserCom = $this->input->post('REGCOM');
            $arrTmUserPrsn = $this->input->post('USERPRSN');
            $arrTmUserRole = $this->input->post('USEROLE');
            
            

            if($sSubmenu == 'new'){

                $arrTmUser['USER_TYPE_ID'] = 'UTY03';
                $this->db->insert('tm_user', $arrTmUser);
                $iUserId = $this->db->insert_id();

                // if($arrTmUserCom['USER_COM_STATUS_ID'] != ''){
                //     $arrTmUserCom['USER_ID'] = $iUserId;
                //     $this->db->insert('tm_user_com', $arrTmUserCom);
                //     $esp->main->get_bol_trans_check($iCounTol);
                // }

                if($arrTmUserPrsn['USER_PRSN_IDTYPE'] != ''){
                    $arrTmUserPrsn['USER_ID'] = $iUserId;
                    $this->db->insert('tm_user_prsn', $arrTmUserPrsn);
                    $esp->main->get_bol_trans_check($iCounTol);
                }

                if($arrTmUserRole['ROLE_ID'] == ''){
                    $arrTmUserRole['USER_ID'] = $iUserId;
                    $arrTmUserRole['USER_ROLE_STATUS'] = '1';
                    $this->db->insert('ts_user_role', $arrTmUserRole);
                    $esp->main->get_bol_trans_check($iCounTol);
                }

            }else if($sSubmenu == 'edit'){ 
                $arrTmUser['USER_ADDR'] = trim($arrTmUser['USER_ADDR']);
                $this->db->where('USER_ID', $iId);
                $this->db->update('tm_user', $arrTmUser);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                // $this->db->where('USER_ID', $iId);
                // $this->db->delete('tm_user_com');
                // $esp->main->get_bol_trans_check($iCounTol,FALSE);

                $this->db->where('USER_ID', $iId);
                $this->db->delete('tm_user_prsn');
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                $this->db->where('USER_ID', $iId);
                $this->db->delete('ts_user_role');
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                // $this->db->where('USER_ID', $iId);
                // $this->db->delete('ts_user_role');
                // $esp->main->get_bol_trans_check($iCounTol,FALSE);

                // if($arrTmUserCom['USER_COM_STATUS_ID'] != ''){
                //     $arrTmUserCom['USER_ID'] = $iId;
                //     $this->db->insert('tm_user_com', $arrTmUserCom);
                //     $esp->main->get_bol_trans_check($iCounTol);
                // }
                if($arrTmUserPrsn['USER_PRSN_IDTYPE'] != ''){
                    $arrTmUserPrsn['USER_ID'] = $iId;
                    $this->db->insert('tm_user_prsn', $arrTmUserPrsn);
                    $esp->main->get_bol_trans_check($iCounTol);
                }

               if($arrTmUserRole['ROLE_ID'] != ''){ 
                    $arrTmUserRole['USER_ID'] = $iId;
                    $arrTmUserRole['USER_ROLE_STATUS'] = '1';
                    $this->db->insert('ts_user_role', $arrTmUserRole);
                    $esp->main->get_bol_trans_check($iCounTol);
                }

                $iUserId = $iId;

            } else if($sSubmenu == 'delete'){ 
        
                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("update tm_user set USER_STATUS_ID = 'US02' WHERE USER_ID IN ('$sTableId') ");
                //print_r($this->db->last_query());die('----');
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_pegawai/mst-pegawai";

            }            

            if($esp->main->get_bol_trans_status($iCounTol)){
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_pegawai/mst-pegawai";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }


        }else if($sMenu=='frm-pemohon'){
            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrTmUser = $this->input->post('REG');
            $arrTmUserCom = $this->input->post('REGCOM');
            $arrTmUserPrsn = $this->input->post('REGPRSN');

            if($sSubmenu == 'new'){
                $this->db->insert('tm_user', $arrTmUser);
                $iUserId = $this->db->insert_id();

                if($arrTmUserCom['USER_COM_STATUS_ID'] != ''){
                    $arrTmUserCom['USER_ID'] = $iUserId;
                    $this->db->insert('tm_user_com', $arrTmUserCom);
                    $esp->main->get_bol_trans_check($iCounTol);
                }
                if($arrTmUserPrsn['USER_PRSN_IDTYPE'] != ''){
                    $arrTmUserPrsn['USER_ID'] = $iUserId;
                    $this->db->insert('tm_user_prsn', $arrTmUserPrsn);
                    $esp->main->get_bol_trans_check($iCounTol);
                }

            }else if($sSubmenu == 'edit'){ 
                $arrTmUser['USER_ADDR'] = trim($arrTmUser['USER_ADDR']);
                $this->db->where('USER_ID', $iId);
                $this->db->update('tm_user', $arrTmUser);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                $this->db->where('USER_ID', $iId);
                $this->db->delete('tm_user_com');
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                $this->db->where('USER_ID', $iId);
                $this->db->delete('tm_user_prsn');
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                if($arrTmUserCom['USER_COM_STATUS_ID'] != ''){
                    $arrTmUserCom['USER_ID'] = $iId;
                    $this->db->insert('tm_user_com', $arrTmUserCom);
                    $esp->main->get_bol_trans_check($iCounTol);
                }
                if($arrTmUserPrsn['USER_PRSN_IDTYPE'] != ''){
                    $arrTmUserPrsn['USER_ID'] = $iId;
                    $this->db->insert('tm_user_prsn', $arrTmUserPrsn);
                    //print_r($_POST);die('ooo');
                    $esp->main->get_bol_trans_check($iCounTol);
                }
                $iUserId = $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("update tm_user set USER_STATUS_ID = 'US02' WHERE USER_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_pemohon/mst-pemohon";

            }        

            if($esp->main->get_bol_trans_status($iCounTol)){
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_pemohon/mst-pemohon";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        }else if($sMenu=='frm-btp'){ 

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrBTP= $this->input->post('BTP');

            if($sSubmenu == 'new'){

                $this->db->insert('tr_btp_type', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('BTP_TYPE_ID', $iId);
                $this->db->update('tr_btp_type', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM tr_btp_type WHERE BTP_TYPE_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_btp";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_btp";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        }else if($sMenu=='frm-btp-function'){ 
            // print_r($_POST); die();

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrBTP= $this->input->post('BTP');

            if($sSubmenu == 'new'){

                $this->db->insert('tr_btp_function', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('BTP_FUNCTION_ID', $iId);
                $this->db->update('tr_btp_function', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM tr_btp_function WHERE BTP_FUNCTION_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_btp_function";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_btp_function";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        }else if($sMenu=='frm-food-type'){ 
            // print_r($_POST); die();

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrBTP= $this->input->post('BTP');

            if($sSubmenu == 'new'){

                $this->db->insert('tr_food_type', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('FOOD_TYPE_ID', $iId);
                $this->db->update('tr_food_type', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM tr_food_type WHERE FOOD_TYPE_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_food_type";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_food_type";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        }else if($sMenu=='frm-piket'){ 
            // print_r($_POST); die();

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrBTP= $this->input->post('USEROLE');

            if($sSubmenu == 'new'){

                $this->db->insert('ts_partition', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('PARTITION_ID', $iId);
                $this->db->update('ts_partition', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM ts_partition WHERE PARTITION_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_piket";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_piket";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        }else if($sMenu=='frm-hari-kerja'){ 

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrBTP= $this->input->post('REG');

            if($sSubmenu == 'new'){

                $this->db->insert('tr_date', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('DATE', $iId);
                $this->db->update('tr_date', $arrBTP);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
            //     echo $this->db->last_query();
            // print_r($_POST); die();
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM tr_date WHERE DATE IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_hari_kerja";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_hari_kerja";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        } else if($sMenu=='frm-permenkes'){  //die('oooo');

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrPMKS= $this->input->post('PMKS');

            if($sSubmenu == 'new'){

                $this->db->insert('tr_permenkes', $arrPMKS);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('PERMENKES_ID', $iId);
                $this->db->update('tr_permenkes', $arrPMKS);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM tr_permenkes WHERE PERMENKES_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_permenkes";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_permenkes";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        } else if($sMenu=='frm-codex1'){ 

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrCODEX1= $this->input->post('CODEX1');

            if($sSubmenu == 'new'){

                $this->db->insert('tr_codex_one', $arrCODEX1);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('CODEX_ONE_ID', $iId);
                $this->db->update('tr_codex_one', $arrCODEX1);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM tr_codex_one WHERE CODEX_ONE_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_codex1";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_codex1";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        } else if($sMenu=='frm-codex3'){ 

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrCodex3= $this->input->post('CODEX3');

            if($sSubmenu == 'new'){

                $this->db->insert('tr_codex_three', $arrCodex3);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('CODEX_THREE_ID', $iId);
                $this->db->update('tr_codex_three', $arrCodex3);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM tr_codex_three WHERE CODEX_THREE_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_codex3";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_codex3";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        } else if($sMenu=='frm-annex3'){ 

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrAnnex3= $this->input->post('ANNEX3');

            if($sSubmenu == 'new'){

                $this->db->insert('tr_codex_annex_three', $arrAnnex3);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('CODEX_ANNEX_THREE_ID', $iId);
                $this->db->update('tr_codex_annex_three', $arrAnnex3);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM tr_codex_annex_three WHERE CODEX_ANNEX_THREE_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_annex3";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_annex3";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        } else if($sMenu=='frm-perka'){ 

            $iCounTol = $esp->main->get_bol_trans_begin();
            $arrPerka= $this->input->post('PERKA');

            if($sSubmenu == 'new'){

                $this->db->insert('tr_perkajinsus', $arrPerka);
                $esp->main->get_bol_trans_check($iCounTol);


            }else if($sSubmenu == 'edit'){

                $this->db->where('PERKAJINSUS_ID', $iId);
                $this->db->update('tr_perkajinsus', $arrPerka);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                
                $iId;

            } else if($sSubmenu == 'delete'){ 

                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");
                $this->db->query("DELETE FROM tr_perkajinsus WHERE PERKAJINSUS_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);
                return "MSG#Proses Berhasil#".site_url()."master/list_perka";

            }         

            if($esp->main->get_bol_trans_status($iCounTol)){ 
                return "MSG#OK#Proses Berhasil#".site_url()."master/list_perka";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

        }
    } 

    public function insert_banyak($tabel="",$data="") {
        //field
        $temp = join(",",array_keys($data[1]));
        $field = "(" . $temp . ")";
        $sql = "INSERT INTO $tabel $field VALUES ";

        //value
        foreach($data as $datas) {
            $bersih = str_replace("'", "\'", $datas);
            $test = join("','",$bersih);
            $value .= "('" . $test . "'),";
        }

        $sql .= rtrim($value,",");
        
        $exec = $this->db->query($sql);
        return $exec; //affected_row / jml row
    }


    function master_get($sMenu, $iId){
        $esp = get_instance();
        $esp->load->model("main","main", true);

        if($sMenu=="frm-user"){

            $arrdata['PROVINCE'] = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) = '00'", "REGION_ID", "REGION_NAME", TRUE);
            $arrdata['USER_TYPE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_ID IN ('UTY01','UTY02')  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['ID_TYPE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE  STATUS_TYPE = 'ID_TYPE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['COMPANY_STATUS'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'COMPANY_STATUS'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['COMPANY_SCALE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'COMPANY_SCALE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['ROLE'] = $esp->main->get_arr_combobox("SELECT ROLE_ID,ROLE_NAME FROM TR_ROLE WHERE ROLE_STATUS = '1'  ORDER BY ROLE_ID", "ROLE_ID", "ROLE_NAME", TRUE);

            if($iId != ''){

                $users = $this->db->query("SELECT a.*, b.`USER_COM_STATUS_ID`, b.`USER_COM_SCALE_ID`, b.`USER_COM_LDR_NAME`, b.`USER_COM_TLDR_NAME`, c.`USER_PRSN_IDTYPE`, c.`USER_PRSN_IDNO`, c.USER_TYPE_ETC, d.`ROLE_ID`, f.`role_name`  FROM tm_user a LEFT JOIN tm_user_com b ON a.`USER_ID`=b.`USER_ID` LEFT JOIN tm_user_prsn c ON a.`USER_ID`=c.`USER_ID` LEFT JOIN ts_user_role d ON a.`USER_ID`=d.`USER_ID` LEFT JOIN tr_role f ON d.`ROLE_ID`=f.`ROLE_ID` WHERE a.`USER_ID`= $iId")->row_array();
               
                $arrdata['arrUser'] = $users;//print_r( $arrdata['arrUser']);die();
                $arrdata['CITY'] = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) <> '00' AND LEFT(REGION_ID, 2) = LEFT('$users[REGION_ID]', 2)" , "REGION_ID", "REGION_NAME", TRUE);

                $arrdata['act'] = site_url('master/post/frm-pegawai/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-pegawai/new');
            }

        }else if($sMenu=="frm-pegawai"){

            $arrdata['PROVINCE'] = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) = '00'", "REGION_ID", "REGION_NAME", TRUE);
            $arrdata['USER_TYPE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_ID IN ('UTY01','UTY02')  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['ID_TYPE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE  STATUS_TYPE = 'ID_TYPE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['COMPANY_STATUS'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'COMPANY_STATUS'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['COMPANY_SCALE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'COMPANY_SCALE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['ROLE'] = $esp->main->get_arr_combobox("SELECT ROLE_ID,ROLE_NAME FROM TR_ROLE WHERE ROLE_STATUS = '1'  ORDER BY ROLE_ID", "ROLE_ID", "ROLE_NAME", TRUE);
            $arrdata['USER_STATUS_ID'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE  STATUS_TYPE = 'USER_STATUS' AND STATUS_ID NOT IN ('US00')  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);

            if($iId != ''){

                $users = $this->db->query("SELECT a.*, b.`USER_COM_STATUS_ID`, b.`USER_COM_SCALE_ID`, b.`USER_COM_LDR_NAME`, b.`USER_COM_TLDR_NAME`, c.`USER_PRSN_IDTYPE`, c.`USER_PRSN_IDNO`, c.USER_TYPE_ETC, d.`ROLE_ID`, f.`role_name`  FROM tm_user a LEFT JOIN tm_user_com b ON a.`USER_ID`=b.`USER_ID` LEFT JOIN tm_user_prsn c ON a.`USER_ID`=c.`USER_ID` LEFT JOIN ts_user_role d ON a.`USER_ID`=d.`USER_ID` LEFT JOIN tr_role f ON d.`ROLE_ID`=f.`ROLE_ID` WHERE a.`USER_ID`= $iId")->row_array();
               
                $arrdata['arrUser'] = $users; //print_r($arrdata['arrUser']);die();
                $arrdata['CITY'] = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) <> '00' AND LEFT(REGION_ID, 2) = LEFT('$users[REGION_ID]', 2)" , "REGION_ID", "REGION_NAME", TRUE);

                $arrdata['act'] = site_url('master/post/frm-pegawai/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-pegawai/new');
            }

        }else if($sMenu=="frm-pemohon"){

            $arrdata['PROVINCE'] = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) = '00'", "REGION_ID", "REGION_NAME", TRUE);
            $arrdata['USER_TYPE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_ID IN ('UTY01','UTY02')  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['ID_TYPE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE  STATUS_TYPE = 'ID_TYPE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['COMPANY_STATUS'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'COMPANY_STATUS'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['COMPANY_SCALE'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'COMPANY_SCALE'  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['USER_STATUS_ID'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE  STATUS_TYPE = 'USER_STATUS' AND STATUS_ID NOT IN ('US00')  ORDER BY STATUS_ID", "STATUS_ID", "STATUS_DETAIL", TRUE);

            if($iId != ''){

                $users = $this->db->query("SELECT a.*, b.`USER_COM_STATUS_ID`, b.`USER_COM_SCALE_ID`, b.`USER_COM_LDR_NAME`, b.`USER_COM_TLDR_NAME`, c.`USER_PRSN_IDTYPE`, c.`USER_PRSN_IDNO`, c.USER_TYPE_ETC FROM tm_user a LEFT JOIN tm_user_com b ON a.`USER_ID`=b.`USER_ID` LEFT JOIN tm_user_prsn c ON a.`USER_ID`=c.`USER_ID` WHERE a.`USER_ID`= $iId ")->row_array();

                $arrdata['arrUser'] = $users;
                $arrdata['CITY'] = $esp->main->get_arr_combobox("SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) <> '00' AND LEFT(REGION_ID, 2) = LEFT('$users[REGION_ID]', 2)" , "REGION_ID", "REGION_NAME", TRUE);

                $arrdata['act'] = site_url('master/post/frm-pemohon/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-pemohon/new');
            }

        }else if($sMenu=="frm-btp"){

            if($iId != ''){  

                $btp = $this->db->query("SELECT BTP_TYPE_ID, BTP_TYPE_NO, BTP_TYPE_NAME FROM tr_btp_type WHERE BTP_TYPE_ID = $iId ")->row_array();
                $arrdata['arrBTP'] = $btp; 
               
                $arrdata['act'] = site_url('master/post/frm-btp/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-btp/new');
            }
 

        }else if($sMenu=="frm-btp-function"){

            if($iId != ''){  

                $btp = $this->db->query("SELECT BTP_FUNCTION_ID, BTP_FUNCTION_NAME, BTP_FUNCTION_NOMOR_PERATURAN, BTP_FUNCTION_YEAR FROM tr_btp_function WHERE BTP_FUNCTION_ID = $iId ")->row_array();
                $arrdata['arrBTP'] = $btp; 
               
                $arrdata['act'] = site_url('master/post/frm-btp-function/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-btp-function/new');
            }
 
        }else if($sMenu=="frm-food-type"){

            if($iId != ''){  

                $btp = $this->db->query("SELECT FOOD_TYPE_ID, FOOD_TYPE_NAME, FOOD_TYPE_NO FROM tr_food_type WHERE FOOD_TYPE_ID = $iId ")->row_array();
                $arrdata['arrBTP'] = $btp; 
               
                $arrdata['act'] = site_url('master/post/frm-food-type/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-food-type/new');
            }
        } else if($sMenu=="frm-piket"){

   
            $arrdata['USER'] = $esp->main->get_arr_combobox("SELECT USER_ID,USER_NAME FROM TM_USER WHERE USER_TYPE_ID = 'UTY03' AND USER_STATUS_ID='US01' ORDER BY USER_ID", "USER_ID", "USER_NAME", TRUE);
            $arrdata['LEVEL'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'level'", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['HARI'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'hari' order by urutan", "STATUS_ID", "STATUS_DETAIL", TRUE);

            
            if($iId != ''){  

                $PMKS = $this->db->query("SELECT a.PARTITION_ID, a.USER_ID,a.PARTITION_DAY,a.LEVEL 
                FROM ts_partition a 
                 WHERE a.PARTITION_ID = $iId ")->row_array();
                $arrdata['arrUser'] = $PMKS;
               
                $arrdata['act'] = site_url('master/post/frm-piket/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-piket/new');
            }
 
        } else if($sMenu=="frm-hari-kerja"){

            $arrdata['STATUS_HARI'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'status_hari'", "STATUS_ID", "STATUS_DETAIL", TRUE);
            $arrdata['HARI'] = $esp->main->get_arr_combobox("SELECT STATUS_ID,STATUS_DETAIL FROM TR_STATUS WHERE STATUS_TYPE = 'hari' order by urutan", "STATUS_ID", "STATUS_DETAIL", TRUE);

            
            if($iId != ''){  

                $PMKS = $this->db->query("SELECT a.DATE, a.DAY_NAME,a.STATUS_ID
                FROM tr_date a 
                 WHERE a.DATE = '$iId' ")->row_array();
                $arrdata['arrUser'] = $PMKS;
               
                $arrdata['act'] = site_url('master/post/frm-hari-kerja/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-hari-kerja/new');
            }
        } else if($sMenu=="frm-permenkes"){

   
            $arrdata['BTP'] = $esp->main->get_arr_combobox("SELECT A.BTP_TYPE_ID AS ID, concat(A.BTP_TYPE_NO,' - ',A.BTP_TYPE_NAME) AS NO FROM TR_BTP_TYPE A", "ID", "NO", TRUE);
            $arrdata['BTPFUNC'] = $esp->main->get_arr_combobox("SELECT A.BTP_FUNCTION_ID AS ID, concat(A.BTP_FUNCTION_ID,' - ',A.BTP_FUNCTION_NAME) AS NO FROM TR_BTP_FUNCTION A", "ID", "NO", TRUE);
            
            if($iId != ''){  

                $PMKS = $this->db->query("SELECT A.PERMENKES_ID, A.BTP_TYPE_ID, A.BTP_FUNCTION_ID, B.BTP_TYPE_NO AS 'NO BTP', B.BTP_TYPE_NAME AS 'NAMA BTP',  C.BTP_FUNCTION_NAME AS 'NAMA FUNGSI BTP', C.BTP_FUNCTION_NOMOR_PERATURAN AS 'NO FUNGSI PERATURAN'
                    FROM tr_permenkes A
                    LEFT JOIN tr_btp_type B ON A.BTP_TYPE_ID = B.BTP_TYPE_ID
                    LEFT JOIN tr_btp_function C ON A.BTP_FUNCTION_ID = C.BTP_FUNCTION_ID WHERE PERMENKES_ID = $iId ")->row_array();
                $arrdata['arrPMKS'] = $PMKS;
               
                $arrdata['act'] = site_url('master/post/frm-permenkes/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-permenkes/new');
            }
 

        } else if($sMenu=="frm-codex1"){

            $arrdata['BTP'] = $esp->main->get_arr_combobox("SELECT A.BTP_TYPE_ID AS ID, concat(A.BTP_TYPE_NO,' - ',A.BTP_TYPE_NAME) AS NO FROM TR_BTP_TYPE A", "ID", "NO", TRUE);

            $arrdata['foodtype'] = $esp->main->get_arr_combobox("SELECT A.FOOD_TYPE_ID AS ID, concat(A.FOOD_TYPE_NO,' - ',A.FOOD_TYPE_NAME) AS NO FROM TR_FOOD_TYPE A","ID","NO", TRUE);

            $arrdata['DENOMINATION'] = $esp->main->get_arr_combobox("SELECT DENOMINATION_ID,DENOMINATION_NAME FROM TR_DENOMINATION   ORDER BY DENOMINATION_ID", "DENOMINATION_ID", "DENOMINATION_NAME", TRUE);

            $arrdata['ONECODE'] = $esp->main->get_arr_combobox("SELECT A.NOTE_CODE AS ID, concat(A.NOTE_CODE,' - ',A.NOTE_DETAIL) AS NO FROM TR_CODEX_ONE_NOTE A", "ID", "NO", TRUE);

            if($iId != ''){  

                $codex1 = $this->db->query("SELECT A.CODEX_ONE_ID, A.BTP_TYPE_ID, A.FOOD_TYPE_ID, C.FOOD_TYPE_NO, A.MAX_LIMIT, A.UNIT, A.CODEX_ONE_NOTE_ID  FROM tr_codex_one A 
                			LEFT JOIN TR_BTP_TYPE B ON A.BTP_TYPE_ID = B.BTP_TYPE_ID 
                            LEFT JOIN TR_FOOD_TYPE C ON A.FOOD_TYPE_ID = C.FOOD_TYPE_ID
                			WHERE A.CODEX_ONE_ID = $iId ")->row_array(); 
                            //LEFT JOIN TR_CODEX_ONE_NOTE C ON A.CODEX_ONE_NOTE_ID = C.NOTE_CODE
                            //print_r($arrdata['arrCodex1']);die();
                $arrdata['arrCodex1'] = $codex1;
               
                $arrdata['act'] = site_url('master/post/frm-codex1/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-codex1/new');
            }
 

        } else if($sMenu=="frm-codex3"){

            $arrdata['BTP'] = $esp->main->get_arr_combobox("SELECT A.BTP_TYPE_ID AS ID, concat(A.BTP_TYPE_NO,' - ',A.BTP_TYPE_NAME) AS NO FROM TR_BTP_TYPE A", "ID", "NO", TRUE);

            if($iId != ''){  

                $codex3 = $this->db->query("SELECT A.CODEX_THREE_ID, A.BTP_TYPE_ID, A.BTP_FUNCTION_NAME, A.CODEX_YEAR 
                                            FROM tr_codex_three A 
                                            LEFT JOIN tr_btp_type B ON A.BTP_TYPE_ID = B.BTP_TYPE_ID
                                            WHERE A.CODEX_THREE_ID = $iId ")->row_array();
                $arrdata['arrCodex3'] = $codex3;
               
                $arrdata['act'] = site_url('master/post/frm-codex3/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-codex3/new');
            }
 

        } else if($sMenu=="frm-annex3"){

            $arrdata['foodtype'] = $esp->main->get_arr_combobox("SELECT A.FOOD_TYPE_ID AS ID, concat(A.FOOD_TYPE_NO,' - ',A.FOOD_TYPE_NAME) AS NO FROM TR_FOOD_TYPE A","ID","NO", TRUE);

            if($iId != ''){  

                $Annex3 = $this->db->query("SELECT A.CODEX_ANNEX_THREE_ID, A.FOOD_TYPE_ID FROM tr_codex_annex_three A 
                							LEFT JOIN TR_FOOD_TYPE B ON A.FOOD_TYPE_ID = B.FOOD_TYPE_ID WHERE A.CODEX_ANNEX_THREE_ID = $iId ")->row_array();
                $arrdata['arrAnnex3'] = $Annex3; //print_r( $arrdata['arrAnnex3']);die();
               
                $arrdata['act'] = site_url('master/post/frm-annex3/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-annex3/new');
            }
 

        } else if($sMenu=="frm-perka"){

        	$arrdata['BTP'] = $esp->main->get_arr_combobox("SELECT A.BTP_TYPE_ID AS ID, concat(A.BTP_TYPE_NO,' - ',A.BTP_TYPE_NAME) AS NO FROM TR_BTP_TYPE A", "ID", "NO", TRUE);

            $arrdata['BTPFUNC'] = $esp->main->get_arr_combobox("SELECT A.BTP_FUNCTION_ID AS ID, concat(A.BTP_FUNCTION_ID,' - ',A.BTP_FUNCTION_NAME) AS NO FROM TR_BTP_FUNCTION A", "ID", "NO", TRUE);

            $arrdata['foodtype'] = $esp->main->get_arr_combobox("SELECT A.FOOD_TYPE_ID AS ID, concat(A.FOOD_TYPE_NO,' - ',A.FOOD_TYPE_NAME) AS NO FROM TR_FOOD_TYPE A","ID","NO", TRUE);

            $arrdata['DENOMINATION'] = $esp->main->get_arr_combobox("SELECT DENOMINATION_ID, DENOMINATION_NAME FROM TR_DENOMINATION ORDER BY DENOMINATION_ID", "DENOMINATION_ID", "DENOMINATION_NAME", TRUE);

            $arrdata['ONECODE'] = $esp->main->get_arr_combobox("SELECT A.NOTE_CODE AS CODE, concat(A.NOTE_CODE,' - ',A.NOTE_DETAIL) AS NO FROM TR_CODEX_ONE_NOTE A", "CODE", "NO", TRUE);

            if($iId != ''){  

                $perka = $this->db->query("SELECT a.*, a.NOTE, a.MAX_LIMIT, b.BTP_TYPE_NAME, c.FOOD_TYPE_NAME FROM tr_perkajinsus a 
                							LEFT JOIN tr_btp_type b ON a.`BTP_TYPE_ID` = b.`BTP_TYPE_ID`
                					        LEFT JOIN tr_food_type c ON a.`FOOD_TYPE_ID` = c.`FOOD_TYPE_ID`
                					        WHERE a.PERKAJINSUS_ID = $iId ")->row_array();
                $arrdata['arrPerka'] = $perka; //print_r( $arrdata['arrAnnex3']);die();
               
                $arrdata['act'] = site_url('master/post/frm-perka/edit');

            }else{

                $arrdata['act'] = site_url('master/post/frm-perka/new');
            }
 

        } else if($sMenu=="list"){
            $sTitle = "Daftar Pemohon Terdaftar";
            $sStatus_id = "TR00";
        }
        return $arrdata;
    }



}
?>
