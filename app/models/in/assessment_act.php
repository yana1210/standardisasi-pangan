<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//error_reporting(E_ERROR);
class Assessment_act extends Model{

    function get_arr_list_tracing($sType) { 
        $ROLE_ID = $this->newsession->userdata('ROLE_ID');
        $USER_ID = $this->newsession->userdata('USER_ID');
        if($ROLE_ID == '607' ){
            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/ass/draft-rec', '1'));
            $sWhere = "WHERE a.`RQS_STATUS_ID` IN ('RS02','RS04','RS05','RS06') AND a.`RQS_PROCESS_ID` IS NOT NULL "; 
            $sTitle = '<span style="color: blue"> Berkas Permohonan yang Masuk </span>';
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/ass/draft-rec/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', CONCAT('<B>',b.`USER_NAME`,'</B>') AS 'VERIFIKATOR',CONCAT('<B>',b.`USER_NAME`,'</B>') AS 'ASESOR', a.RQS_LEVEL AS 'TINGKAT KESULITAN', j.`STATUS_DETAIL` AS 'POSISI PERMOHONAN', k.`STATUS_DETAIL` AS 'STATUS PERMOHONAN' FROM tx_rqs a 
LEFT JOIN tm_user b ON a.`RQS_VER_BY`=b.`USER_ID`
LEFT JOIN tm_user d ON a.`RQS_ASS_BY`=d.`USER_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN TR_STATUS j ON a.`RQS_PROCESS_ID` = j.`STATUS_ID` 
LEFT JOIN TR_STATUS k ON a.`RQS_STATUS_ID` = k.`STATUS_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere";
        }else{
            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/ass/draft-rec', '1'));
            $sWhere = "WHERE a.`RQS_STATUS_ID` IN ('RS02','RS04','RS05','RS06') AND a.`RQS_PROCESS_ID` IS NOT NULL "; 
            $sTitle = 'ASSESSMENT >> <span style="color: blue"> PENELUSURAN PERMOHONAN </span>';
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/ass/draft-rec/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', j.`STATUS_DETAIL` AS 'POSISI PERMOHONAN', k.`STATUS_DETAIL` AS 'STATUS PERMOHONAN' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN TR_STATUS j ON a.`RQS_PROCESS_ID` = j.`STATUS_ID` 
LEFT JOIN TR_STATUS k ON a.`RQS_STATUS_ID` = k.`STATUS_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere";

        }
//d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN',a.`RQS_BTP_MERK` AS 'MERK DAGANG',i.`BTP_FUNCTION_NAME` AS 'GOLONGAN BTP', LEFT JOIN tr_btp_function i ON a.`RQS_BTP_FUNCTION_ID`=i.`BTP_FUNCTION_ID`
        $search = array(array('RQS_NO_AJU', 'NOMOR PERMOHONAN'), array('f.USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('BTP_FUNCTION_NAME', 'GOLONGAN BTP') );
        $action = site_url() . "assessment/tracing/" . $sType;

//echo $sQuery; die();
        $this->load->library('newtable');
        $this->newtable->hiddens(array('RQS_ID'));
        $this->newtable->search($search);
        $this->newtable->action($action);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        $this->newtable->orderby(1);
        $this->newtable->sortby("DESC");
        $this->newtable->keys("RQS_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable);
        return $arrData;
    }
    function get_arr_list_rev($sType) { 
        $ROLE_ID = $this->newsession->userdata('ROLE_ID');
        $USER_ID = $this->newsession->userdata('USER_ID');
        $sWhere = "WHERE a.`RQS_PROCESS_ID` IN ('$sType') "; 
        $sTitle = '<span style="color: blue"> Daftar Revisi Kajian </span>';
        if($ROLE_ID == '606' || $ROLE_ID == '605'){
            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/ass/draft-rec', '1'));
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/ass/draft-rec/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', CONCAT('<B>',b.`USER_NAME`,'</B>') AS 'VERIFIKATOR',CONCAT('<B>',b.`USER_NAME`,'</B>') AS 'ASESOR', a.RQS_LEVEL AS 'TINGKAT KESULITAN', j.`STATUS_DETAIL` AS 'POSISI PERMOHONAN', k.`STATUS_DETAIL` AS 'STATUS PERMOHONAN' FROM tx_rqs a 
LEFT JOIN tm_user b ON a.`RQS_VER_BY`=b.`USER_ID`
LEFT JOIN tm_user d ON a.`RQS_ASS_BY`=d.`USER_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN TR_STATUS j ON a.`RQS_PROCESS_ID` = j.`STATUS_ID` 
LEFT JOIN TR_STATUS k ON a.`RQS_STATUS_ID` = k.`STATUS_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere";
        }else{
            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/ass/draft-rec', '1'));
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/ass/draft-rec/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', j.`STATUS_DETAIL` AS 'POSISI PERMOHONAN', k.`STATUS_DETAIL` AS 'STATUS PERMOHONAN' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN TR_STATUS j ON a.`RQS_PROCESS_ID` = j.`STATUS_ID` 
LEFT JOIN TR_STATUS k ON a.`RQS_STATUS_ID` = k.`STATUS_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere";

        }
//d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN',a.`RQS_BTP_MERK` AS 'MERK DAGANG',i.`BTP_FUNCTION_NAME` AS 'GOLONGAN BTP', LEFT JOIN tr_btp_function i ON a.`RQS_BTP_FUNCTION_ID`=i.`BTP_FUNCTION_ID`
        $search = array(array('RQS_NO_AJU', 'NOMOR PERMOHONAN'), array('f.USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('BTP_FUNCTION_NAME', 'GOLONGAN BTP') );
        $action = site_url() . "assessment/tracing/" . $sType;

//echo $sQuery; die();
        $this->load->library('newtable');
        $this->newtable->hiddens(array('RQS_ID'));
        $this->newtable->search($search);
        $this->newtable->action($action);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        $this->newtable->orderby(1);
        $this->newtable->sortby("DESC");
        $this->newtable->keys("RQS_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable);
        return $arrData;
    }

    function get_arr_list_draft($sType) {
        $ROLE_ID = $this->newsession->userdata('ROLE_ID');
        $USER_ID = $this->newsession->userdata('USER_ID');
        $sWhere = "WHERE a.`RQS_STATUS_ID` IN ('RS02','RS04','RS05','RS06') and a.`RQS_ASSESSMENT_MARK` in ('$sType')  "; 
        if($ROLE_ID == '603' ){
            //die($sWhere);
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/ass/codex/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', a.RQS_ASS_DATE as 'TANGGAL ASSESSMENT', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', j.`STATUS_DETAIL` AS 'POSISI PERMOHONAN', k.`STATUS_DETAIL` AS 'STATUS PERMOHONAN' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN TR_STATUS j ON a.`RQS_PROCESS_ID` = j.`STATUS_ID` 
LEFT JOIN TR_STATUS k ON a.`RQS_STATUS_ID` = k.`STATUS_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere"; //d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN',a.`RQS_BTP_MERK` AS 'MERK DAGANG',i.`BTP_FUNCTION_NAME` AS 'GOLONGAN BTP', LEFT JOIN tr_btp_function i ON a.`RQS_BTP_FUNCTION_ID`=i.`BTP_FUNCTION_ID`
            $prosesnya = array('Download Nota Dinas dan Surat Jawaban' => array('GETNEW', site_url() . 'cetak/nodin', '1'),'Lihat Detail' => array('GET', site_url() . 'assessment/ass/draft-rec', '1')); //,'Print Surat Persetujuan BTP' => array('GETNEW', site_url() . 'cetak/srt_persetujuan', '1'),'Print Surat Penolakan BTP' => array('GETNEW', site_url() . 'cetak/sj_penolakan', '1')
        }else if( $ROLE_ID == "605" || $ROLE_ID == "606" || $ROLE_ID == "607"){
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/ass/draft-rec/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', a.RQS_ASS_DATE as 'TANGGAL ASSESSMENT', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', j.`STATUS_DETAIL` AS 'POSISI PERMOHONAN', k.`STATUS_DETAIL` AS 'STATUS PERMOHONAN' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN TR_STATUS j ON a.`RQS_PROCESS_ID` = j.`STATUS_ID` 
LEFT JOIN TR_STATUS k ON a.`RQS_STATUS_ID` = k.`STATUS_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere"; //d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN',a.`RQS_BTP_MERK` AS 'MERK DAGANG',i.`BTP_FUNCTION_NAME` AS 
            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/ass/draft-rec', '1'));
        }else{
            $sTitle = 'BELUM DISET';
        }
//echo $sQuery; die();
        $sTitle = 'ASSESSMENT >> <span style="color: blue"> DAFTAR HASIL ASSESSMENT </span>';
        $search = array(array('RQS_NO_AJU', 'NOMOR PERMOHONAN'),array('RQS_ASS_DATE', 'TANGGAL ASSESSMENT'), array('f.USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('BTP_FUNCTION_NAME', 'GOLONGAN BTP'), array('h.USER_NAME', 'ASSESSED BY') );
        $action = site_url() . "assessment/list_ass/" . $sType;
        $this->load->library('newtable');
        $this->newtable->hiddens(array('RQS_ID'));
        $this->newtable->search($search);
        $this->newtable->action($action);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        $this->newtable->orderby(1);
        $this->newtable->sortby("DESC");
        $this->newtable->keys("RQS_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable);
        return $arrData;
    }

    function get_arr_list_ass($sType) {
        $ROLE_ID = $this->newsession->userdata('ROLE_ID');
        $USER_ID = $this->newsession->userdata('USER_ID');
        if($ROLE_ID == "603" || $ROLE_ID == "604" ){
//                $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02') AND a.`RQS_PROCESS_ID` in ('PR03','PR10','PR12','PR14') "; 
            $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02') AND a.`RQS_PROCESS_ID` in ('PR03','PR04','PR07','PR10','PR12','PR15','PR16')  AND a.RQS_ASS_BY = '$USER_ID'"; 
            $sTitle = 'ASSESSMENT >> ASSIGNMENT >> <span style="color: blue"> DAFTAR ASSIGNMENT </span>';
        }else if($ROLE_ID == "698" || $ROLE_ID == "699" ){
            $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02') AND a.`RQS_PROCESS_ID` in ('PR02','PR16','PR03','PR04','PR07','PR10','PR12') "; 
            $sTitle = 'DAFTAR SUBMISSION';
        } else {
            $sTitle = 'BELUM DISET';
        }
        if($sType == 'ass'){ // menu assessment di assesor
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/assignment/detail/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', CONCAT('<B>',h.`USER_NAME`,'</B><BR>',a.RQS_VER_DATE) as 'VERIFIKATOR', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN',k.`STATUS_DETAIL` AS 'POSISI PERMOHONAN', CONCAT('<B><a style=\"font-size:11px;color:white\" href= \"" . site_url() . "assessment/ass/form/',a.`RQS_ID`,'\" class=\"btn btn-xs btn-success\"><i class=\"fa fa-pencil-square-o\"></i> Buat Nodin</a> <a style=\"font-size:11px;color:white\" href= \"" . site_url() . "cetak/nodin/',a.`RQS_ID`,'\" class=\"btn btn-xs btn-info\"><i class=\"fa fa-download\"></i> Download Nodin</a></B>') AS 'Action' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN tm_user h ON a.`RQS_VER_BY`=h.`USER_ID` 
LEFT JOIN TR_STATUS k ON a.`RQS_PROCESS_ID` = k.`STATUS_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere"; //d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN',a.`RQS_BTP_MERK` AS 'MERK DAGANG',i.`BTP_FUNCTION_NAME` AS 'GOLONGAN BTP', LEFT JOIN tr_btp_function i ON a.`RQS_BTP_FUNCTION_ID`=i.`BTP_FUNCTION_ID`

            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'submission/assignment/detail', '1') );
             //,'Input No Surat' => array('GET', site_url() . 'assessment/ass/letter_number', '1'),'Setujui Berkas Permohonan' => array('POST', site_url() . 'assessment/post/assessment/setujui', 'N'),'Tolak Berkas Permohonan' => array('POST', site_url() . 'assessment/post/assessment/tolak', 'N')
            $search = array(array('RQS_NO_AJU', 'NOMOR PERMOHONAN') );
            $action = site_url() . "assessment/list_assignment/" . $sType;

        }else if($sType == 'admin'){ 
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/assignment/detail/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', CONCAT('<B>',h.`USER_NAME`,'</B>') as 'VERIFIKATOR', CONCAT('<B>',i.`USER_NAME`,'</B>') as 'ASSESOR', CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN',k.`STATUS_DETAIL` AS 'POSISI PERMOHONAN' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN tm_user h ON a.`RQS_VER_BY`=h.`USER_ID` 
LEFT JOIN tm_user i ON a.`RQS_ASS_BY`=i.`USER_ID` 
LEFT JOIN TR_STATUS k ON a.`RQS_PROCESS_ID` = k.`STATUS_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere"; 
            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'submission/assignment/detail', '1') );
            // $prosesnya = array('');
            $search = array(array('RQS_VER_DATE', 'TANGGAL VERIFIKASI'),array('RQS_NO_AJU', 'NOMOR PERMOHONAN'), array('f.USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('BTP_FUNCTION_NAME', 'GOLONGAN BTP'), array('h.USER_NAME', 'VERIFIED BY') );
            $action = site_url() . "assessment/list_assignment/" . $sType;
        }else{ // menu submission di verifikator ga kepake
            $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:8px\" href= \"" . site_url() . "submission/assignment/detail/',a.`RQS_ID`,'\">',a.`RQS_CREATE_DATE`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'TANGGAL MASUK', f.USER_NAME as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP', d.`FOOD_TYPE_NAME` AS 'JENIS PANGAN', d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN', a.`RQS_BTP_MERK` AS 'MERK DAGANG', a.`RQS_PIC_NAME` AS 'PJ', h.`STATUS_DETAIL` AS 'JENIS PERUSAHAAN' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` 
LEFT JOIN tr_status h ON g.`USER_COM_SCALE_ID`=h.`STATUS_ID`  $sWhere";
            $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'submission/assignment/detail', '1'),'Cetak Surat Rekomendasi' => array('GET', site_url() . 'submission/assignment/print', '1'));
            $search = array(array('RQS_CREATE_DATE', 'TANGGAL MASUK'), array('USER_NAME', 'PEMOHON'), array('BTP_TYPE_NO', 'NO INS'), array('BTP_TYPE_NAME', 'JENIS BTP'), array('FOOD_TYPE_NAME', 'JENIS PANGAN'), array('FOOD_TYPE_NO', 'NO KATEGORI PANGAN'), array('RQS_PIC_NAME', 'PENANGGUNG JAWAB'), array('STATUS_DETAIL', 'JENIS PERUSAHAAN') );
            $action = site_url() . "submission/list_assignment/" . $sType;
        }


        $this->load->library('newtable');
        $this->newtable->hiddens(array('RQS_ID'));

        $this->newtable->search($search);

        $this->newtable->action($action);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        //$this->newtable->columns(array("A.USER_ID", "A.NAMA", "A.JABATAN", "A.EMAIL", "B.URAIAN", "CONVERT(VARCHAR, A.LOGIN, 120)", "A.TELP"));
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("RQS_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,
            "arrProcess" => $arrProcess);
        //print_r($arrData);die();
        return $arrData;
    }
    function get_arr_list_review($sType) {
        $ROLE_ID = $this->newsession->userdata('ROLE_ID');
        $USER_ID = $this->newsession->userdata('USER_ID');
        $sTitle = 'ASSESSMENT >> <span style="color: blue"> DAFTAR REVIEW KAJIAN </span>';
        if($ROLE_ID == "605"){
            $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02') AND a.`RQS_PROCESS_ID` in ('PR06') ";
            $kasie = " CONCAT('<B>',i.`USER_NAME`,'</B><BR>',a.RQS_ASS_DATE) as 'ASESOR', "; 
        } else if($ROLE_ID == "606"){
            $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02') AND a.`RQS_PROCESS_ID` in ('PR09') "; 
            $kasie = " CONCAT('<B>',i.`USER_NAME`,'</B><BR>',a.RQS_ASS_DATE) as 'ASESOR', "; 
        } else if($ROLE_ID == "607"){
            $sWhere = "WHERE a.`RQS_STATUS_ID` in ('RS02') AND a.`RQS_PROCESS_ID` in ('PR11') "; 
            $kasie = " CONCAT('<B>',i.`USER_NAME`,'</B><BR>',a.RQS_ASS_DATE) as 'ASESOR', ";
        }else{
            $sTitle = 'BELUM DISET';
        }
        $sQuery = "SELECT a.RQS_ID, CONCAT('<B>','<a style=\"font-size:11px\" href= \"" . site_url() . "assessment/ass/draft-rec/',a.`RQS_ID`,'\">',a.`RQS_NO_AJU`,'</a></B><BR>',a.`RQS_CREATE_DATE`) AS 'NOMOR PERMOHONAN', CONCAT('<B>',h.`USER_NAME`,'</B><BR>',a.RQS_VER_DATE) as 'VERIFIKATOR', $kasie CONCAT('<B>',f.USER_NAME,'</B><br>',f.USER_ADDR) as 'PEMOHON', b.`BTP_TYPE_NO` AS 'NO INS',b.`BTP_TYPE_NAME` AS 'JENIS BTP',  d.`FOOD_TYPE_NO` AS 'NO KATEGORI PANGAN' FROM tx_rqs a 
LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID`
LEFT JOIN tm_user f ON a.`USER_ID`=f.`USER_ID` 
LEFT JOIN tm_user h ON a.`RQS_VER_BY`=h.`USER_ID` 
LEFT JOIN tm_user i ON a.`RQS_ASS_BY`=i.`USER_ID` 
LEFT JOIN tm_user_com g ON a.`USER_ID`=g.`USER_ID` $sWhere"; 
        $prosesnya = array('Lihat Detail' => array('GET', site_url() . 'assessment/ass/draft-rec', '1'));
        $search = array(array('a.RQS_NO_AJU', 'NOMOR PERMOHONAN'), array('f.USER_NAME', 'PEMOHON'), array('b.BTP_TYPE_NO', 'NO INS') );
        $action = site_url() . "assessment/list_review/review";


        $this->load->library('newtable');
        $this->newtable->hiddens(array('RQS_ID'));
        $this->newtable->search($search);
        $this->newtable->action($action);
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        $this->newtable->orderby(1);
        $this->newtable->sortby("ASC");
        $this->newtable->keys("RQS_ID");
        $this->newtable->menu($prosesnya);
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');
        $sTable = $this->newtable->generate($sQuery);
        $arrData = array("sTitle" => $sTitle,
            "sTable" => $sTable,);
        //print_r($arrData);die();
        return $arrData;
    }


	function set_assessment($sMenu="", $sSubmenu="", $iIdsalah=""){ 
		$esp = get_instance();
		$esp->load->model("main","main", true);
		$iId = $this->input->post('id');
		$RQS_ABOUT = $this->input->post('RQS_ABOUT');

		$submenu = $this->input->post('menu');
		$sUserId = $this->newsession->userdata('USER_ID');
		$ROLE_ID = $this->newsession->userdata('ROLE_ID');
        $now = date('Y-m-d H:i:s');
        if($sMenu=='letter_number'){ 
            $iCounTol = $esp->main->get_bol_trans_begin();
            //print_r($_POST); die();
            $RQS_LETTER_NUMBER = $this->input->post('RQS_LETTER_NUMBER');

            $arrTxRqs['RQS_LETTER_NUMBER'] = $RQS_LETTER_NUMBER; 
            $this->db->where('RQS_ID', $iId);
            $this->db->update('tx_rqs', $arrTxRqs);
            $esp->main->get_bol_trans_check($iCounTol,FALSE);

            $arrTxRqs['RQS_PROCESS_ID'] = 'PR16'; 
            $arrTxRqs['RQS_ASS_DATE'] = $now; 
            $arrTxRqs['RQS_ASS_BY'] = $sUserId; 
            $this->db->where('RQS_ID', $iId);
            $this->db->update('tx_rqs', $arrTxRqs);
            $esp->main->get_bol_trans_check($iCounTol,FALSE);

            $arrTlRqsLog['RQS_ID'] = $iId;
            $arrTlRqsLog['USER_ID'] = $sUserId;
            $arrTlRqsLog['RQS_LOG_DESC_ID'] = '310';
            $this->db->insert('tl_rqs_log', $arrTlRqsLog);
            $esp->main->get_bol_trans_check($iCounTol);
            if($esp->main->get_bol_trans_status($iCounTol)){
                return "MSG#OK#Proses Berhasil#".site_url()."assessment/list_assignment/ass";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }
        }else if($sMenu=='input_manual'){ 
            $iCounTol = $esp->main->get_bol_trans_begin();
            
            $RQS_L = $this->input->post('RQS_L');
            $RQS_L['RQS_ID'] = $iId;
            $TX_RQS_LETTER_SPEC = $this->input->post('TX_RQS_BTP_COMP');

            $this->db->where('RQS_ID', $iId);
            $this->db->delete('TX_RQS_LETTER_SPEC');
            $esp->main->get_bol_trans_check($iCounTol,FALSE);
            $this->insert_banyak('TX_RQS_LETTER_SPEC', $TX_RQS_LETTER_SPEC);          
            $esp->main->get_bol_trans_check($iCounTol);

            if($sSubmenu == 'save'){
	            $this->db->insert('tx_rqs_letter', $RQS_L);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);
            }else if($sSubmenu == 'edit'){
	            $this->db->where('RQS_ID', $iId);
	            $this->db->update('tx_rqs_letter', $RQS_L);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);
            }

            if($esp->main->get_bol_trans_status($iCounTol)){
                return "MSG#OK#Proses Berhasil#".site_url()."assessment/ass/form/".$iId;
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }
        }else if($sMenu=='form'){ 
            $iCounTol = $esp->main->get_bol_trans_begin();
            $RQS_LETTER_NUMBER = $this->input->post('RQS_LETTER_NUMBER');

            $arrTxRqs['RQS_LETTER_NUMBER'] = $RQS_LETTER_NUMBER; 
            $this->db->where('RQS_ID', $iId);
            $this->db->update('tx_rqs', $arrTxRqs);
            $esp->main->get_bol_trans_check($iCounTol,FALSE);
            if($esp->main->get_bol_trans_status($iCounTol)){
                return "MSG#OK#Proses Berhasil#".site_url()."assessment/tracing/all";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }

		}else if($sMenu=='xprt-an-req'){ 
            $iCounTol = $esp->main->get_bol_trans_begin();
            $tx_xprt_note = $this->input->post('TX_RQS_BTP_COMP');
            $RQS_DATE_REC = $this->input->post('RQS_DATE_REC');

			if($submenu=='simpan'){ die('di die');
				$this->db->where('RQS_ID', $iId);
				$this->db->delete('tx_xprt_note');
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$this->db->insert('tx_xprt_note', $tx_xprt_note);
	            $esp->main->get_bol_trans_check($iCounTol);

				if($esp->main->get_bol_trans_status($iCounTol)){
					return "MSG#OK#Rekomendasi Pakar Disimpan#".site_url()."assessment/ass/xprt-an-req/".$iId;
				}else{
					return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
				}
            }else if($submenu=='draft'){ 
                $this->db->where('RQS_ID', $iId);
                $this->db->delete('tx_xprt_note');
                $esp->main->get_bol_trans_check($iCounTol,FALSE);
                $this->insert_banyak('tx_xprt_note', $tx_xprt_note);          
                $esp->main->get_bol_trans_check($iCounTol);

                $arrTxRqs['RQS_DATE_REC'] = $RQS_DATE_REC; 
                $arrTxRqs['RQS_PROCESS_ID'] = 'PR03'; 
                $arrTxRqs['RQS_ASS_DATE'] = $now; 
                $arrTxRqs['RQS_ASS_BY'] = $sUserId; 
                $this->db->where('RQS_ID', $iId);
                $this->db->update('tx_rqs', $arrTxRqs);
                $esp->main->get_bol_trans_check($iCounTol,FALSE);

                $arrTlRqsLog['RQS_ID'] = $iId;
                $arrTlRqsLog['USER_ID'] = $sUserId;
                $arrTlRqsLog['RQS_LOG_DESC_ID'] = '308';
                $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                $esp->main->get_bol_trans_check($iCounTol);
			}
			if($esp->main->get_bol_trans_status($iCounTol)){
				return "MSG#OK#Proses Berhasil#".site_url()."assessment/list_assignment/ass";
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}

		}else if($sMenu=='calc-req'){
	        $tx_exposure = $this->input->post('TX_EXPOSURE');
			$tx_exposure['RQS_ID'] = $iId;
			if($submenu=='draft_rekom'){ 
				$this->db->where('RQS_ID', $iId);
				$this->db->delete('tx_exposure');
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$this->db->insert('tx_exposure', $tx_exposure);
	            $esp->main->get_bol_trans_check($iCounTol);
			}

			if($esp->main->get_bol_trans_status($iCounTol)){
				return "MSG#OK#Proses Berhasil#".site_url()."assessment/ass/draft-rec/".$iId;
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}
        }else if($sMenu=='nodin'){
            $doc = $this->input->post('DOC');
            $doc13 = $this->input->post('DOC13');
            $iId = $doc['RQS_ID'];
            $where = "RQS_ID = ".$iId." and document_id in ('12','13')";
            $this->db->where($where);
            $this->db->delete('TX_RQS_DOC');
            $esp->main->get_bol_trans_check($iCounTol,FALSE);

            $this->db->insert('tx_rqs_doc', $doc13);
            $esp->main->get_bol_trans_check($iCounTol);

            $arrTxRqs['RQS_PROCESS_ID'] = 'PR06'; 
            $arrTxRqs['RQS_ASS_DATE'] = $now; 
            $this->db->where('RQS_ID', $iId);
            $this->db->update('tx_rqs', $arrTxRqs);

            $arrTlRqsLog['RQS_ID'] = $iId;
            $arrTlRqsLog['USER_ID'] = $sUserId;
            $arrTlRqsLog['RQS_LOG_DESC_ID'] = '309';
            $this->db->insert('tl_rqs_log', $arrTlRqsLog);
            $esp->main->get_bol_trans_check($iCounTol);

            if($esp->main->get_bol_trans_status($iCounTol)){
                return "MSG#OK#Proses Berhasil#".site_url()."assessment/list_assignment/ass";
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }
        }else if($sMenu=='nodinkasi'){
            $doc = $this->input->post('DOC');
            //print_r($_POST); die();
            $iId = $doc['RQS_ID'];
            $where = "RQS_ID = ".$iId." and document_id in ('12')";
            $this->db->where($where);
            $this->db->delete('TX_RQS_DOC');
            $esp->main->get_bol_trans_check($iCounTol,FALSE);

            $this->db->insert('tx_rqs_doc', $doc);
            $esp->main->get_bol_trans_check($iCounTol);

            if($esp->main->get_bol_trans_status($iCounTol)){
                return "MSG#OK#Proses Berhasil#".site_url()."assessment/ass/draft-rec/".$iId;
            }else{
                return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
            }
		}else if($sMenu=='draft-rec'){
	        $menu = $this->input->post('menu');
	        $menu2 = $this->input->post('menu2');
	        $RQS_LOG_NOTE = $this->input->post('RQS_LOG_NOTE');
        	if($ROLE_ID=="605"){
        		$setujui = "PR09";
        		$perbaiki = "PR07";
        		$RQS_LOG_DESC_ID = "304";
        	}else if($ROLE_ID=="606"){
        		$setujui = "PR11";
        		$perbaiki = "PR10";
        		$RQS_LOG_DESC_ID = "305";
        	}else if($ROLE_ID=="607"){	        		
        		$setujui = "PR16"; // harus nya pr15
        		$perbaiki = "PR12";
        		$RQS_LOG_DESC_ID = "306";
        	}
            if($menu2 == 'perbaiki'){
				$arrTxRqs['RQS_PROCESS_ID'] = $perbaiki; 

	            $iCounTol = $esp->main->get_bol_trans_begin();

				$this->db->where('RQS_ID', $iId);
				$this->db->update('tx_rqs', $arrTxRqs);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$arrTlRqsLog['RQS_ID'] = $iId;
				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_NOTE'] = $RQS_LOG_NOTE;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = $RQS_LOG_DESC_ID;
				$this->db->insert('tl_rqs_log', $arrTlRqsLog);
	            $esp->main->get_bol_trans_check($iCounTol);
	        }else if($menu2 == 'setujui'){
				$arrTxRqs['RQS_PROCESS_ID'] = $setujui; 

	            $iCounTol = $esp->main->get_bol_trans_begin();

				$this->db->where('RQS_ID', $iId);
				$this->db->update('tx_rqs', $arrTxRqs);
	            $esp->main->get_bol_trans_check($iCounTol,FALSE);

				$arrTlRqsLog['RQS_ID'] = $iId;
				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_NOTE'] = $RQS_LOG_NOTE;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = $RQS_LOG_DESC_ID;
				$this->db->insert('tl_rqs_log', $arrTlRqsLog);
	            $esp->main->get_bol_trans_check($iCounTol);
	        }

			if($esp->main->get_bol_trans_status($iCounTol)){
				return "MSG#OK#Proses Berhasil#".site_url()."assessment/ass/draft-rec/".$iId;
			}else{
				return "MSG#ERR#Proses gagal. Silakan Ulangi Kembali";
			}

		}else if($sMenu=='assessment'){  //dipindahkan ke submission_act
			if($sSubmenu=='setujui'){ 

                $iCounTol = $esp->main->get_bol_trans_begin();
                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");

                $this->db->query("update tx_rqs set RQS_STATUS_ID = 'RS05', RQS_PROCESS_ID = 'PR17' WHERE RQS_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);

				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = '311';
                foreach ($sTbChk as $produk) {
					$arrTlRqsLog['RQS_ID'] = $produk;
					$this->db->insert('tl_rqs_log', $arrTlRqsLog);

                    $rqs = $this->db->query("select * from tx_rqs where rqs_id = $produk")->row_array();
                    $subject = "Pengajuan Penggunaan BTP";
                    $to = $rqs["RQS_PIC_MAIL"];
                    $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial;  width:580px;"><tr><td>Pengajuan Penggunaan BTP Disetujui<br><br>

        Berkas Permohonan Nomor '.$rqs["RQS_NO_AJU"].' sudah selesai, surat jawaban harap diambil ke Direktorat Standardisasi Produk Pangan
        <br><br>
        Terima Kasih</td></tr></table>';
                    $esp->main->send_mail('register', $to, $isi, $subject);

                }                
	            $esp->main->get_bol_trans_check($iCounTol);
                if ($esp->main->get_bol_trans_status($iCounTol)) {
                    return "MSG#Proses Berhasil#" . site_url() . "assessment/list_assignment/ass";
                } else {
                    return "MSG#Proses Gagal. Silakan Ulangi Kembali";
                }

			}else if($sSubmenu=='tolak'){
                $iCounTol = $esp->main->get_bol_trans_begin();
                $sTbChk = $this->input->post('tb_chk');
                $sTableId = JOIN($sTbChk, "', '");

                $this->db->query("update tx_rqs set RQS_STATUS_ID = 'RS06', RQS_PROCESS_ID = 'PR17' WHERE RQS_ID IN ('$sTableId') ");
                $esp->main->get_bol_trans_check($iCounTol, TRUE);

				$arrTlRqsLog['USER_ID'] = $sUserId;
				$arrTlRqsLog['RQS_LOG_DESC_ID'] = '311';
                foreach ($sTbChk as $produk) {
					$arrTlRqsLog['RQS_ID'] = $produk;
					$this->db->insert('tl_rqs_log', $arrTlRqsLog);

                    $rqs = $this->db->query("select * from tx_rqs where rqs_id = $produk")->row_array();
                    $subject = "Pengajuan Penggunaan BTP";
                    $to = $rqs["RQS_PIC_MAIL"];
                    $isi = '<table style="margin: 4px 4px 4px 10px; font-family: arial;  width:580px;"><tr><td>Pengajuan Penggunaan BTP Ditolak<br><br>

        Berkas Permohonan Nomor '.$rqs["RQS_NO_AJU"].' sudah selesai, surat jawaban harap diambil ke Direktorat Standardisasi Produk Pangan
        <br><br>
        Terima Kasih</td></tr></table>';
                    $esp->main->send_mail('register', $to, $isi, $subject);
                }                
	            $esp->main->get_bol_trans_check($iCounTol);
                if ($esp->main->get_bol_trans_status($iCounTol)) {
                    return "MSG#Proses Berhasil#" . site_url() . "assessment/list_assignment/ass";
                } else {
                    return "MSG#Proses Gagal. Silakan Ulangi Kembali";
                }
			}
		}
	}	
    public function insert_banyak($tabel="",$data="") {
        //field
        $temp = join(",",array_keys($data[1]));
        $field = "(" . $temp . ")";
        $sql = "INSERT INTO $tabel $field VALUES ";

        //value
        foreach($data as $datas) {
            $bersih = str_replace("'", "\'", $datas);
            $test = join("','",$bersih);
            $value .= "('" . $test . "'),";
        }

        $sql .= rtrim($value,",");
        
        $exec = $this->db->query($sql);
        return $exec; //affected_row / jml row
    }


	function assessment_get($sMenu, $iId, $ispreview = FALSE){
		$esp = get_instance();
		$esp->load->model("main","main", true);
		$sUserId = $this->newsession->userdata('USER_ID');
        $ROLE_ID = $this->newsession->userdata('ROLE_ID'); 
        $now = date('Y-m-d H:i:s');
		if($sMenu=="codex"){

			$RQS = $this->db->query("SELECT a.USER_ID,a.RQS_ASS_DATE,a.RQS_ID,a.RQS_BTP_FUNCTION_ID, a.RQS_BTP_TYPE_ID, a.RQS_FOOD_TYPE_ID,b.food_type_no FROM tx_rqs a left join tr_food_type b on a.RQS_FOOD_TYPE_ID=b.FOOD_TYPE_ID where a.RQS_ID = '$iId'")->row_array();
			$arrdata['RQS'] = $RQS;
			$C1 = $this->db->query("SELECT b.food_type_no,a.`FOOD_TYPE_ID`,a.`MAX_LIMIT`,a.`UNIT` FROM tr_codex_one a left join tr_food_type b on a.FOOD_TYPE_ID=b.FOOD_TYPE_ID where a.BTP_TYPE_ID='$RQS[RQS_BTP_TYPE_ID]' ")->result_array();
			$arrdata['C1'] = $C1;

            $trimFoodRqs = TRIM($RQS['food_type_no']);
/*Done = belum dikerjain, kalo no katpang anak tidak ada di codex maka cek dulu no katpang induknya sampai 01.0, misal di submission pilih no katpang 05.2.2, tapi dicodex hanya diatur 5.2 maka no katpang tsb ada di codex karena anak dari indux 5.2*/
            $arrdata['asumsi'] = $this->db->query("SELECT * from tr_asumsi_konsumsi")->result_array();
			$arrdata['button'] = '';

			if(count($C1) <= 0){ //tidak ada
                $C3 = $this->db->query("SELECT `BTP_FUNCTION_NAME` FROM tr_codex_three where BTP_TYPE_ID='$RQS[RQS_BTP_TYPE_ID]' ")->result_array();
                if(count($C3) <= 0){ //tidak ada
                    //echo "ga ada di codex 3==";
                    $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" >Codex tidak mengatur maka perlu <span  style="color: orange">bawa pakar</span> </div>';
                    $arrdata['hasil'] = 'bawa pakar';
                    $RQS = $this->db->query("SELECT a.RQS_DATE_REC,a.USER_ID,a.RQS_ASS_DATE,a.RQS_ID,a.RQS_ABOUT,a.RQS_BTP_FUNCTION_ID, a.RQS_BTP_TYPE_ID, a.RQS_FOOD_TYPE_ID FROM tx_rqs a  WHERE a.RQS_ID = '$iId'")->row_array();
                    $arrdata['RQS'] = $RQS;
                    $RQS_X = $this->db->query("SELECT XPRT_REC_NOTE FROM tx_xprt_note WHERE RQS_ID = '$iId'")->result_array();
                    $arrdata['RQS_X'] = $RQS_X;
                    if(is_null($RQS['RQS_ASS_DATE'])){
                        $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM03'; 
                        $this->db->where('RQS_ID', $iId);
                        $this->db->update('tx_rqs', $arrTxRqs);

                        $arrTlRqsLog['RQS_ID'] = $iId;
                        $arrTlRqsLog['USER_ID'] = $sUserId;
                        $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                        $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                        $esp->main->get_bol_trans_check($iCounTol);
                    }

                }else{
                    //echo "ada di codex 3";
                    $C3A = $this->db->query("SELECT `FOOD_TYPE_ID` FROM tr_codex_annex_three")->result_array();
                    if (in_array($RQS['RQS_FOOD_TYPE_ID'], $C3A)){
                        //echo "ada di annex 3";
                        $arrdata['button'] = '<a href="'.site_url().'assessment/ass/draft-rec/'.$iId.'"  class="btn btn-info">Buat Nota Dinas Hasil Kajian</a>';
                        $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" ><span  style="color: red">Rejected</span> </div>';

                        if(is_null($RQS['RQS_ASS_DATE'])){
                            $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM04'; 
                            $arrTxRqs['RQS_ABOUT'] = 'Penolakan '.$RQS_ABOUT; 
                            $this->db->where('RQS_ID', $iId);
                            $this->db->update('tx_rqs', $arrTxRqs);

                            $arrTlRqsLog['RQS_ID'] = $iId;
                            $arrTlRqsLog['USER_ID'] = $sUserId;
                            $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                            $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                            $esp->main->get_bol_trans_check($iCounTol);
                        }
                    }else{
                        //echo "ga ada di annex 3";
                        $arrdata['button'] = '<a href="'.site_url().'cetak/nodin/'.$iId.'"  class="btn btn-info"><i class="fa fa-download"></i> Download Nota Dinas & Surat Jawaban</a><a href="'.site_url().'assessment/ass/form/'.$iId.'"  class="btn btn-success"><i class="fa fa-pencil-square-o"></i> Buat Nota Dinas & Surat Jawaban</a><BR><BR>';
                        $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" ><span  style="color: green">Disetujui CPPB</span> </div>';

                        if(is_null($RQS['RQS_ASS_DATE'])){
                            $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM01'; 
                            $this->db->where('RQS_ID', $iId);
                            $this->db->update('tx_rqs', $arrTxRqs);

                            $arrTlRqsLog['RQS_ID'] = $iId;
                            $arrTlRqsLog['USER_ID'] = $sUserId;
                            $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                            $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                            $esp->main->get_bol_trans_check($iCounTol);
                        }
                    }
                }
			}else{
                $kp = "belum";
                foreach ($C1 as $key => $value) {
                    $trimFoodCodex = TRIM($value['food_type_no']);
                    //echo $trimFoodCodex."<br>";
                    if($trimFoodRqs == $trimFoodCodex){
                        $kp = "ada";
                        if(is_numeric($value['MAX_LIMIT'])){
                            //echo "hitung paparan";
                            $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" >Codex mengatur batas maksimum '.$value['MAX_LIMIT'].' '.$value['UNIT'].' maka <span  style="color: orange">Hitung Paparan</span> </div>';
                            $arrdata['hasil'] = 'hitung';
                            $RQS = $this->db->query("SELECT a.USER_ID,a.RQS_ASS_DATE,a.RQS_ID,a.RQS_ABOUT,a.RQS_BTP_FUNCTION_ID, a.RQS_BTP_TYPE_ID,  a.RQS_FOOD_TYPE_ID, b.`EXPOSURE_AMOUNT`, b.`EXPOSURE_WEIGHT`, b.`EXPOSURE_CONSUMPTION`, b.`EXPOSURE_ADI`, b.`EXPOSURE_MAX_USAGE` FROM tx_rqs a LEFT JOIN tx_exposure b ON a.`RQS_ID`=b.`RQS_ID` WHERE a.RQS_ID = '$iId'")->row_array();
                            $arrdata['RQS'] = $RQS;
                            $arrdata['asumsi'] = $this->db->query("SELECT * from tr_asumsi_konsumsi")->result_array();

                            if(is_null($RQS['RQS_ASS_DATE'])){
                                $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM02'; 
                                $arrTxRqs['RQS_CODEX_MAX_LIMIT'] = $value['MAX_LIMIT']; 
                                $arrTxRqs['RQS_CODEX_UNIT'] = $value['UNIT']; 

                                $this->db->where('RQS_ID', $iId);
                                $this->db->update('tx_rqs', $arrTxRqs);

                                $arrTlRqsLog['RQS_ID'] = $iId;
                                $arrTlRqsLog['USER_ID'] = $sUserId;
                                $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                $esp->main->get_bol_trans_check($iCounTol);
                            }
                        }else{
                            //echo "CPPB";
                            $arrdata['button'] = '<a href="'.site_url().'cetak/nodin/'.$iId.'"  class="btn btn-info"><i class="fa fa-download"></i> Download Nota Dinas & Surat Jawaban</a><a href="'.site_url().'assessment/ass/form/'.$iId.'"  class="btn btn-success"><i class="fa fa-pencil-square-o"></i> Buat Nota Dinas & Surat Jawaban</a><BR><BR>';
                            $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" >Codex mengatur batas maksimum '.$value['MAX_LIMIT'].' '.$value['UNIT'].' maka <span  style="color: green">Disetujui CPPB==</span> </div>';

                            if(is_null($RQS['RQS_ASS_DATE'])){
                                $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM01'; 
                                $arrTxRqs['RQS_CODEX_MAX_LIMIT'] = $value['MAX_LIMIT']; 
                                $arrTxRqs['RQS_CODEX_UNIT'] = $value['UNIT']; 
                                $this->db->where('RQS_ID', $iId);
                                $this->db->update('tx_rqs', $arrTxRqs);

                                $arrTlRqsLog['RQS_ID'] = $iId;
                                $arrTlRqsLog['USER_ID'] = $sUserId;
                                $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                $esp->main->get_bol_trans_check($iCounTol);
                            }
                        }
                        break;
                    }
                }
				//echo $kp; die(); 
                if($kp == "belum"){
                    $iu=0;
                    foreach ($C1 as $key => $value) {
                        $iu++;
                        $trimFoodCodex = TRIM($value['food_type_no']);
                        $lengthFoodRqs = strlen($trimFoodRqs);
                        $lengthFoodCodex = strlen($trimFoodCodex);
                        if($lengthFoodRqs != $lengthFoodCodex){
                            $a = substr($trimFoodRqs, 0,$lengthFoodCodex);
                            if($trimFoodCodex == $a){
                            //echo $trimFoodCodex."=".$a;
                                $kp = "ada dari induknya";
                                if(is_numeric($value['MAX_LIMIT'])){
                                    //echo "hitung paparan";
                                    $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" >Codex mengatur batas maksimum '.$value['MAX_LIMIT'].' '.$value['UNIT'].' maka <span  style="color: orange">Hitung Paparan</span> </div>';
                                    $arrdata['hasil'] = 'hitung';
                                    $arrdata['asumsi'] = $this->db->query("SELECT * from tr_asumsi_konsumsi")->result_array();

                                    if(is_null($RQS['RQS_ASS_DATE'])){
                                        $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM02'; 
                                        $arrTxRqs['RQS_CODEX_MAX_LIMIT'] = $value['MAX_LIMIT']; 
                                        $arrTxRqs['RQS_CODEX_UNIT'] = $value['UNIT']; 
                                        $this->db->where('RQS_ID', $iId);
                                        $this->db->update('tx_rqs', $arrTxRqs);

                                        $arrTlRqsLog['RQS_ID'] = $iId;
                                        $arrTlRqsLog['USER_ID'] = $sUserId;
                                        $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                        $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                        $esp->main->get_bol_trans_check($iCounTol);
                                    }
                                }else{
                                    //echo "CPPB";
                                    $arrdata['button'] = '<a href="'.site_url().'cetak/nodin/'.$iId.'"  class="btn btn-info"><i class="fa fa-download"></i> Download Nota Dinas & Surat Jawaban</a><a href="'.site_url().'assessment/ass/form/'.$iId.'"  class="btn btn-success"><i class="fa fa-pencil-square-o"></i> Buat Nota Dinas & Surat Jawaban</a> <BR><BR>';
                                    $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" >Codex mengatur batas maksimum '.$value['MAX_LIMIT'].' '.$value['UNIT'].' maka <span  style="color: green">Disetujui CPPB++</span> </div>';
                                    if(is_null($RQS['RQS_ASS_DATE'])){
                                        $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM01'; 
                                        $arrTxRqs['RQS_CODEX_MAX_LIMIT'] = $value['MAX_LIMIT']; 
                                        $arrTxRqs['RQS_CODEX_UNIT'] = $value['UNIT']; 
                                        $this->db->where('RQS_ID', $iId);
                                        $this->db->update('tx_rqs', $arrTxRqs);

                                        $arrTlRqsLog['RQS_ID'] = $iId;
                                        $arrTlRqsLog['USER_ID'] = $sUserId;
                                        $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                        $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                        $esp->main->get_bol_trans_check($iCounTol);
                                    }
                                }
                                break;
                            }else if(count($C1) == $iu){ //jika looping terakhir
                                //no katpang dan induknya ga diatur di codex 

                                $C3 = $this->db->query("SELECT `BTP_FUNCTION_NAME` FROM tr_codex_three where BTP_TYPE_ID='$RQS[RQS_BTP_TYPE_ID]' ")->result_array();
                                if(count($C3) <= 0){ //tidak ada
                                    //echo "ga ada di codex 3";
                                    $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" >Codex tidak mengatur maka perlu <span  style="color: orange">bawa pakar</span> </div>';
                                    $arrdata['hasil'] = 'bawa pakar';
                                    $RQS = $this->db->query("SELECT a.RQS_DATE_REC,a.USER_ID,a.RQS_ASS_DATE,a.RQS_ID,a.RQS_ABOUT,a.RQS_BTP_FUNCTION_ID, a.RQS_BTP_TYPE_ID, a.RQS_FOOD_TYPE_ID FROM tx_rqs a  WHERE a.RQS_ID = '$iId'")->row_array();
                                    $arrdata['RQS'] = $RQS;
                                    $RQS_X = $this->db->query("SELECT XPRT_REC_NOTE FROM tx_xprt_note WHERE RQS_ID = '$iId'")->result_array();
                                    $arrdata['RQS_X'] = $RQS_X;

                                    if(is_null($RQS['RQS_ASS_DATE'])){
                                        $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM03'; 
                                        $this->db->where('RQS_ID', $iId);
                                        $this->db->update('tx_rqs', $arrTxRqs);

                                        $arrTlRqsLog['RQS_ID'] = $iId;
                                        $arrTlRqsLog['USER_ID'] = $sUserId;
                                        $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                        $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                        $esp->main->get_bol_trans_check($iCounTol);
                                    }
                                    break;
                                }else{
                                    //echo "ada di codex 3";
                                    $C3A = $this->db->query("SELECT `FOOD_TYPE_ID` FROM tr_codex_annex_three")->result_array();
                                    if (in_array($RQS['RQS_FOOD_TYPE_ID'], $C3A)){
                                        //echo "ada di annex 3";
                                        $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" >Codex tidak mengatur maka perlu <span  style="color: orange">bawa pakar</span> </div>';
                                        $arrdata['hasil'] = 'bawa pakar';
                                        $RQS = $this->db->query("SELECT a.RQS_DATE_REC,a.USER_ID,a.RQS_ASS_DATE,a.RQS_ID,a.RQS_ABOUT,a.RQS_BTP_FUNCTION_ID, a.RQS_BTP_TYPE_ID, a.RQS_FOOD_TYPE_ID FROM tx_rqs a  WHERE a.RQS_ID = '$iId'")->row_array();
                                        $arrdata['RQS'] = $RQS;
                                        $RQS_X = $this->db->query("SELECT XPRT_REC_NOTE FROM tx_xprt_note WHERE RQS_ID = '$iId'")->result_array();
                                        $arrdata['RQS_X'] = $RQS_X;

                                        if(is_null($RQS['RQS_ASS_DATE'])){
                                            $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM03'; 
                                            $this->db->where('RQS_ID', $iId);
                                            $this->db->update('tx_rqs', $arrTxRqs);
                                            $arrTlRqsLog['RQS_ID'] = $iId;
                                            $arrTlRqsLog['USER_ID'] = $sUserId;
                                            $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                            $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                            $esp->main->get_bol_trans_check($iCounTol);
                                        }
                                        break;
                                    }else{
                                        //echo "ga ada di annex 3";
                                        $arrdata['button'] = '<a href="'.site_url().'cetak/nodin/'.$iId.'"  class="btn btn-info"><i class="fa fa-download"></i> Download Nota Dinas & Surat Jawaban</a><a href="'.site_url().'assessment/ass/form/'.$iId.'"  class="btn btn-success"><i class="fa fa-pencil-square-o"></i> Buat Nota Dinas & Surat Jawaban</a> <BR><BR>';
                                        $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" ><span  style="color: green">Disetujui CPPB--</span> </div>';

                                        if(is_null($RQS['RQS_ASS_DATE'])){
                                            $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM01'; 
                                            $this->db->where('RQS_ID', $iId);
                                            $this->db->update('tx_rqs', $arrTxRqs);

                                            $arrTlRqsLog['RQS_ID'] = $iId;
                                            $arrTlRqsLog['USER_ID'] = $sUserId;
                                            $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                            $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                            $esp->main->get_bol_trans_check($iCounTol);
                                        }

                                        break;
                                    }
                                }

                            }
                        }else if(count($C1) == $iu){ //jika looping terakhir
                            //no katpang dan induknya ga diatur di codex 

                            $C3 = $this->db->query("SELECT `BTP_FUNCTION_NAME` FROM tr_codex_three where BTP_TYPE_ID='$RQS[RQS_BTP_TYPE_ID]' ")->result_array();
                            if(count($C3) <= 0){ //tidak ada
                                //echo "ga ada di codex 3";
                                $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" >Codex tidak mengatur maka perlu <span  style="color: orange">bawa pakar</span> </div>';
                                $arrdata['hasil'] = 'bawa pakar';
                                $RQS = $this->db->query("SELECT a.RQS_DATE_REC,a.USER_ID,a.RQS_ASS_DATE,a.RQS_ID,a.RQS_ABOUT,a.RQS_BTP_FUNCTION_ID, a.RQS_BTP_TYPE_ID, a.RQS_FOOD_TYPE_ID FROM tx_rqs a  WHERE a.RQS_ID = '$iId'")->row_array();
                                $arrdata['RQS'] = $RQS;
                                $RQS_X = $this->db->query("SELECT XPRT_REC_NOTE FROM tx_xprt_note WHERE RQS_ID = '$iId'")->result_array();
                                $arrdata['RQS_X'] = $RQS_X;

                                if(is_null($RQS['RQS_ASS_DATE'])){
                                    $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM03'; 
                                    $this->db->where('RQS_ID', $iId);
                                    $this->db->update('tx_rqs', $arrTxRqs);

                                    $arrTlRqsLog['RQS_ID'] = $iId;
                                    $arrTlRqsLog['USER_ID'] = $sUserId;
                                    $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                    $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                    $esp->main->get_bol_trans_check($iCounTol);
                                }
                                break;
                            }else{
                                //echo "ada di codex 3";
                                $C3A = $this->db->query("SELECT `FOOD_TYPE_ID` FROM tr_codex_annex_three")->result_array();
                                if (in_array($RQS['RQS_FOOD_TYPE_ID'], $C3A)){
                                    //echo "ada di annex 3";
                                    $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" >Codex tidak mengatur maka perlu <span  style="color: orange">bawa pakar</span> </div>';
                                    $arrdata['hasil'] = 'bawa pakar';
                                    $RQS = $this->db->query("SELECT a.RQS_DATE_REC,a.USER_ID,a.RQS_ASS_DATE,a.RQS_ID,a.RQS_ABOUT,a.RQS_BTP_FUNCTION_ID, a.RQS_BTP_TYPE_ID, a.RQS_FOOD_TYPE_ID FROM tx_rqs a  WHERE a.RQS_ID = '$iId'")->row_array();
                                    $arrdata['RQS'] = $RQS;
                                    $RQS_X = $this->db->query("SELECT XPRT_REC_NOTE FROM tx_xprt_note WHERE RQS_ID = '$iId'")->result_array();
                                    $arrdata['RQS_X'] = $RQS_X;

                                    if(is_null($RQS['RQS_ASS_DATE'])){
                                        $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM03'; 
                                        $this->db->where('RQS_ID', $iId);
                                        $this->db->update('tx_rqs', $arrTxRqs);
                                        $arrTlRqsLog['RQS_ID'] = $iId;
                                        $arrTlRqsLog['USER_ID'] = $sUserId;
                                        $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                        $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                        $esp->main->get_bol_trans_check($iCounTol);
                                    }
                                    break;
                                }else{
                                    //echo "ga ada di annex 3";
                                    $arrdata['button'] = '<a href="'.site_url().'cetak/nodin/'.$iId.'"  class="btn btn-info"><i class="fa fa-download"></i> Download Nota Dinas & Surat Jawaban</a><a href="'.site_url().'assessment/ass/form/'.$iId.'"  class="btn btn-success"><i class="fa fa-pencil-square-o"></i> Buat Nota Dinas & Surat Jawaban</a> <BR><BR>';
                                    $arrdata['result'] = '<div class="col-md-6 col-sm-6 col-xs-6" ><span  style="color: green">Disetujui CPPB--</span> </div>';

                                    if(is_null($RQS['RQS_ASS_DATE'])){
                                        $arrTxRqs['RQS_ASSESSMENT_MARK'] = 'RM01'; 
                                        $this->db->where('RQS_ID', $iId);
                                        $this->db->update('tx_rqs', $arrTxRqs);

                                        $arrTlRqsLog['RQS_ID'] = $iId;
                                        $arrTlRqsLog['USER_ID'] = $sUserId;
                                        $arrTlRqsLog['RQS_LOG_DESC_ID'] = '303';
                                        $this->db->insert('tl_rqs_log', $arrTlRqsLog);
                                        $esp->main->get_bol_trans_check($iCounTol);
                                    }

                                    break;
                                }
                            }
                        }
                    }
                }
//                echo $kp;
//                echo "<pre>"; print_r($C1); echo "</pre>"; die();

            $arrdata['arrTxRqsDoc12'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='12'")->row_array();
            $arrdata['arrTxRqsDoc13'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='13'")->row_array();
			}
        }else if($sMenu=="letter_number"){
            $RQS = $this->db->query("SELECT * FROM tx_rqs a WHERE a.RQS_ID = '$iId'")->row_array();
            $arrdata['RQS'] = $RQS;
		}else if($sMenu=="draft-rec"){
			$arrdata['arrTxRqs'] = $this->db->query("SELECT m.TRADER_TYPE,l.USER_NAME as ASS_BY, k.USER_NAME as VER_BY ,g.USER_NAME,g.USER_ADDR,j.`STATUS_DETAIL` as 'STAT',i.`STATUS_DETAIL` as 'CERTIFICATION_STATUS',h.`STATUS_DETAIL` as 'JENIS_P',f.`STATUS_DETAIL`,c.`BTP_TYPE_NO`,d.`FOOD_TYPE_NAME`, d.`FOOD_TYPE_NO`,c.`BTP_TYPE_NAME`,b.`BTP_FUNCTION_NAME`,a.* FROM tx_rqs a 
LEFT JOIN tr_btp_function b ON a.`RQS_BTP_FUNCTION_ID` = b.`BTP_FUNCTION_ID` 
LEFT JOIN tr_btp_type c ON a.`RQS_BTP_TYPE_ID`=c.`BTP_TYPE_ID`
LEFT JOIN TR_FOOD_TYPE d ON a.`RQS_FOOD_TYPE_ID` = d.`FOOD_TYPE_ID` 
LEFT JOIN TM_USER g ON a.`USER_ID` = G.`USER_ID` 
LEFT JOIN TR_STATUS f ON G.`USER_TYPE_ID` = f.`STATUS_ID` 
LEFT JOIN TR_STATUS h ON a.`RQS_FAC_TYPE_ID` = h.`STATUS_ID` 
LEFT JOIN TR_STATUS i ON a.`RQS_CTF_STATUS` = i.`STATUS_ID` 
LEFT JOIN TR_STATUS j ON a.`RQS_STATUS_ID` = j.`STATUS_ID` 
LEFT JOIN tm_user k ON a.`RQS_VER_BY`=k.`USER_ID` 
LEFT JOIN tm_user l ON a.`RQS_ASS_BY`=l.`USER_ID` 
LEFT JOIN tm_user_com m ON a.`USER_ID`=m.`USER_ID` 
WHERE a.`RQS_ID` = $iId ")->row_array();

			if($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY01'){
				$arrdata['arrTxRqsDln'] = $this->db->query("SELECT a.* FROM tx_rqs_dln a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY02'){
				$arrdata['arrTxRqsPckg'] = $this->db->query("SELECT a.* FROM tx_rqs_pckg a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY03'){
				$arrdata['arrTxRqsLcns'] = $this->db->query("SELECT a.* FROM tx_rqs_lcns a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY04'){
				$arrdata['arrTxRqsImpr'] = $this->db->query("SELECT a.* FROM tx_rqs_impr a WHERE a.`RQS_ID` = $iId ")->row_array();
			}

			$arrdata['arrTxRqsBtpComp'] = $this->db->query("SELECT a.* FROM TX_RQS_BTP_COMP a WHERE a.`RQS_ID` = $iId and a.rqs_btp_comp_status_id = 'COS1' ")->result_array();
			$arrdata['arrTxRqsFoodComp'] = $this->db->query("SELECT a.* FROM TX_RQS_FOOD_COMP a WHERE a.`RQS_ID` = $iId and a.rqs_food_comp_status_id = 'COS1' ")->result_array();
			$arrdata['arrTxRqsPrdRegDoc'] = $this->db->query("SELECT d.`STATUS_DETAIL`,a.* FROM tx_rqs_prd_reg_doc a left join tr_status d on a.RQS_PRD_REG_DOC_STATUS_ID=d.STATUS_ID WHERE a.`RQS_ID` = $iId and a.rqs_prd_reg_doc_status_id = 'UPS02' ")->result_array();
			
			$arrdata['arrTxRqsDoc05'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='05'")->row_array();
			$arrdata['arrTxRqsDoc06'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='06'")->row_array();
			$arrdata['arrTxRqsDoc07'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='07'")->row_array();
			$arrdata['arrTxRqsDoc08'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='08'")->row_array();
			$arrdata['arrTxRqsDoc09'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='09'")->row_array();
			$arrdata['arrTxRqsDoc11'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='11'")->row_array();
            $arrdata['arrTxRqsDoc12'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='12'")->row_array();
            $arrdata['arrTxRqsDoc13'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='13'")->row_array();

		    $countlog = "SELECT RQS_LOG_DESC_ID FROM tl_rqs_log WHERE RQS_ID  = '$iId' ";
		    $arrdata['jumlahlog'] = $this->db->query($countlog)->num_rows();
            if($ROLE_ID == "603" || $ROLE_ID == "604"){
            	$arrdata['title'] = "BUAT DRAFT REKOMENDASI";
            }else{
            	$arrdata['title'] = "REVIEW DRAFT REKOMENDASI";
            }

        }else if($sMenu=="form"){
            $RQS = $this->db->query("SELECT * FROM tx_rqs a WHERE a.RQS_ID = '$iId'")->row_array();
            $arrdata['RQS'] = $RQS;
            $RQS_L = $this->db->query("SELECT * FROM tx_rqs_letter a WHERE a.RQS_ID = '$iId'")->row_array();
            $arrdata['RQS_L'] = $RQS_L;

            $arrdata['arrTxRqsBtpComp'] = $this->db->query("SELECT a.* FROM TX_RQS_LETTER_SPEC a WHERE a.`RQS_ID` = $iId  ")->result_array();
            if($RQS_L['RQS_ID'] == ''){
            	$arrdata['action'] = "save";
            }else{
            	$arrdata['action'] = "edit";
            }
            $arrdata['arrTxRqsDoc12'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='12'")->row_array();
            $arrdata['arrTxRqsDoc13'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='13'")->row_array();

		}else if($sMenu=="detail"){ 
            if($ROLE_ID == "603" || $ROLE_ID == "604"){
	            $arrdata['sTitle'] = 'ASSESSMENT >> ASSIGNMENT >> DAFTAR ASSIGNMENT >> <span style="color: blue"> DETAIL ASSIGNMENT </span>';
	            $arrdata['back'] = "assessment/list_assignment/ass";
            }else if($ROLE_ID == "698"){
                $arrdata['sTitle'] = 'ASSESSMENT >> <span style="color: blue"> DETAIL ASSIGNMENT </span>';
                $arrdata['back'] = "assessment/list_assignment/ass";
            } else {
	            $arrdata['sTitle'] = 'BELUM DISET';
	            $arrdata['back'] = "assessment";
            }
			$arrdata['arrTxRqs'] = $this->db->query("SELECT m.TRADER_TYPE,l.USER_NAME as ASS_BY, k.USER_NAME as VER_BY ,g.USER_NAME,g.USER_ADDR,j.`STATUS_DETAIL` as 'STAT',i.`STATUS_DETAIL` as 'CERTIFICATION_STATUS',h.`STATUS_DETAIL` as 'JENIS_P',f.`STATUS_DETAIL`,c.`BTP_TYPE_NO`,d.`FOOD_TYPE_NAME`, d.`FOOD_TYPE_NO`,c.`BTP_TYPE_NAME`,b.`BTP_FUNCTION_NAME`,a.* FROM tx_rqs a 
LEFT JOIN tr_btp_function b ON a.`RQS_BTP_FUNCTION_ID` = b.`BTP_FUNCTION_ID` 
LEFT JOIN tr_btp_type c ON a.`RQS_BTP_TYPE_ID`=c.`BTP_TYPE_ID`
LEFT JOIN TR_FOOD_TYPE d ON a.`RQS_FOOD_TYPE_ID` = d.`FOOD_TYPE_ID` 
LEFT JOIN TM_USER g ON a.`USER_ID` = G.`USER_ID` 
LEFT JOIN TR_STATUS f ON G.`USER_TYPE_ID` = f.`STATUS_ID` 
LEFT JOIN TR_STATUS h ON a.`RQS_FAC_TYPE_ID` = h.`STATUS_ID` 
LEFT JOIN TR_STATUS i ON a.`RQS_CTF_STATUS` = i.`STATUS_ID` 
LEFT JOIN TR_STATUS j ON a.`RQS_STATUS_ID` = j.`STATUS_ID` 
LEFT JOIN tm_user k ON a.`RQS_VER_BY`=k.`USER_ID` 
LEFT JOIN tm_user l ON a.`RQS_ASS_BY`=l.`USER_ID` 
LEFT JOIN tm_user_com m ON a.`USER_ID`=m.`USER_ID` 
WHERE a.`RQS_ID` = $iId ")->row_array();

			if($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY01'){
				$arrdata['arrTxRqsDln'] = $this->db->query("SELECT a.* FROM tx_rqs_dln a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY02'){
				$arrdata['arrTxRqsPckg'] = $this->db->query("SELECT a.* FROM tx_rqs_pckg a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY03'){
				$arrdata['arrTxRqsLcns'] = $this->db->query("SELECT a.* FROM tx_rqs_lcns a WHERE a.`RQS_ID` = $iId ")->row_array();
			}else if ($arrdata['arrTxRqs']['RQS_FAC_TYPE_ID'] == 'FTY04'){
				$arrdata['arrTxRqsImpr'] = $this->db->query("SELECT a.* FROM tx_rqs_impr a WHERE a.`RQS_ID` = $iId ")->row_array();
			}

			$arrdata['arrTxRqsBtpComp'] = $this->db->query("SELECT a.* FROM TX_RQS_BTP_COMP a WHERE a.`RQS_ID` = $iId and a.rqs_btp_comp_status_id = 'COS1' ")->result_array();
			$arrdata['arrTxRqsFoodComp'] = $this->db->query("SELECT a.* FROM TX_RQS_FOOD_COMP a WHERE a.`RQS_ID` = $iId and a.rqs_food_comp_status_id = 'COS1' ")->result_array();
			$arrdata['arrTxRqsPrdRegDoc'] = $this->db->query("SELECT d.`STATUS_DETAIL`,a.* FROM tx_rqs_prd_reg_doc a  left join tr_status d on a.RQS_PRD_REG_DOC_STATUS_ID=d.STATUS_ID WHERE a.`RQS_ID` = $iId and a.rqs_prd_reg_doc_status_id = 'UPS02' ")->result_array();
			
			$arrdata['arrTxRqsDoc05'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='05'")->row_array();
			$arrdata['arrTxRqsDoc06'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='06'")->row_array();
			$arrdata['arrTxRqsDoc07'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='07'")->row_array();
			$arrdata['arrTxRqsDoc08'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='08'")->row_array();
			$arrdata['arrTxRqsDoc09'] = $this->db->query("SELECT b.`STATUS_DETAIL`,a.* FROM tx_rqs_doc a LEFT JOIN tr_status b ON a.`RQS_DOC_STATUS_ID`=b.`STATUS_ID` WHERE a.`RQS_ID` = $iId and a.rqs_doc_status_id = 'UPS02' and a.document_id='09'")->row_array();
            $countlog = "SELECT RQS_LOG_DESC_ID FROM tl_rqs_log WHERE RQS_ID  = '$iId' ";
            $arrdata['jumlahlog'] = $this->db->query($countlog)->num_rows();

		}
		return $arrdata;
	}

	function list_log($iId = "", $ispreview = FALSE){

        $query = "SELECT A.RQS_ID,A.RQS_LOG_DATE AS TANGGAL, CONCAT('<b>',B.USER_NAME,'</b><br>',A.RQS_LOG_DATE) AS 'OLEH',CONCAT('<b>',C.LOG_DESC,'</b>') AS 'KEGIATAN',A.RQS_LOG_NOTE AS 'CATATAN'
FROM TL_RQS_LOG A 
LEFT JOIN TM_USER B ON A.USER_ID = B.USER_ID 
LEFT JOIN TR_LOG_DESC C ON A.RQS_LOG_DESC_ID=C.LOG_DESC_ID

WHERE A.RQS_ID  = '$iId' "; //LEFT JOIN TR_STATUS E ON D.RQS_STATUS_ID=E.STATUS_ID
        $jumlah = $this->db->query($query)->num_rows();
        $this->load->library('newtable');
        $this->newtable->hiddens(array('TANGGAL', 'RQS_ID'));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($this->uri->segment_array());
        $this->newtable->orderby(1);
        $this->newtable->sortby('DESC');
        $this->newtable->keys(array("RQS_ID"));
        if ($ispreview) {
            $this->newtable->rowcount("ALL");
            $this->newtable->show_chk(FALSE);
            $this->newtable->show_search(FALSE);
        }
        $this->newtable->columns(array("OLEH", "KEGIATAN"));
        $this->newtable->use_ajax(TRUE);
        $this->newtable->js_file('<script type="text/javascript" src="' . base_url() . 'js/newtable.js"></script>');

        $tabel = $this->newtable->generate($query);

        if ($ispreview) {
            $tabel = "<script type=\"text/javascript\" src=\"" . base_url() . "js/newtable.js\"></script>" . $tabel;
            $arrdata = $tabel;
        } else {
            $arrdata = array("htmltable" => $tabel,
                "jmltable" => $jumlah,
                "sTitle" => $sTitle);
        }
        return $arrdata;
	}

}
?>