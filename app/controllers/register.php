<?php if(!defined('BASEPATH'))exit('No direct script access allowed'); 
class Register extends Controller{ 
	var $content = "";	
	function Home(){
		parent::Controller();
	}

	function index(){

		if($this->newsession->userdata('LOGGED_IN')){
			if($this->content==""){
				$gambar = TRUE;
			}
			$sUserid = $this->newsession->userdata('USER_ROLE');
			$this->menu = $this->load->view('in/menu', '', true);
			$data = array('_content_' => $this->load->view('menu/welcome', '', true),
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('in/footer', '', true),
						  '_header_' => $this->load->view('in/header', '', true));
			$this->parser->parse('in/home', $data);
		}else{
			
			$gambar = FALSE;			
			$this->load->model('login_act');
			
			if($this->content==""){
				$this->newsession->sess_destroy();
				$this->content = $this->load->view('login', $arrdata, true);
				
				$gambar = TRUE;
			}

			$this->menu = $this->load->view('out/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('out/footer', '', true),
						  'gambar' => $gambar,
						  'captcha' => $arrdata['captcha'],
						  '_header_' => $this->load->view('out/header', '', true));
			$this->parser->parse('out/home', $data);
			
		}
	}
	
	public function reg_person(){
		$this->load->model("out/register_act");
		$arrData = $this->register_act->get_registrasi();
		$this->content = $this->load->view('out/frm-reg-person', $arrData, true);
		$this->index();                   

	}
	public function new_reg($sMenu="", $iId=""){  //print_r($_SESSION); die();
		if($this->newsession->userdata('USER_ID') != "" && $this->newsession->userdata('USER_ID') != $iId){
			$iId = $this->newsession->userdata('USER_ID');
		}
		$this->load->model("out/register_act");
		$arrData = $this->register_act->get_registrasi($sMenu, $iId);
		$this->content = $this->load->view('out/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	function post($sMenu="", $sSubmenu="", $sSubmenu2=""){
		if($sMenu == "person"){
			$this->load->model('out/register_act');
			$sRet = $this->register_act->set_registrasi($sMenu, $sSubmenu, $sSubmenu2);
		}else if($sMenu == "business"){
			$this->load->model('out/register_act');
			$sRet = $this->register_act->set_registrasi($sMenu, $sSubmenu, $sSubmenu2);
		}
		$this->load->model('out/register_act');
		$sRet = $this->register_act->set_register($sMenu, $sSubmenu, $sSubmenu2);
		echo $sRet;
	}

    public function email_exists_check() {
        $sMail = $this->input->post('email');
        $query = $this->db->query("select USER_MAIL from TM_USER where USER_MAIL = '$sMail' limit 1 ")->row_array();
        echo "0|||Email Tersedia"; //json_encode(TRUE);
        // if ($query['USER_MAIL'] == '') {
        //     echo "0|||Email Tersedia"; //json_encode(TRUE);
        // } else {
        //     echo "1|||Email ".$this->input->post('email')." Sudah digunakan"; //json_encode(TRUE);
        // }
    }
    public function chk_npwp() {
        $npwp = $this->input->post('npwp');

        $where = "USER_NPWP = '$npwp' and user_status_id= 'US01' ";
        $this->db->where($where);
        $queryCek = $this->db->get('TM_USER');
        if ($queryCek->num_rows() > 0) {
            echo "0|||NPWP sudah digunakan";
        } else {
            echo "1|||";
        }
    }
    public function chk_username() {
        $usr = $this->input->post('usr');

        $this->db->where('USER_USERNAME', $usr);
        $queryCek = $this->db->get('TM_USER');
        if ($queryCek->num_rows() > 0) {
            echo "0|||Username sudah digunakan";
        } else {
            echo "1|||";
        }
    }

	
}