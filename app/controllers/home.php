<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Controller{
	var $content = "";
	
	function Home(){
		parent::Controller();
	}

	function index(){

		if($this->newsession->userdata('LOGGED_IN')){
			if($this->content==""){
				$gambar = TRUE;
			}
			$sUserid = $this->newsession->userdata('USER_ROLE');
			$this->menu = $this->load->view('in/menu', '', true);
			$data = array('_content_' => $this->load->view('menu/welcome', '', true),
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('in/footer', '', true),
						  '_header_' => $this->load->view('in/header', '', true));
			$this->parser->parse('in/home', $data);
		}else{
			
			$gambar = FALSE;			
			$this->load->model('login_act');

			if($this->content==""){
				$this->newsession->sess_destroy();
				$this->content = $this->load->view('login', $arrdata, true);
				
				$gambar = TRUE;
			}

			$this->menu = $this->load->view('out/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('out/footer', '', true),
						  'gambar' => $gambar,
						  'captcha' => $arrdata['captcha'],
						  '_header_' => $this->load->view('out/header', '', true));
			$this->parser->parse('out/home', $data);
			
		}
	}

	function scheduler($sMenu="",$id=""){
		//1. cek draft yg disimpan, setelah 3 hari maka status_id jd archive
		//2. perbaikan dari verifikator, setelah 5 hari maka status_id jd archive
	}

	function role($sMenu="",$id=""){
		$arrLog['USER_ID']= $this->newsession->userdata('USER_ID');
		$arrLog['USER_LOG_DESC_ID']= "202";
		$this->db->insert('tl_user_log',$arrLog);	
		
		$sql = "SELECT A.*,B.ROLE_ID, C.`ROLE_NAME` FROM tm_user A
				LEFT JOIN TS_USER_ROLE B ON A.USER_ID=B.USER_ID
				LEFT JOIN TR_ROLE C ON C.ROLE_ID=B.ROLE_ID
				WHERE A.USER_ID = $id "; 
		$arrData = $this->db->query($sql)->row_array();
		$id2 = $arrData['USER_ID'];

		$sqlMenu = "SELECT E.`MENU_NAME`, D.MENU_ITEM_NAME,D.MENU_ITEM_LINK,D.MENU_ITEM_ORDER 
					FROM  TS_PERMISSION C 
					LEFT JOIN TR_MENU_ITEM D ON C.MENU_ITEM_ID=D.MENU_ITEM_ID
					LEFT JOIN TR_MENU E ON E.`MENU_ID`=D.`MENU_ID`
					WHERE C.ROLE_ID='$arrData[ROLE_ID]' AND D.`MENU_ITEM_STATUS`=1 AND E.`MENU_STATUS`=1 ORDER BY E.`MENU_ORDER`,D.`MENU_ITEM_ORDER`";
		$arrData['MENU'] = $this->db->query($sqlMenu)->result_array();
		$arrData['LOGGED_IN'] = true;
		
		$sql_log = $this->db->query("SELECT USER_LOG_DATE FROM TL_USER_LOG WHERE USER_ID='$id2' AND USER_LOG_DESC_ID ='201' ORDER BY USER_LOG_ID DESC LIMIT 1")->row_array();
		$arrData['USER_LOG_DATE'] = $sql_log['USER_LOG_DATE'];

		
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){
					 $ip=$_SERVER['HTTP_CLIENT_IP'];
			//Is it a proxy address
		}else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
					 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
				 $ip=$_SERVER['REMOTE_ADDR'];
		}				
		$arrData['USER_IP'] = $ip;
		$arrData['USER_LOGIN_DATE'] = date("Y-m-d H:i:s");
		$this->newsession->set_userdata($arrData);
		$arrLog['USER_ID']= $arrData['USER_ID'];
		$arrLog['USER_LOG_DESC_ID']= "201";
		$this->db->insert('tl_user_log',$arrLog);	

		print('<script>alert("Ganti Role Berhasil");location.href = "'.site_url().'";</script>');
	
	}

	function upload($sMenu=""){
		$iUserId	= $sMenu;
		$files      = $_FILES['files'];
		$fileParts  = pathinfo($files['name']);
	//			print_r($fileParts);
		$newFile    = $iUserId.'-'.$_POST['kdDok'].'-'.date('YmdHis').'.'.$fileParts['extension']; 
		$allowext   = explode(';',$_POST['format']);
		$path       = 'docs/reg/';
		$location   = $path.date('Y')."/".date('m')."/".$iUserId."/".$newFile;

		$kd = $_POST['kdDok'];
		$rqs_id = $_POST['rqs_id'];
		$urutan = $_POST['urutan'];
		if($kd == '01' || $kd == '02' || $kd == '03' || $kd == '04'){
	        $arrDokHapus = $this->db->query("SELECT USER_DOC_PATH FROM TM_USER_DOC WHERE USER_ID = $iUserId AND DOCUMENT_ID = '$kd' AND USER_DOC_STATUS_ID = 'UPS02' ")->row_array();
	        unlink($arrDokHapus['USER_DOC_PATH']);
		}else if($kd == '05' || $kd == '06' || $kd == '07' || $kd == '08' || $kd == '09'){
	        $arrDokHapus = $this->db->query("SELECT RQS_DOC_PATH FROM TX_RQS_DOC WHERE RQS_ID = '$rqs_id' AND DOCUMENT_ID = '$kd' AND RQS_DOC_STATUS_ID = 'UPS02'")->row_array();
	        unlink($arrDokHapus['RQS_DOC_PATH']);
		}else if($kd == '10'){
	        $arrDokHapus = $this->db->query("SELECT RQS_PRD_REG_DOC_PATH FROM tx_rqs_prd_reg_doc WHERE RQS_ID = '$rqs_id' AND DOCUMENT_ID = '$kd' AND RQS_PRD_REG_DOC_STATUS_ID = 'UPS02' and RQS_PRD_REG_ORDER= '$urutan'")->row_array();
	        //echo $_POST['urutan']; die('sd');
	        unlink($arrDokHapus['RQS_PRD_REG_DOC_PATH']);
		}

		if(is_dir(str_replace('sys/','',BASEPATH).$path.date('Y'))){
			if(is_dir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m'))){
				if(is_dir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m')."/".$iUserId)){
				}else{
					mkdir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m')."/".$iUserId,0777,TRUE) ;
				}
			}else{
				mkdir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m'),0777,TRUE) ;
				if(is_dir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m')."/".$iUserId)){
				}else{
					mkdir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m')."/".$iUserId,0777,TRUE) ;
				}
			}
		}else{
			mkdir(str_replace('sys/','',BASEPATH).$path.date('Y'),0777,TRUE) ;
			if(is_dir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m'))){
				if(is_dir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m')."/".$iUserId)){
				}else{
					mkdir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m')."/".$iUserId,0777,TRUE) ;
				}
			}else{
				mkdir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m'),0777,TRUE) ;
				if(is_dir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m')."/".$iUserId)){
				}else{
					mkdir(str_replace('sys/','',BASEPATH).$path.date('Y')."/".date('m')."/".$iUserId,0777,TRUE) ;
				}
			}

		}
		if(in_array(strtolower($fileParts['extension']), $allowext)){
	//			if(@filesize($files['tmp_name'])>$_POST['maxsize']){
	//			echo @filesize($files['tmp_name'])."=".$_POST['maxsize']."-".$files['size']; print_r($_FILES); die();
			if($files['size']>$_POST['maxsize']){
				$status = 'false';
				$error  = 'Besar File Maksimal 5MB!';				
			}else{
				if(move_uploaded_file($files['tmp_name'], $location)){
					$status = 'true';
				}else{
					$status = 'false';
					$error  = 'Upload File Gagal!';
				}    			
			}
		}else{
			$status = 'false';
			$error  = 'Format File Tidak Diperbolehkan!';
		}
		$url = site_url()."download/data/".base64_encode($location)."/".$_FILES['files']['name'];
		echo '{"status":"'.$status.'", "error":"'.$error.'", "path":"'.$location.'", "urll":"'.$url.'", "ext":"'.$fileParts['extension'].'", "rename":"'.$newFile.'", "name":"'.$_FILES['files']['name'].'", "filename":"'.$fileParts['filename'].'", "size":"'.$_FILES['files']['size'].'"}';

	}

	function register($sId="", $sFactid = "", $sId2 = ""){
		$a=$this->newsession->userdata('TRADER_ID');
		$this->load->model('register_act');
		if($sId == "1"){

			$this->newsession->sess_destroy();
		}elseif($sId == "2" && $this->newsession->userdata('TRADER_ID')==''){
			redirect(site_url());
			die();
		}elseif($sId == "2A" && $this->newsession->userdata('TRADER_ID')==''){
			redirect(site_url());
			die();
		}elseif($sId == "2B" && $this->newsession->userdata('TRADER_ID')==''){
			redirect(site_url());
			die();
		}elseif($sId == "3" && $this->newsession->userdata('TRADER_ID')==''){
			redirect(site_url());
			die();
			
		}
		
		$arrData = $this->register_act->get_arr_register($sId,$sFactid,$sId2);
		$arrData['sFactid'] = $sFactid;
		
		$this->content = $this->load->view('register/'.$sId, $arrData, true);	
		$this->index();
	}

	function login($sId=""){

		$this->load->model('register_act');
		
		$arrData = $this->register_act->get_arr_register($sId);
		
		$this->content = $this->load->view('logins', $arrData, true);	
		$this->index();
	}

	function depan($sMenu="", $iId = ""){	
		if($iId != "" && $sMenu =="berita"){
			$this->load->model('login_act');
			$arrData['news'] = $this->login_act->get_arr_news($iId);			
			$this->content = $this->load->view('menu/informasi/'.$sMenu, $arrData, true);	
		}else if($sMenu =="more"){
			$this->load->model('login_act');
			$arrData['news'] = $this->login_act->get_more();
			$this->content = $this->load->view('menu/informasi/'.$sMenu, $arrData, true);	
		}else{
			$this->content = $this->load->view('menu/informasi/'.$sMenu, '', true);	
		}
		$this->index();
	}

	function post($sMenu="", $sSubmenu="", $sSubmenu2=""){ 
		if($sMenu == "register"){
			$this->load->model('register_act');
			$sRet = $this->register_act->set_register($sSubmenu, $sSubmenu2);
		}else if($sMenu == "logout"){
			$this->load->model('login_act');
			$sRet = $this->login_act->set_logout($sSubmenu,$sSubmenu2);
		}else if($sMenu == "login"){		
			$this->load->model('login_act');
			$sRet = $this->login_act->set_login($sSubmenu);
		}else if($sMenu == "forgot"){		
			$this->load->model('login_act');
			$sRet = $this->login_act->set_forgot($sSubmenu);
		}
		echo $sRet;
	}


	function user($sMenu=""){
		$this->content = "";
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
		if($sMenu=="data"){ #Ubah Profil
			$this->content = $this->load->view('user/profil', '', true);
		}else if($sMenu=="password"){ #Ubah Password
			$this->content = $this->load->view('user/password', '', true);
		}
		$this->index();
	}

	
	
	function logout(){
		$iId = $this->newsession->userdata('USER_ID');
		if($iId != ''){
			if (!empty($_SERVER['HTTP_CLIENT_IP'])){
						 $ip=$_SERVER['HTTP_CLIENT_IP'];
				//Is it a proxy address
			}else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
						 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
			}else{
					 $ip=$_SERVER['REMOTE_ADDR'];
			}
			$arrLog['USER_ID']= $iId;
			$arrLog['USER_LOG_DESC_ID']= "202";
			$arrLog['USER_LOG_IP']= $ip;
			$this->db->insert('tl_user_log',$arrLog);	
			$this->db->query("UPDATE tm_user set USER_LOGIN = '0'  where USER_ID = '$iId' ");
		}
		$this->newsession->sess_destroy();
		$url = explode('/', base_url());
		$this->load->helper('url');
		redirect(base_url());  
	}



}
?>