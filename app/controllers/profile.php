<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends Controller{
	var $content = "";
	
	function Profile(){
		parent::Controller();
	}
	
	function index(){

		if($this->newsession->userdata('LOGGED_IN')){
			if($this->content==""){
				$gambar = TRUE;
			}
			$sUserid = $this->newsession->userdata('USER_ROLE');
			$this->menu = $this->load->view('in/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('in/footer', '', true),
						  '_header_' => $this->load->view('in/header', '', true));
			$this->parser->parse('in/home', $data);
		}else{
			
			$gambar = FALSE;			
			$this->load->model('login_act');
			
			if($this->content==""){
				$this->newsession->sess_destroy();
				$this->content = $this->load->view('login', $arrdata, true);
				
				$gambar = TRUE;
			}

			$this->menu = $this->load->view('out/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('out/footer', '', true),
						  'gambar' => $gambar,
						  'captcha' => $arrdata['captcha'],
						  '_header_' => $this->load->view('out/header', '', true));
			$this->parser->parse('out/home', $data);
			
		}
	}


	public function account($sMenu="", $iId=""){ 
		$this->load->model("in/profile_act"); 
		if($sMenu == "frm-password"){
			$arrData = $this->profile_act->get_account($sMenu, $iId);
			$this->content = $this->load->view('in/profile/'.$sMenu, $arrData, true);
		} else if($sMenu == "frm-profile"){
			$arrData = $this->profile_act->get_account($sMenu, $iId);
			$this->content = $this->load->view('in/profile/'.$sMenu, $arrData, true);
		}
		
		$this->index();                   
	}

	function post($sMenu="", $sSubmenu="", $sSubmenu2=""){
		$this->load->model('in/profile_act');
		if($sMenu == "frm-password") {
			$sRet = $this->profile_act->set_account($sMenu, $sSubmenu, $sSubmenu2);
			echo $sRet;
		} else if($sMenu == "frm-profile") {
			$sRet = $this->profile_act->set_account($sMenu, $sSubmenu, $sSubmenu2);
			echo $sRet;
		}
	}
	
}
?>