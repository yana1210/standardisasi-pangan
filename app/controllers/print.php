<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Print extends Controller{

	var $content = "";
	
	function print(){
		parent::Controller();
	}
	
	
    function index(){

		if($this->newsession->userdata('LOGGED_IN')){
			if($this->content==""){
				$gambar = TRUE;
			}
			$sUserid = $this->newsession->userdata('USER_ROLE');
			$this->menu = $this->load->view('in/menu', '', true);
			$data = array('_content_' => $this->load->view('menu/welcome', '', true),
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('in/footer', '', true),
						  '_header_' => $this->load->view('in/header', '', true));
			$this->parser->parse('in/home', $data);
		}else{
			
			$gambar = FALSE;			
			$this->load->model('login_act');
			
			if($this->content==""){
				$this->newsession->sess_destroy();
				$this->content = $this->load->view('login', $arrdata, true);
				
				$gambar = TRUE;
			}

//			$this->menu = $this->load->view('menu/out', '', true);
			$this->menu = $this->load->view('out/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('out/footer', '', true),
						  'gambar' => $gambar,
						  'captcha' => $arrdata['captcha'],
						  '_header_' => $this->load->view('out/header', '', true));
			$this->parser->parse('out/home', $data);
			
		}
	}
	
	function guide() {
    //if ($this->newsession->userdata('_LOGGED')) {
      
	  $file = str_replace('core/', '', BASEPATH) . 'data/guide/manual.pdf';
	  //$file = str_replace('core/', '', BASEPATH) . 'data/e-filing/' . $folder . "/" . $file;
		$imginfo = getimagesize($file);
			header("Content-type: $imginfo[mime]");
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
			exit;
		//} else {
			//redirect(base_url());
		//}
 	}


    function manual($iId){
		if($this->newsession->userdata('_LOGGED')){
			ob_end_clean();
			$this->load->library('mpdf');
			//$this->load->model('print/print_act');
			$mpdf=new mPDF('utf-8', 'A4-L');
			$mpdf->AddPage('P', // L - landscape, P - portrait
            '', '', '', '',
            15, // margin_left
            15, // margin right
            16, // margin top
            16, // margin bottom
            9, // margin header
            9); // margin footer
			$mpdf->useOnlyCoreFonts = true;
			$mpdf->SetProtection(array('print'));
			$mpdf->SetAuthor("Inspektorat - BPOM");
			$mpdf->SetDisplayMode('fullpage');
			//$arrdata = $this->print_act->get_data("audit", $id);
			$html = $this->load->view('in/print/print-manual', $arrdata, true);
			$pdfFilePath = "Aplikasi e-Standar Pangan.pdf";
			$mpdf->WriteHTML($html);
			header('Content-Type: application/pdf');
			$mpdf->Output($pdfFilePath, "I");
		}else{
			redirect(base_url());
		}
	}
	
	// function st_audit($id){
	// 	if($this->newsession->userdata('_LOGGED')){
	// 		ob_end_clean();
	// 		$this->load->library('mpdf');
	// 		$this->load->model('print/print_act');
	// 		$mpdf=new mPDF('utf-8', 'A4-L');
	// 		$mpdf->AddPage('P', // L - landscape, P - portrait
 //            '', '', '', '',
 //            15, // margin_left
 //            15, // margin right
 //            50, // margin top
 //            16, // margin bottom
 //            9, // margin header
 //            9); // margin footer
	// 		$mpdf->useOnlyCoreFonts = true;
	// 		$mpdf->SetProtection(array('print'));
	// 		$mpdf->SetAuthor("Inspektorat - BPOM");
	// 		$mpdf->SetDisplayMode('fullpage');
	// 		$arrdata = $this->print_act->get_data("st_audit", $id);
	// 		$html = $this->load->view('back/print/ST-audit', $arrdata, true);
	// 		$pdfFilePath = "Aplikasi Managemen Audit.pdf";

	// 		if($arrdata['arrAudit']['AUDIT_STATUS'] != '06' && $arrdata['arrAudit']['AUDIT_STATUS'] != '07' && $arrdata['arrAudit']['AUDIT_STATUS'] != '08' && $arrdata['arrAudit']['AUDIT_STATUS'] != '09'){
	// 			$mpdf->SetWatermarkText('DRAFT');
	// 			$mpdf->showWatermarkText = true;	
	// 		}
			

	// 		$mpdf->WriteHTML($html);
	// 		header('Content-Type: application/pdf');
	// 		$mpdf->Output($pdfFilePath, "I");
	// 	}else{
	// 		redirect(base_url());
	// 	}
	// }
	
	// function sp_pbj($id){
	// 	if($this->newsession->userdata('_LOGGED')){
	// 		ob_end_clean();
	// 		$this->load->library('mpdf');
	// 		$this->load->model('print/print_act');
	// 		$mpdf=new mPDF('utf-8', 'A4-L');
	// 		$mpdf->AddPage('P', // L - landscape, P - portrait
 //            '', '', '', '',
 //            15, // margin_left
 //            15, // margin right
 //            50, // margin top
 //            16, // margin bottom
 //            9, // margin header
 //            9); // margin footer
	// 		$mpdf->useOnlyCoreFonts = true;
	// 		$mpdf->SetProtection(array('print'));
	// 		$mpdf->SetAuthor("Inspektorat - BPOM");
	// 		$mpdf->SetDisplayMode('fullpage');
	// 		$arrdata = $this->print_act->get_data("sp_pjb", $id);
	// 		/*header*/
	// 		$head = $this->load->view('back/print/PKA-Header', $arrdata, true);
	// 		$mpdf->SetHTMLHeader($head, 'O', true);
	// 		/*footer*/
	// 		$foot = $this->load->view('back/print/PKA-Footer', $arrdata, true);
	// 		$mpdf->SetHTMLFooter($foot, 'O');
	// 		/*main*/
	// 		$html = $this->load->view('back/print/SP-PBJ', $arrdata, true);
	// 		$pdfFilePath = "Aplikasi Managemen Audit.pdf";
	// 		$mpdf->WriteHTML($html);
	// 		header('Content-Type: application/pdf');
	// 		$mpdf->Output($pdfFilePath, "I");
	// 	}else{
	// 		redirect(base_url());
	// 	}
	// }

	// function kka($id){
	// 	if($this->newsession->userdata('_LOGGED')){
	// 		ob_end_clean();
	// 		$this->load->library('mpdf');
	// 		$this->load->model('print/print_act');
	// 		$mpdf=new mPDF('utf-8', 'A4-L');
	// 		$mpdf->AddPage('P', // L - landscape, P - portrait
 //            '', '', '', '',
 //            15, // margin_left
 //            15, // margin right
 //            45, // margin top
 //            16, // margin bottom
 //            9, // margin header
 //            9); // margin footer
	// 		$mpdf->useOnlyCoreFonts = true;
	// 		$mpdf->SetFooter('{PAGENO} / {nbpg}');
	// 		$mpdf->SetProtection(array('print'));
	// 		$mpdf->SetAuthor("Inspektorat - BPOM");
	// 		$mpdf->SetDisplayMode('fullpage');
	// 		$arrdata = $this->print_act->get_data_kka($id);
	// 		/*header*/
	// 		$head = $this->load->view('back/print/KKA-Header', $arrdata, true);
	// 		$mpdf->SetHTMLHeader($head, 'O', true);
	// 		/*main*/
	// 		$html = $this->load->view('back/print/KKA', $arrdata, true);
			
	// 		$pdfFilePath = "Aplikasi Managemen Audit.pdf";
	// 		$mpdf->WriteHTML($html);
	// 		header('Content-Type: application/pdf');
	// 		$mpdf->Output($pdfFilePath, "I");
	// 	}else{
	// 		redirect(base_url());
	// 	}
	// }
	
	// function temuan_s($id){
	// 	if($this->newsession->userdata('_LOGGED')){
	// 		ob_end_clean();
	// 		$this->load->library('mpdf');
	// 		$this->load->model('print/print_act');
	// 		$mpdf=new mPDF('utf-8', 'A4-L');
	// 		$mpdf->AddPage('P', // L - landscape, P - portrait
 //            '', '', '', '',
 //            15, // margin_left
 //            15, // margin right
 //            16, // margin top
 //            16, // margin bottom
 //            9, // margin header
 //            9); // margin footer
	// 		$mpdf->useOnlyCoreFonts = true;
	// 		$mpdf->SetProtection(array('print'));
	// 		$mpdf->SetAuthor("Inspektorat - BPOM");
	// 		$mpdf->SetDisplayMode('fullpage');
	// 		$arrdata = $this->print_act->get_data_pengantar_temuan($id);
	// 		$html = $this->load->view('back/print/print-temuans', $arrdata, true);
	// 		$pdfFilePath = "Aplikasi Managemen Audit.pdf";
	// 		$mpdf->WriteHTML($html);
	// 		header('Content-Type: application/pdf');
	// 		$mpdf->Output($pdfFilePath, "I");
	// 	}else{
	// 		redirect(base_url());
	// 	}
	// }
	
	// function temuan_d($id){
	// 	if($this->newsession->userdata('_LOGGED')){
	// 		ob_end_clean();
	// 		$this->load->library('mpdf');
	// 		$this->load->model('print/print_act');
	// 		$mpdf=new mPDF('utf-8', 'A4-L');
	// 		$mpdf->AddPage('P', // L - landscape, P - portrait
 //            '', '', '', '',
 //            15, // margin_left
 //            15, // margin right
 //            16, // margin top
 //            16, // margin bottom
 //            9, // margin header
 //            9); // margin footer
	// 		$mpdf->useOnlyCoreFonts = true;
	// 		$mpdf->SetProtection(array('print'));
	// 		$mpdf->SetAuthor("Inspektorat - BPOM");
	// 		$mpdf->SetDisplayMode('fullpage');
	// 		$arrdata = $this->print_act->get_data_temuan($id);
	// 		$html = $this->load->view('back/print/print-temuand', $arrdata, true);
	// 		$pdfFilePath = "Aplikasi Managemen Audit.pdf";
	// 		$mpdf->WriteHTML($html);
	// 		header('Content-Type: application/pdf');
	// 		$mpdf->Output($pdfFilePath, "I");
	// 	}else{
	// 		redirect(base_url());
	// 	}
	// }
	
	
}
