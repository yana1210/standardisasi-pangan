<?php if(!defined('BASEPATH'))exit('No direct script access allowed'); 
class Submission extends Controller{ 
	var $content = "";	
	function Home(){
		parent::Controller();
	}
	function index(){

		if($this->newsession->userdata('LOGGED_IN')){
			if($this->content==""){
				$gambar = TRUE;
			}
			$sUserid = $this->newsession->userdata('USER_ROLE');
			$this->menu = $this->load->view('in/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('in/footer', '', true),
						  '_header_' => $this->load->view('in/header', '', true));
			$this->parser->parse('in/home', $data);
		}else{
			
			$gambar = FALSE;			
			$this->load->model('login_act');
			
			if($this->content==""){
				$this->newsession->sess_destroy();
				$this->content = $this->load->view('login', $arrdata, true);
				
				$gambar = TRUE;
			}

			$this->menu = $this->load->view('out/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('out/footer', '', true),
						  'gambar' => $gambar,
						  'captcha' => $arrdata['captcha'],
						  '_header_' => $this->load->view('out/header', '', true));
			$this->parser->parse('out/home', $data);
			
		}
	}
    function list_rqs($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
    	//echo "<pre>"; print_r($_SESSION); die();
		if($this->newsession->userdata('USER_ID') == ""){
			redirect(site_url());
		}
		$this->load->model("in/submission_act");
        $arrData = $this->submission_act->get_arr_list_rqs($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
    function list_assignment($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
    	//echo "<pre>"; print_r($_SESSION); die();
		if($this->newsession->userdata('USER_ID') == ""){
			redirect(site_url());
		}
		$this->load->model("in/submission_act");
        $arrData = $this->submission_act->get_arr_list_ass($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
	public function new_rqs($sMenu="", $iId=""){ //echo "<pre>"; print_r($_SESSION);  die();
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		if($this->newsession->userdata('USER_ID') == ""){
			redirect(site_url());
		}
		if($sMenu== "frm-adm" || $sMenu== "frm-btp" || $sMenu== "frm-tech"){
			if($iId != ""){ 
				$rqs = $this->db->query("select USER_ID from tx_rqs where rqs_id = '$iId' ")->row_array();
				if($rqs['USER_ID'] != $this->newsession->userdata('USER_ID')){
					redirect(site_url());
				}
			}
		}
		$this->load->model("in/submission_act");
		$arrData = $this->submission_act->submission_get($sMenu, $iId);
		$this->content = $this->load->view('in/submission/'.$sMenu, $arrData, true);
		$this->index();                   
	}
	public function assignment($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/submission_act");
		$arrData = $this->submission_act->submission_get($sMenu, $iId);
		$this->content = $this->load->view('in/assignment/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	function post($sMenu="", $sSubmenu="", $sSubmenu2=""){
		$this->load->model('in/submission_act');
		$sRet = $this->submission_act->set_submission($sMenu, $sSubmenu, $sSubmenu2);
		echo $sRet;
	}
   public function email_exists_check() {
        $sMail = $this->input->post('email');
        $query = $this->db->query("select USER_MAIL from TM_USER where USER_MAIL = '$sMail' limit 1 ")->row_array();
        if ($query['USER_MAIL'] == '') {
            echo "0|||Email Tersedia"; //json_encode(TRUE);
        } else {
            echo "1|||Email ".$this->input->post('email')." Sudah digunakan"; //json_encode(TRUE);
        }
    }

    public function cek_perkajinsus() {
        $noisn = $this->input->post('noisn');
        $food_type_id = $this->input->post('food_type_id');
        $RQS_BTP_NETTO = $this->input->post('RQS_BTP_NETTO');
        $RQS_BTP_UNIT = $this->input->post('RQS_BTP_UNIT');

        $tr_btp_type = $this->db->query("select btp_type_name,btp_type_no from tr_btp_type where BTP_TYPE_ID = '$noisn'  ")->row_array();
        $tr_food_type = $this->db->query("select food_type_name,food_type_no from tr_food_type where  FOOD_TYPE_ID = '$food_type_id' ")->row_array();
		$nokatpang = $tr_food_type['food_type_no'];
		$data2 = $this->db->query("SELECT concat(food_type_no, '. ', food_type_name) as kategori_pangan FROM tr_food_type WHERE food_type_no = '$nokatpang' ORDER BY food_type_id LIMIT 1 ")->row_array();

        $query = $this->db->query("select max_limit,note from tr_perkajinsus where BTP_TYPE_ID = '$noisn' and FOOD_TYPE_ID = '$food_type_id' and UNIT = '$RQS_BTP_UNIT'")->row_array();
/*
        if($query){ //jika diatur dalam perkajinsus
	        if ($RQS_BTP_NETTO >= $query['max_limit'] && $query['max_limit'] != 'CPPB') {
	            echo "1|||Lanjut|||Penggunaan ".$tr_btp_type['btp_type_name']." (INS. ".$tr_btp_type['btp_type_no'].") pada Kategori Pangan ".$data2['kategori_pangan']."|||Apakah ".$query['note']." ?"; //json_encode(TRUE);
	        } else {
	            echo "0|||Berdasarkan data yang Anda pilih, Anda tidak bisa melanjutkan permohonan. Silakan hubungi standard pangan!|||Penggunaan ".$tr_btp_type['btp_type_name']." (INS. ".$tr_btp_type['btp_type_no'].") pada Kategori Pangan ".$data2['kategori_pangan']; //json_encode(TRUE);
	        }
        }else{
            echo "2|||Lanjut|||Penggunaan ".$tr_btp_type['btp_type_name']." (INS. ".$tr_btp_type['btp_type_no'].") pada Kategori Pangan ".$data2['kategori_pangan']; //json_encode(TRUE);
        }
*/
        if($RQS_BTP_NETTO <= $query['max_limit'] && $query['max_limit'] != 'CPPB'){ //jika diatur dalam perkajinsus
            echo "0|||Berdasarkan data yang Anda pilih, Anda tidak bisa melanjutkan permohonan. Silakan hubungi standard pangan!|||Penggunaan ".$tr_btp_type['btp_type_name']." (INS. ".$tr_btp_type['btp_type_no'].") pada Kategori Pangan ".$data2['kategori_pangan']; //json_encode(TRUE);
        }else{
            echo "2|||Lanjut|||Penggunaan ".$tr_btp_type['btp_type_name']." (INS. ".$tr_btp_type['btp_type_no'].") pada Kategori Pangan ".$data2['kategori_pangan']; //json_encode(TRUE);
        }
    }

	
}