<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Download extends Controller {

    function Download() {
        parent::Controller();
        //$this->appname = 'e-Registration';
    }

    function index() {
        
    }
    function data11($data, $file) {
        $data = base64_decode($data);
        $file = str_replace('sys/', '', BASEPATH) . $data;
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment; Filename = nodin.doc");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }

    function data($data, $file) {
        $data = base64_decode($data);
        $file = str_replace('sys/', '', BASEPATH) . $data;
        $imginfo = getimagesize($file);
        header("Content-type: $imginfo[mime]");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }

    function data2($id = "", $data = "", $file = "") {
        $arrData = $this->db->query("SELECT * FROM TM_FACTORY_DOCUMENT WHERE FACTORY_DOC_ID = $id ")->row_array();
        $data = $arrData['FACTORY_DOC_PATH'];
        $file = str_replace('sys/', '', BASEPATH) . $data;
        $imginfo = getimagesize($file);
        header("Content-type: $imginfo[mime]");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }

    function data3SAMA($id = "", $data = "", $file = "") {//print_r($id.'/'.$data.'/'.$file);die();
        $arrData = $this->db->query("SELECT * FROM TM_TRADER_DOCUMENT WHERE TRADER_DOC_ID = $id ")->row_array();
        //$data = decrypt($data);
        $data = $arrData['TRADER_DOC_PATH'];
        $file = str_replace('sys/', '', BASEPATH) . $data;
    }

    function data3($id = "", $data = "", $file = "") {
        $arrData = $this->db->query("SELECT * FROM TM_FACTORY_DOCUMENT WHERE FACTORY_DOC_ID = $id ")->row_array();
        $data = $arrData['FACTORY_DOC_PATH'];
        $file = str_replace('sys/', '', BASEPATH) . $data;
        $imginfo = getimagesize($file);
        header("Content-type: $imginfo[mime]");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }

    function data_trader($path, $data, $file) {
        $ereg = & get_instance();
        $ereg->load->model("main", "main", true);
        if (stripos($data, 'EREG') === FALSE && stripos($data, 'MREG') === FALSE) {
            $file = str_replace('sys/', '', BASEPATH) . 'dat/' . $data . '/' . $file;
        } else {
            $file = str_replace('sys/', '', BASEPATH) . 'dat/' . md5($ereg->main->get_uraian("SELECT B.NPWP FROM T_PRODUK_2 A LEFT JOIN M_TRADER B ON B.TRADER_ID = A.TRADER_ID WHERE A.PRODUK_ID = '" . $data . "'", "NPWP")) . '/' . $data . '/' . $file;
        }
        //echo $file;
        //die();
        if (!file_exists($file)) {
            $file = str_replace('dat', 'datold', $file);
        }

        //if(!is_dir($file)) $file = str_replace('dat','datold',$file);
        //echo $file; die();
        //echo $file;
        //die();
        $imginfo = getimagesize($file);
        header("Content-type: $imginfo[mime]");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }

    function data_pendukung($file) {
        $ereg = & get_instance();
        $ereg->load->model("main", "main", true);
        $file = '/home/dat/' . $file;
        $imginfo = getimagesize($file);
        header("Content-type: $imginfo[mime]");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }

}

?>