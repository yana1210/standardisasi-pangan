<?php if(!defined('BASEPATH'))exit('No direct script access allowed'); 
class Master extends Controller{ 
	var $content = "";	
	function Home(){
		parent::Controller();
	}

	function index(){

		if($this->newsession->userdata('LOGGED_IN')){
			if($this->content==""){
				$gambar = TRUE;
			}
			$sUserid = $this->newsession->userdata('USER_ROLE');
			$this->menu = $this->load->view('in/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('in/footer', '', true),
						  '_header_' => $this->load->view('in/header', '', true));
			$this->parser->parse('in/home', $data);
		}else{
			
			$gambar = FALSE;			
			$this->load->model('login_act');
			$arrdata['captcha'] = $this->login_act->get_captcha();
			if($this->content==""){
				$this->newsession->sess_destroy();
				$this->content = $this->load->view('login', $arrdata, true);
				
				$gambar = TRUE;
			}

			$this->menu = $this->load->view('out/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('out/footer', '', true),
						  'gambar' => $gambar,
						  'captcha' => $arrdata['captcha'],
						  '_header_' => $this->load->view('out/header', '', true));
			$this->parser->parse('out/home', $data);
			
		}
	}

	//FUNCTION LIST DATA
    function list_user($sType) { 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_user($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_pemohon($sType) { 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act"); 
        $arrData = $this->master_act->get_list_pemohon($sType); 
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_pegawai($sType) { 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_pegawai($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_btp($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_btp($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_btp_function($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_btp_function($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_food_type($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_food_type($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_piket($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_piket($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_hari_kerja($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_hari_kerja($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_permenkes($sType) { 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_permenkes($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_codex1($sType) { //print_r($sType);die('dfdfdf');
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_codex1($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_codex3($sType) { //print_r($sType);die('dfdfdf');
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_codex3($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_annex3($sType) { //print_r($sType);die('dfdfdf');
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_annex3($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

     function list_perka($sType) { //print_r($sType);die('dfdfdf');
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_perka($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }

    function list_request($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
    	//echo "<pre>"; print_r($_SESSION); die();
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_request($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
    function list_our_contact($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
    	//echo "<pre>"; print_r($_SESSION); die();
		$this->load->model("in/master_act");
        $arrData = $this->master_act->get_list_our_contact($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }


    //FUNCTION NEW DATA
	public function new_user($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	public function new_pemohon($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	public function new_pegawai($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	public function new_btp($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	public function new_permenkes($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	public function new_codex1($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	public function new_codex3($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	public function new_annex3($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	public function new_perka($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	public function new_request($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/master_act");
		$arrData = $this->master_act->master_get($sMenu, $iId);
		$this->content = $this->load->view('in/master/'.$sMenu, $arrData, true);
		$this->index();                   
	}


	//FUNCTION POST
	function post($sMenu="", $sSubmenu="", $sSubmenu2=""){
		$this->load->model('in/master_act');
		$sRet = $this->master_act->set_master($sMenu, $sSubmenu, $sSubmenu2);
		echo $sRet;
	}

}