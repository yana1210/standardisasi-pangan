<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak extends Controller{
	var $content = "";
	
	function Cetak(){
		parent::Controller();
	}
	function nodin($id){

		$rqs = $this->db->query("select a.*,
		 CONCAT(f.TRADER_TYPE, ' ', e.USER_NAME) AS 'NAMA',f.TRADER_TYPE,e.USER_NAME, c.BTP_FUNCTION_NAME,c.BTP_FUNCTION_NOMOR_PERATURAN, c.BTP_FUNCTION_YEAR,
			b.`BTP_TYPE_NO` ,b.`BTP_TYPE_NAME`, d.`FOOD_TYPE_NO`,d.`FOOD_TYPE_NAME`,
			g.XPRT_REC_NOTE, h.EXPOSURE_AMOUNT,j.USER_PIC_ADDR,j.REGION_ID ,j.USER_PIC_ZIP ,j.USER_PIC_TELP ,j.USER_PIC_FAX, j.USER_PIC_MAIL,
			i.`LETTER_ID`,i.`RQS_ID`,i.`ND_NUMBER`,i.`ND_DEFINITION_BTP`,i.`ND_RISK_CHARACTERISTICS`,i.`ND_VALUE_ADI`,i.`ND_STATUS_PERMENKES`,i.`ND_STATUS_PERKA`,i.`ND_VALUE_ASSUMPTION`,i.`ND_ADULT_USE`,i.`ND_CHILDREN_USE`,i.`ND_ADULT_CODEX`,i.`ND_CHILDREN_CODEX`,i.`ND_MAX_LIMIT`,i.`LETTER_TYPE`,i.`S_LETTER_NUMBER`,i.`S_DATE`,i.`S_ATTACHMENT`,i.`S_TO`,i.`S_MAX_LIMIT_APPROVED`,i.`S_NUMBER_APPROVED`,i.`S_SPECIFICATION`,i.`S_SOURCE_BTP_SPECIFICATION`,i.`T_LETTER_NUMBER`,i.`T_DATE`,i.`T_ATTACHMENT`,i.`T_TO`,i.`T_JUSTIFICATION`,
			k.BTP_TYPE_NAME_ENGLISH
			from tx_rqs a 
			LEFT JOIN tr_btp_type b ON a.`RQS_BTP_TYPE_ID`=b.`BTP_TYPE_ID`
			LEFT JOIN tr_btp_function c ON a.`RQS_BTP_FUNCTION_ID`=c.`BTP_FUNCTION_ID`
			LEFT JOIN  tr_food_type d ON a.`RQS_FOOD_TYPE_ID`=d.`FOOD_TYPE_ID` 
			LEFT JOIN  tm_user e ON a.`USER_ID`=e.`USER_ID` 
			LEFT JOIN  tm_user_com f ON a.`USER_ID`=f.`USER_ID` 
			LEFT JOIN  tm_user_pic j ON a.`USER_ID`=j.`USER_ID` 
			LEFT JOIN  TX_XPRT_NOTE g ON a.`RQS_ID`=g.`RQS_ID` 
			LEFT JOIN  TX_EXPOSURE h ON a.`RQS_ID`=h.`RQS_ID` 
			LEFT JOIN  TX_RQS_LETTER i ON a.`RQS_ID`=i.`RQS_ID` 
			LEFT JOIN  tr_btp_type k ON a.`RQS_BTP_TYPE_ID`=k.`BTP_TYPE_ID` 
			where a.rqs_id = $id")->row_array();
		$nama = $rqs['TRADER_TYPE']." ".$rqs['USER_NAME']; 
		$btp = $rqs['RQS_BTP_TYPE_ID'];
		$ex = explode('Kategori Pangan ', $rqs['RQS_ABOUT']);
		$rqs_k = $this->db->query("SELECT RQS_PRD_REG_COUNTRY, RQS_PRD_REG_FOOD_TYPE, RQS_PRD_REG_MAX_QTY FROM tx_rqs_prd_reg_doc WHERE RQS_ID = $id")->result_array();
		$query_p = $this->db->query("SELECT b.BTP_FUNCTION_NAME FROM tr_permenkes a left join tr_btp_function b on a.BTP_FUNCTION_ID=b.BTP_FUNCTION_ID WHERE a.BTP_TYPE_ID = $btp");
		$rqs_permenkes = $query_p->result_array();
		$num = $query_p->num_rows();
		$rqs_letter_spec = $this->db->query("SELECT SPEC_DESC FROM tx_rqs_letter_spec WHERE RQS_ID = $id")->result_array();

		$PERSEN = $rqs['RQS_BTP_NETTO']/10000;

        $RQS_X = $this->db->query("SELECT XPRT_REC_NOTE FROM tx_xprt_note WHERE RQS_ID = '$id' ORDER BY XPRT_NOTE_ID")->result_array();
//		echo $num; print_r($rqs_permenkes);

//die('============================================================');
		header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename=Nota_Dinas_".date("d_m_Y_G_i").".doc");
		//header("font-family:'Cambria;'");

		if($rqs['RQS_ASSESSMENT_MARK'] == 'RM02'){ //HITUNG PAPARAN
			$codex = "<tr>
						<td width='95%' colspan='2' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> Perhitungan paparan, dengan nilai asumsi konsumsi ".$rqs['ND_VALUE_ASSUMPTION']." untuk kategori pangan ".$ex[1].", yaitu: </p>
						</td>
					</tr>

					<tr>
						<td width='1%' style='vertical-align:top;'> <li> a. </li> </td>
						<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> 
							<p> Dengan batas maksimum ".$PERSEN."% (".$rqs['RQS_BTP_NETTO']." ".$rqs['RQS_BTP_UNIT'].") (sesuai penggunaan) diperoleh nilai paparan sebesar ".$rqs['ND_ADULT_USE']." % ADI dewasa, dan ".$rqs['ND_CHILDREN_USE']." % ADI anak-anak.</p>
						</td>
					</tr>

					<tr>
						<td width='1%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'> <li> b. </li> </td>
						<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> 
							<p> Dengan batas maksimum ".$rqs['RQS_CODEX_MAX_LIMIT']." ".$rqs['RQS_CODEX_UNIT']." (sesuai Codex) diperoleh nilai paparan sebesar ".$rqs['ND_ADULT_CODEX']." % ADI dewasa, dan ".$rqs['ND_CHILDREN_CODEX']." % ADI anak-anak </p>
						</td>
					</tr>";
		}else if($rqs['RQS_ASSESSMENT_MARK'] == 'RM03'){ //bawa pakar
					$codex = "<tr>
						<td width='95%' colspan='2' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> Berdasarkan hasil rapat pengkajian bahan baku, bahan tambahan pangan, dan bahan penolong pada tanggal ".$this->tanggal_indo($rqs['RQS_DATE_REC']).", adalah sebagai berikut: </p>
						</td>
					</tr>";
					$i2 =0;
					foreach ($RQS_X as $key => $value1) {
						$i2++;
						$codex .= "<tr>
						<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><p> ".$i2.". </p></td>
						<td width='90%' style='line-height:1.5; text-align:justify; font-family: Cambria,Georgia,serif;'> 
							<p> ".$value1['XPRT_REC_NOTE']."</p>
						</td>
					</tr>";
					}

		}else{
			$codex = "";
		}

		echo '<html>
		<meta http-equiv="Content-Type" content="text/html" charset="Windows-1252">
		    <xml>
		        <w:worddocument xmlns:w="#unknown">
		            <w:view>Print</w:view>
		            <w:zoom>90</w:zoom>
		            <w:donotoptimizeforbrowser />
		        </w:worddocument>
		    </xml>
		    <style>
		        @page Section1
			        {size:215mm 330mm;
			         margin:25.4mm 28.6mm 25.4mm 31.7mm ;
			         mso-header-margin:.5in;
			         mso-footer-margin:.5in; 
			         mso-paper-source:0;}
		        div.Section1
			        {page:Section1;}
        @media print {
            tr.page-break  { display: block; page-break-before: always; }
        }   

		    </style>';

		echo "
			<div class='Section1' style='text-align:center;font-size:12.0pt;'>

				<table width='100%' style='font-family:Cambria,Georgia,serif; '>
								<tr>
									<td align='center' colspan='3'>
											<b> BADAN PENGAWAS OBAT DAN MAKANAN RI </b>
									</td>
								</tr>
								<tr>
									<td align='center' colspan='3'>										
											<b> DIREKTORAT STANDARDISASI PRODUK PANGAN </b> <br>
									</td>
								</tr>
								<tr>
									<td align='center' colspan='3'>										
									</td>
								</tr>
								<tr>
									<td align='center' colspan='3'>										
											<b> NOTA DINAS </b>
									</td>
								</tr>
								<tr>
									<td align='center' colspan='3'>										
											<b>  NOMOR : ".$rqs['ND_NUMBER']." </b>
									</td>
								</tr>
				</table>

				<br/><br/>
				<table width='100%' style='font-family: Cambria,Georgia,serif; '>

								<tr>
									<td align='center' colspan='3'>										
									</td>
								</tr>
								<tr> 
									<td  style='line-height:1.5; text-align:left; font-family:Cambria,Georgia,serif;'><p> Yth </p></td>
									<td  style='line-height:1.5; text-align:left; font-family:Cambria,Georgia,serif;'><p> : </p></td>
									<td  style='line-height:1.5; text-align:left; font-family:Cambria,Georgia,serif;'>
										<p> 
											Direktur Standardisasi Produk Pangan
										</p>
									</td>
								</tr>

								<tr> 
									<td style='line-height:1.5; text-align:left; font-family:Cambria,Georgia,serif;'><p> Dari </p></td>
									<td style='line-height:1.5; text-align:left; font-family:Cambria,Georgia,serif;'><p> : </p></td>
									<td style='line-height:1.5; text-align:left; font-family:Cambria,Georgia,serif;'>
										<p> 
											Kasubdit Standardisasi Bahan Baku dan Bahan Tambahan Pangan 
										</p>
									</td>
								</tr>

								<tr> 
									<td style='line-height:1.5; vertical-align: top; text-align:left; font-family:Cambria,Georgia,serif;'><p> Perihal </p></td>
									<td style='line-height:1.5; vertical-align: top; text-align:left; font-family:Cambria,Georgia,serif;'><p> : </p></td>
									<td style='line-height:1.5; vertical-align: top; text-align:left; font-family:Cambria,Georgia,serif;'>
										<p> 
											".$rqs['RQS_ABOUT']." 
										</p>
									</td>
								</tr>
								<tr> 
									<td style='line-height:1.5; vertical-align: top; text-align:left; font-family:Cambria,Georgia,serif;'><p> Tanggal </p></td>
									<td style='line-height:1.5; vertical-align: top; text-align:left; font-family:Cambria,Georgia,serif;'><p> : </p></td>
									<td style='line-height:1.5; vertical-align: top; text-align:left; font-family:Cambria,Georgia,serif;'>
									</td>
								</tr>

								<tr>
									<td style='border-bottom:2px solid; border-bottom-color: #000000;' colspan='3'>
										&nbsp;
										<p>&nbsp;</p>
									</td>
								</tr>
				</table>

				<br/><br/>
				<table width='100%' style='font-family: Cambria,Georgia,serif; '>
								<tr>
									<td width='100%' colspan='3' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif; '>
										<p> Sehubungan dengan permohonan dari ".$nama." dengan nomor ".$rqs['RQS_NO_AJU'].", tanggal ".$this->tanggal_indo($rqs['RQS_CREATE_DATE']).", yang kami terima tanggal ".$this->tanggal_indo($rqs['RQS_VER_DATE']).", perihal ".$rqs['RQS_ABOUT'].", dengan hormat kami sampaikan telaahan sebagai berikut: </p>
									</td>
								</tr>

								<tr>
									<td width='100%' colspan='3'>&nbsp;</td>
								</tr>

				</table>

				<br/><br/>
				<table width='100%' style='font-family: Cambria,Georgia,serif; '>
								<tr>
									<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><b><p> A. </p></b></td>
									<td width='95%' colspan='2' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'><p><b> Kategori Pangan </b></p></td>
								</tr>

								<tr>
									<td width='2%'> &nbsp; </td>
									<td width='95%' colspan='2' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> 
										<p> Berdasarkan Peraturan Kepala Badan Pengawas Obat dan Makanan RI No. 21 tahun 2016 tentang Kategori Pangan, produk yang diajukan yaitu ".$rqs['FOOD_TYPE_NAME']." termasuk dalam kategori pangan ".$ex[1].". </p>
									</td>
								</tr>

								<tr>
									<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><b> <p> B. </p> </b></td>
									<td width='95%' colspan='2' style='line-height: 1.5; text-align: justify;font-family: Cambria,Georgia,serif; '><b><p> Bahan Tambahan Pangan </p></b>
									</td>
								</tr>

								<tr>
									<td width='5%'> &nbsp; </td>
									<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'> <p> 1. </p> </td>
									<td width='90%' style='line-height: 1.5; text-align: justify;font-family: Cambria,Georgia,serif; '> 
										<p> ".$rqs['BTP_TYPE_NAME']." (INS. ".$rqs['BTP_TYPE_NO'].") adalah ".$rqs['ND_DEFINITION_BTP'].". </p>
									</td>
								</tr>

								<tr>
									<td width='5%'> &nbsp; </td>
									<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><p> 2. </p></td>
									<td width='90%' style='line-height:1.5; text-align:justify;nfont-family:Cambria,Georgia,serif;'> 
										<p> ".$rqs['ND_RISK_CHARACTERISTICS']." ".$rqs['BTP_TYPE_NAME']." adalah ".$rqs['ND_VALUE_ADI'].". </p>
									</td>
								</tr>

								<tr>
									<td width='5%'> &nbsp; </td>
									<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><p> 3. </p></td>
									<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> 
										<p> Fungsi yang diajukan ialah sebagai BTP ".$rqs['BTP_FUNCTION_NAME']." dengan kadar penggunaan sebesar ".$rqs['RQS_BTP_NETTO']." ".$rqs['RQS_BTP_UNIT'].". </p>
									</td>
								</tr>

								<tr>
									<td width='5%'> &nbsp; </td>
									<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><p> 4. </p></td>
									<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> 
										<p> Peraturan Menteri Kesehatan nomor 033 Tahun 2012 tentang BTP ".$rqs['ND_STATUS_PERMENKES']." ".$rqs['BTP_TYPE_NAME']." sebagai BTP ";
										$ab = 1;
										foreach ($rqs_permenkes as $key => $value) {
											if($ab == $num && $num > 1){
												echo "dan ".$value['BTP_FUNCTION_NAME'];
											}else if($ab == $num && $num == 1){
												echo $value['BTP_FUNCTION_NAME'];
											}else if($ab < $num && $num > 1){
												echo $value['BTP_FUNCTION_NAME'].", ";
											}
											$ab++;
										}
										echo "</p>
									</td>
								</tr>

								<tr>
									<td width='5%'> &nbsp; </td>
									<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><p> 5. </p></td>
									<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> 
										<p> Peraturan Kepala Badan POM Nomor ".$rqs['BTP_FUNCTION_NOMOR_PERATURAN']." Tahun ".$rqs['BTP_FUNCTION_YEAR']." tentang Batas Maksimum Penggunaan Bahan Tambahan Pangan ".$rqs['BTP_FUNCTION_NAME'].", ".$rqs['ND_STATUS_PERKA']." penggunaan ".$rqs['BTP_TYPE_NAME']." pada kategori pangan ".$ex[1].". </p>
									</td>
								</tr>
								<tr>
									<td width='5%'> &nbsp; </td>
									<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><p> 6. </p></td>
									<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> ";

							if($rqs['ND_STATUS_PERMENKES'] == "mengatur"){
								echo "<p style='color:red'> Codex stan 192-1995 Rev.17 tahun 2016 MENGATUR penggunaan ".$rqs['BTP_TYPE_NAME']." pada kategori pangan ".$ex[1]." dengan batas maksimum sebesar ".$rqs['RQS_CODEX_MAX_LIMIT']." ".$rqs['RQS_CODEX_UNIT'].". </p>";
							}else if($rqs['ND_STATUS_PERMENKES'] == "tidak mengatur"){
								echo "<p style='color:red'> Codex stan 192-1995 Rev.17 tahun 2016 TIDAK MENGATUR penggunaan ".$rqs['BTP_TYPE_NAME']." pada kategori pangan ".$ex[1].". </p>";
							}	
										
								echo "	</td>
								</tr>

								<tr>
									<td width='5%'> &nbsp; </td>
									<td width='2%' style='vertical-align:top;'><p> 7. </p></td>
									<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> 
										<p> Status penggunaan ".$rqs['BTP_TYPE_NAME']." pada produk yang diajukan di beberapa negara sebagai berikut: </p>
										<p>
											<table style='width:100%; border:1px solid; border-collapse:collapse; font-family:Cambria,Georgia,serif;'>
												<tr>
													<td style='width:8%; border:0px solid; background-color:#A09999; font-family:Cambria,Georgia,serif;' align='center'>
														<b><p> NO. </p></b>
													</td>
													<td style='width:25%; border:0px solid; background-color:#A09999; font-family:Cambria,Georgia,serif;'>
														<b><p> NEGARA </p></b>
													</td>
													<td style='width:30%; border:0px solid; background-color:#A09999; font-family:Cambria,Georgia,serif;'>
														<b><p> JENIS PANGAN </p></b>
													</td>
													<td style='width:37%; border:0px solid; background-color:#A09999; font-family:Cambria,Georgia,serif;'>
														<b><p> BATAS MAKSIMUM (mg/kg)</p></b>
													</td>
												</tr>";
										$i = 0;		
										foreach ($rqs_k as $key => $value) {
											$i++;
											echo "<tr>
													<td style='border:0px solid;' align='center'>".$i."</td>
													<td style='border:0px solid;'>".$value['RQS_PRD_REG_COUNTRY']."</td>
													<td style='border:0px solid;'>".$value['RQS_PRD_REG_FOOD_TYPE']."</td>
													<td style='border:0px solid;'>".$value['RQS_PRD_REG_MAX_QTY']." </td>
												</tr>";
										}	

									echo "	</table>
										</p>
									</td>
								</tr>

								<tr>
									<td width='5%'> &nbsp; </td>
									<td width='2%' style='vertical-align:top;'> <p> 8. </p> </td>
									<td width='90%' style='line-height:1.5; text-align:justify;'> 
										
										<table style='width:100%;' border='0' style='font-family:Cambria,Georgia,serif;'>
											".$codex."
										</table>
									</td>
								</tr>

								<tr>
									<td width='2%' style='vertical-align:top; font-family:Cambria,Georgia,serif;'><b><p> C. </p></b></td>
									<td width='95%' colspan='2' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
										<b><p> Rekomendasi </p></b>
									</td>
								</tr>

								<tr>
									<td width='5%'> &nbsp; </td>
									<td width='2%' style='vertical-align:top; font-family:Cambria,Georgia,serif;'><p> 1. </p></td>
									<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>"; 
								if($rqs['LETTER_TYPE'] == 'setuju'){
										echo "<p> Berdasarkan hal tersebut di atas, maka kami mengusulkan untuk <b>menyetujui</b> penggunaan BTP ".$rqs['BTP_TYPE_NAME']." (INS. ".$rqs['BTP_TYPE_NO'].") sebagai BTP ".$rqs['BTP_FUNCTION_NAME']." pada kategori pangan pangan ".$ex[1].", dengan batas maksimum sebesar ".$rqs['ND_MAX_LIMIT']." (terlampir konsep surat persetujuan). 
										</p>";
								}else if($rqs['LETTER_TYPE'] == 'tolak'){
										echo "<p> Berdasarkan hal tersebut di atas, maka kami mengusulkan untuk <b><u>tidak menyetujui</u></b> penggunaan BTP ".$rqs['BTP_TYPE_NAME']." (INS. ".$rqs['BTP_TYPE_NO'].") pada kategori pangan ".$ex[1].". (terlampir konsep surat penolakan) 
										</p>";
										}
										echo "<p>&nbsp;</p>
									</td>
								</tr>
								<tr>
									<td width='100%' colspan='3' style='font-family:Cambria,Georgia,serif;'><p> Demikian disampaikan, kami mohon arahan Bapak selanjutnya. Terima kasih. </p></td>
								</tr>

								<tr><td width='100%' colspan='3'>&nbsp;</td></tr>
								<tr><td width='100%' colspan='3'>&nbsp;</td></tr>
								<tr><td width='100%' colspan='3'>&nbsp;</td></tr>
								<tr><td width='100%' colspan='3'>&nbsp;</td></tr>

								<tr>
									<td width='5%' >&nbsp;</td>
									<td width='2%' >&nbsp;</td>
									<td width='90%' align='right' style='font-family:Cambria,Georgia,serif;'><p><u> Dra. Deksa Presiana, Apt., M.Kes. </u></p></td>
								</tr>
								<tr><td colspan='3'></td></tr>
								<tr><td colspan='3'></td></tr>
								<tr><td colspan='3'></td></tr>
								<tr><td colspan='3'></td></tr>
								<tr><td colspan='3'></td></tr>
								<tr><td colspan='3'></td></tr>
				</table>
			</div>";

			if($rqs['LETTER_TYPE'] == 'setuju'){

        $PROVINCE_NAME = $this->db->query("SELECT REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) = '00' AND LEFT(REGION_ID, 2) = LEFT('$rqs[REGION_ID]', 2)")->row_array();
        $CITY_NAME = $this->db->query("SELECT REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) <> '00' AND REGION_ID = '$rqs[REGION_ID]' ")->row_array();

		echo "
			<div class='Section1' style='text-align: center;font-size:11.0pt;'>

				<table width='100%' style='font-family: Cambria,Georgia,serif; '>
					<tr> 
						<td width='12%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> <p> Nomor </p></td>
						<td width='3%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> <p> : </p></td>
						<td width='50%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> ".$rqs['S_LETTER_NUMBER']." </p>
						</td>
						<td width='35%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> Jakarta, ".$this->tanggal_indo($rqs['S_DATE'])." </p>
						</td>
					</tr>

					<tr> 
						<td style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'><p> Lampiran </p></td>
						<td style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'><p> : </p></td>
						<td style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;' colspan='2'>
							<p> ".$rqs['S_ATTACHMENT']." Berkas </p>
						</td>
					</tr>

					<tr> 
						<td style='line-height:1.5; vertical-align: top; text-align:left; font-family:Cambria,Georgia,serif;'><p> Perihal </p></td>
						<td style='line-height:1.5; vertical-align: top; text-align:left; font-family:Cambria,Georgia,serif;'><p> : </p></td>
						<td style='line-height:1.5; vertical-align: top; text-align:left; font-family:Cambria,Georgia,serif;' colspan='2'>
							<p> ".$rqs['RQS_ABOUT']."</p>
						</td>
					</tr>
				</table>

				<br/><br/>
				<table width='100%' style='font-family: Cambria,Georgia,serif; '>
					<tr>
						<td width='100%' colspan='3' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> Kepada Yth. </p>
						</td>
					</tr> 

					<tr>
						<td width='100%' colspan='3' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> ".$nama." </p>
						</td>
					</tr> 

					<tr>
						<td width='100%' colspan='3' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> ".$rqs['USER_PIC_ADDR']."</p>
						</td>
					</tr> 
					<tr>
						<td width='100%' colspan='3' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> ".$CITY_NAME['REGION_NAME'].", ".$PROVINCE_NAME['REGION_NAME']." ".$rqs['USER_PIC_ZIP']."</p>
						</td>
					</tr> 

					<tr>
						<td width='100%' colspan='3' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> T/F: ".$rqs['USER_PIC_TELP']."/".$rqs['USER_PIC_FAX']." </p>
						</td>
					</tr> 

					<tr>
						<td width='100%' colspan='3' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> Email: ".$rqs['USER_PIC_MAIL']."</p>
						</td>
					</tr> 


					<tr><td width='100%' colspan='3'>&nbsp;</td></tr>
					<tr><td width='100%' colspan='3'>&nbsp;</td></tr>


					<tr>
						<td width='100%' colspan='3' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<p> Menindaklanjuti surat permohonan dari ".$nama.", dengan nomor ".$rqs['RQS_NO_AJU']." yang kami terima tanggal ".$this->tanggal_indo($rqs['RQS_VER_DATE']).", perihal penggunaan BTP ".$rqs['BTP_FUNCTION_NAME']." ".$rqs['BTP_TYPE_NAME']." ".$rqs['BTP_TYPE_NO']." pada Kategori Pangan ".$rqs['FOOD_TYPE_NO'].", dengan hormat kami sampaikan bahwa:
							</p>
						</td>
					</tr>

					<tr><td width='100%' colspan='3'>&nbsp;</td></tr>

					<tr>
						<td width='2%' style='vertical-align:top; text-align:justify;font-family:Cambria,Georgia,serif;'><b><p> A. </p></b></td>
						<td width='95%' colspan='2' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<b><p> Kategori Pangan </p></b>
						</td>
					</tr>

					<tr>
						<td width='5%'> &nbsp; </td>
						<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;' colspan='2'> 
							<p> Berdasarkan Peraturan Kepala Badan Pengawas Obat dan Makanan No. 21 tahun 2016 tentang Kategori Pangan, produk yang diajukan, yaitu ".$rqs['FOOD_TYPE_NAME']." termasuk dalam kategori pangan ".$ex[1].". 
							</p>
						</td>
					</tr>

					<tr>
						<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><b><p> B. </p></b></td>
						<td width='95%' colspan='2' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<b><p> Bahan Tambahan Pangan </p></b>
						</td>
					</tr>

					<tr>
						<td width='5%'> &nbsp; </td>
						<td width='2%' style='vertical-align:top;'><p> 1. </p></td>
						<td width='90%' style='line-height:1.5; text-align:justify;'> 
							<p> ".$rqs['BTP_TYPE_NAME']." (INS ".$rqs['BTP_TYPE_NO'].") dapat digunakan sebagai BTP ".$rqs['BTP_FUNCTION_NAME']." pada kategori pangan ".$ex[1].", dengan batas maksimum sebesar ".$rqs['S_MAX_LIMIT_APPROVED']." </p>
						</td>
					</tr>

					<tr>
						<td width='5%'> &nbsp; </td>
						<td width='2%' style='vertical-align:top;'><p> 2. </p></td>
						<td width='90%' style='line-height:1.5; text-align:justify;'> 
							<p> Pencantuman BTP ".$rqs['BTP_FUNCTION_NAME']." ".$rqs['BTP_TYPE_NAME']." pada label harus mengikuti ketentuan Peraturan Pemerintah Republik Indonesia Nomor 69 Tahun 1999 tentang Label dan Iklan Pangan dan Peraturan Menteri Kesehatan Nomor 033 Tahun 2012 tentang Bahan Tambahan Pangan. </p>
						</td>
					</tr>

					<tr>
						<td width='5%'> &nbsp; </td>
						<td width='2%' style='vertical-align:top; text-align:justify; font-family:Cambria,Georgia,serif;'><p> 3. </p></td>
						<td width='90%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'> 
							<p> Terlampir persetujuan penggunaan Bahan Tambahan Pangan Nomor: ".$rqs['S_NUMBER_APPROVED']." terkait penggunaan ".$rqs['BTP_TYPE_NAME']." sebagai BTP ".$rqs['BTP_FUNCTION_NAME']." pada Kategori Pangan ".$ex[1].". </p>
						</td>
					</tr>
				</table>

				<br/>
				<table width='100%' style='font-family: Cambria,Georgia,serif; '>
					<tr>
						<td width='100%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;' colspan='2'>
							<p> Persetujuan ini dapat ditinjau kembali apabila berdasarkan perkembangan ilmu pengetahuan dan teknologi ditemukan hal-hal yang tidak sesuai lagi. </p>
							<p> Demikian, atas perhatian Saudara/i kami sampaikan terima kasih. </p>
							<p>&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width='40%'>&nbsp;</td>
						<td width='60%' style='text-align:center; font-family:Cambria,Georgia,serif;'><p><b> a.n.  Kepala Badan Pengawas Obat dan Makanan </b></p></td>
					</tr>

					<tr>
						<td width='40%'>&nbsp;</td>
						<td width='60%' style='text-align:center; font-family:Cambria,Georgia,serif;'><p><b> Direktur Standardisasi Produk Pangan, </b></p></td>
					</tr>

					<tr><td width='100%' colspan='2'>&nbsp;</td></tr>
					<tr><td width='100%' colspan='2'>&nbsp;</td></tr>
					<tr><td width='100%' colspan='2'>&nbsp;</td></tr>
					<tr><td width='100%' colspan='2'>&nbsp;</td></tr>

					<tr>
						<td width='40%'>&nbsp;</td>
						<td width='60%' style='text-align:center; font-family:Cambria,Georgia,serif;'>
							<p><b> Drs. Tepy Usia, Apt., M.Phil., Ph.D. </b></p>
							<p>&nbsp;</p>
						</td>
					</tr>
		
				</table>
				<table width='100%' style='font-family: Cambria,Georgia,serif; '>
					<tr>
						<td width='100%' colspan='2' style='jtext-align:justify; font-family:Cambria,Georgia,serif;'><p><b><u> Tembusan Yth. </u></b></p></td>
					</tr>

					<tr>
						<td width='5%' style='vertical-align: top; text-align:left;'><p> 1. </p></td>
						<td width='95%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'><p> Deputi Bidang Pengawasan Keamanan Pangan dan Bahan Berbahaya (sebagai bahan laporan) </p></td>
					</tr>

					<tr>
						<td width='5%' style='vertical-align: top; text-align:left;'><p> 2. </p></td>
						<td width='95%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'><p> Direktur Inspeksi dan Sertifikasi Pangan </p></td>
					</tr>

					<tr>
						<td width='5%' style='vertical-align: top; text-align:left;'><p> 3. </p></td>
						<td width='95%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'><p> Direktur Penilaian Keamanan Pangan </p></td>
					</tr>
				</table>	
			</div>

			
			<table width='100%' style='font-family: Cambria,Georgia,serif; page-break-before:always; '>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>

								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>

			<table width='100%' style='font-family:Cambria,Georgia,serif; font-size:11.0pt;' class='page-break'>
				<tr>
					<td align='center' style='text-align:justify; font-family:Cambria,Georgia,serif;text-align:center'>
						<p><b><u> Persetujuan Penggunaan Bahan Tambahan Pangan </u></b></p>
						<p><b> Nomor : ".$rqs['S_NUMBER_APPROVED']." </b>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
					
				<tr>
					<td style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
						<p> Berdasarkan Peraturan Menteri Kesehatan No. 033 Tahun 2012 tentang Bahan Tambahan Pangan dan Peraturan Kepala Badan Pengawas Obat dan Makanan Nomor ".$rqs['BTP_FUNCTION_NOMOR_PERATURAN']." Tahun ".$rqs['BTP_FUNCTION_YEAR']." tentang Batas Maksimum Penggunaan Bahan Tambahan Pangan ".$rqs['BTP_FUNCTION_NAME'].", serta hasil kajian yang dilakukan terhadap aspek keamanan ".$rqs['BTP_TYPE_NAME']." dan status regulasi di beberapa negara, bahan tersebut di bawah ini dapat digunakan sebagai bahan tambahan pangan. 
						</p>
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td>
						<table style='width:100%; border:1px solid; border-collapse: collapse; font-family:Cambria,Georgia,serif;'>
							<tr>
								<td style='border:0px solid; background-color:#A09999; text-align:justify; font-family:Cambria,Georgia,serif;'  colspan='2'><p> Nama Bahan Tambahan Pangan </p></td>
								<td style='width:15%; border:0px solid; background-color:#A09999; text-align:justify; font-family:Cambria,Georgia,serif;' rowspan='2'><p> INS </p></td>
								<td style='width:20%; border:0px solid; background-color:#A09999; text-align:justify; font-family:Cambria,Georgia,serif;' rowspan='2'><p> Kategori Pangan </p></td>
								<td style='width:30%; border:0px solid; background-color:#A09999; text-align:justify; font-family:Cambria,Georgia,serif;' rowspan='2'><p> Batas Maksimum Penggunaan </p></td>
							</tr>
							<tr>
								<td style='width:25%; border:0px solid; background-color:#A09999; text-align:justify; font-family:Cambria,Georgia,serif;'><p> Bahasa Indonesia </p></td>
								<td style='width:20%; border:0px solid; background-color:#A09999; text-align:justify; font-family:Cambria,Georgia,serif;'><p> Bahasa Inggris </p></td>
							</tr>

							<tr>
								<td style='border:0px solid; text-align:center; font-family:Cambria,Georgia,serif;'>
									<p> ".$rqs['BTP_TYPE_NAME']." </p>
								</td>
								<td style='border:0px solid; text-align:center; font-family:Cambria,Georgia,serif;'>
									<p> <i>".$rqs['BTP_TYPE_NAME_ENGLISH']."</i> <p>
								</td>
								<td style='border:0px solid; text-align:center; font-family:Cambria,Georgia,serif;'>
									<p> ".$rqs['BTP_TYPE_NO']." </p>
								</td>
								<td style='border:0px solid; text-align:center; font-family: Cambria,Georgia,serif;'>
									<p> ".$ex[1]." </p>
								</td>
								<td style='border:0px solid; text-align:center; font-family: Cambria,Georgia,serif;'>
									<p> ".$rqs['S_MAX_LIMIT_APPROVED']." </p>
								</td>
							</tr>
						</table>
						<p>&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td>
						<table style='font-family: Cambria,Georgia,serif; '>
							<tr>
								<td style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;' colspan='2'>
									<p> ".$rqs['BTP_TYPE_NAME']." yang digunakan sebagai Bahan Tambahan Pangan (BTP) harus mempunyai spesifikasi sebagai berikut: 
									</p>
								</td>
							</tr>";

						$ac = 1;
						foreach ($rqs_letter_spec as $key => $value) {
							echo "<tr>
								<td width='5%' style='text-align:justify; font-family:Cambria,Georgia,serif;'><p> ".$ac.". </p></td>
								<td width='95%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
									<p> ".$value['SPEC_DESC']." </p>
								</td>
							</tr>";
							$ac++;
						}


							echo "<tr><td width='100%' colspan='2'>&nbsp;</td></tr>

							<tr>
								<td style='line-height:1.5; text-align:text-align:justify; font-family:Cambria,Georgia,serif;' colspan='2'>
									<p> Disamping itu, ".$rqs['BTP_TYPE_NAME']." harus memenuhi persyaratan lainnya yang tercantum dalam ".$rqs['S_SOURCE_BTP_SPECIFICATION'].".
									</p>
									<p>&nbsp;</p>
								</td>
							</tr>

							<tr>
								<td style='line-height:1.5; text-align:text-align:justify; font-family:Cambria,Georgia,serif;' colspan='2'>
									<p><b> Fungsi/ Tujuan Penggunaan : ".$rqs['BTP_FUNCTION_NAME']." </b></p>
									<p>&nbsp;</p>
								</td>
							</tr>

							<tr>
								<td style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;' colspan='2'>
									<p>Selain hal tersebut di atas, pencantuman BTP ".$rqs['BTP_TYPE_NAME']." dalam label harus mengikuti ketentuan Peraturan Pemerintah Republik Indonesia Nomor 69 Tahun 1999 tentang Label dan Iklan Pangan dan Peraturan Menteri Kesehatan Republik Indonesia Nomor 033 Tahun 2012 tentang Bahan Tambahan Pangan.
									</p>

									<p> Persetujuan ini dapat ditinjau kembali apabila berdasarkan perkembangan ilmu pengetahuan dan teknologi ditemukan hal-hal yang tidak sesuai lagi. </p>	
								</td>
							</tr>
						</table>
						<p>&nbsp;</p>
					</td>
				</tr>

				<tr>
					<td>
						<table width='100%' style='font-family:Cambria,Georgia,serif; '>
							<tr>
								<td width='30%'>&nbsp;</td>
								<td width='80%' align='center' style='text-align:justify; font-family:Cambria,Georgia,serif;'>
									<p> Jakarta, ".$this->tanggal_indo($rqs['S_DATE'])." </p>
								</td>
							</tr>

							<tr>
								<td width='30%'>&nbsp;</td>
								<td width='80%' style='text-align:center; font-family:Cambria,Georgia,serif;'>
									<p><b> a.n.  Kepala Badan Pengawas Obat dan Makanan </b></p>
								</td>
							</tr>

							<tr>
								<td width='30%'>&nbsp;</td>
								<td width='80%' style='text-align:center; font-family:Cambria,Georgia,serif;'>
									<p><b> Direktur Standardisasi Produk Pangan, </b></p>
								</td>
							</tr>

							<tr><td width='100%' colspan='2'>&nbsp;</td></tr>
							<tr><td width='100%' colspan='2'>&nbsp;</td></tr>

							<tr>
								<td width='30%'>&nbsp;</td>
								<td width='80%' style='text-align:center; font-family:Cambria,Georgia,serif;'>
									<p><b> Drs. Tepy Usia, Apt., M.Phil., Ph.D. </b></p>
								</td>
							</tr>
						</table>
						<p>&nbsp;</p>
					</td>
				</tr>

			</table>
			<table width='100%' style='font-family: Cambria,Georgia,serif; '>
				<tr>
					<td width='100%' colspan='2' style='text-align:justify; font-family:Cambria,Georgia,serif;'><p><b><u> Tembusan Yth. </u></b></p></td>
				</tr>

				<tr>
					<td width='5%' style='vertical-align: top; text-align:left;'><p> 1. </p></td>
					<td width='95%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'><p> Deputi Bidang Pengawasan Keamanan Pangan dan Bahan Berbahaya (sebagai bahan laporan) </p></td>
				</tr>

				<tr>
					<td width='5%' style='vertical-align: top; text-align:left;'><p> 2. </p></td>
					<td width='95%' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'><p> Direktur Inspeksi dan Sertifikasi Pangan </p></td>
				</tr>

				<tr>
					<td width='5%' style='vertical-align: top; text-align:left;'><p> 3. </p></td>
					<td width='95%' style='line-height: 1.5; text-align:justify; font-family:Cambria,Georgia,serif;'><p> Direktur Penilaian Keamanan Pangan </p></td>
				</tr>
			</table>	

		
			<table width='100%' style='font-family: Cambria,Georgia,serif;  page-break-before:always;'>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>

			<table style='width:100%;border:1px solid; border-collapse:collapse; font-family:Cambria,Georgia,serif; font-size:11.0pt;' class='page-break'>
					<tr>
						<td style='width:100%;border:1px solid; border-collapse: collapse; text-align:justify; font-family: Cambria,Georgia,serif; font-size:11.0pt; text-align:center'>
							<h5>
								<p><b> BADAN PENGAWAS OBAT DAN MAKANAN RI <br>
								 DIREKTORAT STANDARDISASI PRODUK PANGAN <br>
								 Jl. Percetakan Negara 23 Jakarta </b></p>
							</h5>
						</td>
					</tr>

					<tr>
						<td>
							<table border='0' style='font-family: Cambria,Georgia,serif; font-size:11.0pt;'>
								<tr>
									<td width='20%' style='justify;font-family: Cambria,Georgia,serif; '><p> Diterima oleh </p></td>
									<td width='5%' style='justify;font-family: Cambria,Georgia,serif; '><p> : </p></td>
									<td width='20%' style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p></td>

									<td width='20%' style='justify;font-family: Cambria,Georgia,serif; '><p> Diterima bagian ketik  </p></td>
									<td width='5%' style='justify;font-family: Cambria,Georgia,serif; '><p> : </p></td>
									<td width='20%' style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p></td>
								</tr>
								<tr>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> Diselesaikan oleh </p></td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> : </p> </td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p></td>

									<td style='justify;font-family: Cambria,Georgia,serif; '><p> Diketik oleh </p></td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> : </p> </td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p></td>
								</tr>
								<tr>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> Diperiksa oleh Kasie Standardisasi ...  </p></td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> : </p> </td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p></td>

									<td style='justify;font-family: Cambria,Georgia,serif; '><p> Dibaca oleh</p></td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> : </p> </td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p></td>
								</tr>
								<tr>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> Diterima dibagian arsip </p></td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> : </p> </td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p></td>

									<td style='justify;font-family: Cambria,Georgia,serif; '><p> Diperiksa oleh Kasubdit Standardisasi Bahan Baku dan Bahan Tambahan Pangan </p></td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> : </p> </td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p></td>
								</tr>
								<tr>
									<td> &nbsp;</td>
									<td> &nbsp;</td>
									<td> &nbsp;</td>

									<td style='justify;font-family: Cambria,Georgia,serif; '><p> Dikirim pada tanggal </p></td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> : </p> </td>
									<td style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td style='justify;font-family: Cambria,Georgia,serif; '><p> &nbsp; </p><p>  Diajukan kembali pada tanggal : </p><p> &nbsp; </p></td>
					</tr>

					<tr>
						<td>
							<table style='width:100%;border:1px solid; border-collapse: collapse;font-size:11.0pt;'>
								<tr>
									<td style='justify;font-family: Cambria,Georgia,serif;width:50%; line-height: 1.5;vertical-align: top; text-align:left;'>
										<p> HAL  &nbsp;&nbsp; : ".$rqs['RQS_ABOUT']."</p>

									</td>
									<td style='justify;font-family: Cambria,Georgia,serif;width:50%; line-height: 1.5;vertical-align: top; text-align:left;'>
										<p> No. Agenda &nbsp;&nbsp; : </p>

									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<table width='100%' style='font-family: Cambria,Georgia,serif;font-size:11.0pt; '>
								<tr>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '><p> Nomor &nbsp;&nbsp; : ....  </p></td>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '><p> Jakarta, ".$this->tanggal_indo($rqs['S_DATE'])." </p></td>
								</tr>

								<tr>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '><p> Nota &nbsp;&nbsp; : ....  </p></td>
									<td width='50%' align='left'>&nbsp;</td>
								</tr>

								<tr><td width='100%' align='left' colspan='2'>&nbsp;</td></tr>

								<tr>
									<td width='50%'>&nbsp;</td>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '><p> Memperhatikan: </p></td>
								</tr>
								<tr>
									<td width='50%'>&nbsp;</td>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '><p> Kepada Yth. </p></td>
								</tr>

								<tr>
									<td width='50%'>&nbsp;</td>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '><p><b> ".$nama." </b></p></td>
								</tr>

								<tr>
									<td width='50%'>&nbsp;</td>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '><p><b> ".$rqs['USER_PIC_ADDR']." </b></p></td>
								</tr>
								<tr>
									<td width='50%'>&nbsp;</td>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '><p><b> ".$CITY_NAME['REGION_NAME'].", ".$PROVINCE_NAME['REGION_NAME']." ".$rqs['USER_PIC_ZIP']." </b></p></td>
								</tr>

								<tr>
									<td width='50%'>&nbsp;</td>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '><p><b> T/F: ".$rqs['USER_PIC_TELP']."/".$rqs['USER_PIC_FAX']." </b></p></td>
								</tr>

								<tr>
									<td width='50%'>&nbsp;</td>
									<td width='50%' align='left' style='justify;font-family: Cambria,Georgia,serif; '>
										<p><b> Email: ".$rqs['USER_PIC_MAIL']." </b></p>
										<p>&nbsp;</p>
										<p>&nbsp;</p>
										<p>&nbsp;</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td style='justify;font-family: Cambria,Georgia,serif; '><p> Ditetapkan : </p></td>
					</tr>
					<tr>
						<td>
							<p style='justify;font-family: Cambria,Georgia,serif; '> Direktur Standardisasi Produk Pangan, </p>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
						</td>
					</tr>

					<tr>
						<td style='justify;font-family: Cambria,Georgia,serif; '><p><u> Drs. Tepy Usia, Apt., M.Phil., Ph.D.  </u></p></td>
					</tr>
					<tr>
						<td style='justify;font-family: Cambria,Georgia,serif; '>
							<p> NIP. 19670614 199303 1 002 </p>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
						</td>
					</tr>

					<tr>
						<td style='padding-left: 5px;'>
							<table width='30%' align='left' style='border:1px solid; border-collapse: collapse; padding-left: 5px; padding-bottom: 5px; justify;font-family: Cambria,Georgia,serif; '	>
								<tr>
									<td style='justify;font-family: Cambria,Georgia,serif; '>
										<p><b> SD &nbsp;&nbsp; : </b> .... </p>
									</td>	
								</tr>
							</table>
						</td>
					</tr>
				</table

			</div>";

			}else if($rqs['LETTER_TYPE'] == 'tolak'){

        $PROVINCE_NAME = $this->db->query("SELECT REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) = '00' AND LEFT(REGION_ID, 2) = LEFT('$rqs[REGION_ID]', 2)")->row_array();
        $CITY_NAME = $this->db->query("SELECT REGION_NAME FROM TM_REGION WHERE RIGHT(REGION_ID, 2) <> '00' AND REGION_ID = '$rqs[REGION_ID]' ")->row_array();

		echo "<div  class='Section1' style='text-align: center;font-size:11.0pt;'>

				<table width='100%' style='font-family: Cambria,Georgia,serif; ;'>
					<tr> 
						<td width='12%' style='line-height: 1.5;'><p> Nomor </p></td>
						<td width='3%' style='line-height: 1.5;'><p> : </p></td>
						<td width='50%' style='line-height: 1.5; text-align: justify;'>
							<p> ".$rqs['T_LETTER_NUMBER']." </p>
						</td>
						<td width='35%' style='line-height: 1.5; text-align: justify;'>
							<p> Jakarta, ".$this->tanggal_indo($rqs['T_DATE'])." </p>
						</td>
					</tr>

					<tr> 
						<td style='line-height: 1.5;'><p> Lampiran </p></td>
						<td style='line-height: 1.5;'><p> : </p></td>
						<td style='line-height: 1.5; text-align: justify;' colspan='2'>
							<p> ".$rqs['T_ATTACHMENT']." Berkas </p>
						</td>
					</tr>

					<tr> 
						<td style='line-height: 1.5;vertical-align: top; text-align:left;'><p> Perihal </p></td>
						<td style='line-height: 1.5;vertical-align: top; text-align:left;'><p> : </p></td>
						<td style='line-height: 1.5; text-align: justify;vertical-align: top; text-align:left;' colspan='2'>
							<p> ".$rqs['RQS_ABOUT']."  </p>
						</td>
					</tr>
				</table>

				<br/><br/>
				<table width='100%' style='font-family: Cambria,Georgia,serif; ;'>
					<tr>
						<td width='100%' colspan='3' style='line-height: 1.5; text-align: justify;'>
							<p> Kepada Yth. </p>
						</td>
					</tr> 

					<tr>
						<td width='100%' colspan='3' style='line-height: 1.5; text-align: justify;'>
							<p> ".$nama." </p>
						</td>
					</tr> 
					<tr>
						<td width='100%' colspan='3' style='line-height: 1.5; text-align: justify;'>
							<p> ".$rqs['USER_PIC_ADDR'].", ".$CITY_NAME['REGION_NAME'].", ".$PROVINCE_NAME['REGION_NAME']." ".$rqs['USER_PIC_ZIP']."</p>
						</td>
					</tr> 

					<tr>
						<td width='100%' colspan='3' style='line-height: 1.5; text-align: justify;'>
							<p> T/F: ".$rqs['USER_PIC_TELP']."/".$rqs['USER_PIC_FAX']." </p>
						</td>
					</tr> 

					<tr>
						<td width='100%' colspan='3' style='line-height: 1.5; text-align: justify;'>
							<p> Email: ".$rqs['USER_PIC_MAIL']."</p>
						</td>
					</tr> 


					<tr><td width='100%' colspan='3'>&nbsp;</td></tr>
					<tr><td width='100%' colspan='3'>&nbsp;</td></tr>


					<tr>
						<td width='100%' colspan='3' style='line-height: 1.5; text-align: justify;'>
							<p> Menindaklanjuti surat permohonan dari ".$nama.", dengan nomor ".$rqs['RQS_NO_AJU']." yang kami terima tanggal ".$this->tanggal_indo($rqs['RQS_VER_DATE'])." perihal penggunaan BTP ".$rqs['BTP_FUNCTION_NAME']." ".$rqs['BTP_TYPE_NAME']." (INS ".$rqs['BTP_TYPE_NO'].") pada Kategori Pangan ".$rqs['FOOD_TYPE_NO'].", dengan ini kami sampaikan bahwa :
							</p>
						</td>
					</tr>

					<tr><td width='100%' colspan='3'>&nbsp;</td></tr>

					<tr>
						<td width='2%' style='vertical-align:top; text-align:justify;font-family:Cambria,Georgia,serif;'><b><p> A. </p></b></td>
						<td width='95%' colspan='2' style='line-height:1.5; text-align:justify; font-family:Cambria,Georgia,serif;'>
							<b><p> Kategori Pangan </p></b>
						</td>
					</tr>

					<tr>
						<td width='5%'> &nbsp; </td>
						<td width='90%' style='line-height: 1.5; text-align: justify;' colspan='2'> 
							<p> Berdasarkan Peraturan Kepala Badan Pengawas Obat dan Makanan RI Nomor 21 tahun 2016 tentang Kategori Pangan, produk yang diajukan, yaitu ".$rqs['FOOD_TYPE_NAME']." termasuk dalam kategori pangan ".$ex[1].".  
							</p>
						</td>
					</tr>

					<tr>
						<td width='2%' style='vertical-align:top;'><b><p> B. </p></b></td>
						<td width='95%' colspan='2' style='line-height: 1.5; text-align: justify;'>
							<b><p> Bahan Tambahan Pangan </p></b>
						</td>
					</tr>

					<tr>
						<td width='5%'> &nbsp; </td>
						<td width='2%' style='vertical-align:top;'><p> 1. </p></td>
						<td width='90%' style='line-height: 1.5; text-align: justify;'> 
							<p> Peraturan Menteri Kesehatan nomor 033 Tahun 2012 tentang BTP mengatur ".$rqs['BTP_TYPE_NAME']." sebagai BTP ";
							$ab = 1;
							foreach ($rqs_permenkes as $key => $value) {
								if($ab == $num && $num > 1){
									echo "dan ".$value['BTP_FUNCTION_NAME'];
								}else if($ab == $num && $num == 1){
									echo $value['BTP_FUNCTION_NAME'];
								}else if($ab < $num && $num > 1){
									echo $value['BTP_FUNCTION_NAME'].", ";
								}
								$ab++;
							}
							echo "</p>
						</td>
					</tr>

					<tr>
						<td width='5%'> &nbsp; </td>
						<td width='2%' style='vertical-align:top;'><p> 2. </p></td>
						<td width='90%' style='line-height: 1.5; text-align: justify;'> 
							<p> Peraturan Kepala Badan POM Nomor ".$rqs['BTP_FUNCTION_NOMOR_PERATURAN']." Tahun ".$rqs['BTP_FUNCTION_YEAR']." tentang Batas Maksimum Penggunaan Bahan Tambahan Pangan ".$rqs['BTP_FUNCTION_NAME'].", tidak mengatur penggunaan ".$rqs['BTP_TYPE_NAME']." pada kategori pangan ".$ex[1]."  </p>
						</td>
					</tr>

					<tr>
						<td width='5%'> &nbsp; </td>
						<td width='2%' style='vertical-align:top;'><p> 3. </p></td>
						<td width='90%' style='line-height: 1.5; text-align: justify;'>";

							if($rqs['ND_STATUS_PERMENKES'] == "mengatur"){
								echo "<p style='color:red'> Codex stan 192-1995 Rev.17 tahun 2016 MENGATUR penggunaan ".$rqs['BTP_TYPE_NAME']." pada kategori pangan ".$ex[1]." dengan batas maksimum sebesar ".$rqs['RQS_CODEX_MAX_LIMIT']." ".$rqs['RQS_CODEX_UNIT'].". </p>";
							}else if($rqs['ND_STATUS_PERMENKES'] == "tidak mengatur"){
								echo "<p style='color:red'> Codex stan 192-1995 Rev.17 tahun 2016 TIDAK MENGATUR penggunaan ".$rqs['BTP_TYPE_NAME']." pada kategori pangan ".$ex[1].". </p>";
							}											
							echo "	</td>
					</tr>

					<tr>
						<td width='5%'> &nbsp; </td>
						<td width='2%' style='vertical-align:top;'><p> 4. </p></td>
						<td width='90%' style='line-height: 1.5; text-align: justify;'> 
							<p> ".$rqs['T_JUSTIFICATION'].", maka kami tidak merekomendasikan penggunaan ".$rqs['BTP_TYPE_NAME']." pada produk yang dimaksud. </p>
							<p>&nbsp;</p>
						</td>
					</tr>
				</table>

				<br/>
				<table width='100%' style='font-family: Cambria,Georgia,serif; '>
					<tr>
						<td width='100%' style='line-height: 1.5; text-align: justify;' colspan='2'>
							<p> Berdasarkan hal tersebut di atas, maka permohonan Saudara/i tidak dapat kami setujui. </p>
							<p> Demikian, atas perhatiannya kami sampaikan terima kasih. </p>
							<p>&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td width='30%'>&nbsp;</td>
						<td width='80%' style='text-align:center'><p><b>  a.n Kepala Badan Pengawas Obat dan Makanan </b></p></td>
					</tr>

					<tr>
						<td width='30%'>&nbsp;</td>
						<td width='80%' style='text-align:center'><p><b> Direktur Standardisasi Produk Pangan, </b></p></td>
					</tr>

					<tr><td width='100%' colspan='2'>&nbsp;</td></tr>
					<tr><td width='100%' colspan='2'>&nbsp;</td></tr>

					<tr>
						<td width='30%'>&nbsp;</td>
						<td width='80%' style='text-align:center'><p><b> Drs. Tepy Usia, Apt., M.Phil., Ph.D. </b></p></td>
					</tr>
				</table>
	

				<br/>
				<table width='100%' style='font-family: Cambria,Georgia,serif; ;'>
					<tr>
						<td width='100%' colspan='2'><p><b><u> Tembusan Yth. </u></b></p></td>
					</tr>

					<tr>
						<td width='5%' style='vertical-align: top; text-align:left;'><p> 1. </p></td>
						<td width='95%' style='font-family: Cambria,Georgia,serif; line-height: 1.5; text-align: justify;'><p> Deputi Bidang Pengawasan Keamanan Pangan dan Bahan Berbahaya (sebagai bahan laporan) </p></td>
					</tr>

					<tr>
						<td width='5%' style='vertical-align: top; text-align:left;'><p> 2. </p></td>
						<td width='95%' style='font-family: Cambria,Georgia,serif; line-height: 1.5; text-align: justify;'><p> Direktur Inspeksi dan Sertifikasi Pangan </p></td>
					</tr>

					<tr>
						<td width='5%' style='vertical-align: top; text-align:left;'><p> 3. </p></td>
						<td width='95%' style='font-family: Cambria,Georgia,serif;  line-height: 1.5; text-align: justify;'><p> Direktur Penilaian Keamanan Pangan </p></td>
					</tr>
				</table>
			</div>

			<table width='100%' style='font-family: Cambria,Georgia,serif; page-break-before:always;'>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>

								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>
								<p> &nbsp; </p>


			<table style='width:100%;border:1px solid; border-collapse: collapse; font-family: Cambria,Georgia,serif; text-align:center;font-size:11.0pt;' class='page-break'>
				<tr>
					<td align='center' style='width:100%;border:1px solid; border-collapse: collapse;text-align:center;'>
						<h5>
							<p style='font-family: Cambria,Georgia,serif;'><b> BADAN PENGAWAS OBAT DAN MAKANAN RI <br>
							DIREKTORAT STANDARDISASI PRODUK PANGAN </b><br>
							Jl. Percetakan Negara 23 Jakarta </b><br></p>
							<p> &nbsp; </p>
						</h5>
					</td>
				</tr>

				<tr>
					<td>
						<table border='0' style='font-family: Cambria,Georgia,serif;'>
							<tr>
								<td width='20%'><p> Diterima oleh </p></td>
								<td width='5%'><p> : </p></td>
								<td width='20%'><p> &nbsp; </p></td>

								<td width='20%'><p> Diterima bagian ketik  </p></td>
								<td width='5%'><p> : </p></td>
								<td width='20%'><p> &nbsp; </p></td>
							</tr>
							<tr>
								<td><p> Diselesaikan oleh </p></td>
								<td><p> : </p> </td>
								<td><p> &nbsp; </p></td>

								<td><p> Diketik oleh </p></td>
								<td><p> : </p> </td>
								<td><p> &nbsp; </p></td>
							</tr>
							<tr>
								<td><p> Diperiksa oleh Kasie Standardisasi ...  </p></td>
								<td><p> : </p> </td>
								<td><p> &nbsp; </p></td>

								<td><p> Dibaca oleh</p></td>
								<td><p> : </p> </td>
								<td><p> &nbsp; </p></td>
							</tr>
							<tr>
								<td><p> Diterima dibagian arsip </p></td>
								<td><p> : </p> </td>
								<td><p> &nbsp; </p></td>

								<td><p> Diperiksa oleh Kasubdit Standardisasi Bahan Baku dan Bahan Tambahan Pangan </p></td>
								<td><p> : </p> </td>
								<td><p> &nbsp; </p></td>
							</tr>
							<tr>
								<td> &nbsp;</td>
								<td> &nbsp;</td>
								<td> &nbsp;</td>

								<td><p> Dikirim pada tanggal </p></td>
								<td><p> : </p> </td>
								<td><p> &nbsp; </p></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td><p> &nbsp; </p><p>  Diajukan kembali pada tanggal : </p><p> &nbsp; </p></td>
				</tr>

				<tr>
					<td>
						<table style='width:100%;border:1px solid; border-collapse: collapse; font-family: Cambria,Georgia,serif; ;'>
								<tr>
									<td style='justify;font-family: Cambria,Georgia,serif;width:50%; line-height: 1.5;vertical-align: top; text-align:left;'>
										<p> HAL  &nbsp;&nbsp; : ".$rqs['RQS_ABOUT']."</p>

									</td>
									<td style='justify;font-family: Cambria,Georgia,serif;width:50%; line-height: 1.5;vertical-align: top; text-align:left;'>
										<p> No. Agenda &nbsp;&nbsp; : </p>

									</td>
								</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td>
						<table width='100%' style='font-family: Cambria,Georgia,serif; ;'>
							<tr>
								<td width='50%' align='left'>
									<p style='font-family: Cambria,Georgia,serif;'> Nomor &nbsp;&nbsp; : ....  </p>
								</td>
								<td width='50%' align='left'>
									<p style='font-family: Cambria,Georgia,serif;'> Jakarta, 10 Juni 2017 </p>
								</td>
							</tr>

							<tr>
								<td width='50%' align='left'>
									<p style='font-family: Cambria,Georgia,serif;'> Nota &nbsp;&nbsp; : ....  </p>
								</td>
								<td width='50%' align='left'>&nbsp;</td>
							</tr>

							<tr><td width='100%' align='left' colspan='2'>&nbsp;</td></tr>

							<tr>
								<td width='50%'>&nbsp;</td>
								<td width='50%' align='left'>
									<p style='font-family: Cambria,Georgia,serif;'> Memperhatikan: </p>
								</td>
							</tr>
							<tr>
								<td width='50%'>&nbsp;</td>
								<td width='50%' align='left'>
									<p style='font-family: Cambria,Georgia,serif;'> Kepada Yth. </p>
								</td>
							</tr>

							<tr>
								<td width='50%'>&nbsp;</td>
								<td width='50%' align='left'>
									<p style='font-family: Cambria,Georgia,serif;'><b> Nama Perusahaan </b></p>
								</td>
							</tr>

							<tr>
								<td width='50%'>&nbsp;</td>
								<td width='50%' align='left'>
									<p style='font-family: Cambria,Georgia,serif;'><b> Alamat Perusahaan dan kode pos (jika ada) </b></p>
								</td>
							</tr>

							<tr>
								<td width='50%'>&nbsp;</td>
								<td width='50%' align='left'>
									<p style='font-family: Cambria,Georgia,serif;'><b> T/F: Nomor telpon/fax perusahaan </b></p>
								</td>
							</tr>

							<tr>
								<td width='50%'>&nbsp;</td>
								<td width='50%' align='left'>
									<p style='font-family: Cambria,Georgia,serif;'><b> Email: abcdefg@gmail.com </b></p>
									<p>&nbsp;</p>
									<p>&nbsp;</p>
									<p>&nbsp;</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td><p style='font-family: Cambria,Georgia,serif;'> Ditetapkan : </p></td>
				</tr>
				<tr>
					<td>
						<p style='font-family: Cambria,Georgia,serif;'> Direktur Standardisasi Produk Pangan, </p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</td>
				</tr>

				<tr>
					<td><p style='font-family: Cambria,Georgia,serif;'><u> Drs. Tepy Usia, Apt., M.Phil., Ph.D.  </u></p></td>
				</tr>
				<tr>
					<td>
						<p style='font-family: Cambria,Georgia,serif;'> NIP. 19670614 199303 1 002 </p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</td>
				</tr>

				<tr>
					<td style='padding-left: 5px;'>
						<table width='30%' align='left' style='border:1px solid; border-collapse: collapse; padding-left: 5px; padding-bottom: 5px; font-family: Cambria,Georgia,serif; ;'>
							<tr>
								<td>
									<p style='font-family: Cambria,Georgia,serif;'><b> SD &nbsp;&nbsp; : </b> .... </p>
								</td>	
							</tr>
						</table>
					</td>
				</tr>
			</table>";

			}
		echo '
		</body>
		</html>
		';
		
	}

	 function coba(){ 
		header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename=report_".date("d_m_Y_G_i").".doc");
	 	$arrData['surat'] = '$sType'; 
		$this->load->view('prints',true); // ga bisa pake parameter $arrData
	 }

	 function coba2(){ 
		$today = date("Y-m-d-His");
		header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename=Invoice-".$today.".doc");
		$html = '<html>
		<meta http-equiv="Content-Type" content="text/html" charset="Windows-1252">
		    <xml>
		        <w:worddocument xmlns:w="#unknown">
		            <w:view>Print</w:view>
		            <w:zoom>90</w:zoom>
		            <w:donotoptimizeforbrowser />
		        </w:worddocument>
		    </xml>
		    <style>
				@page Section2 
					{size:841.7pt 595.45pt;
					mso-page-orientation:landscape;
					margin:1.25in 1.0in 1.25in 1.0in;
					mso-header-margin:.5in;
					mso-footer-margin:.5in;
					mso-paper-source:0;}
		        @page Section3
			        {size:8.5in 11.0in;
			         margin:1.0in 1.15in 1.0in 1.25in ;
			         mso-header-margin:.5in;
			         mso-footer-margin:.5in; 
			         mso-paper-source:0;}
		        @page Section1
			        {size:215mm 330mm;
			         margin:25.4mm 28.6mm 25.4mm 31.7mm ;
			         mso-header-margin:.5in;
			         mso-footer-margin:.5in; 
			         mso-paper-source:0;}
		        div.Section1
			        {page:Section1;}
		    </style>
		<body class="Section1">
		';
		//invoice header
		$html .= '
		<table width="800px">
		<tbody>
		<tr style="border-collapse:collapse">
			<td rowspan="3" width="50%">
			<p>hai</p>
			</td>
			<td width="50%">
		        <p>'.$today.'</p>
		        </td>
		</tr>
		<tr>
			<td>sadasd</td>
		</tr>
		</tbody>
		</table>
		';
		$html .='
		</body>
		</html>
		';
		echo $html;	 
	}
	function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$explo = explode(' ', $tanggal);
		$split = explode('-', $explo[0]);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}
	function uji($id){
		echo $this->tanggal_indo('2017-11-12');
	}


}
?>