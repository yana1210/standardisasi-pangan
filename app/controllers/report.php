<?php if(!defined('BASEPATH'))exit('No direct script access allowed'); 
class Report extends Controller{ 
	var $content = "";	
	function Home(){
		parent::Controller();
	}

	function index(){

		if($this->newsession->userdata('LOGGED_IN')){
			if($this->content==""){
				$gambar = TRUE;
			}
			$sUserid = $this->newsession->userdata('USER_ROLE');
			$this->menu = $this->load->view('in/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('in/footer', '', true),
						  '_header_' => $this->load->view('in/header', '', true));
			$this->parser->parse('in/home', $data);
		}else{
			
			$gambar = FALSE;			
			$this->load->model('login_act');
			
			if($this->content==""){
				$this->newsession->sess_destroy();
				$this->content = $this->load->view('login', $arrdata, true);
				
				$gambar = TRUE;
			}

//			$this->menu = $this->load->view('menu/out', '', true);
			$this->menu = $this->load->view('out/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('out/footer', '', true),
						  'gambar' => $gambar,
						  'captcha' => $arrdata['captcha'],
						  '_header_' => $this->load->view('out/header', '', true));
			$this->parser->parse('out/home', $data);
			
		}
	}

    function list_report($sType) {
		$this->load->model("in/report_act");
        $arrData = $this->report_act->get_arr_list_report($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
    function periodic() {
        $this->content = $this->load->view('in/report/periodic', '', true);
        $this->index();
    }
    function post($sMenu = "", $sSubMenu = "", $sSubMenu2 = "") {
        //print_r($_POST);die();
        $this->load->model('in/report_act');
        $sRet = $this->report_act->download($sMenu, $sSubMenu, $sSubMenu2);
        $a = 'Periodik';
        $count = count($sRet);
        $arrdata = array('baris' => $sRet, 'a' => $a, 'count' => $count);
        $ret = $this->load->view('in/report/Laporan1', $arrdata, true);
        $namafile = "Laporan $a (" . date("Y-m-d") . ")";

        $headers = '';
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$namafile.xls\"");
        echo "$headers\n$ret";

        die();
        echo $sRet;
    }

    function list_log($id = "") {
        $data = "";
        $this->load->model('in/report_act');
        $data = $this->report_act->list_log($id, TRUE);
        echo $data;
    }
	
}