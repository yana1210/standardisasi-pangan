<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Help extends Controller{
	var $content = "";
	
	function Help(){ 
		parent::Controller();
	}
	
		function index(){

		if($this->newsession->userdata('LOGGED_IN')){
			if($this->content==""){
				$gambar = TRUE;
			}
			$sUserid = $this->newsession->userdata('USER_ROLE');
			$this->menu = $this->load->view('in/menu', '', true);
			$data = array('_content_' => $this->load->view('menu/welcome', '', true),
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('in/footer', '', true),
						  '_header_' => $this->load->view('in/header', '', true));
			$this->parser->parse('in/home', $data);
		}else{
			
			$gambar = FALSE;			
			$this->load->model('login_act');
			if($this->content==""){
				$this->newsession->sess_destroy();
				$this->content = $this->load->view('login', $arrdata, true);
				
				$gambar = TRUE;
			}

			$this->menu = $this->load->view('out/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('out/footer', '', true),
						  'gambar' => $gambar,
						  'captcha' => $arrdata['captcha'],
						  '_header_' => $this->load->view('out/header', '', true));
			$this->parser->parse('out/home', $data);
			
		}
	}


	public function sub($sMenu="", $iId=""){
		$this->load->model("out/help_act"); 
		if($sMenu == "frm-reset"){
			$arrData = $this->help_act->get_help($sMenu, $iId);
			$this->content = $this->load->view('out/help/'.$sMenu, $arrData, true);
		} else if($sMenu == "frm-contact"){
			$arrData = $this->help_act->get_help($sMenu, $iId); 
			$this->content = $this->load->view('out/help/'.$sMenu, $arrData, true);
		} else if($sMenu == "frm-manual"){
			$arrData = $this->help_act->get_help($sMenu, $iId);
			$this->content = $this->load->view('out/help/'.$sMenu, '', true);
		}
		
		$this->index();                   
	}


	function post($sMenu="", $sSubmenu="", $sSubmenu2=""){
		$this->load->model('out/help_act');
		if($sMenu == "frm-reset"){ 
			$sRet = $this->help_act->set_help($sMenu, $sSubmenu, $sSubmenu2);
		}else if($sMenu == "frm-contact"){
			$sRet = $this->help_act->set_help($sMenu, $sSubmenu, $sSubmenu2);
		}else if($sMenu == "frm-password"){
			$sRet = $this->help_act->set_help($sMenu, $sSubmenu, $sSubmenu2);
		}
		echo $sRet;
	}

	
	public function reset($sMenu="", $iId=""){
		$arrdata['act'] = site_url('help/post/frm-password');
		$arrdata['id']  = $iId;
		$this->content = $this->load->view('out/help/frm-password',$arrdata, true);
		$this->index();

	}

}
?>