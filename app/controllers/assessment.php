<?php if(!defined('BASEPATH'))exit('No direct script access allowed'); 
class Assessment extends Controller{ 
	var $content = "";	
	function Home(){
		parent::Controller();
	}

	function index(){

		if($this->newsession->userdata('LOGGED_IN')){
			if($this->content==""){
				$gambar = TRUE;
			}
			$sUserid = $this->newsession->userdata('USER_ROLE');
			$this->menu = $this->load->view('in/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('in/footer', '', true),
						  '_header_' => $this->load->view('in/header', '', true));
			$this->parser->parse('in/home', $data);
		}else{
			
			$gambar = FALSE;			
			$this->load->model('login_act');
			
			if($this->content==""){
				$this->newsession->sess_destroy();
				$this->content = $this->load->view('login', $arrdata, true);
				
				$gambar = TRUE;
			}

//			$this->menu = $this->load->view('menu/out', '', true);
			$this->menu = $this->load->view('out/menu', '', true);
			$data = array('_content_' => $this->content,
						  '_menu_' => $this->menu,
						  '_footer_' => $this->load->view('out/footer', '', true),
						  'gambar' => $gambar,
						  'captcha' => $arrdata['captcha'],
						  '_header_' => $this->load->view('out/header', '', true));
			$this->parser->parse('out/home', $data);
			
		}
	}
    function get_arr_list($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/assessment_act");
        $arrData = $this->assessment_act->get_arr_list($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
    function list_assignment($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/assessment_act");
        $arrData = $this->assessment_act->get_arr_list_ass($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
    function list_ass($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/assessment_act");
        $arrData = $this->assessment_act->get_arr_list_draft($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
    function tracing($sType) { 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/assessment_act");
        $arrData = $this->assessment_act->get_arr_list_tracing($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
    function list_rev($sType) { 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/assessment_act");
        $arrData = $this->assessment_act->get_arr_list_rev($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
    function list_review($sType) {
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/assessment_act");
        $arrData = $this->assessment_act->get_arr_list_review($sType);
        $this->content = $this->load->view('table', $arrData, true);
        $this->index();
    }
	public function assignment($sMenu="", $iId=""){ 
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/assessment_act");
		$arrData = $this->assessment_act->assessment_get($sMenu, $iId);
		$this->content = $this->load->view('in/assignment/'.$sMenu, $arrData, true);
		$this->index();                   
	}
	public function ass($sMenu="", $iId=""){  
		if(!$this->newsession->userdata('LOGGED_IN')){
			redirect(base_url());
			die();
		}
		$this->load->model("in/assessment_act");
		$arrData = $this->assessment_act->assessment_get($sMenu, $iId);
		$this->content = $this->load->view('in/assessment/'.$sMenu, $arrData, true);
		$this->index();                   
	}

	function post($sMenu="", $sSubmenu="", $sSubmenu2=""){
		$this->load->model('in/assessment_act');
		$sRet = $this->assessment_act->set_assessment($sMenu, $sSubmenu, $sSubmenu2);
		echo $sRet;
	}
    function list_log($id = "") {
        $data = "";
        $this->load->model('in/assessment_act');
        $data = $this->assessment_act->list_log($id, TRUE);
        echo $data;
    }

    public function email_exists_check() {
        $sMail = $this->input->post('email');
        $query = $this->db->query("select USER_MAIL from TM_USER where USER_MAIL = '$sMail' limit 1 ")->row_array();
        if ($query['USER_MAIL'] == '') {
            echo "0|||Email Tersedia"; //json_encode(TRUE);
        } else {
            echo "1|||Email ".$this->input->post('email')." Sudah digunakan"; //json_encode(TRUE);
        }
    }

	
}