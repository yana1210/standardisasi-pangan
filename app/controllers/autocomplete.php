<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autocomplete extends Controller{
	function Autocomplete(){
		parent::Controller();
	}
	
	function index(){
		
	}
	function cek_sess(){ //print_r($_POST); die();
		$sess = $this->newsession->userdata('LOGGED_IN');
		$user_id = $this->input->post('user_id');
		//echo $sess; die();
		if($sess != 1){
			$this->db->query("update tm_user set user_login='0' where user_id= '$user_id' ");
			$this->newsession->sess_destroy();
			$return = 0;
		}else{
			$this->db->query("update tm_user set user_login='0' where user_id= '$user_id' ");
			$this->newsession->sess_destroy();
			$return = 1;
		}
		echo $return;
	}
	
	function get_arr_ingredients($iId=""){     
		if($this->newsession->userdata('LOGGED_IN')){
			$ereg = get_instance();
			$ereg->load->model("main","main", true);
			$key = str_replace("'", "''", strtolower($_REQUEST['q']));
			$this->load->model('main');
			$sKat = $ereg->main->get_arr_uraian("SELECT CATEGORY_ID FROM TX_PRODUCT WHERE PRODUCT_ID = '$iId'", "CATEGORY_ID");
			$iTot = $ereg->main->get_arr_uraian("SELECT COUNT(*) AS TOT FROM TM_CATEGORY_INGREDIENTS WHERE CATEGORY_ID = '$sKat' AND INGREDIENTS_ID <> '0'", "TOT");
			$jenis = $ereg->main->get_arr_uraian("SELECT PRODUCT_TYPE FROM TX_PRODUCT WHERE PRODUCT_ID = '$iId'", "PRODUCT_TYPE");
			$PRODUCT_DOCUMENT_TYPE = $ereg->main->get_arr_uraian("SELECT DOCUMENT_TYPE FROM TX_PRODUCT WHERE PRODUCT_ID = '$iId'", "DOCUMENT_TYPE");
			//print_r($PRODUCT_DOCUMENT_TYPE);die();
			if($this->newsession->userdata('ROLE') == "RL00"){
				$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') AND A.STATUS_ID <> 'IN02' ORDER BY 1 LIMIT 10";
			}else if($PRODUCT_DOCUMENT_TYPE == "DTY208"){
				$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%' ) AND A.STATUS_ID in ('IN03','IN01') ORDER BY 1 LIMIT 15";
				//data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%' ) AND A.INGREDIENTS_ID IN (SELECT INGREDIENTS_ID FROM TM_CATEGORY_INGREDIENTS WHERE CATEGORY_ID = '$sKat') AND A.STATUS_ID in ('IN03','IN01') ORDER BY 1 LIMIT 15";
			}else if($PRODUCT_DOCUMENT_TYPE != "DTY201"){
				$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') AND A.STATUS_ID = 'IN01' ORDER BY 1 LIMIT 15";
			}else{
				if($jenis=="DTY202"||$jenis=="DTY213"){
					if($iTot>0) $data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%' ) ORDER BY 1 LIMIT 10";
					else $data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') ORDER BY 1 LIMIT 10";	
				}else{

					if($iTot>0){ 
						if($sKat == '010101'){
							$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') AND A.STATUS_ID = 'IN01' AND A.INGREDIENTS_ID NOT IN (7794, 29315, 111, 4389, 18659, 17225) ORDER BY 1 LIMIT 15";
						}else{

							$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%' ) AND A.INGREDIENTS_ID IN (SELECT INGREDIENTS_ID FROM TM_CATEGORY_INGREDIENTS WHERE CATEGORY_ID = '$sKat') AND A.STATUS_ID = 'IN01' ORDER BY 1 LIMIT 15";

						}
					}else{ 
						$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') AND A.STATUS_ID = 'IN01' ORDER BY 1 LIMIT 15";
					}
				}
			}	
			$res = $this->main->get_arr_result($data);
			if($res){
				foreach($data->result_array() as $row){
					$bahanereg[] = "<b>".trim($row['INGREDIENTS_NAME_SIMPLISIA'])."</b><br>&bull; ".trim($row['INGREDIENTS_SECTION'])." ".$row['INGREDIENTS_NAME']."<br>&bull; <i>".trim($row['INGREDIENTS_NAME_LATIN'])."</i>|".trim($row['INGREDIENTS_NAME_SIMPLISIA'])."|".trim(ucwords(strtolower($row['INGREDIENTS_NAME_SIMPLISIA'])))."|".$row['INGREDIENTS_ID']."|".$row['INGREDIENTS_NAME']."|".$row['INGREDIENTS_NAME_LATIN']."|1\n";
				}
			}else{
				$bahanereg = array();	
			}
			$bahansimplisia = array();
			$bahanotsm=array();
			$bahan = array_merge($bahansimplisia, $bahanotsm, $bahanereg);
			foreach($bahan as $a){
				echo $a;	
			}
		}
	}
function get_arr_factorys($iId=""){     
			$ereg = get_instance();
			$ereg->load->model("main","main", true);
			$key = str_replace("'", "''", strtolower($_REQUEST['q']));
			$this->load->model('main');
			$data = "SELECT factory_id,factory_name,factory_address FROM tm_factory where factory_name like '%$key%'";
			
			$res = $this->main->get_arr_result($data);
			if($res){
				foreach($data->result_array() as $row){
					$pabrik[] = "<b>".trim($row['factory_name'])."</b><br>&bull; ".trim($row['factory_address'])." ";
				}
			}else{
				$pabrik = array();	
			}
			
			$bahansimplisia = array();
			$bahanotsm=array();
			$bahan = array_merge($bahansimplisia, $bahanotsm, $pabrik);
			foreach($bahan as $a){
				echo $a;	
			}
		}

		function get_arr_bahan(){     
			if($this->newsession->userdata('LOGGED_IN')){
			$ereg = get_instance();
			$ereg->load->model("main","main", true);
			$key = str_replace("'", "''", strtolower($_REQUEST['q']));
			$this->load->model('main');
			$sKat = $ereg->main->get_arr_uraian("SELECT CATEGORY_ID FROM TX_PRODUCT WHERE PRODUCT_ID = '$iId'", "CATEGORY_ID");
			$iTot = $ereg->main->get_arr_uraian("SELECT COUNT(*) AS TOT FROM TM_CATEGORY_INGREDIENTS WHERE CATEGORY_ID = '$sKat' AND INGREDIENTS_ID <> '0'", "TOT");
			$jenis = $ereg->main->get_arr_uraian("SELECT PRODUCT_TYPE FROM TX_PRODUCT WHERE PRODUCT_ID = '$iId'", "PRODUCT_TYPE");
			$PRODUCT_DOCUMENT_TYPE = $ereg->main->get_arr_uraian("SELECT DOCUMENT_TYPE FROM TX_PRODUCT WHERE PRODUCT_ID = '$iId'", "DOCUMENT_TYPE");
			//print_r($PRODUCT_DOCUMENT_TYPE);die();
			if($this->newsession->userdata('ROLE') == "RL00"){
				$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') AND A.STATUS_ID <> 'IN02' ORDER BY 1 LIMIT 10";
			}else if($PRODUCT_DOCUMENT_TYPE == "DTY208"){
				$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%' ) AND A.STATUS_ID in ('IN03','IN01') ORDER BY 1 LIMIT 15";
				//data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%' ) AND A.INGREDIENTS_ID IN (SELECT INGREDIENTS_ID FROM TM_CATEGORY_INGREDIENTS WHERE CATEGORY_ID = '$sKat') AND A.STATUS_ID in ('IN03','IN01') ORDER BY 1 LIMIT 15";
			}else if($PRODUCT_DOCUMENT_TYPE == ""){
				$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%')  ORDER BY 1 LIMIT 15";
			}else if($PRODUCT_DOCUMENT_TYPE != "DTY201"){
				$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') AND A.STATUS_ID = 'IN01' ORDER BY 1 LIMIT 15";
			}else{
				if($jenis=="DTY202"||$jenis=="DTY213"){
					if($iTot>0) $data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%' ) ORDER BY 1 LIMIT 10";
					else $data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') ORDER BY 1 LIMIT 10";	
				}else{

					if($iTot>0){ 
						if($sKat == '010101'){
							$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') AND A.STATUS_ID = 'IN01' AND A.INGREDIENTS_ID NOT IN (7794, 29315, 111, 4389, 18659, 17225) ORDER BY 1 LIMIT 15";
						}else{

							$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%' ) AND A.INGREDIENTS_ID IN (SELECT INGREDIENTS_ID FROM TM_CATEGORY_INGREDIENTS WHERE CATEGORY_ID = '$sKat') AND A.STATUS_ID = 'IN01' ORDER BY 1 LIMIT 15";

						}
					}else{ 
						$data = "SELECT A.INGREDIENTS_ID, A.INGREDIENTS_NAME, A.INGREDIENTS_NAME_LATIN, A.INGREDIENTS_NAME_SIMPLISIA, A.INGREDIENTS_SECTION FROM TM_INGREDIENTS A WHERE (A.INGREDIENTS_NAME LIKE '%$key%' OR A.INGREDIENTS_NAME_SIMPLISIA LIKE '%$key%' OR A.INGREDIENTS_NAME_LATIN LIKE '%$key%') AND A.STATUS_ID = 'IN01' ORDER BY 1 LIMIT 15";
					}
				}
			}	
			$res = $this->main->get_arr_result($data);
			if($res){
				foreach($data->result_array() as $row){
					$bahanereg[] = "<b>".$row['INGREDIENTS_NAME']."<br>&bull; <i>".trim($row['INGREDIENTS_NAME_LATIN'])."</i>|".trim($row['INGREDIENTS_NAME_SIMPLISIA'])."|".trim(ucwords(strtolower($row['INGREDIENTS_NAME_SIMPLISIA'])))."|".$row['INGREDIENTS_ID']."|".$row['INGREDIENTS_NAME']."|".$row['INGREDIENTS_NAME_LATIN']."|1\n";
				}
			}else{
				$bahanereg = array();	
			}
			$bahansimplisia = array();
			$bahanotsm=array();
			$bahan = array_merge($bahansimplisia, $bahanotsm, $bahanereg);
			foreach($bahan as $a){
				echo $a;	
			}
		}
		}
	

	function get_arr_city($province=""){
		echo '<option value="">&nbsp;</option>';
		if($province!=""){
			$this->load->model('main');
			$province = substr($province, 0, 2);
			$data = "SELECT REGION_ID, REGION_NAME FROM TM_REGION WHERE REGION_ID LIKE '$province%' AND RIGHT(REGION_ID, 2) <> '00' ORDER BY 2";
			
			$data = $this->db->query($data)->result_array();
			
			
				foreach($data as $row){
					echo '<option value="'.$row['REGION_ID'].'">'.$row['REGION_NAME'].'</option>';
				}
		}
	}
/*
	function get_arr_btntype($noisn=""){
		$data = $this->db->get_where('TR_BTP_TYPE', array('BTP_TYPE_ID'=>$_POST['id']))->row();
		echo $data->BTP_TYPE_NAME;
	}

	function get_arr_btnfoodtype($foodtype=""){
		$data = $this->db->get_where('TR_FOOD_TYPE', array('FOOD_TYPE_ID'=>$_POST['id']))->row();
		echo $data->FOOD_TYPE_NO;
	}
*/
	function get_arr_btntype($noisn=""){
        $RQS_FOOD_TYPE_ID = $this->input->post('RQS_FOOD_TYPE_ID');
		$data = $this->db->query("select food_type_name,food_type_no from TR_FOOD_TYPE where FOOD_TYPE_ID = '$RQS_FOOD_TYPE_ID' ")->row_array();
		$nokatpang = $data['food_type_no'];
		$data2 = $this->db->query("SELECT concat(food_type_no, '. ', food_type_name) as kategori_pangan FROM tr_food_type WHERE food_type_no = '$nokatpang' ORDER BY food_type_id LIMIT 1 ")->row_array();

		$id = $_POST['id'];
		$tr_btp_type = $this->db->query("select btp_type_name,btp_type_no from TR_BTP_TYPE where BTP_TYPE_ID = $id ")->row_array();
		echo $tr_btp_type['btp_type_name']."|||Penggunaan ".$tr_btp_type['btp_type_name']." (INS. ".$tr_btp_type['btp_type_no'].") pada Kategori Pangan ".$data2['kategori_pangan'];
//		echo $tr_btp_type['btp_type_name']."|||Penggunaan Bahan Tambahan Pangan ".$tr_btp_type['btp_type_name']." (".$tr_btp_type['btp_type_no'].") pada produk ".$data['food_type_name']." (".$data['food_type_no'].")";
	}

	function get_arr_btnfoodtype($foodtype=""){
		$id = $_POST['id'];
		$data = $this->db->query("select food_type_name,food_type_no, concat(food_type_no, ' - ', food_type_name) from TR_FOOD_TYPE where FOOD_TYPE_ID = $id ")->row_array();
		$nokatpang = $data['food_type_no'];
		$data2 = $this->db->query("SELECT concat(food_type_no, '. ', food_type_name) as kategori_pangan FROM tr_food_type WHERE food_type_no = '$nokatpang' ORDER BY food_type_id LIMIT 1 ")->row_array();

        $noisn = $this->input->post('noisn');
        $tr_btp_type = $this->db->query("select btp_type_name,btp_type_no from tr_btp_type where BTP_TYPE_ID = '$noisn'  ")->row_array();

		echo $data2['kategori_pangan']."|||Penggunaan ".$tr_btp_type['btp_type_name']." (INS. ".$tr_btp_type['btp_type_no'].") pada Kategori Pangan ".$data2['kategori_pangan'];
//		echo $data2['kategori_pangan']."|||Penggunaan ".$tr_btp_type['btp_type_name']." (INS. ".$tr_btp_type['btp_type_no'].") pada Kategori Pangan ".$data['food_type_name']." (".$data['food_type_no'].")";
	}

	function get_arr_sediaan($FACTORY_ID="",$a=""){//die($FACTORY_ID);
		echo '<option value="">&nbsp;</option>';
		if($FACTORY_ID!=""){
			$this->load->model('main');
			$data = "SELECT A.`PREP_ID`,B.`PREP_NAME` FROM TM_TRADER_PREP A LEFT JOIN TM_PREP B ON A.`PREP_ID`=B.`PREP_ID` WHERE A.FACTORY_ID = '$FACTORY_ID' AND A.STATUS_ID='TP01' ";
			$data = $this->db->query($data)->result_array();
			
			//print_r($data);die();
			
				foreach($data as $row){
					echo '<option value="'.$row['PREP_ID'].'">'.$row['PREP_NAME'].'</option>';
				}

		}
	}
	function get_arr_sediaan_ot($FACTORY_ID=""){
		echo '<option value="">&nbsp;</option>';
		if($FACTORY_ID!=""){
			$this->load->model('main');
			$data = "SELECT A.`PREP_ID`,B.`PREP_NAME` FROM TM_TRADER_PREP A LEFT JOIN TM_PREP B ON A.`PREP_ID`=B.`PREP_ID` WHERE A.FACTORY_ID = '$FACTORY_ID' AND B.status_id = '1' AND A.STATUS_ID='TP01'";
			
			$data = $this->db->query($data)->result_array();
			
			
				foreach($data as $row){
					echo '<option value="'.$row['PREP_ID'].'">'.$row['PREP_NAME'].'</option>';
				}
		}
	}

	function get_arr_kelompok($CATEGORY_PARENT=""){
		echo '<option value="">&nbsp;</option>';
		if($CATEGORY_PARENT!=""){
			$this->load->model('main');
			if(strlen($CATEGORY_PARENT) == 2){
				$data = "SELECT * FROM TM_CATEGORY WHERE LENGTH(CATEGORY_ID) = 4 AND CATEGORY_PARENT = '$CATEGORY_PARENT' ";
			}else{
				$data = "SELECT * FROM TM_CATEGORY WHERE LENGTH(CATEGORY_ID) = 6 AND CATEGORY_PARENT = '$CATEGORY_PARENT' ";
			}
			
			$data = $this->db->query($data)->result_array();
			
			
				foreach($data as $row){
					echo '<option value="'.$row['CATEGORY_ID'].'">'.$row['CATEGORY_NAME'].'</option>';
				}
		}
	}

	function negara(){
		if($this->newsession->userdata('LOGGED')){
			$key = str_replace("'", "''", strtolower($_REQUEST['q']));
			$this->load->model('main');
			$data = "SELECT * FROM M_NEGARA WHERE LOWER(NEGARA_ID) LIKE '%$key%' OR LOWER(NAMA) LIKE '%$key%' ORDER BY NAMA LIMIT 15";
			$res = $this->main->get_result($data);
			if($res){
				foreach($data->result_array() as $row){
					echo "<b>".strtoupper($row['NEGARA_ID'])."</b><br>".strtoupper($row['NAMA'])."|".strtoupper($row['NEGARA_ID'])."|".strtoupper($row['NAMA'])."\n";
				}
			}
		}
	}

	function pabrik($status=1, $md=""){ 
		$this->load->model('main');
		if($status==1){
			$key = str_replace("'", "''", strtolower($_REQUEST['q']));
			if(array_key_exists('12', $this->newsession->userdata('ROLE'))){#edityo
				$data = "SELECT A.FACTORY_NAME, REPLACE(A.FACTORY_ADDRESS,'\n','-') AS FACTORY_ADDRESS, CONCAT(LEFT(A.REGION_ID, 2), '00') AS PROVINSI, A.REGION_ID, A.FACTORY_ID, B.REGION_NAME AS NAMA_DAERAH FROM TM_FACTORY A LEFT JOIN TM_REGION B ON B.REGION_ID = CONCAT(LEFT(A.REGION_ID, 2), '00') WHERE A.FACTORY_NAME LIKE '%$key%' AND A.FACTORY_NAME <> '' ORDER BY 1 LIMIT 15"; 
			}else{
				if($md=="ML")
					$data = "SELECT A.FACTORY_NAME, REPLACE(A.FACTORY_ADDRESS,'\n','-') AS FACTORY_ADDRESS, A.COUNTRY_ID, B.COUNTRY_NAME AS NAMA_NEGARA, A.FACTORY_ID FROM TM_FACTORY A LEFT JOIN TM_COUNTRY B ON B.COUNTRY_ID = A.COUNTRY_ID WHERE A.FACTORY_NAME LIKE '%key%' AND A.FACTORY_NAME <> '' AND A.COUNTRY_ID <> 'ID' ORDER BY 1 LIMIT 15";
				else
					$data = "SELECT A.FACTORY_NAME, REPLACE(A.FACTORY_ADDRESS,'\n','-') AS FACTORY_ADDRESS, CONCAT(LEFT(A.REGION_ID, 2), '00') AS PROVINSI, A.REGION_ID, A.FACTORY_ID, B.REGION_NAME AS NAMA_DAERAH FROM TM_FACTORY A LEFT JOIN TM_REGION B ON B.REGION_ID = CONCAT(LEFT(A.REGION_ID, 2), '00') WHERE A.FACTORY_NAME LIKE '%$key%' AND A.FACTORY_NAME <> '' AND A.COUNTRY_ID = 'ID' ORDER BY 1 LIMIT 15";
			}			
			$res = $this->main->get_arr_result($data);
			if($res){
				foreach($data->result_array() as $row){
					if($md=="ML")
						echo "<b>".strtoupper($row['FACTORY_NAME'])."</b><br>".$row['FACTORY_ADDRESS']."|".strtoupper($row['FACTORY_NAME'])."|".$row['FACTORY_ADDRESS']."|".$row['COUNTRY_ID']."|".$row['FACTORY_ID']."\n";
					else
						echo "<b>".strtoupper($row['FACTORY_NAME'])."</b><br>".$row['FACTORY_ADDRESS']."|".strtoupper($row['FACTORY_NAME'])."|".$row['FACTORY_ADDRESS']."|".$row['PROVINSI']."|".$row['REGION_ID']."|".$row['FACTORY_ID']."\n";
				}
			}
		}else{
			echo '<option value="">&nbsp;</option>';
			if($md!="" && $md!="MD" && $md!="ML"){
				$data = "SELECT A.PABRIK_ID, B.NAMA_PABRIK + ' D/A ' + B.ALAMAT_PABRIK AS NAMA_PABRIK  FROM M_TRADER_PABRIK A LEFT JOIN M_PABRIK B ON A.PABRIK_ID = B.PABRIK_ID WHERE A.PABRIK_ID > 0 AND A.TRADER_ID = ".$this->newsession->userdata('TRADER_ID')." AND A.STATUS_USAHA = '$md' AND A.STATUS <> '0' ORDER BY 2";
				$res = $this->main->get_arr_result($data);
				if($res){
					foreach($data->result_array() as $row){
						echo '<option value="'.$row['PABRIK_ID'].'">'.$row['NAMA_PABRIK'].'</option>';
					}
				}
			}
		}
	}
	function ins($status=1, $md=""){ 
		$this->load->model('main');
		$key = str_replace("'", "''", strtolower($_REQUEST['q']));
			$data = "select concat(btp_type_no,' [',btp_type_name,']') as 'ins',btp_type_id,btp_type_name from tr_btp_type  WHERE btp_type_no like '%$key%' order by 1 limit 15";
		$res = $this->main->get_arr_result($data);
		if($res){
			foreach($data->result_array() as $row){
				echo $row['ins']."|".$row['btp_type_id']."|".$row['btp_type_name']."\n";
//				echo "<b>".strtoupper($row['FACTORY_NAME'])."</b><br>".$row['FACTORY_ADDRESS']."|".strtoupper($row['FACTORY_NAME'])."|".$row['FACTORY_ADDRESS']."|".$row['PROVINSI']."|".$row['REGION_ID']."|".$row['FACTORY_ID']."\n";
			}
		}
	}
	function jenis_pangan($status=1, $md=""){ 
		$this->load->model('main');
		$key = str_replace("'", "''", strtolower($_REQUEST['q']));
			$data = "select food_type_name,food_type_id,food_type_no from tr_food_type  WHERE food_type_name like '%$key%' order by 1 limit 15";
		$res = $this->main->get_arr_result($data);
		if($res){
			foreach($data->result_array() as $row){
				echo $row['food_type_name']."|".$row['food_type_id']."|".$row['food_type_no']."\n";
			}
		}
	}
		
	
}
?>