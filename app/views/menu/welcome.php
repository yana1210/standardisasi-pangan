            <div class="page-title">
              <div class="title_left">
               <!--  <h3>SELAMAT DATANG</h3> -->
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title"> -->
                  <div class="title_left">
                    <h3>SELAMAT DATANG</h3>
                    <h4><small> Anda login sebagai <?= $this->newsession->userdata('ROLE_NAME') ?>, Terakhir login <?=  $this->newsession->userdata('USER_LOG_DATE')?> </small></h4>
                  </div>
                  <div class="x_content">
                    <table class="table" style="padding-top:0px;">
                      <tr>
                        <td rowspan="5" width="30%"> 
                          <!--<img src="<?=site_url('download/photo/'.$_SESSION['USER_PATH']);?>" class="img-responsive" alt="" width="250px" height="250px">-->
                        </td>
                        <td width="25%">Nama Lengkap </td>
                        <td width="5%">&nbsp;:&nbsp;</td>
                        <td><?= $this->newsession->userdata('USER_NAME') ?></td>
                      </tr>
                      <tr>
                        <td width="25%">Email </td>
                        <td width="5%">&nbsp;:&nbsp;</td>
                        <td><?= $this->newsession->userdata('USER_MAIL') ?></td>
                      </tr>
                      <tr>
                        <td colspan="3">&nbsp;</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>

<?php 
if($this->newsession->userdata('ROLE_ID') == '607'){
?>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12">
                <div class="">
                  <div class="x_content">

                      <div class=" animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                        <a href="<?= site_url()."assessment/tracing/all"; ?>">
                          <div class="icon"><i class="fa fa-check-square-o"></i>
                          </div>
                          <div class="count" data-toggle="tooltip" data-placement="top" title="Klik untuk melihat berkas permohonan yang masuk">
                          <?php
                            $co = $this->db->query("select count(rqs_id) as jml from tx_rqs WHERE `RQS_STATUS_ID` IN ('RS02','RS04','RS05','RS06') AND `RQS_PROCESS_ID` IS NOT NULL")->row_array();
                            echo $co['jml'];
                          ?>

                          </div>
                          <p style="font-size: 15px">Berkas Permohonan yang Masuk</p>
                        </a>
                        </div>
                      </div>
                    <?php //$sTable ?>
                  </div>
                </div>
              </div>
            </div>
<?php }else if($this->newsession->userdata('ROLE_ID') == '606'){ ?>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12">
                <div class="">
                  <div class="x_content">

                      <div class=" animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                        <a href="<?= site_url()."assessment/tracing/all"; ?>">
                          <div class="icon"><i class="fa fa-check-square-o"></i>
                          </div>
                          <div class="count" data-toggle="tooltip" data-placement="top" title="Klik untuk melihat berkas permohonan yang masuk">
                          <?php
                            $co = $this->db->query("select count(rqs_id) as jml from tx_rqs WHERE `RQS_STATUS_ID` IN ('RS02','RS04','RS05','RS06') AND `RQS_PROCESS_ID` IS NOT NULL")->row_array();
                            echo $co['jml'];
                          ?>

                          </div>
                          <p style="font-size: 15px">Berkas Permohonan yang Masuk</p>
                        </a>
                        </div>
                      </div>
                      <div class=" animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                        <a href="<?= site_url()."assessment/list_rev/PR07"; ?>">
                          <div class="icon"><i class="fa fa-undo"></i>
                          </div>
                          <div class="count" data-toggle="tooltip" data-placement="top" title="Klik untuk melihat kajian yang di revisi">
                          <?php
                            $co = $this->db->query("select count(rqs_id) as jml from tx_rqs WHERE `RQS_PROCESS_ID` IN ('PR07') AND `RQS_PROCESS_ID` IS NOT NULL")->row_array();
                            echo $co['jml'];
                          ?>

                          </div>
                          <p style="font-size: 15px">Revisi Kajian Dari Direktur</p>
                        </a>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
<?php }else if($this->newsession->userdata('ROLE_ID') == '605'){ ?>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12">
                <div class="">
                  <div class="x_content">

                      <div class=" animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                        <a href="<?= site_url()."assessment/tracing/all"; ?>">
                          <div class="icon"><i class="fa fa-check-square-o"></i>
                          </div>
                          <div class="count" data-toggle="tooltip" data-placement="top" title="Klik untuk melihat berkas permohonan yang masuk">
                          <?php
                            $co = $this->db->query("select count(rqs_id) as jml from tx_rqs WHERE `RQS_STATUS_ID` IN ('RS02','RS04','RS05','RS06') AND `RQS_PROCESS_ID` IS NOT NULL")->row_array();
                            echo $co['jml'];
                          ?>

                          </div>
                          <p style="font-size: 15px">Berkas Permohonan yang Masuk</p>
                        </a>
                        </div>
                      </div>
                      <div class=" animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                        <a href="<?= site_url()."assessment/list_rev/PR10"; ?>">
                          <div class="icon"><i class="fa fa-rotate-left"></i>
                          </div>
                          <div class="count" data-toggle="tooltip" data-placement="top" title="Klik untuk melihat kajian yang di revisi">
                          <?php
                            $co = $this->db->query("select count(rqs_id) as jml from tx_rqs WHERE `RQS_PROCESS_ID` IN ('PR10') AND `RQS_PROCESS_ID` IS NOT NULL")->row_array();
                            echo $co['jml'];
                          ?>

                          </div>
                          <p style="font-size: 15px">Revisi Kajian Dari Kasubdit</p>
                        </a>
                        </div>
                      </div>
                      <div class=" animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                        <a href="<?= site_url()."assessment/list_rev/PR07"; ?>">
                          <div class="icon"><i class="fa fa-undo"></i>
                          </div>
                          <div class="count" data-toggle="tooltip" data-placement="top" title="Klik untuk melihat kajian yang di revisi">
                          <?php
                            $co = $this->db->query("select count(rqs_id) as jml from tx_rqs WHERE `RQS_PROCESS_ID` IN ('PR07') AND `RQS_PROCESS_ID` IS NOT NULL")->row_array();
                            echo $co['jml'];
                          ?>

                          </div>
                          <p style="font-size: 15px">Revisi Kajian Dari Direktur</p>
                        </a>
                        </div>
                      </div>

                  </div>
                </div>
              </div>
            </div>
<?php } ?>
