        <div id="wrap">
            <div class="main" id="main-no-space">  
                <div id="main-page">
                    <div id="wrapper" class="container">
                         
                      <div class="headline"><h3><font style="color:rgb(0, 128, 0)"><b>BROSUR</b></font></h3></div>
                      <div class = "row"></div>
                      <div class="isi_">
    <br />
      <ol>
          <B><font style="color:rgb(0, 128, 0)"><li>Produk apakah yang bisa didaftarkan di sistem e-registration OTSM ?</b></font>
 <p>Untuk saat ini, pendaftaran hanya berlaku untuk obat tradisional dengan komposisi sederhana  yang hanya mengandung simplisia yang sudah dikenal secara empiris dengan klaim penggunaan tradisional , dengan tingkat pembuktian umum, dalam bentuk sediaan sederhana minyak obat luar, parem, tapel, pilis, rempah mandi, serbuk luar, salep, ratus, serbuk, dan cairan obat dalam dimana profil keamanan dan kemanfaatan telah diketahui pasti.</p></li>
      <B><font style="color:rgb(0, 128, 0)"><li>Ada  berapa tahapan dalam proses pendaftaran ?</b></font>
<p>Proses pendaftaran terdiri atas 2 tahapan, yaitu:<br />
&bull; Pendaftaran perusahaan untuk mendapatkan id perusahaan<br />
&bull; Pendaftaran produk<br />
Sistem e-registrasi ini melibatkan proses self assessment, sehingga produk harus dipastikan tidak mengandung bahan yang dilarang digunakan dan memenuhi persayaratan mutu.</p>
</li>
      <B><font style="color:rgb(0, 128, 0)"><li>Berapa lama proses pendaftaran ?<p></b></font>
Lama proses pendaftaran adalah 7 hari kerja semenjak produsen menyerahkan bukti hasil pembayaran Surat Perintah Bayar (SPB) Registrasi.</p>
</li>
      <B><font style="color:rgb(0, 128, 0)"><li>Berapa tarif yang dikenakan dalam proses pendaftaran ?<p></b></font>
Tarif dikenakan per produk yang didaftarkan sesuai dengan PNBP  (Penerimaan Negara Bukan Pajak) yang tertuang dalam Peraturan Pemerintah RI NOMOR 48 TAHUN 2010 tentang  Jenis Dan Tarif Atas Jenis Penerimaan Negara Bukan Pajak Yang Berlaku Pada Badan Pengawas Obat Dan Makanan.
Surat Perintah Bayar per produk dikeluarkan 2 kali, yaitu:<br />
&bull; Surat Perintah Bayar Pra Registrasi sebesar Rp. 50.000,-<br />
&bull; Surat Perintah Bayar Registrasi </p>
</li>
        </ol>
  </div>
                        
                        
                        
                        
                       </div>
                    </div>
                </div>
            </div>  
        