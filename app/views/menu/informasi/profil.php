        <div id="wrap">
            <div class="main" id="main-no-space">  
                <div id="main-page">
                    <div id="wrapper" class="container">
                         
                      <div class="headline"><h3><font style="color:rgb(0, 128, 0)"><b>PROFIL</b></font></h3></div>
                      <div class = "row"></div>
                      <div class="isi_" style="font-size: 16px">
                        <br />
                        <center><b>PROFIL DIREKTORAT PENILAIAN OBAT TRADISIONAL,<br />SUPLEMEN MAKANAN DAN KOSMETIK</b></center>
                        <p>Direktorat Penilaian Obat Tradisional, Suplemen Makanan dan Kosmetik merupakan salah satu dari 4 (empat) direktorat di Kedeputian Bidang Pengawasan Obat Tradisional, Kosmetik dan Produk Komplemen. Direktorat Penilaian Obat Tradisional, Suplemen Makanan dan Kosmetik dalam menjalankan Visi dan Misi Badan POM melaksanakan pengawasan Pre-Market Evaluation  di bidang pendaftaran obat tradisional, suplemen makanan dan notifikasi kosmetika.</p>
                        <p>Berdasarkan Keputusan Kepala Badan Pengawas Obat dan Makanan Nomor 02001/SK KBPOM Direktorat Penilaian Obat Tradisional, Suplemen Makanan dan Kosmetik sebagaimana telah diubah dengan Keputusan Kepala Badan Pengawas Obat dan Makanan Nomor HK.00.05.21.4231 Tahun 2004 bahwa Direktorat Penilaian Obat Tradisional, Suplemen Makanan dan Kosmetik mempunyai tugas : </p>
                        <p>"Penyiapan Perumusan Kebijakan, Penyusunan Pedoman, Standar, Kriteria dan Prosedur, serta Pelaksanaan Pengendalian, Bimbingan Teknis dan Evaluasi di Bidang Penilaian Obat Tradisional, Suplemen Makanan dan Kosmetika"</p>
                        <center><b>STRUKTUR ORGANISASI</b><br /><img src="<?= base_url();?>img/struktur.png" width="510" height="410" /></center>
                        <br />
                        <br />

                      </div>
                        
                        
                        
                        
                       </div>
                    </div>
                </div>
            </div>  
        