<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); error_reporting(E_ERROR); 
?>
<style>
table.xcel{text-align: left;border:1px solid #000; border-collapse:collapse; color:#000; font-family: "lucida grande", Tahoma, verdana, arial, sans-serif; font-size:10px; direction:ltr;}
table.xcelheader{text-align: left; border-collapse:collapse; color:#000; font-family: "lucida grande", Tahoma, verdana, arial, sans-serif; font-size:10px; direction:ltr;}
</style>
<table width="100%">
	  <tr>
		<td colspan="22" align="center"><h4>Laporan <?= $a ?> Produk OTSM</h4></td>
	  </tr>
	  <tr>
	    <td colspan="22" align="center"><h4>Badan Pengawas Obat dan Makanan Republik Indonesia</h4></td>
  </tr>
</table>
<br />
<table class="xcel" width="100%">
  <tr>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">No</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">PRODUK ID</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">NIE</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">NAMA PRODUK</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">SEDIAAN</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">KEMASAN PRIMER</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">KEMASAN SEKUNDER</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">PENDAFTAR</th>	
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL TERBIT</th> 
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">STATUS PRODUK</th>    
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">KLASIFIKASI</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">JNS DOKUMEN</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">STS REGISTRASI</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">PROSES AKHIR</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL SPB PRAREG</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL BYR SPB PRAREG</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">BIAYA PRAREG</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">NTPN PRAREG</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL SPB REG</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL BYR SPB REG</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">BIAYA REG</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">NTPN REG</th>	
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL EVALUATOR</th> 
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL KASIE</th>    
	<th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL KASUBDIT</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL DIREKTUR</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">TGL DEPUTI</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#CCC;">STATUS BERKAS</th>
  </tr>
  <?php 
  if(count($baris) > 0){
	  $no = 1;
	  for($i=0; $i<count($baris); $i++){ 
	  ?>
	  <tr>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $no; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['PRODUK_ID']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['NIE']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['NAMA_PRODUK']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['SEDIAAN']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['KEMASAN1']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['KEMASAN2']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TRADER']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TERBIT']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['STATUS_PRODUK']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['KLASIFIKASI']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['JNS_DOKUMEN']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['STATUS_REGISTRASI']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['LAST_PROCES']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TGL_SPB_PRAREG']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TGL_BYR_SPB_PRAREG']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['BIAYA_PRAREG']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo "&nbsp;".$baris[$i]['NTPN_PRAREG']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TGL_SPB_REG']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TGL_BYR_SPB_REG']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['BIAYA_REG']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo "&nbsp;".$baris[$i]['NTPN_REG']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TGL_EVALUATOR']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TGL_KASIE']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TGL_KASUBDIT']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TGL_DIREKTUR']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['TGL_DEPUTI']; ?></td>      
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['STAT']; ?></td>        
	  </tr>
	  <?php
	  $no++;
	  }
  }else{
  ?>
  <tr>
    <td valign="top" style="border:1px solid #000; border-collapse:collapse;" colspan="14">Data Tidak Ditemukan</td>
  </tr>
  <?php } ?>
</table>