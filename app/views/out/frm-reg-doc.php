<style>
	.red{
		color:red;
	}
</style>
<script>
	function chkkat(kat) {
		$('.0,.1').hide(500);
		$('.'+kat).show(500);
	}
</script>
<section id="page-header" class="page-section">
	<hr>
</section>

<section id="page-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<form action="<?= site_url() ?>register/post/doc/save" method="POST" id="fpendukung" autocomplete="off" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?= $iId ?>">
					<h6 class="text-uppercase">DATA PEMOHON >> DATA PENANGGUNGJAWAB >> <span style="color: blue">DOKUMEN PERSYARATAN</span> >> DATA USER</h6>
					<hr>


					Keterangan : <br>
					- Ukuran File Maksimal 5 MB <br>
					- Tipe File : jpg, jpeg, png, pdf <br>
					<span class="red">*</span> Dokumen WAJIB di upload<br>


					<br><span>Dokumen Yang Di Upload :</span><br><br>
					<?php
					$i = 0;
					foreach ($DATA_DOC as $key => $value) {
						$i++;
						$kdDok = $value['DOCUMENT_ID'];
						if (ereg("^[[:upper:]]+$", $value['TITLE'])) {
							$title = ucwords(strtolower($value['TITLE']));
						} else {
							$title = $value['TITLE'];
						}

						?>
						<div class="col-xs-12">
							<div class="col-xs-5">
								<?= $title ?> 
							</div>
							<div class="col-xs-1">
								<span class="btn btn-warning fileinput-button" style="" ><i class="fa fa-upload"></i> 

									<span>Pilih</span>
									<input id="fileupload<?= $i ?>" type="file" name="files"  accept="application/pdf,image/x-png,image/jpeg" multiple >
								</span>
							</div>
							<input type="hidden" name="DOC[<?= $i ?>][USER_ID]" value="<?= $iId; ?>">
							<input type="hidden" id="DOC_PATH<?= $i ?>" name="DOC[<?= $i ?>][USER_DOC_PATH]" value="<?= $value['USER_DOC_PATH'] ?>">
							<input type="hidden" id="DOC_NAME<?= $i ?>" name="DOC[<?= $i ?>][USER_DOC_NAME]" value="<?= $value['USER_DOC_NAME'] ?>">
							<input type="hidden" id="DOC_EXT<?= $i ?>" name="DOC[<?= $i ?>][USER_DOC_EXT]" value="<?= $value['USER_DOC_EXT'] ?>">
							<input type="hidden" id="DOC_SIZE<?= $i ?>" name="DOC[<?= $i ?>][USER_DOC_SIZE]" value="<?= $value['USER_DOC_SIZE'] ?>">
							<input type="hidden" id="DOC_FILE<?= $i ?>" name="DOC[<?= $i ?>][USER_DOC_FILE]" value="<?= $value['USER_DOC_FILE'] ?>">
							<input type="hidden" id="DOC_CODE<?= $i ?>" name="DOC[<?= $i ?>][DOCUMENT_ID]" value="<?= $kdDok ?>">

							<div class="col-xs-6">
								<div id="resultDok<?= $i ?>"> 
									<?= ($value['USER_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="link' . $i . '" style="padding-top:20px;" href="' . site_url() . "download/data/" . base64_encode($value['USER_DOC_PATH']) . "/" . $value['USER_DOC_NAME'] . '" target="_blank">' . $value['USER_DOC_NAME'] . '</a>&nbsp;&nbsp;<span id="btn' . $i . '"><a style="" class="btn btn-xs btn-danger" onclick="hapus(' . $i . ');">Hapus</a></span>' : '') ?>
									<div id="progress<?= $i ?>" class="progress" style="background:#fff !important; border:none !important; box-shadow:none !important;">
										<div id="progress-bar<?= $i ?>" class="progress-bar progress-bar-success"></div>
									</div>
								</div>    
							</div>
						</div>
						<br>
						<br>

						<script>
							$(function () {
								'use strict';
								$('#fileupload<?= $i ?>').fileupload({
									url: '<?= site_url() ?>home/upload/<?= $iId; ?>',
									dataType: 'json',
									formData: {action: 'upload', kdDok: '<?= $kdDok ?>', format: 'jpg;jpeg;pdf', maxsize : '5242880'},
									start: function (e) {
										$('#resultDok<?= $i ?>').html('<div id="progress<?= $i ?>" class="progress" style="margin-top:20px;"><div id="progress-bar<?= $i ?>" class="progress-bar progress-bar-success"></div></div>');
									},
									done: function (e, data) {
										if(data.result.status=='true'){
											$('#DOC_PATH<?= $i ?>').val(data.result.path);
											$('#DOC_NAME<?= $i ?>').val(data.result.name);
											$('#DOC_EXT<?= $i ?>').val(data.result.ext);
											$('#DOC_SIZE<?= $i ?>').val(data.result.size);
											$('#DOC_FILE<?= $i ?>').val(data.result.rename);
											$('#progress<?= $i ?>').fadeOut('fast', function(){
												$('#resultDok<?= $i ?>').html('<div style="padding:0px 0px 20px 0px;"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a></div>');
											});
										}else{
											$('#progress<?= $i ?>').fadeOut('fast', function(){
												$('#resultDok<?= $i ?>').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
											});                                
										}
									},
									progressall: function (e, data) {
										var progress = parseInt(data.loaded / data.total * 100, 10);
										$('#progress<?= $i ?> .progress-bar').css(
											'width',
											progress + '%'
											);
									}
								}).prop('disabled', !$.support.fileInput)
								.parent().addClass($.support.fileInput ? undefined : 'disabled');
							});                    
						</script>                                       


						<?php
					}
					?>

					<input type="hidden" name="jml_doc" id="jml_doc" value="<?= $i ?>">

					<div class="col-xs-12" style="" >
						<br>

						<!--<input type="button" value="<< Kembali" class="btn " style="background-color: yellow" onclick="window.history.back();">-->
						<a href="<?= site_url(); ?>register/new_reg/frm-reg-pic/<?= $iId; ?>"  class="btn " style="background-color: yellow" ><i class="fa fa-angle-double-left"></i> Kembali</a>
						<button type="button" onclick="simpan();" class="btn btn-success" >Lanjut <i class="fa fa-angle-double-right"></i></button>

						<div class="notification hidden mbot-0"><div></div></div>
					</div>
				</form>
				<script>

					function hapus(id) {
						$('#DOC_PATH' + id).val('');
						$('#DOC_NAME' + id).val('');
						$('#DOC_EXT' + id).val('');
						$('#DOC_SIZE' + id).val('');
						$('#DOC_FILE' + id).val('');
						$('#link' + id).html('');
						$('#btn' + id).html('');

					}

					function simpan() {
						var count = 0;
						for (i = 1; i <= <?= $i ?>; i++) {
							if ($("#DOC_CODE" + i).val() != 68 && $("#DOC_NAME" + i).val() == '') {
								alert("Lengkapi upload dokumen yang bertanda *");
								count++;
								break;
							}
						}
						if (count == 0) {
							save_post('#fpendukung');
						}
					}
				</script>


			</div>
		</div>
	</div>
</section>

