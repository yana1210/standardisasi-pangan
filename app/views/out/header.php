<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<meta name="description" content="e-Standardisasi Pangan">
	<meta name="author" content="yana">
	<link rel="shortcut icon" href="<?= base_url(); ?>img/logo.png">


    <title>e-Standardisasi Pangan</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!--     <link href="<?= base_url(); ?>css/out/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url(); ?>css/out/bootstrap.min.css" rel="stylesheet">
 -->
    <!-- Css animations  -->
    <link href="<?= base_url(); ?>css/out/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="<?= base_url(); ?>css/out/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="<?= base_url(); ?>css/out/custom.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/newtable-green.css" />
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/datepicker.css" />
	<!-- bxslider -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>js/jquery-upload/css/jquery.fileupload.css" />

    <!-- Responsivity for older IE -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <!-- owl carousel css -->

    <link href="<?= base_url(); ?>css/out/owl.carousel.css" rel="stylesheet">
    <link href="<?= base_url(); ?>css/out/owl.theme.css" rel="stylesheet">

    <!-- #### JAVASCRIPT FILES ### -->
    <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--     <script src="<?= base_url(); ?>js/out/jquery-1.11.0.min.js"></script>
 -->
    <script>
        window.jQuery || document.write('<script src="js/out/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!--     <script src="<?= base_url(); ?>js/out/bootstrap.min.js"></script>
 -->
    <script src="<?= base_url(); ?>js/out/jquery.cookie.js"></script>
    <script src="<?= base_url(); ?>js/out/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>js/out/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>js/out/jquery.parallax-1.1.3.js"></script>
    <script src="<?= base_url(); ?>js/out/front.js"></script>

    <!-- owl carousel -->
    <script src="<?= base_url(); ?>js/out/owl.carousel.min.js"></script>

    <script src="<?= base_url(); ?>js/autocomplete.js"></script>
    <script src="<?= base_url(); ?>js/maskedinput.js"></script>
    <script src="<?= base_url(); ?>js/main.js?v=<?=date('YmdHis')?>"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/newtable.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/newtablehash.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/clock.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=base_url()?>js/jquery-upload/js/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/jquery-upload/js/jquery.fileupload.js"></script>



</head>


	<script>

function lupa(){
	$('#aforgot').show(200);
	$('#alogin').hide(200);
	$( "#userid" ).focus();
	document.getElementById("userid").focus();
}
function signin(){
	$('#aforgot').hide(200);
	$('#alogin').show(200);
	$( "#userid" ).focus();
	document.getElementById("userid").focus();
}


	(function($) {
		"use strict";

		$('document').ready(function() {
/*
			var $container = $('body'),
				tweetsTimer;

			$container.imagesLoaded( function() {

				$('#spinner-wrapper').hide();

			});

			clearTimeout(tweetsTimer);
*/
            $('.datepickerx').datepicker({
                format: "yyyy-mm-dd",
                orientation: "bottom right",
                autoclose: true,
                todayBtn: "linked",
                todayHighlight: true
            });

		    $("#mega-menu-111-hover,#mega-menu-111").mouseover(function(){
		        $("#mega-menu-111").css("display", "block").addClass('block').fadeIn(3000000);
		    });
		    $("#mega-menu-111-hover,#mega-menu-111").mouseout(function(){
		        $("#mega-menu-111").css("display", "none").fadeOut(30000000);
		    });

		});

	})(jQuery);


	</script>
<style type="text/css" media="screen">
.loading_div{
    text-align:center;
    background-color: white;
}    
</style>
  <!-- Modal -->
  <div class="modal fade " id="myModal" role="dialog" >
    <div class="modal-dialog modal-sm " style="background-color: #bababa">
    
      <!-- Modal content-->
      <div class="modal-content " data-backdrop="false" style="background-color: #bababa">
        <!--<div class="modal-header" data-backdrop="false">
          <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>-->
        <div class="modal-body " data-backdrop="false" style="background-color: #bababa">
          <div class='loading_div'><img src='<?=base_url(); ?>img/ajax-loader.gif'></div>
        </div>
        <!--<div class="modal-footer" data-backdrop="false">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
