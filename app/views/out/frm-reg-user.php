<style>
	.red{
		color:red;
	}
	#scroll {
		border: 1px solid black;
		height: 400px;
		overflow: scroll;
	}
</style>
<section id="page-header" class="page-section">
	<hr>
</section>

<section id="page-content">
	<div class="container">
		<div class="row">
			<form class="form-horizontal form-label-left " action="<?= site_url() ?>register/post/user" method="POST" id="fregister" autocomplete="off" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?= $iId ?>">
				<div class="col-md-12">
					<h6 class="text-uppercase">DATA PEMOHON >> DATA PENANGGUNGJAWAB >> DOKUMEN PERSYARATAN >> <span style="color: blue">DATA USER</span></h6>
					<hr>


					<!--<div class="form-group">
						<label class="control-label col-md-4 col-sm-3 col-xs-12">Username<span class="red"> *</span></label>
						<div class="col-md-4 col-sm-6 col-xs-9">
							<input type="text" id="username" required class="form-control keyup-username" name="REG[USER_USERNAME]" maxlength="12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-3 col-xs-12">Password<span class="red"> *</span></label>
						<div class="col-md-4 col-sm-6 col-xs-9 input-group">
							<input type="password" id="pass" required class="form-control keyup-username2" name="REG[USER_PASSWORD]">
							<span class="input-group-btn">
                                <button id= "show_password2" class="btn btn-secondary" type="button">
                                            <span class="glyphicon glyphicon-eye-open glyphicon2 "></span>
                                        </button>
                              </span>
						</div>
					</div>
                  <div class="a2"></div>-->

                      <label for="fullname">Username <span class="red"> *</span> :</label>
                  <div class="input-group col-md-4 col-sm-6 col-xs-9">
							<input type="text" id="username" required class="form-control keyup-username" name="REG[USER_USERNAME]" maxlength="12" placeholder="Username">
                  </div><br>
                      <label for="fullname">Password <span class="red"> *</span> :</label>
                  <div class="input-group col-md-4 col-sm-6 col-xs-9">
                    <input type="password" id="pass" placeholder="Password" required class="form-control keyup-username2" name="REG[USER_PASSWORD]">
                    <span class="input-group-btn">
                      <button type="button" id= "show_password2" class="btn btn-secondary"><span class="glyphicon glyphicon-eye-open glyphicon2 "></span></button>
                    </span>
                  </div><br>
                  <div class="a2"></div>


					<div class="col-md-12">
						<br> <span class="red"> *</span> minimal 6 karakter, maksimal 12 karakter terdiri dari huruf kapital, huruf kecil dan angka
						<!--<br> <span class="red"> **</span> minimal 8 karakter terdiri dari huruf kapital, huruf kecil, angka dan karakter spesial (seperti @, #, $, &, *)--><br><br>
					</div>
					<div class="col-md-12">

						<div id="scroll" class="alert alert-success" style="background-color: rgba(137, 212, 169, 0.46);border-color: #00ab48;color: #040404;" role="alert">
							<div style="text-align: center; font-size: 16px"><b>USER AGREEMENT</b><br><br></div>
							<p>
								Pengguna diwajibkan untuk membaca seluruh persyaratan yang diajukan oleh Direktorat Standardisasi Produk Pangan. Bacalah User Agreement berikut dengan seksama dan teliti, bila pengguna sudah menyetujui seluruh persyaratan yang diajukan oleh Direktorat Standardisasi Produk Pangan silahkan centang checkbox persetujuan kemudian klik tombol SUBMIT.
							 </p>
							<p><i>
								License User Agreement e-standardisasi pangan 
							</i></p>
							<p style="color: red">
								USER AGREEMENT
							</p>
							<p>
								Layanan Aplikasi e-standardisasi pangan merupakan bentuk pelayanan publik berbasis elektronik yang dilakukan oleh Direktorat Standardisasi Produk Pangan. Aplikasi ini terdiri dari Pengajuan permohonan penggunaan BTP, Pengkajian terhadap permohonan sehingga system dapat mengeluarkan suatu rekomendasi dari setiap permohonan, dan  Layanan Self-Service konsultasi yang dapat dilakukan oleh perusahaan untuk mengetahui BTP, Bahan Baku, maupun Bahan Penolong Golongan Enzim yang diatur dalam Peraturan Kepala Badan POM serta yang diperbolehkan ataupun ditolak penggunaannya.  
							</p>
							<p>
								Aplikasi ini diharapkan dapat mempercepat proses pelayanan di Direktorat Standardisasi Produk Pangan. Layanan Aplikasi e-standardisasi merupakan system elektronik yang hanya bisa di akses melalui jaringan internal di Lingkungan Direktorat Standardisasi Produk Pangan. Dengan mengakses atau menggunakan seluruh layangan dari e-standardisasi pangan, pengguna setuju untuk terikat dengan syarat-syarat dan ketentuan yang diatur di bawah ini.
							</p>
							<p>
								<ol start="1">
									<li>LISENSI</li>
									<ol type="a" start="1">
										<li>Badan POM memiliki hak cipta terhadap layanan aplikasi e-standardisasi pangan</li>
										<li>Perjanjian Lisensi User Agreement ini dibuat dan efektif oleh dan antara Badan POM ("Penyedia layanan") dan Pengguna ("Pemakai layanan").</li>
										<li>Badan Pengawas Obat dan Makanan memiliki hak untuk membuat ketentuan sebagai syarat yang wajib dipatuhi pengguna untuk dapat menggunakan layanan aplikasi e-standardisasi pangan.</li>
										<li>User Agreement ini tidak memerlukan tanda tangan dan cap basah ataupun disaksikan oleh notaris.</li>
										<li>User Agreement ini dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya kepada pengguna.</li>
									</ol>
									<li>REGISTRASI</li>
									<ol type="a" start="1">
										<li>Untuk dapat menggunakan layanan aplikasi, pengguna wajib mendaftar dan melakukan registrasi data, termasuk Ststus Perusahaan (Produsen/ Manufacture, Importir, atau Distrbutor), Skala Industri (Sesuai dengan UU No. 20 Tahun 2008 Tentang Usaha Mikro, Kecil dan Menengah), No NPWP, Nama Perusahaan, Alamat Perusahaan, Provinsi, Kota/ Kabupaten, Kode Pos, No Telp, No Fax, Email Perusahaan, Nama Penanggung Jawab, No Identitas Penanggung Jawab, Alamat Penanggung Jawab, No Telp Penanggung Jawab, Jabatan Penanggung Jawab serta Email Penanggung Jawab yang sebenar-benarnya.</li>
										<li>Pengguna wajib menyertakan dokumen persyartaan yang di upload sesuai dengan format yang diizinkan.</li>
									</ol>
									<li>BATAS KEWAJIBAN</li>
									<ol type="a" start="1">
										<li>Badan POM RI menjamin kerahasiaan seluruh data registrasi yang diisi oleh pengguna layanan. Badan POM RI tidak akan memberikan informasi tersebut kepada pihak manapun.</li>
										<li>Badan POM menjamin bahwa elemen data yang ada di layanan aplikasi e-standardisasi pangan adalah benar dan update.</li>
										<li>Badan POM RI tidak bertanggung jawab atas User ataupun Password yang hilang atas hacking, keyloger, penipuan ataupun kelalaian Pengguna. Pengguna bertanggung jawab atas kerahasiaan User dan Password sendiri.</li>
										<li>Badan POM RI tidak bertanggung jawab atas email pengguna yang hilang atas hacking, keyloger, penipuan ataupun kelalaian pengguna. Pengguna bertanggung jawab atas kerahasiaan User dan Password email sendiri.</li>
										<li>Pengguna juga diminta melakukan tindakan preventif dalam melindungi dan menjaga kestabilan Sistem Operasi, keamanan data komputer serta ID pengguna sendiri. Badan POM tidak bertanggung jawab atas kerusakan ataupun kerugian yang terjadi karena virus, spyware ataupun usaha hacking dari pihak lain untuk mengambil data anda.</li>
										<li>Bacalah User Guide penggunaan aplikasi e-standardisasi pangan terlebih dahulu sebelum menggunakan layanan aplikasi e-standardisasi pangan.</li>
									</ol>
									<li>PEMBAYARAN</li>
									<ol type="a" start="1">
										<li>Pengguna tidak dipungut bayaran untuk menggunakan layanan aplikasi e-standardisasi pangan.</li>
									</ol>
									<li>KESEPAKATAN LAYANAN</li>
									<ol type="a" start="1">
										<li>Layanan Aplikasi e-standardisasi pangan dapat diakses selama hari kerja (5 hari) di lingkungan Direktorat Standardisasi Produk Pangan mulai jam 08.30 – 16.00 WIB.</li>
										<li>Pengguna tidak akan menuntut Badan POM RI terkait dengan maintenance rutin maupun kejadian yang membuat penggunaan layanan e-standardisasi pangan terhenti baik yang dikarenakan gangguan teknis maupun nonteknis.</li>
										<li>Apabila terjadi permasalahan dalam pemberian jasa layanan kepada para pelanggan, Badan POM RI berhak membatasi atau bahkan memberhentikan layanan sewaktu-waktu tanpa keharusan memberikan pemberitahuan terlebih dulu. Badan POM RI dapat memberikan penjelasan mengenai permasalahan tersebut sesuai dengan kebijakan Badan POM.</li>
										<li>Pengguna layanan aplikasi diwajibkan mengupload data persyaratan dokumen dengan benar dan sesuai dengan konten. Badan POM RI berhak memberikan sanksi atau bahkan memblok ID pengguna yang mengupload data tidak sesuai dengan konten. Kegagalan pengguna dalam hal melakukan upload data bukan merupakan tanggung jawab Badan POM.</li>
										<li>Persayaratan upload data serta konten lainnya tertera di User Guide dan website Badan POM</li>
										<li>Pengguna bertanggung jawab atas segala tindakan, perkataan, pesan maupun tulisan yang disampaikan, baik di dalam FAQ maupun ke dalam pesan yang disampaikan ke layanan aplikasi, baik melalui email maupun penyampaian pesan langsung yang ada di bawah layanan Badan POM RI</li>
										<li>Anda setuju untuk membebaskan Badan POM RI beserta seluruh staffnya dan pihak lain yang bekerja sama dengan Badan POM RI dari tuntutan dan kerugian yang timbul karena tindakan, tingkah laku dan pesan yang dituliskan ataupun penyampaian langsung oleh pengguna di dalam email ataupun helpdesk layanan kami.</li>
										<li>Pengguna tidak diperbolehkan melakukan usaha untuk mendapatkan keuntungan dengan memanfaatkan layanan yang diberikan oleh Badan POM RI</li>
										<li>Elemen yang terkandung di dalam aplikasi yang dipublikasikan oleh Badan POM RI adalah milik Badan POM RI</li>
										<li>Badan POM RI tidak bertanggung jawab atas kehilangan yang terjadi atas kelalaian pengguna, berkaitan dengan ID pengguna ataupun isi yang terdapat di dalam ID tersebut.</li>
										<li>Kegagalan login pengguna melalui fasilitas Single Sign ON (SSO) baik melalui aplikasi e-standardisasi pangan maupun melalui aplikasi yang terintegrasi pada SSO bukan merupakan tanggung jawab Badan POM.</li>
										<li>Badan POM tidak bertanggung jawab atas kehilangan ID pengguna layanan lain yang telah terintegrasi melalui fasilitas Single Sign ON (SSO) dengan layanan e-standardisasi pangan</li>
										<li>Untuk memenuhi percepatan arus data layanan aplikasi e- standar pangan telah terintegrasi dengan layanan Single Sign ON (SSO). Kehilangan User dan password layanan lain yang telah terintegrasi Single Sign ON (SSO) dengan layanan e- standar pangan bukan merupakan tanggung jawab Badan POM</li>
										<li>Pengguna tidak diperbolehkan untuk membuat, mendistribusikan, menjual-belikan segala produk yang merupakan hak paten/ hak cipta layanan aplikasi tanpa ijin resmi dari Badan POM RI.</li>
										<li>Pengguna diwajibkan untuk menaati peraturan layanan yang telah ditentukan oleh Badan POM RI. Badan POM RI berhak memberikan sanksi atau bahkan memblok ID pengguna yang melanggar peraturan layanan.</li>
										<li>Badan POM tidak membenarkan penyalahgunaan kesalahan sistem. Apabila pengguna terbukti secara sadar dan sengaja memanfaatkan kesalahan pada sistem yang terdapat pada layanan aplikasi maka Badan POM RI akan memberikan sanksi yang sesuai.</li>
										<li>Pengguna setuju untuk tidak menggunakan fasilitas, server dan jaringan yang dipergunakan untuk layanan kami untuk mendapatkan akses tanpa ijin ke dalam sistem ataupun jaringan lainnya. Bila pengguna melanggarnya, maka Badan POM RI berhak menuntut pengguna (baik tuntutan sipil ataupun krminal).</li>
										<li>Badan POM RI dapat melakukan perubahan di subsite layanan e- standar pangan, peraturan dan User Agreement yang ada untuk meningkatkan kualitas layanan yang diberikan, tanpa harus melakukan pemberitahuan terlebih dulu. Kami menganjurkan para pelanggan untuk rutin mengunjungi website Badan POM ataupun layanan aplikasi e- standar pangan Badan POM RI untuk mendapatkan berita-berita terbaru.</li>
										<li>Bila diperlukan, Badan POM RI berhak untuk mengambil tindakan hukum terhadap pelanggaran User Agreement ini ataupun terhadap tindakan yang dianggap merugikan perusahaan selaku pemegang hak cipta atas jasa layanan.</li>
									</ol>
									<li>Pengguna harus menyimpan salinan User Agreement ini untuk catatan</li>
									<li>Badan POM tidak bertanggung jawab terhadap tindakan penipuan atau tindakan lain sejenisnya yang mengatasnamakan Badan POM.</li>
									</ol>
								</p>
							</div>
						</div>
						<div class="col-md-12" style="text-align: center;">
							<input type="checkbox" id="agg"> Saya setuju dengan User Agreement diatas dan telah menginput data dengan sebenarnya
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
									<div class="notification hidden mbot-0"><div></div></div>
									<a href="<?= site_url(); ?>register/new_reg/frm-reg-doc/<?= $iId; ?>"  class="btn " style="background-color: yellow" ><i class="fa fa-angle-double-left"></i> Kembali</a>
				                      <button type="button" class="btn btn-danger" onclick="reset();"><i class="fa fa-eraser"></i> Reset</button>
				                      <button id="reg" type="submit" class="btn btn-primary" style="display: none;" onclick=" ">Register <i class="fa fa-angle-double-right"></i></button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
<script>
    $('#fregister').submit(function(){
    	var username=/^(?=(?:.*[A-Z]))(?=(?:.*[a-z]))(?=(?:.*\d))([A-Za-z0-9]{6,12})$/;var password=/^(?=(?:.*[A-Z]))(?=(?:.*[a-z]))(?=(?:.*\d))(?=(?:.*[!@#$%^&*()\-_=+{};:,<.>]))([A-Za-z0-9!@#$%^&*()\-_=+{};:,<.>]{8,100})$/;
	    if(!username.test($('#username').val())){ 
	    	alert('Username harus terdiri minimal 6 karakter, maksimal 12 karakter terdiri dari huruf kapital, huruf kecil dan angka !'); 
		}else if(!username.test($('#pass').val())){ 
	    	alert('Password harus terdiri minimal 6 karakter, maksimal 12 karakter terdiri dari huruf kapital, huruf kecil dan angka !'); 
//			alert('Password harus terdiri minimal 8 karakter terdiri dari huruf kapital, huruf kecil, angka dan karakter spesial (seperti @, #, $, &, *) !'); 
	    } else{
	    	var usr =  $('#username').val();
	        $.ajax({
	            type: "POST",
	            url: "<?= site_url() ?>register/chk_username",
	            data: "usr=" + usr ,
	            success: function (msg) {
	                msg2 = msg.split('|||');
	                if(msg2[0] == 1){
	                    save_post('#fregister');
						 return false;
		            }else if(msg2[0] == 0){
	                    $(" div.notification").removeClass('success').removeClass('error').removeClass('attention').removeClass('hidden').removeClass('information');
	                    $(" div.notification").addClass('attention');
	                    $(" div.notification div").html(msg2[1]).css('color', 'red');;
	                    $("#input-npwp").val('').focus();
						$('html, body').stop().animate({scrollTop:0}, 500, 'swing', function() { 
						   //alert("NPWP sudah digunakan");
						});
//	                    $("#myModal").hide();
					   alert(msg2[1]);
	                }else{
	                  
	                }
	            }

	        });
	    }
        return false;
    });
</script>

		<script type="text/javascript">
			$(document).ready(function(e){

				$('.pass').change(function() {
					$('span.error-keyup-7').remove();
					var inputVal = $(this).val();
					var pass = $("#pass").val();
					if(pass != inputVal) {	
						$(this).after('<span style="color:red" class="error error-keyup-7">Password Tidak Sama</span>');
					}
				});
				$('.keyup-password').keyup(function() {
					$('span.error-keyup-1').remove();
					var inputVal = $(this).val();
					var characterReg =  /^(?=(?:.*[A-Z]))(?=(?:.*[a-z]))(?=(?:.*\d))(?=(?:.*[!@#$%^&*()\-_=+{};:,<.>]))([A-Za-z0-9!@#$%^&*()\-_=+{};:,<.>]{8,100})$/;
					if(!characterReg.test(inputVal)) {	
						$(this).after('<span style="color:red" class="error error-keyup-1">Password harus terdiri minimal 8 karakter terdiri dari huruf kapital, huruf kecil, angka dan karakter spesial (seperti @, #, $, &, *)</span>');
					}
				});

				$('.keyup-username').keyup(function() {
					$('span.error-keyup-3').remove();
					var inputVal = $(this).val();
					var characterReg =  /^(?=(?:.*[A-Z]))(?=(?:.*[a-z]))(?=(?:.*\d))([A-Za-z0-9]{6,12})$/;
					if(!characterReg.test(inputVal)) {
						$(this).after('<span style="color:red" class="error error-keyup-3">Username harus terdiri minimal 6 karakter, maksimal 12 karakter terdiri dari huruf kapital, huruf kecil dan angka </span>');
					}
				});
				$('.keyup-username2').keyup(function() {
					$('span.error-keyup-4').remove();
					var inputVal = $(this).val();
					var characterReg =  /^(?=(?:.*[A-Z]))(?=(?:.*[a-z]))(?=(?:.*\d))([A-Za-z0-9]{6,12})$/;
					if(!characterReg.test(inputVal)) {
						$('.a2').html('<span style="color:red" class="error error-keyup-4">Username harus terdiri minimal 6 karakter, maksimal 12 karakter terdiri dari huruf kapital, huruf kecil dan angka </span>');
					}
				});
				$("#agg").click(function(){
					$("#reg").toggle();
				});

			});






$('#show_password2').hover(function functionName() { 
                        //Change the attribute to text
                        $('#pass').attr('type', 'text');
                        $('.glyphicon2').removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
                    }, function () {
                        //Change the attribute back to password
                        $('#pass').attr('type', 'password');
                        $('.glyphicon2').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
                    }
                );

		</script>

<!---
 ^ # start of line
(?=(?:.*[A-Z])) #  upper case letters
(?=(?:.*[a-z])) #  lower case letters
(?=(?:.*\d)) #  digits
(?=(?:.*[!@#$%^&*()\-_=+{};:,<.>])) #  special characters

([A-Za-z0-9!@#$%^&*()\-_=+{};:,<.>]{6,20}) # length 6-20, only above char classes (disallow spaces)
$ # end of line
-->