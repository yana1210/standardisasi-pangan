<?php 
	if($this->uri->segment(1) == 'register'){
		$register = "active";
		$beranda = "";
		$help = "";
	}else if($this->uri->segment(1) == 'help'){
		$register = "";
		$beranda = "";
		$help = "active";
	}else{
		$register = "";
		$beranda = "active";
		$help = "";
	}

	 
?>
							<ul class="nav navbar-nav navbar-right">
                                <li class="dropdown <?= $beranda ?>">
                                    <a href="<?= site_url(); ?>" class="" data-toggle="">Beranda </a>
                                </li>
                                <!-- ========== FULL WIDTH MEGAMENU ================== -->


                                <li class="dropdown <?= $register ?>">
                                    <a href="<?= site_url(); ?>register/new_reg/frm-reg" class="" data-toggle="">Registrasi </a>
                                </li>
                                <li class="dropdown <?= $help ?>">
                                    <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown">Bantuan <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                        	<!-- <a href="#" data-toggle="modal" data-target="#reset-modal">Reset Password</a> -->
                                        	<a href="<?= site_url(); ?>help/sub/frm-reset" class="" data-toggle="">Reset Password</a>
                                        </li>
                                        <!--<li>
                                        	!-- <a href="#" data-toggle="modal" data-target="#contact-modal">Kontak Kami</a> --
                                        	<a href="<?php //site_url(); ?>help/sub/frm-contact" class="" data-toggle="">Kontak Kami</a>
                                        </li>-->
                                        <li><a href="<?= base_url(); ?>data/guide/manual.pdf" target="_blank">Petunjuk</a>
                                        </li>
                                    </ul>
                                </li>
                                <!--<li class="dropdown">
                                    <a href="javascript: void(0)" class="" data-toggle="">Kontak Kami </a>
                                </li>-->
                            </ul>

<script>
	$('#flogin').submit(function(){
		$('#btnlogin').html('Verifikasi..');
		$('#msglogin').html('');
		$.ajax({
			type: 'POST',
			url: $('#flogin').attr('action'),
			data: $('#flogin').serialize(),
			success: function(data){	
				arrdata = data.split('#');
				if(arrdata[1]=='YES'){
					location.href = '<?=site_url()?>';
				}else{
					$('#btnlogin').html('Login');
					$('#msglogin').html('<span style="color:#FF0000; font-size:14px;"> &nbsp; '+arrdata[2].toString()+'</span>');
				}
			}
		});
		return false;
	});


	$('#fforgot').submit(function(){
		$('#btnforgot').html('Verifikasi..');
		$('#msgforgot').html('');
		$.ajax({
			type: 'POST',
			url: $('#fforgot').attr('action'),
			data: $('#fforgot').serialize(),
			success: function(data){	
				arrdata = data.split('#');
				if(arrdata[1]=='YES'){
					$('#btnforgot').html('Reset Password');
					$('#msgforgot').html('<span style="color:#FF0000; font-size:14px;"> &nbsp; '+arrdata[2].toString()+'</span>');
//					location.href = '<?=site_url()?>';
				}else{
					$('#btnforgot').html('Reset Password');
					$('#msgforgot').html('<span style="color:#FF0000; font-size:14px;"> &nbsp; '+arrdata[2].toString()+'</span>');
				}
			}
		});
		return false;
	});

	// function guide(){
	// 	document.flogin.action = '<?= site_url(); ?>/print/guide';
	// 	document.flogin.target = "_blank";
	// 	document.flogin.submit();			
	// }

</script>

                      