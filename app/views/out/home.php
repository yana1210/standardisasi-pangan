<!DOCTYPE html>
<html lang="en">

{_header_}
<script>
    /* $(document).ready(function () {
        document.getElementById('captcha').src='<?= base_url(); ?>img/captcha/captcha.php?'+Math.random();
    }); */
</script>
<style type="text/css" media="screen">
 .group {position: relative;}   
</style>
<body  onload="startclock()">

    <div id="all">

        <header>

            <!-- *** TOP ***
_________________________________________________________ -->
            <div id="top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-5 contact">
                            <p class="">
                                <?php //echo hash("sha256","Espbpom10");
                                    $_SESSION['captkodex'] = $captcha; // salah
                                    function konversi_hari()
                                    {
                                        switch(date("l"))
                                        {
                                            case 'Monday':$nmh="Senin";break; 
                                            case 'Tuesday':$nmh="Selasa";break; 
                                            case 'Wednesday':$nmh="Rabu";break; 
                                            case 'Thursday':$nmh="Kamis";break; 
                                            case 'Friday':$nmh="Jum'at";break; 
                                            case 'Saturday':$nmh="Sabtu";break; 
                                            case 'Sunday':$nmh="Minggu";break; 
                                        }
                                        echo '<span class="hidden-sm hidden-xs" style="font-size:12px;">'.$nmh.'</span>';
                                    }
                                    konversi_hari();

                                ?>
                                <span id="date" style="font-weight: normal; font-size: 12px;"></span>, 
                                <span id="clock" style="font-weight: normal; font-size: 12px;"></span>
                            </p>
                            
                        </div>
                        <div class="col-xs-7">
                            <div class="social">
                            </div>

                            <div class="login">
                                <a href="#" data-toggle="modal" data-target="#login-modal"><i class="fa fa-sign-in"></i> <span class="hidden-xs text-uppercase">Login</span></a>
                                <!--<a href="customer-register.html"><i class="fa fa-user"></i> <span class="hidden-xs text-uppercase">Registrasi</span></a>-->
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- *** TOP END *** -->

            <!-- *** NAVBAR ***
    _________________________________________________________ -->

            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        <div class="navbar-header">

                            <a class="navbar-brand home" style="height: 42px" href="http://www.pom.go.id/new/">
                                <img width="50px" src="<?= base_url(); ?>img/logo-title.png" alt="BPOM logo" class="hidden-xs hidden-sm">
                            </a>
                            <a class="navbar-brand home" style="height: 42px" href="http://www.pom.go.id/new/">
                                <div style="font-size: 22px; font-weight: bold; color: #2C79B3; padding-top: 5px">e-Standardisasi Pangan</div>
                                <span style="font-size: 13px;  color: black">Direktorat Standardisasi Produk Pangan</span>
                            </a>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>
                        <!--/.navbar-header -->

                        <div class="navbar-collapse collapse" id="navigation">
                        <?php 
                        if($_SERVER['PATH_INFO'] != "/registrasi/reg_person" && $_SERVER['PATH_INFO'] != "/registrasi/reg_business"){

                        ?>
                            {_menu_}

                        <?php } ?>

                        </div>

                    </div>


                </div>
                <!-- /#navbar -->

            </div>

            <!-- *** NAVBAR END *** -->

        </header>

        <!-- *** LOGIN MODAL ***
_________________________________________________________ -->

        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Login</h4>
                    </div>
                    <div class="modal-body">
                        <form id="flogin" action="<?= site_url() ?>home/post/login" autocomplete="off" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" class="form-control" name="login[user]" id="userid" placeholder="Username" autofocus >
                            </div>
                            <div class="input-group">
                              <input name="login[pass]" id = "password" type="password" class="form-control" placeholder="Password">
                              <span class="input-group-btn">
                                <button id= "show_password" class="btn btn-secondary" type="button">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </button>
                              </span>
                            </div>

                    <p style="margin-top:5px;"><a href="<?= site_url(); ?>help/sub/frm-reset"> Lupa Password</a></p>
                    <!--
                    <p style="margin-top:18px;">Kode Keamanan</p>
                    <img src="<?= base_url(); ?>img/captcha/captcha.php?v=<?=date('YmdHis')?>" id="captcha" style="border:1px solid #ddd;"/>
                    <p style="margin-top:5px;"><a href="#" style="color:blue !important;" onclick="document.getElementById('captcha').src='<?= base_url(); ?>img/captcha/captcha.php?'+Math.random();" id="change-image">Ganti Kode Keamanan</a></p>
                            <div class="form-group">
                                <input type="text" class="form-control" name="KODE" id="KODE" placeholder="Kode Keamanan" style="margin-top:5px;">
                            </div>
                        -->

                            <p class="text-center">
                                <button id="btnlogin" class="btn btn-template-main"><i class="fa fa-sign-in"></i> Login</button>
                                <span id="msglogin"></span>
                            </p>

                        </form>


                    </div>
                </div>
            </div>
        </div>

        <!-- *** LOGIN MODAL END *** -->

        <div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="Contact" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="reset">Form Kontak Kami</h4>
                    </div>
                    <div class="modal-body">
                        <form id="fcontact" action="<?= site_url()?>home/post/contact" autocomplete="off" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" class="form-control" name="contact[NAME]" id="NAME" placeholder="Nama Lengkap" autofocus >
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="contact[EMAIL]" id="EMAIL" placeholder="Email" autofocus >
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" rows="3" name="contact[QUESTION]" id="QUESTION" placeholder="Pertanyaan" autofocus ></textarea>
                            </div>
                            <p class="text-center">
                                <button id="btncontact" class="btn btn-template-main"><i class="fa fa-sign-in"></i>Submit</button>
                                <span id="msgcontact"></span>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {_content_}
        {_footer_}

    </div>
    <!-- /#all -->
  
<script>

    $('#flogin').submit(function(){
        $('#btnlogin').html('Verifikasi..');
        $('#msglogin').html('');
        $.ajax({
            type: 'POST',
            url: $('#flogin').attr('action'),
            data: $('#flogin').serialize(),
            success: function(data){    
                arrdata = data.split('#');
                if(arrdata[1]=='YES'){
                    location.href = '<?=site_url()?>';
                }else{
                    $('#btnlogin').html('Login');
                    $('#msglogin').html('<span style="color:#FF0000; font-size:14px;"> &nbsp; '+arrdata[2].toString()+'</span>');
                }
            }
        });
        return false;
    });

    $('#freset').submit(function(){
        $('#btnreset').html('Verifikasi..');
        $('#msgreset').html('');
        $.ajax({
            type: 'POST',
            url: $('#freset').attr('action'),
            data: $('#freset').serialize(),
            success: function(data){    
                arrdata = data.split('#');
                if(arrdata[1]=='YES'){
                    location.href = '<?=site_url()?>';
                }else{
                    $('#btnreset').html('loading');
                    $('#msgreset').html('<span style="color:#FF0000; font-size:14px;"> &nbsp; '+arrdata[2].toString()+'</span>');
                }
            }
        });
        return false;
    });

    $('#fcontact').submit(function(){
        $('#btncontact').html('Verifikasi..');
        $('#msgcontact').html('');
        $.ajax({
            type: 'POST',
            url: $('#fcontact').attr('action'),
            data: $('#fcontact').serialize(),
            success: function(data){    
                arrdata = data.split('#');
                if(arrdata[1]=='YES'){
                    location.href = '<?=site_url()?>';
                }else{
                    $('#btncontact').html('send');
                    $('#msgcontact').html('<span style="color:#FF0000; font-size:14px;"> &nbsp; '+arrdata[2].toString()+'</span>');
                }
            }
        });
        return false;
    });


    $('#fforgot').submit(function(){
        $('#btnforgot').html('Verifikasi..');
        $('#msgforgot').html('');
        $.ajax({
            type: 'POST',
            url: $('#fforgot').attr('action'),
            data: $('#fforgot').serialize(),
            success: function(data){    
                arrdata = data.split('#');
                if(arrdata[1]=='YES'){
                    $('#btnforgot').html('Reset Password');
                    $('#msgforgot').html('<span style="color:#FF0000; font-size:14px;"> &nbsp; '+arrdata[2].toString()+'</span>');
//                  location.href = '<?=site_url()?>';
                }else{
                    $('#btnforgot').html('Reset Password');
                    $('#msgforgot').html('<span style="color:#FF0000; font-size:14px;"> &nbsp; '+arrdata[2].toString()+'</span>');
                }
            }
        });
        return false;
    });

$('#show_password').hover(function functionName() {
                        //Change the attribute to text
                        $('#password').attr('type', 'text');
                        $('.glyphicon').removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
                    }, function () {
                        //Change the attribute back to password
                        $('#password').attr('type', 'password');
                        $('.glyphicon').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
                    }
                );

</script>

</body>

</html>
