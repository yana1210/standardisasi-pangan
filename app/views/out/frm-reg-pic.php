<style>
	.upper{text-transform: uppercase !important;}
	.red{color:red;} 
</style>
<script type="text/javascript">
    $(document).ready(function () {
        USER_PIC_IDTYPE = "<?= $arrUser['USER_PIC_IDTYPE']; ?>";
        if (USER_PIC_IDTYPE == 'IDT04') {
            $('.lain').removeAttr('disabled').attr('required', 'required');
            $('.etc').show();
            $('#USER_PRSN_IDNO').attr('disabled', 'disabled').hide();
        } else {
            $('.etc').hide();
            $('.lain').attr('disabled', 'disabled');
            $('#USER_PRSN_IDNO').removeAttr('disabled').attr('required', 'required').show();
        }

    });
	function idType(id) {
        if (id == 'IDT04') {
            $('.lain').removeAttr('disabled').attr('required', 'required');
            $('.etc').show();
            $('#USER_PRSN_IDNO').attr('disabled', 'disabled').hide();
        } else {
            $('.etc').hide();
            $('.lain').attr('disabled', 'disabled');
            $('#USER_PRSN_IDNO').removeAttr('disabled').attr('required', 'required').show();
        }
	}

</script>
<section id="page-header" class="page-section">
	<hr>
</section>

<section id="page-content">
	<div class="container">
		<div class="row">
			<form class="form-horizontal form-label-left " action="<?= site_url() ?>register/post/pic/<?= $action ?>" method="POST" id="fregister" autocomplete="off" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?= $arrUser['USER_ID'] ?>">
				<div class="col-md-12">
					<h6 class="text-uppercase">DATA PEMOHON >> <span style="color: blue">DATA PENANGGUNGJAWAB</span> >> DOKUMEN PERSYARATAN >> DATA USER</h6>
					<hr>
				</div>

				<div class="col-md-6">

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap<span class="red"> *</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="hidden" name="REG[USER_ID]" value="<?= $iId; ?>">
							<input type="text" required class="form-control upper" name="REG[USER_PIC_NAME]" value="<?= $arrUser['USER_PIC_NAME'] ?>">
						</div>
					</div>
					<div class="form-group" >
						<label class="control-label col-md-3 col-sm-3 col-xs-12">No Identitas Diri<span class="red"> *</span></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
							<?= form_dropdown('REG[USER_PIC_IDTYPE]', $ID_TYPE, $arrUser['USER_PIC_IDTYPE'], ' class="text form-control" style="min-width:60px"  onchange="idType($(this).val())"  '); ?>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" required id="USER_PRSN_IDNO" class="form-control keyup-numeric" name="REG[USER_PIC_IDNO]" value="<?= $arrUser['USER_PIC_IDNO'] ?>" placeholder="No Identitas Diri">
						</div>
					</div>

					<div class="form-group etc" style="display:none">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
							<input type="text" class="form-control lain" placeholder="Jenis Identitas Diri" name="REG[USER_TYPE_ETC]" value="<?= $arrUser['USER_TYPE_ETC'] ?>" disabled>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" class="form-control keyup-numeric lain" name="REG[USER_PIC_IDNO]" value="<?= $arrUser['USER_PIC_IDNO'] ?>" disabled placeholder="No Identitas Diri">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Jabatan <span class="red"> *</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" required class="form-control " name="REG[USER_PIC_POS]" value="<?= $arrUser['USER_PIC_POS'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat (Kantor) <span class="red"> *</span>
						</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<textarea required class="form-control" name="REG[USER_PIC_ADDR]" rows="3" ><?= $arrUser['USER_PIC_ADDR'] ?></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Propinsi<span class="red"> *</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<?= form_dropdown('REG[REGION_ID]', $PROVINCE, substr($arrUser['REGION_ID'], 0, 2)."00", ' class="text form-control" style="" onchange="setnextcb($(this));" url="'.site_url().'autocomplete/get_arr_city/" id="province" required'); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Kabupaten/Kota<span class="red"> *</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<?= form_dropdown('REG[REGION_ID]', $CITY, $arrUser['REGION_ID'], ' class="text form-control" id="city" style="" required'); ?> 
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Kode Pos<span class="red"> *</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" required class="form-control kodepos" maxlength="5" name="REG[USER_PIC_ZIP]"  value="<?= $arrUser['USER_PIC_ZIP'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">No Telp<span class="red"> *</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" required class="form-control keyup-numeric telp" name="REG[USER_PIC_TELP]" maxlength="16" value="<?= $arrUser['USER_PIC_TELP'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">No Fax</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text"  class="form-control keyup-numeric telp" name="REG[USER_PIC_FAX]" maxlength="16" value="<?= $arrUser['USER_PIC_FAX'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Email <span class="red"> *</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" required id="mail" class="form-control keyup-email" data-parsley-trigger="change" name="REG[USER_PIC_MAIL]"  value="<?= $arrUser['USER_PIC_MAIL'] ?>">
						</div>
					</div>


				</div>
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
							<div class="notification hidden mbot-0"><div></div></div>
							<a href="<?= site_url(); ?>register/new_reg/frm-reg/<?= $iId; ?>"  class="btn " style="background-color: yellow" ><i class="fa fa-angle-double-left"></i> Kembali</a>
		                      <button type="button" class="btn btn-danger" onclick="reset();"><i class="fa fa-eraser"></i> Reset</button>
		                      <button id="simpan" type="button" class="btn btn-success" onclick="emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; if(!emailReg.test($('#mail').val()) ) { alert('Format Email Salah !'); }else{ save_post('#fregister'); return false;}">Lanjut <i class="fa fa-angle-double-right"></i></button>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>
<script type="text/javascript">
	function changeImage() {
		document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd="+Math.random();
	}
	function back(){
		document.location = '<?= site_url(); ?>';
	}
	$(document).ready(function(e){
		$(".datepicker-input").datepicker({
			autoclose: true
		});
		
/*		
		$(".npwp").mask("99.999.999.9-999.999");
		$('#FAC_ZIP').numeric();
		$('#FAC_TELP').numeric();
		$('#FAC_FACTORY_ZIP').numeric();
		$('#FAC_FACTORY_TELP').numeric();
		$('#FAC_PIC_TELP').numeric();
		$('#USER_PHONE').numeric();
		*/		
		$('.pass').change(function() {
			$('span.error-keyup-7').remove();
			var inputVal = $(this).val();
			var pass = $("#pass").val();
			if(pass != inputVal) {	
				$(this).after('<span style="color:red" class="error error-keyup-7">Password Tidak Sama</span>');
			}
		});
		$('.keyup-email').change(function() {
			$('span.error-keyup-7').remove();
			var inputVal = $(this).val();
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if(!emailReg.test(inputVal)) {	
				$(this).after('<span style="color:red" class="error error-keyup-7">Format Email Salah</span>');
			}
		});
		
		$('.keyup-numeric').keyup(function() {
			$('span.error-keyup-1').remove();
			var inputVal = $(this).val();
			var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
			if(!numericReg.test(inputVal)) {	
				$(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
			}
		});
		
		$('.keyup-limit-6').keyup(function() {
			$('span.error-keyup-3').remove();
			var inputVal = $(this).val();
			var characterReg = /^([a-zA-Z0-9]{6,100})$/;
			if(!characterReg.test(inputVal)) {
				$(this).after('<span style="color:red" class="error error-keyup-3">Minimal 6 Karakter.</span>');
			}
		});

	});
</script>
