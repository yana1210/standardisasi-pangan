        <footer>
          <div class="pull-right">
            <!--&copy; 2016 - --><small class="copyright">Badan Pengawas Obat dan Makanan - Republik Indonesia</small>
          </div>
          <div class="clearfix"></div>
        </footer>

    <script src="<?=base_url()?>css/in/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>js/in/autosize.min.js"></script>
    <script src="<?=base_url()?>js/in/bootstrap-progressbar.min.js"></script>
    <script src="<?=base_url()?>js/in/bootstrap-wysiwyg.min.js"></script>
    <script src="<?=base_url()?>js/in/daterangepicker.js"></script>
    <script src="<?=base_url()?>js/in/fastclick.js"></script>
    <script src="<?=base_url()?>css/in/iCheck/icheck.min.js"></script>
    <script src="<?=base_url()?>js/in/jquery.autocomplete.min.js"></script>
    <script src="<?=base_url()?>js/in/jquery.hotkeys.js"></script>
    <script src="<?=base_url()?>js/in/jquery.tagsinput.js"></script>
    <script src="<?=base_url()?>js/in/moment.min.js"></script>
    <script src="<?=base_url()?>js/in/nprogress.js"></script>
    <script src="<?=base_url()?>js/in/parsley.min.js"></script>
    <script src="<?=base_url()?>js/in/prettify.js"></script>
    <script src="<?=base_url()?>js/in/select2.full.min.js"></script>
    <script src="<?=base_url()?>js/in/starrr.js"></script>
    <script src="<?=base_url()?>js/in/switchery.min.js"></script>


    <script src="<?=base_url()?>js/redactor.js" type="text/javascript" ></script>
    <script src="<?=base_url()?>js/autocomplete.js"></script>

    <script src="<?=base_url()?>js/maskedinput.js"></script>
    <script src="<?=base_url()?>js/main.js?v=<?=date('YmdHis')?>"></script>
    <script src="<?=base_url()?>js/newtable.js?v=<?=date('YmdHis')?>" type="text/javascript" ></script>
    <script src="<?=base_url()?>js/newtablehash.js" type="text/javascript" ></script>
    <script src="<?=base_url()?>js/bootstrap-datepicker.js" type="text/javascript" ></script>

    <script src="<?=base_url()?>js/jquery-upload/js/jquery.ui.widget.js" type="text/javascript" ></script>
    <script src="<?=base_url()?>js/jquery-upload/js/jquery.fileupload.js" type="text/javascript" ></script>


    <script src="<?=base_url()?>js/tinymce/tinymce.min.js" type="text/javascript" ></script>

    <script src="<?=base_url()?>js/bootstrap-dialog/bootstrap-dialog.min.js" type="text/javascript" ></script>

    <!-- <script src="<?= base_url(); ?>js/in/datatables/datatables.min.js"></script>
    <script src="<?= base_url(); ?>js/in/datatables/buttons.min.js"></script>
    <script src="<?= base_url(); ?>js/in/datatables/select.min.js"></script> -->
    <script src="<?=base_url()?>js/in/custom2.js"></script>

    <script src="<?=base_url()?>js/tinymce/tinymce.min.js" type="text/javascript" ></script>
    <script>
    function show_input2(status,url, id){
        $.get(url + status + '/' + id, function(hasil){
            $("#pilih").html(hasil);
        });

    }
    $(document).ready(function() {
        $('.datepickerx').datepicker({
            format: "yyyy-mm-dd",
            //orientation: "bottom right",
            autoclose: true,
            todayBtn: "linked",
            todayHighlight: true
        });
    });
        tinymce.init({
            selector: "textarea.textline",
			menubar: false,
			toolbar: false,
			statusbar: false,
			format: false,
            theme: "modern",
            width: 300,
            height: 10
/*            ,    setup: function (editor) {
        editor.on('change', function () {
            tinymce.triggerSave();
        });
    } */

        });

        tinymce.init({
            selector: "textarea.textline2",
            menubar: false,

    
//            toolbar: false,
//            statusbar: false,
            format: false,
            theme: "modern",
            toolbar1 : "styleselect,bold,italic,underline,separator,strikethrough,undo,redo,alignleft,aligncenter,alignright,alignjustify,superscript,subscript,bullist,numlist,outdent,indent",
            toolbar2 : "",


            //            width: 300,
            height: 70,
            element_format : 'html',
            encoding: 'xml',
            plugins : "bbcode",
            theme_advanced_styles : "Code=codeStyle;Quote=quoteStyle",
            content_css : "bbcode.css",
            entity_encoding : "raw",
            add_unload_trigger : false,
            remove_linebreaks : false
/*            ,    setup: function (editor) {
        editor.on('change', function () {
            tinymce.triggerSave();
        });
    } */

        });
        tinymce.init({
            selector: "textarea.textline3",
            menubar: false,
            format: false,
            theme: "modern",
            toolbar1 : "bold,italic,underline,separator,strikethrough,superscript,subscript,bullist,numlist,outdent,indent",
            toolbar2 : "",
            height: 10,
            element_format : 'html',
            encoding: 'xml',
            plugins : "bbcode",
            theme_advanced_styles : "Code=codeStyle;Quote=quoteStyle",
            content_css : "bbcode.css",
            entity_encoding : "raw",
            add_unload_trigger : false,
            remove_linebreaks : false
        });

    </script>
