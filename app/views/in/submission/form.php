


    <div class="layout-content">
        <div class="layout-content-body">
          <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib"><?= $sTitle ?></span>
            </h1>
            
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <form action="<?= site_url() ?>ingredients/post/form" method="POST" id="fregister" autocomplete="off" enctype="multipart/form-data">
                    <input type="hidden" value="<?= $arrIngredients['INGREDIENTS_ID'] ?>" name="INGREDIENTS_ID">
                    <div class="col-xs-6">
                      <div class="form-group row">
                        <label class="col-xs-3 col-form-label">Nama Bahan <span class="red">*</span></label>
                        <div class="col-xs-9">
                          <input type="text" name="INGREDIENTS[INGREDIENTS_NAME]" value="<?= $arrIngredients['INGREDIENTS_NAME'] ?>" class="form-control" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-xs-3 col-form-label">Nama Latin <span class="red">*</span></label>
                        <div class="col-xs-9">
                          <input type="text" name="INGREDIENTS[INGREDIENTS_NAME_LATIN]" value="<?= $arrIngredients['INGREDIENTS_NAME_LATIN'] ?>" class="form-control" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-xs-3 col-form-label">Nama Simplisia <span class="red">*</span></label>
                        <div class="col-xs-9">
                          <input type="text" name="INGREDIENTS[INGREDIENTS_NAME_SIMPLISIA]" value="<?= $arrIngredients['INGREDIENTS_NAME_SIMPLISIA'] ?>" class="form-control" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-xs-3 col-form-label">Bagian </label>
                        <div class="col-xs-9">
                          <input type="text" name="INGREDIENTS[INGREDIENTS_SECTION]" value="<?= $arrIngredients['INGREDIENTS_SECTION'] ?>" class="form-control" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-xs-3 col-form-label">Status </label>
                        <div class="col-xs-9">
                          <?= form_dropdown('INGREDIENTS[INGREDIENTS_STATUS]', array('1' => 'Low Risk List', '2' => 'Negative List', '3' => 'All List'), $arrIngredients['INGREDIENTS_STATUS'], 'style="min-width:60px" wajib="yes" class="text form-control" required'); ?>
                        </div>
                      </div>

                  </form>
                  <div class="col-xs-12">
                      <br>
                      <p>
                      <button type="button" class="btn btn-primary" onclick="save_post('#fregister'); return false;">Simpan</button>
                      <a href="<?= site_url().'ingredients/ingredients_list'?>"><button type="button" class="btn">Batal</button></a>
                      </p>
                  </div>
                  <div class="notification hidden mbot-0"><div></div></div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>