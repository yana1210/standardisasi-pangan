<!-- <?php print_r($arrdata['apptype']);?> -->


<section id="page-content">
  <div class="container">
    <div class="row">
      <form class="form-horizontal form-label-left " action="<?= $act ?>" id="frqs" name="frqs" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $arrTxRqs['RQS_ID'] ?>">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <h6 class="text-uppercase"> DATA ADMINISTRASI PEMOHON >> <span style="color: blue"> DATA UMUM BTP </span> >> DATA TEKNIS</h6>
          <hr>
          <div class="form-group">
            <label class="control-label col-md-5 col-sm-8 col-xs-12" style="text-align: left;"> <span style="color: blue;"> DATA UMUM BAHAN TAMBAHAN PANGAN </span></label>
          </div>
          <br/>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Jenis BTP </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input class="form-control" style="width:87%;" type="text" name="REQUEST[BTP_TYPE_NAME]" ID="" value="<?= $arrTxRqs['BTP_TYPE_NAME'] ?>" readonly disabled>
            </div>
          </div>

          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-3"> Jumlah Penggunaan BTP dalam Produk Pangan </label>
              <div class="col-md-1 col-sm-2 col-xs-2 ">
                <input class="form-control" type="text"  name="" disabled value="<?= $arrTxRqs['RQS_BTP_NETTO'] ?>"> 
              </div>
              <div class="col-md-2 col-sm-2 col-xs-2 ">
                <?= form_dropdown('RQS_BTP_UNIT', $DENOMINATION, $arrTxRqs['RQS_BTP_UNIT'], 'style="min-width:60px" class="form-control" disabled'); ?>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama/Merk Dagang BTP <span class="red"> * </span></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control" style="width:87%;" type="text" name="REQUEST[RQS_BTP_MERK]" ID="" value="<?= $arrTxRqs['RQS_BTP_MERK'] ?>" required>
              </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Jenis Kemasan <span class="red"> * </span></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control" style="width:87%;" type="text" name="REQUEST[RQS_BTP_PCKG_TYPE]" ID="" value="<?= $arrTxRqs['RQS_BTP_PCKG_TYPE'] ?>" required>
              </div>
          </div>
<?php
  if($this->newsession->userdata('USER_TYPE_ID') != 'UTY01'){
    $sign = '<span class="red"> * </span>';
    $wajib = "required";
}else{
  $sign ="";
  $wajib= "";
}
?>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Jenis Pabrik <?= $sign ?></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
            <?php if($arrTxRqs['RQS_FAC_TYPE_ID'] == ""){ $RQS_FAC_TYPE_ID = "FTY01"; }else{$RQS_FAC_TYPE_ID =$arrTxRqs['RQS_FAC_TYPE_ID']; } ?>
             <?= form_dropdown('REQUEST[RQS_FAC_TYPE_ID]', $FACTORY_TYPE, $arrTxRqs['RQS_FAC_TYPE_ID'], 'id="FACTORY_TYPE" onclick="chkkat($(this).val())" style="width:87%;" class="form-control" '.$wajib); ?>
            </div>
          </div> 
          <!-- Data Pabrik 1 -->
          <input type="hidden" class="" name="DLN" value="<?= $arrTxRqsDln['RQS_DLN_ID'] ?>">
          <div class="form-group FTY01" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Pabrik/Perusahaan <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control FTY01" style="width:87%;" type="text" name="REQUEST_DLN[RQS_DLN_FAC_NAME]" ID="" value="<?= $arrTxRqsDln['RQS_DLN_FAC_NAME'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>

          <div class="form-group FTY01" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat Pabrik/Perusahaan <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <textarea class="resizable_textarea form-control FTY01" rows="3" placeholder='' style="width:87%;" name="REQUEST_DLN[RQS_DLN_FAC_ADDR]" disabled <?= $wajib ?>><?= $arrTxRqsDln['RQS_DLN_FAC_ADDR'] ?></textarea>
              </div>
          </div>

           <div class="form-group FTY01" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No. Telp <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input id="RQS_PIC_TELP" class="form-control FTY01 keyup-numeric" style="width:87%;" type="text" name="REQUEST_DLN[RQS_DLN_FAC_TELP]"  value="<?= $arrTxRqsDln['RQS_DLN_FAC_TELP'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>
          <!-- End Data Pabrik 1 -->

          <!-- Data Pabrik 2 -->
          <input type="hidden" class="" name="PCKG" value="<?= $arrTxRqsPckg['RQS_PCKG_ID'] ?>">
          <div class="form-group FTY02" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Pabrik Pengemas <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control FTY02" style="width:87%;" type="text" name="REQUEST_PCKG[RQS_PCKG_PACKAGER_NAME]" ID="" value="<?= $arrTxRqsPckg['RQS_PCKG_PACKAGER_NAME'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>

          <div class="form-group FTY02" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat Pabrik Pengemas <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <textarea class="resizable_textarea form-control FTY02" rows="3" placeholder='' style="width:87%;" name="REQUEST_PCKG[RQS_PCKG_PACKAGER_ADDR]" disabled <?= $wajib ?>><?= $arrTxRqsPckg['RQS_PCKG_PACKAGER_ADDR'] ?></textarea>
              </div>
          </div>

           <div class="form-group FTY02" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No. Telp <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input id="RQS_PIC_TELP" class="form-control FTY02 keyup-numeric" style="width:87%;" type="text" name="REQUEST_PCKG[RQS_PCKG_PACKAGER_TELP]" value="<?= $arrTxRqsPckg['RQS_PCKG_PACKAGER_TELP'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>

           <div class="form-group FTY02" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Pabrik Asal <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control FTY02" style="width:87%;" type="text" name="REQUEST_PCKG[RQS_PCKG_SOURCE_NAME]" ID="" value="<?= $arrTxRqsPckg['RQS_PCKG_SOURCE_NAME'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>

          <div class="form-group FTY02" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat Pabrik Asal <?= $sign ?>
            </label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <textarea class="resizable_textarea form-control FTY02" rows="3" placeholder='' style="width:87%;" name="REQUEST_PCKG[RQS_PCKG_SOURCE_ADDR]" disabled <?= $wajib ?>><?= $arrTxRqsPckg['RQS_PCKG_SOURCE_ADDR'] ?></textarea>
              </div>
          </div>
          <!-- End Data Pabrik 2 -->

          <!-- Data Pabrik 3 -->
          <input type="hidden" class="" name="LCNS" value="<?= $arrTxRqsLcns['RQS_LCNS_ID'] ?>">
          <div class="form-group FTY03" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Pabrik/Perusahaan <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control FTY03" style="width:87%;" type="text" name="REQUEST_LCNS[RQS_LCNS_FAC_NAME]" ID="" value="<?= $arrTxRqsLcns['RQS_LCNS_FAC_NAME'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>

          <div class="form-group FTY03" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat Pabrik/Perusahaan <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <textarea class="resizable_textarea form-control FTY03" rows="3" placeholder='' style="width:87%;" name="REQUEST_LCNS[RQS_LCNS_FAC_ADDR]" disabled <?= $wajib ?>><?= $arrTxRqsLcns['RQS_LCNS_FAC_ADDR'] ?></textarea>
              </div>
          </div>

           <div class="form-group FTY03" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No. Telp <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input id="RQS_PIC_TELP" class="form-control FTY03 keyup-numeric" style="width:87%;" type="text" name="REQUEST_LCNS[RQS_LCNS_FAC_TELP]" value="<?= $arrTxRqsLcns['RQS_LCNS_FAC_TELP'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>

           <div class="form-group FTY03" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Pabrik Pemberi Lisensi <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control FTY03" style="width:87%;" type="text" name="REQUEST_LCNS[RQS_LCNS_LICENSER_NAME]" ID="" value="<?= $arrTxRqsLcns['RQS_LCNS_LICENSER_NAME'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>

          <div class="form-group FTY03" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat Pemberi Lisensi <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <textarea class="resizable_textarea form-control FTY03" rows="3" placeholder='' style="width:87%;" name="REQUEST_LCNS[RQS_LCNS_LICENSER_ADDR]" disabled <?= $wajib ?>><?= $arrTxRqsLcns['RQS_LCNS_LICENSER_ADDR'] ?></textarea>
              </div>
          </div>
          <!-- End Data Pabrik 3 -->

           <!-- Data Pabrik 4 -->
          <input type="hidden" class="" name="IMPR" value="<?= $arrTxRqsImpr['RQS_IMPR_ID'] ?>">
          <div class="form-group FTY04" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Pabrik/Perusahaan <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control FTY04" style="width:87%;" type="text" name="REQUEST_IMPR[RQS_IMPR_FAC_NAME]" ID="" value="<?= $arrTxRqsImpr['RQS_IMPR_FAC_NAME'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>

          <div class="form-group FTY04" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat Pabrik/Perusahaan <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <textarea class="resizable_textarea form-control FTY04" rows="3" placeholder='' style="width:87%;" name="REQUEST_IMPR[RQS_IMPR_FAC_ADDR]" disabled <?= $wajib ?>><?= $arrTxRqsImpr['RQS_IMPR_FAC_ADDR'] ?></textarea>
              </div>
          </div>

           <div class="form-group FTY04" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Importir <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control FTY04" style="width:87%;" type="text" name="REQUEST_IMPR[RQS_IMPR_IMPORTIR_NAME]" ID="" value="<?= $arrTxRqsImpr['RQS_IMPR_IMPORTIR_NAME'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>

          <div class="form-group FTY04" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat Importir <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <textarea class="resizable_textarea form-control FTY04" rows="3" placeholder='' style="width:87%;" name="REQUEST_IMPR[RQS_IMPR_IMPORTIR_ADDR]" disabled <?= $wajib ?>><?= $arrTxRqsImpr['RQS_IMPR_IMPORTIR_ADDR'] ?></textarea>
              </div>
          </div>

          <div class="form-group FTY04" style="display: none;">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No. Telp <?= $sign ?></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input id="RQS_PIC_TELP" class="form-control FTY04 keyup-numeric" style="width:87%;" type="text" name="REQUEST_IMPR[RQS_IMPR_IMPORTIR_TELP]" value="<?= $arrTxRqsImpr['RQS_IMPR_IMPORTIR_TELP'] ?>" disabled <?= $wajib ?>>
              </div>
          </div>
          <!-- End Data Pabrik 4 -->

          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                <div class="notification hidden mbot-0"><div></div></div>
                <a href="<?= site_url(); ?>submission/new_rqs/frm-adm/<?= $arrTxRqs['RQS_ID'] ?>"  class="btn" style="background-color: yellow" ><i class="fa fa-angle-double-left"></i> Kembali</a>
                <button type="button" class="btn btn-danger" onclick="reset();"><i class="fa fa-eraser"></i> Reset</button>
                <button type="button" class="btn btn-success" onclick="save_post('#frqs'); return false;" id="simpan" >Lanjut <i class="fa fa-angle-double-right"></i></button>


              </div>
            </div>
        </div>
      </form>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</section>

<script>
var site_url = '<?= site_url(); ?>';

    $(document).ready(function () {

        var user_type = "<?= $this->newsession->userdata('USER_TYPE_ID'); ?>";
        //if(user_type == 'UTY01'){ //PERORANGAN
          //$('#FACTORY_TYPE').attr('disabled', 'disabled').after('<input type="hidden" name="REQUEST[RQS_FAC_TYPE_ID]" value="FTY01">');
        //}else{
          RQS_FAC_TYPE_ID = "<?= $arrTxRqs['RQS_FAC_TYPE_ID']; ?>";
          for(i = 1; i <= 4; i++){
            if(RQS_FAC_TYPE_ID == 'FTY0'+i){
              $('.'+RQS_FAC_TYPE_ID).show(500).removeAttr('disabled');
            }else{
              $('.FTY0'+i).hide(500).attr('disabled', 'disabled');
            }
          }        
        //}
  
    });


  $(document).ready(function(e){
    $(".datepicker-input").datepicker({
      autoclose: true
    });
    
/*    
    $(".npwp").mask("99.999.999.9-999.999");
    $('#FAC_ZIP').numeric();
    $('#FAC_TELP').numeric();
    $('#FAC_FACTORY_ZIP').numeric();
    $('#FAC_FACTORY_TELP').numeric();
    $('#FAC_PIC_TELP').numeric();
    $('#USER_PHONE').numeric();
    */    
    $('.pass').change(function() {
      $('span.error-keyup-7').remove();
      var inputVal = $(this).val();
      var pass = $("#pass").val();
      if(pass != inputVal) {  
        $(this).after('<span style="color:red" class="error error-keyup-7">Password Tidak Sama</span>');
      }
    });
    $('.keyup-email').change(function() {
      $('span.error-keyup-7').remove();
      var inputVal = $(this).val();
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      if(!emailReg.test(inputVal)) {  
        $(this).after('<span style="color:red" class="error error-keyup-7">Format Email Salah</span>');
      }
    });
    
    $('.keyup-numeric').keyup(function() {
      $('span.error-keyup-1').remove();
      var inputVal = $(this).val();
      var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
      if(!numericReg.test(inputVal)) {  
        $(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
      }
    });
    
    $('.keyup-limit-6').keyup(function() {
      $('span.error-keyup-3').remove();
      var inputVal = $(this).val();
      var characterReg = /^([a-zA-Z0-9]{6,100})$/;
      if(!characterReg.test(inputVal)) {
        $(this).after('<span style="color:red" class="error error-keyup-3">Minimal 6 Karakter.</span>');
      }
    });

  });



  function chkkat(kat) {
    for(i = 1; i <= 4; i++){
      if(kat == 'FTY0'+i){
        $('.'+kat).show(500).removeAttr('disabled');
      }else{
        $('.FTY0'+i).hide(500).attr('disabled', 'disabled');
      }
    }
  }
</script>
