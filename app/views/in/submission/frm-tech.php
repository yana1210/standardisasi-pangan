<?php 
$looping="";
foreach($RQS_PRD_REG_FOOD_TYPE_ID as $key2 => $value2){ $looping .= '<option value='. $key2  .'>'. $value2 .'</option>';  }
//echo $looping; 
$base = base_url();
$user_id = $arrTxRqs['USER_ID'];
$rqs_id = $arrTxRqs['RQS_ID']; 
?>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<link rel="stylesheet" href="<?=base_url()?>js/chosen.css">

<script type="text/javascript" language="javascript">
  var site_url = '<?= site_url(); ?>';

  function addberat() { 
    var countTX_RQS_BTP_COMP = $(".TX_RQS_BTP_COMP").length;
    if(countTX_RQS_BTP_COMP == 1000){
      alert("Batas Maksimal Komposisi BTP adalah 1000");
    }else{
      var nom = $('.addberat').attr('terakhir');
      var idtr = $('.addberat').attr('periksa');
      var cls = idtr + nom;
      $('#hideme').before('<div class="TX_RQS_BTP_COMP form-group row ' + cls + '"><div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3"><input type="hidden" name="TX_RQS_BTP_COMP['+nom+'][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>"><input required class="form-control" type="text" name="TX_RQS_BTP_COMP['+nom+'][RQS_BTP_COMP_NAME]" id="" value=""></div><label class="control-label col-md-1 col-sm-1 col-xs-1"> Kadar </label><div class="col-md-2 col-sm-2 col-xs-2"><input required class="form-control  keyup-100desimal" onkeyup="cek100desimal(\'RQS_BTP_COMP_QTY'+nom+'\',$(this).val())" type="text" name="TX_RQS_BTP_COMP['+nom+'][RQS_BTP_COMP_QTY]" id="RQS_BTP_COMP_QTY'+nom+'" value=""></div><div class="col-md-1 col-sm-1 col-xs-1"><label class="control-label col-md-1 col-sm-1 col-xs-1">%</label></div><div class="col-md-2 col-sm-2 col-xs-2"><button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple min_" onClick="$(\'.' + cls + '\').remove();return false;"><i class="fa fa-trash"></i> Hapus</button></div></div>');
      $('.addberat').attr('terakhir', parseInt(nom) + 1);
    }
    return false;

  }
  function addberat2() {
    var countTX_RQS_FOOD_COMP = $(".TX_RQS_FOOD_COMP").length;
    if(countTX_RQS_FOOD_COMP == 1000){
      alert("Batas Maksimal Komposisi Produk Pangan adalah 1000");
    }else{
      var nom = $('.2addberat').attr('terakhir');
      var idtr = $('.2addberat').attr('periksa');
      var cls = idtr + nom;
      $('#hideme2').before('<div class="TX_RQS_FOOD_COMP form-group row ' + cls + '"><div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3"><input type="hidden" name="TX_RQS_FOOD_COMP['+nom+'][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>"><input required class="form-control keyup-100" type="text" name="TX_RQS_FOOD_COMP['+nom+'][RQS_FOOD_COMP_NAME]" id="" value=""></div><label class="control-label col-md-1 col-sm-1 col-xs-1"> Kadar </label><div class="col-md-2 col-sm-2 col-xs-2"><input required class="form-control keyup-100desimal" onkeyup="cek100desimal(\'RQS_FOOD_COMP_QTY'+nom+'\',$(this).val())" type="text" name="TX_RQS_FOOD_COMP['+nom+'][RQS_FOOD_COMP_QTY]" id="RQS_FOOD_COMP_QTY'+nom+'" value=""></div><div class="col-md-1 col-sm-1 col-xs-1"><label class="control-label col-lg-1">%</label></div><div class="col-md-2 col-sm-2 col-xs-2"><button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple 2min_" onClick="$(\'.' + cls + '\').remove();return false;"><i class="fa fa-trash"></i> Hapus</button></div></div>');
  /*
  <label class="control-label col-md-1 col-sm-1 col-xs-1"> Kadar </label><div class="col-md-1 col-sm-1 col-xs-1"><input required class="form-control keyup-100" onkeyup="cek100(\'RQS_FOOD_COMP_QTY'+nom+'\',$(this).val())" type="text" name="TX_RQS_FOOD_COMP['+nom+'][RQS_FOOD_COMP_QTY]" id="RQS_FOOD_COMP_QTY'+nom+'" value=""></div><div class="col-md-1 col-sm-1 col-xs-1"><label class="control-label col-lg-1">%</label></div>
  */
      $('.2addberat').attr('terakhir', parseInt(nom) + 1);      
    }
    return false;

  }

  function chkAda(sertifikat) {
      if (sertifikat == 'CES02') {
          $('.RQS_UNCTF_RSN').show(500).removeAttr('disabled').attr('required','required');
          $('.fileuploadDSAB').hide(500).attr('disabled', 'disabled');
          $('#DOC_IDDSAB, #DOC_PATHDSAB, #DOC_NAMEDSAB, #DOC_EXTDSAB, #DOC_SIZEDSAB, #DOC_FILEDSAB, #DOC_CODEDSAB ').attr('disabled', 'disabled');
      } else {
          $('.RQS_UNCTF_RSN').hide(500).attr('disabled', 'disabled');
          $('.fileuploadDSAB').show(500).removeAttr('disabled');
          $('#DOC_IDDSAB, #DOC_PATHDSAB, #DOC_NAMEDSAB, #DOC_EXTDSAB, #DOC_SIZEDSAB, #DOC_FILEDSAB, #DOC_CODEDSAB ').removeAttr('disabled');
      }
  }

  function addNew(nomor) {
    var countRQS_PRD_REG_FOOD_TYPE_ID = $(".RQS_PRD_REG_FOOD_TYPE_ID").length;
    if(countRQS_PRD_REG_FOOD_TYPE_ID == 10){
      alert("Batas Maksimal Sandingan Regulasi adalah 10");
    }else{
      if(countRQS_PRD_REG_FOOD_TYPE_ID < 4){
        alert("Batas Minimal Sandingan Regulasi adalah 5");
      }      
      var nom = $('.fileuploadSKNL').attr('terakhir');
      //var idtr = $('.fileuploadSKNL').attr('periksa');
      var cls =  parseInt(nomor) + 1;
      var o1 = "";
      var o2 = "";
      var o3 = "";
      var o4 = "";
      var looping = "<?= $looping ?>";
      //<select name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_FOOD_TYPE_ID]" style=\'width:87%;\' class=\'form-control chosen-select\' required>'+looping+'</select>
      $('#hideme212').before('<tr class="RQS_PRD_REG_FOOD_TYPE_ID" id="line'+cls+'"><td style="width:16%;"><input required class="form-control" type="text" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_COUNTRY]"></td><td style="width:25%;"><input required class="form-control" type="text" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_FOOD_TYPE]" value=""></td><td style="width:14%;"><input required class="form-control" id="RQS_PRD_REG_MAX_QTY'+cls+'" type="text" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_MAX_QTY]" value=""></td><td style="width:35%;"><div class="form-group"><div class="col-md-2 col-sm-2 col-xs-2 form-group"><span class="btn btn-warning btn-xs fileinput-button" style="" ><i class="fa fa-upload"></i> <span>Upload Bukti</span><input class="form-control fileuploadSKNL" onClick="uploadFile('+cls+')" id="fileuploadSKNL'+cls+'" terakhir="'+cls+'" accept="application/pdf,image/jpeg,image/jpg" type="file" name="files" multiple ></span></div><div class="col-md-9 col-sm-10 col-xs-10 form-group"><div id="resultDokSKNL'+cls+'"><img src="<?= $base ?>/img/warning_sign2.png" alt="Upload Bukti" height="30px" style="padding-left:50px"><a id="linkSKNL'+cls+'" style="padding-top:20px;" href="" target="_blank"></a>&nbsp;&nbsp;</div></div></div><input type="hidden" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>"><input type="hidden" id="DOC_PATHSKNL'+cls+'" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_DOC_PATH]" value=""><input type="hidden" id="DOC_NAMESKNL'+cls+'" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_DOC_NAME]" value=""><input type="hidden" id="DOC_EXTSKNL'+cls+'" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_DOC_EXT]" value=""><input type="hidden" id="DOC_SIZESKNL'+cls+'" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_DOC_SIZE]" value=""><input type="hidden" id="DOC_FILESKNL'+cls+'" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_DOC_FILE]" value=""><input type="hidden" id="DOC_CODESKNL'+cls+'" name="TX_RQS_PRD_REG_DOC['+cls+'][DOCUMENT_ID]" value="10"><input type="hidden" name="TX_RQS_PRD_REG_DOC['+cls+'][RQS_PRD_REG_ORDER]" value="'+cls+'"><td style="width:10%;">&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple" onclick="$(\'#line'+cls+'\').remove();return false;"><i class="fa fa-trash"></i> Hapus</button></td></tr>'); // onkeyup="keyupNumeric2(\'RQS_PRD_REG_MAX_QTY'+cls+'\',$(this).val())"
      //$('.fileuploadSKNL').attr('terakhir', parseInt(nomor) + 1);
      $('#SKNL').val(cls);
    }
    return false;
  }

  function uploadFile(nomor){
    //console.log(nomor);
    $('#fileuploadSKNL'+nomor).fileupload({
      url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
      dataType: 'json',
      formData: {action: 'upload', kdDok: '10', format: 'jpg;jpeg;pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>', urutan : nomor },
      start: function (e) {
        $('#resultDokSKNL'+nomor).html('<div id="progressSKNL'+nomor+'" class="progress" style="margin-top:5px;margin-left:40px"><div id="progress-bar'+nomor+'" class="progress-bar progress-bar-success"></div></div>');
      },
      done: function (e, data) {
        //console.log(data);
        if(data.result.status=='true'){
          $('#DOC_PATHSKNL'+nomor).val(data.result.path);
          $('#DOC_NAMESKNL'+nomor).val(data.result.name);
          $('#DOC_EXTSKNL'+nomor).val(data.result.ext);
          $('#DOC_SIZESKNL'+nomor).val(data.result.size);
          $('#DOC_FILESKNL'+nomor).val(data.result.rename);
          $('#progressSKNL'+nomor).fadeOut('fast', function(){
            $('#resultDokSKNL'+nomor).html('<div style="padding:0px 0px 20px 0px;margin-left:45px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a><span style="color:green; margin-left: 10px" class="alert">Upload Successful!</span></div>');
          });
                      hitung();
        }else{
          $('#progressSKNL'+nomor).fadeOut('fast', function(){
            $('#resultDokSKNL'+nomor).html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
          });                                
        }
      },
      progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progressSKNL'+nomor+' .progress-bar').css(
          'width',
          progress + '%'
          );
      }
    }).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled');  
  }

</script>
          <section id="page-content">
            <div class="container">
              <div class="row">
                <form class="form-horizontal form-label-left " action="<?= $act ?>" id="fpemohon" name="fpemohon" enctype="multipart/form-data">
                  <input type="hidden" name="id" id="RQS_ID" value="<?= $arrTxRqs['RQS_ID'] ?>">
                  <input type="hidden" name="RQS_NO_AJU" id="RQS_NO_AJU" value="<?= $arrTxRqs['RQS_NO_AJU'] ?>">
                  <input type="hidden" name="RQS_PIC_MAIL" id="RQS_PIC_MAIL" value="<?= $arrTxRqs['RQS_PIC_MAIL'] ?>">
                  <div class="col-md-12">
                    <h6 class="text-uppercase"> DATA ADMINISTRASI PEMOHON >> DATA UMUM BTP >> <span style="color: blue"> DATA TEKNIS </span> </h6>
                    <hr>
                    <div class="form-group">
                      <label class="control-label col-md-5 col-sm-8 col-xs-12" style="text-align: left;"> <span style="color: blue"> DATA TEKNIS </span></label>
                    </div>
                    <br/>

                    <!-- Bahan Tambahan Pangan -->          
                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" style="color: black; text-align: left"> <b>Bahan Tambahan Pangan </b></label>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"> No INS </label>
                      <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="hidden" value="<?= $arrTxRqs['RQS_BTP_TYPE_ID'] ?>" name="btp_id" >
                        <input type="hidden" value="<?= $arrTxRqs['RQS_FOOD_TYPE_ID'] ?>" name="food_id" >
                        <input class="form-control" style="width:87%;" type="text" value="<?= $arrTxRqs['BTP_TYPE_NO'] ?>"  disabled>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Jenis BTP </label>
                      <div class="col-md-8 col-sm-8 col-xs-12">
                        <input class="form-control" style="width:87%;" type="text" value="<?= $arrTxRqs['BTP_TYPE_NAME'] ?>" disabled>
                      </div>
                    </div>

                    <?php
                    $jmldata = count($arrTxRqsBtpComp)+1;
                    if ($jmldata == 1) {
                      $jmldata = 2;
                      $arrTxRqsBtpComp[] = "";
                    }
                    $i = 1;
                    foreach ($arrTxRqsBtpComp as $key => $value) {
                      ?>

                      <div class="TX_RQS_BTP_COMP form-group row berat<?= $i; ?>">
                      <?php
                      if ($i == 1) {
                        ?>
                        <label class="control-label col-md-3 col-sm-3 col-xs-3"> Komposisi BTP <span class="red"> * </span></label>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <?php
                          } else {
                            ?>
                        <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <?php
                      }
                      ?>
                          <input type="hidden" name="TX_RQS_BTP_COMP[<?= $i ?>][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>">
                          <input required class="form-control" type="text" name="TX_RQS_BTP_COMP[<?= $i ?>][RQS_BTP_COMP_NAME]"  value="<?= $value['RQS_BTP_COMP_NAME'] ?>">
                        </div>

                        

                        <label class="control-label col-md-1 col-sm-1 col-xs-1"> Kadar </label>
                        <div class="col-md-2 col-sm-1 col-xs-1">
                          <input required class="form-control keyup-100desimal" type="text" name="TX_RQS_BTP_COMP[<?= $i ?>][RQS_BTP_COMP_QTY]"  value="<?= $value['RQS_BTP_COMP_QTY'] ?>"> 
                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-1"><label class="control-label col-lg-1">%</label></div>

                        <div class="col-md-2 col-sm-3 col-xs-3">
                          <?php
                          if ($i == 1) {
                            ?>
                            <button type="button" class="btn btn-info btn-rounded btn-xs legitRipple add_ addberat" onclick="addberat();" periksa="berat" terakhir="<?= $jmldata; ?>"><i class="fa fa-plus-square"></i> Tambah</button>&nbsp;&nbsp;

                            <?php
                          } else {
                            ?>
                            <button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple min_" onclick="$('.berat<?= $i; ?>').remove();return false;"><i class="fa fa-trash"></i> Hapus</button>
                            <?php
                          }
                          ?>
                        </div>
                      </div>

                      <?php
                      $i++;
                    }
                    ?>
                    <div id="hideme"></div>


                    <!-- Bahan Tambahan Pangan End -->
                    <div id="KomposisiBTP"></div>
                    <div class="form-group">
                      <div class="col-md-1 col-sm-1 col-xs-1 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 ">

                        <span class="btn btn-warning btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Bukti</span>
                            <input class="form-control fileupload" id="fileuploadBKB" terakhir="12345" accept="application/pdf,image/jpeg,image/jpg" type="file" name="files" multiple >
                        </span>

                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokBKB"> 
                          <?= ($arrTxRqsDoc05['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkBKB" style="padding-top:20px;color:blue;margin-left:8px" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc05['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc05['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc05['RQS_DOC_NAME'] . '</a>'  : '<img src="'.base_url().'/img/warning_sign2.png" alt="Upload Bukti" height="30px" style="padding-left:50px" data-toggle="tooltip" data-placement="right" title="Dokumen dalam format .pdf atau .jpg (Maksimal 5MB)">') ?>
                        </div>    

                      </div>
                    </div>
                    <input type="hidden" name="DOC[1][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>">
                    <input type="hidden" id="DOC_PATH" name="DOC[1][RQS_DOC_PATH]" value="<?= $arrTxRqsDoc05['RQS_DOC_PATH'] ?>">
                    <input type="hidden" id="DOC_NAME" name="DOC[1][RQS_DOC_NAME]" value="<?= $arrTxRqsDoc05['RQS_DOC_NAME'] ?>">
                    <input type="hidden" id="DOC_EXT" name="DOC[1][RQS_DOC_EXT]" value="<?= $arrTxRqsDoc05['RQS_DOC_EXT'] ?>">
                    <input type="hidden" id="DOC_SIZE" name="DOC[1][RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc05['RQS_DOC_SIZE'] ?>">
                    <input type="hidden" id="DOC_FILE" name="DOC[1][RQS_DOC_FILE]" value="<?= $arrTxRqsDoc05['RQS_DOC_FILE'] ?>">
                    <input type="hidden" id="DOC_CODE" name="DOC[1][DOCUMENT_ID]" value="05">

            <script>
              $(function () {
                'use strict';
                $('#fileuploadBKB').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '05', format: 'jpg;jpeg;pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokBKB').html('<div id="progressBKB" class="progress" style="margin-top:5px;"><div id="progress-barBKB" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATH').val(data.result.path);
                      $('#DOC_NAME').val(data.result.name);
                      $('#DOC_EXT').val(data.result.ext);
                      $('#DOC_SIZE').val(data.result.size);
                      $('#DOC_FILE').val(data.result.rename);
                      $('#u1').html('');
                      $('#progressBKB').fadeOut('fast', function(){
                        $('#resultDokBKB').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a><span style="color:green; margin-left: 100px" class="alert">Upload Successful!</span></div>');
                      });
                      hitung();
                    }else{
                      $('#progressBKB').fadeOut('fast', function(){
                        $('#resultDokBKB').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressBKB .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });  
            </script>   
            <?php if($arrTxRqsDoc05['RQS_DOC_NAME'] == ''){ ?>
            <div id="u1" class="col-md-9 col-md-offset-3" style="margin-top: -5px; color: red; ">
              
            </div>              
            <?php } ?>
<!--
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"><p> Spesifikasi Mutu Bahan </p><p> (Fisika & Kimia) </p></label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" name="TX_RQS[RQS_BTP_SPEC]" required><?= $arrTxRqs['RQS_BTP_SPEC'] ?></textarea>
                      </div>
                    </div>
-->
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Spesifikasi Mutu Bahan <span class="red"> * </span></label>
                      <div class="col-md-1 col-sm-1 col-xs-1 form-group">

                        <span class="btn btn-warning btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Bukti</span>
                            <input class="form-control" id="fileuploadDSMB" accept="application/pdf,image/jpeg,image/jpg" type="file" name="files" multiple >
                        </span>

                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokDSMB"> 
                          <?= ($arrTxRqsDoc06['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkDSMB" style="padding-top:20px;color:blue;margin-left:8px" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc06['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc06['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc06['RQS_DOC_NAME'] . '</a>' : '<img src="'.base_url().'/img/warning_sign2.png" alt="Upload Bukti" height="30px" style="padding-left:50px" data-toggle="tooltip" data-placement="right" title="Dokumen dalam format .pdf atau .jpg (Maksimal 5MB)">') ?>
                        </div>    

                      </div>
                    </div>

              <input type="hidden" name="DOC[2][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>">
              <input type="hidden" id="DOC_PATHDSMB" name="DOC[2][RQS_DOC_PATH]" value="<?= $arrTxRqsDoc06['RQS_DOC_PATH'] ?>">
              <input type="hidden" id="DOC_NAMEDSMB" name="DOC[2][RQS_DOC_NAME]" value="<?= $arrTxRqsDoc06['RQS_DOC_NAME'] ?>">
              <input type="hidden" id="DOC_EXTDSMB" name="DOC[2][RQS_DOC_EXT]" value="<?= $arrTxRqsDoc06['RQS_DOC_EXT'] ?>">
              <input type="hidden" id="DOC_SIZEDSMB" name="DOC[2][RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc06['RQS_DOC_SIZE'] ?>">
              <input type="hidden" id="DOC_FILEDSMB" name="DOC[2][RQS_DOC_FILE]" value="<?= $arrTxRqsDoc06['RQS_DOC_FILE'] ?>">
              <input type="hidden" id="DOC_CODEDSMB" name="DOC[2][DOCUMENT_ID]" value="06">
            <script>
              $(function () {
                'use strict';
                $('#fileuploadDSMB').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '06', format: 'jpg;jpeg;pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokDSMB').html('<div id="progressDSMB" class="progress" style="margin-top:5px;"><div id="progress-barDSMB" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHDSMB').val(data.result.path);
                      $('#DOC_NAMEDSMB').val(data.result.name);
                      $('#DOC_EXTDSMB').val(data.result.ext);
                      $('#DOC_SIZEDSMB').val(data.result.size);
                      $('#DOC_FILEDSMB').val(data.result.rename);
                      $('#u2').html('');
                      $('#progressDSMB').fadeOut('fast', function(){
                        $('#resultDokDSMB').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a><span style="color:green; margin-left: 100px" class="alert">Upload Successful!</span></div>');
                      });
                      hitung();
                    }else{
                      $('#progressDSMB').fadeOut('fast', function(){
                        $('#resultDokDSMB').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressDSMB .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       
            <?php if($arrTxRqsDoc06['RQS_DOC_NAME'] == ''){ ?>
            <div id="u2" class="col-md-9 col-md-offset-3" style="margin-top: -10px; color: red; ">
              
            </div>              
            <?php } ?>


                    <!-- Produk Pangan -->          
                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" style="color: black; text-align: left"><b> Produk Pangan </b></label>
                    </div>

                    <?php
                    $jmldata = count($arrTxRqsFoodComp)+1;
                    if ($jmldata == 1) {
                      $jmldata = 2;
                      $arrTxRqsFoodComp[] = "";
                    }
                    $i = 1;
                    foreach ($arrTxRqsFoodComp as $key => $value) {
                      ?>
                      
                      <div class="TX_RQS_FOOD_COMP form-group row 2berat<?= $i; ?>">
                      <?php
                      if ($i == 1) {
                        ?>
                        <label class="control-label col-md-3 col-sm-3 col-xs-3"> Komposisi Produk Pangan <span class="red"> * </span></label>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <?php
                          } else {
                            ?>
                        <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <?php
                      }
                      ?>
                          <input type="hidden" name="TX_RQS_FOOD_COMP[<?= $i ?>][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>">
                          <input required class="form-control" type="text" name="TX_RQS_FOOD_COMP[<?= $i ?>][RQS_FOOD_COMP_NAME]" value="<?= $value['RQS_FOOD_COMP_NAME'] ?>">
                        </div>

                        

                        <label class="control-label col-md-1 col-sm-1 col-xs-1"> Kadar </label>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <input required class="form-control keyup-100desimal" type="text" name="TX_RQS_FOOD_COMP[<?= $i ?>][RQS_FOOD_COMP_QTY]" value="<?= $value['RQS_FOOD_COMP_QTY'] ?>"> 
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1"><label class="control-label col-lg-1">%</label></div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <?php
                          if ($i == 1) {
                            ?>
                            <button type="button" class="btn btn-info btn-rounded btn-xs legitRipple add_ 2addberat" onclick="addberat2();" periksa="2berat" terakhir="<?= $jmldata; ?>"><i class="fa fa-plus-square"></i> Tambah</button>&nbsp;&nbsp;
                            
                            <?php
                          } else {
                            ?>
                            <button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple 2min_" onclick="$('.2berat<?= $i; ?>').remove();return false;"><i class="fa fa-trash"></i> Hapus</button>
                            <?php
                          }
                          ?>
                        </div>
                      </div>

                      <?php
                      $i++;
                    }
                    ?>
                    <div id="hideme2"></div>

                    <!-- Komposisi BTP End -->

                    <div class="form-group">
                      <div class="col-md-1 col-sm-1 col-xs-1 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 ">

                        <span class="btn btn-warning btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Bukti</span>
                            <input class="form-control" id="fileuploadBKPP" accept="application/pdf,image/jpeg,image/jpg" type="file" name="files" multiple >
                        </span>

                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokBKPP"> 
                          <?= ($arrTxRqsDoc07['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkBKPP" style="padding-top:20px;color:blue;margin-left:8px" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc07['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc07['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc07['RQS_DOC_NAME'] . '</a></span>' : '<img src="'.base_url().'/img/warning_sign2.png" alt="Upload Bukti" height="30px" style="padding-left:50px" data-toggle="tooltip" data-placement="right" title="Dokumen dalam format .pdf atau .jpg (Maksimal 5MB)">') ?>
                        </div>    

                      </div>
                    </div>  

              <input type="hidden" name="DOC[3][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>">
              <input type="hidden" id="DOC_PATHBKPP" name="DOC[3][RQS_DOC_PATH]" value="<?= $arrTxRqsDoc07['RQS_DOC_PATH'] ?>">
              <input type="hidden" id="DOC_NAMEBKPP" name="DOC[3][RQS_DOC_NAME]" value="<?= $arrTxRqsDoc07['RQS_DOC_NAME'] ?>">
              <input type="hidden" id="DOC_EXTBKPP" name="DOC[3][RQS_DOC_EXT]" value="<?= $arrTxRqsDoc07['RQS_DOC_EXT'] ?>">
              <input type="hidden" id="DOC_SIZEBKPP" name="DOC[3][RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc07['RQS_DOC_SIZE'] ?>">
              <input type="hidden" id="DOC_FILEBKPP" name="DOC[3][RQS_DOC_FILE]" value="<?= $arrTxRqsDoc07['RQS_DOC_FILE'] ?>">
              <input type="hidden" id="DOC_CODEBKPP" name="DOC[3][DOCUMENT_ID]" value="07">
            <script>
              $(function () {
                'use strict';
                $('#fileuploadBKPP').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '07', format: 'jpg;jpeg;pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokBKPP').html('<div id="progressBKPP" class="progress" style="margin-top:5px;"><div id="progress-barBKPP" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHBKPP').val(data.result.path);
                      $('#DOC_NAMEBKPP').val(data.result.name);
                      $('#DOC_EXTBKPP').val(data.result.ext);
                      $('#DOC_SIZEBKPP').val(data.result.size);
                      $('#DOC_FILEBKPP').val(data.result.rename);
                      $('#u3').html('');
                      $('#progressBKPP').fadeOut('fast', function(){
                        $('#resultDokBKPP').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a><span style="color:green; margin-left: 100px" class="alert">Upload Successful!</span></div>');
                      });
                      hitung();
                    }else{
                      $('#progressBKPP').fadeOut('fast', function(){
                        $('#resultDokBKPP').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressBKPP .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       
            <?php if($arrTxRqsDoc07['RQS_DOC_NAME'] == ''){ ?>
            <div id="u3" class="col-md-9 col-md-offset-3" style="margin-top: -5px; color: red; ">
              
            </div>              
            <?php } ?>


                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-3"> Jumlah Penggunaan BTP dalam Produk Pangan <span class="red"> * </span></label>
                    <div class="col-md-1 col-sm-2 col-xs-2 "><?php //echo "<pre>"; print_r($arrTxRqs); echo "</pre>"; ?>
                      <input class="form-control" type="text"  name="" disabled value="<?= $arrTxRqs['RQS_BTP_NETTO'] ?>"> <!-- name="TX_RQS[<?= $i ?>][RQS_FOOD_COMP_NAME]" -->
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2 "><?php //echo "<pre>"; print_r($arrTxRqs); echo "</pre>";array('mg/kg' => 'mg/kg', 'mg/kg lemak' => 'mg/kg lemak', 'mg/L' => 'mL/L') ?>
                      <?= form_dropdown('RQS_BTP_UNIT', $DENOMINATION, $arrTxRqs['RQS_BTP_UNIT'], 'style="min-width:60px" class="form-control" disabled'); ?>
                    </div>
                  </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Fungsi BTP dalam Produk Pangan <span class="red"> * </span></label>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                       <?= form_dropdown('TX_RQS[RQS_BTP_FUNCTION_ID]', $RQS_BTP_FUNCTION_ID, $arrTxRqs['RQS_BTP_FUNCTION_ID'], 'id="RQS_BTP_FUNCTION_ID" wajib="" style="width:87%;" class="form-control" required'); ?>
                     </div>
                   </div> 

                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-3"> Mekanisme kerja BTP dalam Produk Pangan <span class="red"> * </span></label>
                    <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                      <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" name="TX_RQS[RQS_BTP_MECHANISM]" required><?= $arrTxRqs['RQS_BTP_MECHANISM'] ?></textarea>
                    </div>
                  </div> 

                   <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-3"> Sertifikat Analisis BTP dalam Produk Pangan <span class="red"> * </span></label>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                     <?= form_dropdown('TX_RQS[RQS_CTF_STATUS]', $RQS_CTF_STATUS, $arrTxRqs['RQS_CTF_STATUS'], 'id="RQS_CTF_STATUS" wajib="" style="width:87%;" class="form-control" onchange="chkAda($(this).val());"  required'); ?>
                   </div>
                 </div>                   

                 <div class="form-group RQS_UNCTF_RSN" style="display: none;">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3"> Alasan Tidak Memiliki Sertifikat <span class="red"> * </span></label>
                  <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                    <textarea class="resizable_textarea form-control RQS_UNCTF_RSN" rows="3"  style="width:87%;" name="TX_RQS[RQS_UNCTF_RSN]" ><?= $arrTxRqs['RQS_UNCTF_RSN'] ?></textarea>
                  </div>
                </div>

                <div class="form-group fileuploadDSAB"  style="display: none;">
                      <div class="col-md-1 col-sm-1 col-xs-1 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 ">

                        <span class="btn btn-warning btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Bukti</span>
                            <input class="form-control" id="fileuploadDSAB" accept="application/pdf,image/jpeg,image/jpg" type="file" name="files" multiple >
                        </span>

                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokDSAB"> 
                          <?= ($arrTxRqsDoc08['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkDSAB" style="padding-top:20px;color:blue;margin-left:8px" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc08['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc08['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc08['RQS_DOC_NAME'] . '</a>' : '<img src="'.base_url().'/img/warning_sign2.png" alt="Upload Bukti" height="30px" style="padding-left:50px" data-toggle="tooltip" data-placement="right" title="Dokumen dalam format .pdf atau .jpg (Maksimal 5MB)">') ?>
                        </div>    

                      </div>
                  <input type="hidden" id="DOC_IDDSAB" name="DOC[6][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>" disabled>
                  <input type="hidden" id="DOC_PATHDSAB" name="DOC[6][RQS_DOC_PATH]" value="<?= $arrTxRqsDoc08['RQS_DOC_PATH'] ?>" disabled>
                  <input type="hidden" id="DOC_NAMEDSAB" name="DOC[6][RQS_DOC_NAME]" value="<?= $arrTxRqsDoc08['RQS_DOC_NAME'] ?>" disabled>
                  <input type="hidden" id="DOC_EXTDSAB" name="DOC[6][RQS_DOC_EXT]" value="<?= $arrTxRqsDoc08['RQS_DOC_EXT'] ?>" disabled>
                  <input type="hidden" id="DOC_SIZEDSAB" name="DOC[6][RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc08['RQS_DOC_SIZE'] ?>" disabled>
                  <input type="hidden" id="DOC_FILEDSAB" name="DOC[6][RQS_DOC_FILE]" value="<?= $arrTxRqsDoc08['RQS_DOC_FILE'] ?>" disabled>
                  <input type="hidden" id="DOC_CODEDSAB" name="DOC[6][DOCUMENT_ID]" value="08" disabled>

                </div> 
            <script>
              $(function () {
                'use strict';
                $('#fileuploadDSAB').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '08', format: 'jpg;jpeg;pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokDSAB').html('<div id="progressDSAB" class="progress" style="margin-top:5px;"><div id="progress-barDSAB" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHDSAB').val(data.result.path);
                      $('#DOC_NAMEDSAB').val(data.result.name);
                      $('#DOC_EXTDSAB').val(data.result.ext);
                      $('#DOC_SIZEDSAB').val(data.result.size);
                      $('#DOC_FILEDSAB').val(data.result.rename);
                      $('#u4').html('');
                      $('#progressDSAB').fadeOut('fast', function(){
                        $('#resultDokDSAB').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a><span style="color:green; margin-left: 100px" class="alert">Upload Successful!</span></div>');
                      });
                      hitung();
                    }else{
                      $('#progressDSAB').fadeOut('fast', function(){
                        $('#resultDokDSAB').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressDSAB .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       
            <?php if($arrTxRqsDoc08['RQS_DOC_NAME'] == ''){ ?>
            <div id="u4" class="col-md-9 col-md-offset-3 fileuploadDSAB"  style="display: none;margin-top: -5px; color: red; ">
              
            </div>              
            <?php } ?>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3"> Metode Pengujian BTP <span class="red"> * </span></label>
                  <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                    <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" name="TX_RQS[RQS_BTP_TEST_METHOD]"  required><?= $arrTxRqs['RQS_BTP_TEST_METHOD'] ?></textarea>
                  </div>
                </div> 

                <div class="form-group">
                      <div class="col-md-1 col-sm-1 col-xs-1 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 ">

                        <span class="btn btn-warning btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Bukti</span>
                            <input class="form-control" id="fileuploadDMPB" accept="application/pdf,image/jpeg,image/jpg" type="file" name="files" multiple >
                        </span>

                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokDMPB"> 
                          <?= ($arrTxRqsDoc09['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkDMPB" style="padding-top:20px;color:blue;margin-left:8px" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc09['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc09['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc09['RQS_DOC_NAME'] . '</a>' : '<img src="'.base_url().'/img/warning_sign2.png" alt="Upload Bukti" height="30px" style="padding-left:50px" data-toggle="tooltip" data-placement="right" title="Dokumen dalam format .pdf atau .jpg (Maksimal 5MB)">') ?>
                        </div>    

                      </div>
                </div> 
              <input type="hidden" name="DOC[4][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>">
              <input type="hidden" id="DOC_PATHDMPB" name="DOC[4][RQS_DOC_PATH]" value="<?= $arrTxRqsDoc09['RQS_DOC_PATH'] ?>">
              <input type="hidden" id="DOC_NAMEDMPB" name="DOC[4][RQS_DOC_NAME]" value="<?= $arrTxRqsDoc09['RQS_DOC_NAME'] ?>">
              <input type="hidden" id="DOC_EXTDMPB" name="DOC[4][RQS_DOC_EXT]" value="<?= $arrTxRqsDoc09['RQS_DOC_EXT'] ?>">
              <input type="hidden" id="DOC_SIZEDMPB" name="DOC[4][RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc09['RQS_DOC_SIZE'] ?>">
              <input type="hidden" id="DOC_FILEDMPB" name="DOC[4][RQS_DOC_FILE]" value="<?= $arrTxRqsDoc09['RQS_DOC_FILE'] ?>">
              <input type="hidden" id="DOC_CODEDMPB" name="DOC[4][DOCUMENT_ID]" value="09">
            <script>
              $(function () {
                'use strict';
                $('#fileuploadDMPB').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '09', format: 'jpg;jpeg;pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokDMPB').html('<div id="progressDMPB" class="progress" style="margin-top:5px;"><div id="progress-barDMPB" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHDMPB').val(data.result.path);
                      $('#DOC_NAMEDMPB').val(data.result.name);
                      $('#DOC_EXTDMPB').val(data.result.ext);
                      $('#DOC_SIZEDMPB').val(data.result.size);
                      $('#DOC_FILEDMPB').val(data.result.rename);
                      $('#u5').html('');
                      $('#progressDMPB').fadeOut('fast', function(){
                        $('#resultDokDMPB').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a><span style="color:green; margin-left: 100px" class="alert">Upload Successful!</span></div>');
                      });
                      hitung();
                    }else{
                      $('#progressDMPB').fadeOut('fast', function(){
                        $('#resultDokDMPB').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressDMPB .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       
            <?php if($arrTxRqsDoc09['RQS_DOC_NAME'] == ''){ ?>
            <div id="u5" class="col-md-9 col-md-offset-3" style="margin-top: -5px;color: red; ">
              
            </div>              
            <?php } ?>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3">  Alur Proses Produksi Produk Pangan <span class="red"> * </span></label>
                      <div class="col-md-1 col-sm-1 col-xs-1 form-group">

                        <span class="btn btn-warning btn-sm btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Bukti</span>
                            <input class="form-control" id="fileuploadAlur" accept="application/pdf,image/jpeg,image/jpg" type="file" name="files" multiple >
                        </span>

                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokAlur"> 
                          <?= ($arrTxRqsDoc11['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkAlur" style="padding-top:20px;color:blue;margin-left:8px" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc11['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc11['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc11['RQS_DOC_NAME'] . '</a>' : '<img src="'.base_url().'/img/warning_sign2.png" alt="Upload Bukti" height="30px" style="padding-left:50px" data-toggle="tooltip" data-placement="right" title="Dokumen dalam format .pdf atau .jpg (Maksimal 5MB)">') ?>
                        </div>    

                      </div>
                </div> 
              <input type="hidden" name="DOC[5][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>">
              <input type="hidden" id="DOC_PATHAlur" name="DOC[5][RQS_DOC_PATH]" value="<?= $arrTxRqsDoc11['RQS_DOC_PATH'] ?>">
              <input type="hidden" id="DOC_NAMEAlur" name="DOC[5][RQS_DOC_NAME]" value="<?= $arrTxRqsDoc11['RQS_DOC_NAME'] ?>">
              <input type="hidden" id="DOC_EXTAlur" name="DOC[5][RQS_DOC_EXT]" value="<?= $arrTxRqsDoc11['RQS_DOC_EXT'] ?>">
              <input type="hidden" id="DOC_SIZEAlur" name="DOC[5][RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc11['RQS_DOC_SIZE'] ?>">
              <input type="hidden" id="DOC_FILEAlur" name="DOC[5][RQS_DOC_FILE]" value="<?= $arrTxRqsDoc11['RQS_DOC_FILE'] ?>">
              <input type="hidden" id="DOC_CODEAlur" name="DOC[5][DOCUMENT_ID]" value="11">
            <script>
              $(function () {
                'use strict';
                $('#fileuploadAlur').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '11', format: 'jpg;jpeg;pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokAlur').html('<div id="progressAlur" class="progress" style="margin-top:5px;"><div id="progress-barAlur" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHAlur').val(data.result.path);
                      $('#DOC_NAMEAlur').val(data.result.name);
                      $('#DOC_EXTAlur').val(data.result.ext);
                      $('#DOC_SIZEAlur').val(data.result.size);
                      $('#DOC_FILEAlur').val(data.result.rename);
                      $('#u6').html('');
                      $('#progressAlur').fadeOut('fast', function(){
                        $('#resultDokAlur').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a><span style="color:green; margin-left: 100px" class="alert">Upload Successful!</span></div>');
                      });
                      hitung();
                    }else{
                      $('#progressAlur').fadeOut('fast', function(){
                        $('#resultDokAlur').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressAlur .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       
            <?php if($arrTxRqsDoc11['RQS_DOC_NAME'] == ''){ ?>
            <div id="u6" class="col-md-9 col-md-offset-3" style="margin-top: -10px; color: red; ">
              
            </div>             
            <?php } ?> 

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3"> Cara Penggunaan Produk Pangan <span class="red"> * </span></label>
                  <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                    <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" name="TX_RQS[RQS_FOOD_HOWTO]" required><?= $arrTxRqs['RQS_FOOD_HOWTO'] ?></textarea>
                  </div>
                </div>
                <br/>
                <br/>

                <!-- Sandingan Regulasi -->          
                <div class="form-group">
                  <label><span style="color: black"><b> Sandingan Regulasi </b></span></label>
                </div>

                      
                  <div class=" col-md-12 col-sm-12 col-xs-12 form-group " >
                    <table class="table" style="width:100%;">
                      <thead>
                        <th>Negara/Organisasi <span class="red"> * </span></th>
                        <th>Jenis Pangan <span class="red"> * </span></th>
                        <th>Batas Maksimal (mg/kg) <span class="red"> * </span></th>
                        <th>File <span class="red"> * </span></th>
                        <th>&nbsp;</th>
                      </thead>
                      <tbody>
                <?php
                //echo "<pre>";print_r($arrTxRqsPrdRegDoc);echo "</pre>";
                $jmldata = count($arrTxRqsPrdRegDoc)+1;
                if ($jmldata == 1) {
                  $jmldata = 2;
                  $arrTxRqsPrdRegDoc = array(array(),array(),array(),array(),array());
                }else if($jmldata <= 5){
                //echo  $jmldata;
                  for ($i=$jmldata-1; $i < 5; $i++) { 
                    $arrTxRqsPrdRegDoc[$i] = array();
                  }

                }
                //echo "<pre>";print_r($arrTxRqsPrdRegDoc);echo "</pre>";
                $i = 1;
                foreach ($arrTxRqsPrdRegDoc as $key => $value) {
                  $jml_sandingan= count($arrTxRqsPrdRegDoc);
                  ?>
                  <tr class="RQS_PRD_REG_FOOD_TYPE_ID" id="line<?= $i; ?>">
                        <td style="width:16%;">
                          <input required class="form-control" type="text" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_PRD_REG_COUNTRY]" value="<?= $value['RQS_PRD_REG_COUNTRY'] ?>">                        
                          <?php /*form_dropdown('TX_RQS_PRD_REG_DOC['.$i.'][RQS_PRD_REG_COUNTRY_ID]', $RQS_PRD_REG_COUNTRY_ID, $value['RQS_PRD_REG_COUNTRY_ID'], ' style="width:87%;" class="form-control" required'); */?>
                        </td>
                        <td style="width:25%;">
                          <input required class="form-control" type="text" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_PRD_REG_FOOD_TYPE]" value="<?= $value['RQS_PRD_REG_FOOD_TYPE'] ?>">                        
                          <?php //form_dropdown('TX_RQS_PRD_REG_DOC['.$i.'][RQS_PRD_REG_FOOD_TYPE_ID]', $RQS_PRD_REG_FOOD_TYPE_ID, $value['RQS_PRD_REG_FOOD_TYPE_ID'], ' style="width:87%;" class="form-control chosen-select" required'); ?>
                        </td>
                        <td style="width:14%;">
                          <input required class="form-control" type="text" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_PRD_REG_MAX_QTY]" value="<?= $value['RQS_PRD_REG_MAX_QTY'] ?>">
                        </td>
                        <td style="width:35%;">

                          <div class="form-group">
                            <div class="col-md-2 col-sm-2 col-xs-2 form-group">

                              <span class="btn btn-warning btn-xs fileinput-button" style="" ><i class="fa fa-upload"></i>
                                <span>Upload Bukti</span>
                                  <input class="form-control fileuploadSKNL" id="fileuploadSKNL<?= $i ?>" onclick="uploadFile(<?= $i ?>);" terakhir="<?= $i ?>" accept="application/pdf,image/jpeg,image/jpg" type="file" name="files" multiple >
                              </span>

                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-10 form-group">
                              <div id="resultDokSKNL<?= $i ?>"> 
                                <?= ($value['RQS_PRD_REG_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkSKNL'.$i.'" style="padding-top:20px;margin-left:30px;color:blue;" href="' . site_url() . "download/data/" . base64_encode($value['RQS_PRD_REG_DOC_PATH']) . "/" . $value['RQS_PRD_REG_DOC_NAME'] . '" target="_blank">' . $value['RQS_PRD_REG_DOC_NAME'] . '</a>' : '<img src="'.base_url().'/img/warning_sign2.png" alt="Upload Bukti" height="30px" style="padding-left:50px" data-toggle="tooltip" data-placement="right" title="Dokumen dalam format .pdf atau .jpg (Maksimal 5MB)">') ?>
                              </div>    

                            </div>
                          </div>
                          <input type="hidden" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>">
                          <input type="hidden" id="DOC_PATHSKNL<?= $i ?>" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_PRD_REG_DOC_PATH]" value="<?= $value['RQS_PRD_REG_DOC_PATH'] ?>">
                          <input type="hidden" id="DOC_NAMESKNL<?= $i ?>" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_PRD_REG_DOC_NAME]" value="<?= $value['RQS_PRD_REG_DOC_NAME'] ?>">
                          <input type="hidden" id="DOC_EXTSKNL<?= $i ?>" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_PRD_REG_DOC_EXT]" value="<?= $value['RQS_PRD_REG_DOC_EXT'] ?>">
                          <input type="hidden" id="DOC_SIZESKNL<?= $i ?>" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_PRD_REG_DOC_SIZE]" value="<?= $value['RQS_PRD_REG_DOC_SIZE'] ?>">
                          <input type="hidden" id="DOC_FILESKNL<?= $i ?>" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_PRD_REG_DOC_FILE]" value="<?= $value['RQS_PRD_REG_DOC_FILE'] ?>">
                          <input type="hidden" id="DOC_CODESKNL<?= $i ?>" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][DOCUMENT_ID]" value="10">
                          <input type="hidden" id="ORDER<?= $i ?>" name="TX_RQS_PRD_REG_DOC[<?= $i ?>][RQS_PRD_REG_ORDER]" value="<?= $i ?>">
                        </td>
                        <td style="width:10%;">
                          <?php
                          if ($i == 5) {
                            ?>
                            <button type="button" id="SKNL" class="btn btn-info btn-rounded btn-xs legitRipple" value="<?= $jml_sandingan ?>" onclick="addNew(this.value);"><i class="fa fa-plus-square"></i> Tambah</button>&nbsp;&nbsp;
<!--&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple" onclick="$('#line1').remove();return false;"><i class="icon-cancel-circle2"></i> Hapus</button>-->                            
                            <?php
                          } else if($i > 5){
                            ?>
&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple" onclick="$('#line<?= $i ?>').remove();return false;"><i class="fa fa-trash"></i> Hapus</button>
                            <?php
                          }
                          ?>


                        </td>
                        </tr>
                  <?php
                  $i++;
                }
                ?>      <tr id="hideme212"></tr>
                      </tbody>
                    </table>
                  </div>

                <!-- END -->


                <div class="form-group" align="center">
                  <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                    <input type="checkbox" id="agg"> Saya setuju bahwa data diatas telah diisi dengan sebenarnya
                  </div>
                </div>
                <br/> 

                <div class="col-md-12">
                  <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                      <div class="notification hidden mbot-0"><div></div></div>
                      <a href="<?= site_url(); ?>submission/new_rqs/frm-btp/<?= $arrTxRqs['RQS_ID'] ?>"  class="btn" style="background-color: yellow" ><i class="fa fa-angle-double-left"></i> Kembali</a>
                      <button type="button" class="btn btn-danger" onclick="reset();"><i class="fa fa-eraser"></i> Reset</button>
                      <!--<button type="button" class="btn btn-danger" onclick="abc();"><i class="fa fa-eraser"></i> cek</button>
                      !--<button type="button" class="btn btn-info agg" onclick="validate('simpan');"  style="display: none;"><i class="fa fa-save"></i> Simpan</button>-->
                      <button type="button" class="btn btn-success agg" onclick="validate('submit');"  style="display: none;">Submit <i class="fa fa-angle-double-right"></i></button>
                    </div>
                  </div>
                </div>


              </form>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.container -->
        </section>
  <script src="<?=base_url()?>js/chosen.jquery.js" type="text/javascript"></script>

        <script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});
          $(document).ready(function(e){

            $('.keyup-numeric').keyup(function() {
              $('span.error-keyup-1').remove();
              var inputVal = parseFloat($(this).val());
              var numericReg = /^-{0,1}\d*\.{0,1}\d+$/;
              if(!numericReg.test(inputVal) || inputVal == 0) {  
                //$(this).val('');
                $(this).after('<span style="color:red" class="error error-keyup-1">Input harus bilangan asli atau bilangan desimal, contoh : 1.2 </span>');
              }
            });

            $('.keyup-numeric2').keyup(function() {
              $('span.error-keyup-1').remove();
              var inputVal = $(this).val();
              var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
              if(!numericReg.test(inputVal)) {  
                $(this).val('');  
                $(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
              }
            });
            $('.keyup-100').keyup(function() {
              $('span.error-keyup-1').remove();
              var inputVal = parseInt($(this).val());
              var numericReg = 100 ;
              var numericReg2 = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
              if(!numericReg2.test(inputVal) || inputVal > numericReg || inputVal <= 0) {  
                $(this).val('');  
                $(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka 1-100</span>');
              }
            });
            $('.keyup-100desimal').keyup(function() {
              $('span.error-keyup-1').remove();
              var inputVal = parseFloat($(this).val());
              var numericReg = 100 ;
              var numericReg2 = /^-{0,1}\d*\.{0,1}\d+$/;
              if(!numericReg2.test(inputVal) || inputVal > numericReg || inputVal < 0) {  
                $(this).val('');  
                $(this).after('<span style="color:red" class="error error-keyup-1">Input harus bilangan asli 1-100 atau bilangan desimal, contoh : 1.2 </span>');
              }
            });

            $("#agg").click(function(){
              if($(this).prop("checked") == true){
                $(".agg").show();
              }
              else if($(this).prop("checked") == false){
                $(".agg").hide();
              }
//              $(".agg").toggle();
            });

            var RQS_CTF_STATUS = "<?= $arrTxRqs['RQS_CTF_STATUS'] ?>";
            if(RQS_CTF_STATUS == 'CES01'){
              $('.fileuploadDSAB').show();
              $('#DOC_IDDSAB, #DOC_PATHDSAB, #DOC_NAMEDSAB, #DOC_EXTDSAB, #DOC_SIZEDSAB, #DOC_FILEDSAB, #DOC_CODEDSAB ').removeAttr('disabled');
            }else if(RQS_CTF_STATUS == 'CES02'){
              $('.RQS_UNCTF_RSN').show();
              $('#DOC_IDDSAB, #DOC_PATHDSAB, #DOC_NAMEDSAB, #DOC_EXTDSAB, #DOC_SIZEDSAB, #DOC_FILEDSAB, #DOC_CODEDSAB ').attr('disabled', 'disabled');
            }
          });

          function keyupNumeric(id,val){
              $('span.error-keyup-1').remove();
              var numericReg = /^-{0,1}\d*\.{0,1}\d+$/;
              var inputVal = val;
              if(!numericReg.test(inputVal) || inputVal == 0) {  
                //$(this).val('');
                $("#"+id).after('<span style="color:red" class="error error-keyup-1">Input harus bilangan asli atau bilangan desimal, contoh : 1.2 </span>');
              }

          }
          function keyupNumeric2(id,val){
              $('span.error-keyup-1').remove();
              var inputVal = val;
              var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
              if(!numericReg.test(inputVal)) {  
                $("#"+id).val('');  
                $("#"+id).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
              }
          }
          function cek100(id,val){
              $('span.error-keyup-1').remove();
              var inputVal = val;
              var numericReg = 100 ;
              var numericReg2 = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
              if(!numericReg2.test(inputVal) || inputVal > numericReg || inputVal <= 0) {  
                $("#"+id).val('');  
                $("#"+id).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka 1-100</span>');
              }
          }
          function cek100desimal(id,val){
              $('span.error-keyup-1').remove();
              var inputVal = parseFloat(val);
              var numericReg = 100 ;
              var numericReg2 = /^-{0,1}\d*\.{0,1}\d+$/;
              if(!numericReg2.test(inputVal) || inputVal > numericReg || inputVal < 0) {  
                $("#"+id).val('');  
                $("#"+id).after('<span style="color:red" class="error error-keyup-1">Input harus bilangan asli 1-100 atau bilangan desimal, contoh : 1.2 </span>');
              }

          }
          function abc(RQS_STATUS_ID){
              var jml = 0;
              $('input[name*=RQS_FOOD_COMP_QTY]').each(function(){
                jml = +(parseFloat(jml) + parseFloat($(this).val())).toFixed(12);
                console.log('produk'+jml);
              });
              var jml_btp = 0;
              $('input[name*=RQS_BTP_COMP_QTY]').each(function(){
                 jml_btp = +(parseFloat(jml_btp) + parseFloat($(this).val())).toFixed(11);
                console.log('komp'+jml_btp);
              });
          }
          function validate(RQS_STATUS_ID){
              var jml = 0;
              $('input[name*=RQS_FOOD_COMP_QTY]').each(function(){
                jml = +(parseFloat(jml) + parseFloat($(this).val())).toFixed(12);
                console.log('produk'+jml);
              });
              var jml_btp = 0;
              $('input[name*=RQS_BTP_COMP_QTY]').each(function(){
                 jml_btp = +(parseFloat(jml_btp) + parseFloat($(this).val())).toFixed(12);
                console.log(jml_btp);
              });

              var sknl = $('#SKNL').val();
              if(RQS_STATUS_ID == 'simpan'){
                $('#RQS_STATUS_ID').remove();
                $('#RQS_ID').after('<input type="hidden" id="RQS_STATUS_ID" name="TX_RQS[RQS_STATUS_ID]" value="RS01">');
              }else if(RQS_STATUS_ID == 'submit'){
                $('#RQS_STATUS_ID').remove();
                $('#RQS_ID').after('<input type="hidden" id="RQS_STATUS_ID" name="TX_RQS[RQS_STATUS_ID]" value="RS02">');
              }
              sandingan = 'tidak';
              for(i=1; i<=sknl; i++){
                if($('#DOC_NAMESKNL'+i).val() == ''){
                  sandingan = 'ya';
                  alert("Silakan Upload Sandingan Komparasi Negara Lain");
                  $('#DOC_NAMESKNL'+i).focus();
                  break;
                  return false;
                }
              }

              var countRQS_PRD_REG_FOOD_TYPE_ID = $(".RQS_PRD_REG_FOOD_TYPE_ID").length;
              if(countRQS_PRD_REG_FOOD_TYPE_ID < 4){
                  alert("Batas Minimal Sandingan Regulasi adalah 5");
                  return false;
              }else if($('#DOC_NAME').val() == ''){
                  alert("Silakan Upload Komposisi BTP");
                  $('#fileuploadBKB').focus();
                  return false;
              }else if(jml != 100){
                  alert("Komposisi produk pangan harus 100%");
                  $('.keyup-100desimal').focus();
                  return false;
              }else if(jml_btp != 100){
                  alert("Komposisi BTP harus 100%");
                  $('.keyup-100').focus();
                  return false;
              }else if($('#DOC_NAMEBKPP').val() == ''){
                  alert("Silakan Upload Komposisi Produk Pangan");
                  $('#fileuploadBKPP').focus();
                  return false;
              }else if($('#DOC_NAMEDMPB').val() == ''){
                  alert("Silakan Upload Dokumen Metode Pengujian BTP");
                  $('#fileuploadDMPB').focus();
                  return false;
              }else if($('#DOC_NAMEDSMB').val() == ''){
                  alert("Silakan Upload Dokumen Spesifikasi Mutu Bahan");
                  $('#fileuploadDSMB').focus();
                  return false;
              }else if($('#RQS_CTF_STATUS').val() == 'CES01' && $('#DOC_NAMEDSAB').val() == ''){
                  alert("Silakan Upload Dokumen Sertifikat Analisis BTP");
                  $('#fileuploadDSAB').focus();
                  return false;        
              }else if($('#DOC_NAMEAlur').val() == ''){
                  alert("Silakan Upload Dokumen Metode Pengujian BTP");
                  $('#fileuploadAlur').focus();
                  return false;
              }else{
                  if(sandingan == 'tidak'){
                    var r = confirm("Pastikan data yang diisikan benar, lanjut ?");
                    if(r == true) {
                      save_post('#fpemohon');
                      return false;     
                    }else{
                      return false;     
                    }                   
                  }else{
                    return false;
                  }
              }
          }
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : { allow_single_deselect: true },
  '.chosen-select-no-single' : { disable_search_threshold: 10 },
  '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
  '.chosen-select-rtl'       : { rtl: true },
  '.chosen-select-width'     : { width: '95%' }
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
        </script>


