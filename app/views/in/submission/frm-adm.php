<!-- <?php print_r($arrdata['apptype']);?> -->
  <link rel="stylesheet" href="<?=base_url()?>js/chosen.css">
<script>
var site_url = '<?= site_url(); ?>';
</script>
<!--
<script src="<?= base_url(); ?>js/in/datatables/datatables.min.js"></script>
<script src="<?= base_url(); ?>js/in/datatables/buttons.min.js"></script>
<script src="<?= base_url(); ?>js/in/datatables/select.min.js"></script>
<script src="<?= base_url(); ?>js/in/datatables_extension_buttons_init.js"></script>
<link href="<?= base_url(); ?>js/in/summernote-master/summernote.css" rel="stylesheet">
<script src="<?= base_url(); ?>js/in/summernote-master/summernote.js"></script>
-->

<style>
  .red{
    color:red;
  }
  #scroll {
    border: 1px solid black;
    height: 400px;
    overflow: scroll;
  }
</style>
<section id="page-header" class="page-section">
  <hr>
</section>
<?php //print_r($_SESSION) ; echo $this->newsession->userdata('USER_TYPE_ID'); ?>

<section id="page-content">
  <div class="container">
    <div class="row">
      <form class="form-horizontal form-label-left " action="<?= $act ?>" id="fsubmit" name="fsubmit" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $arrTxRqs['RQS_ID'] ?>">
        <div class="col-md-12">
          <h6 class="text-uppercase"> <span style="color: blue">DATA ADMINISTRASI PEMOHON </span> >> DATA UMUM BTP >> DATA TEKNIS</h6>
          <hr>
          <div class="form-group">
            <label class="control-label col-md-5 col-sm-8 col-xs-12" style="text-align: left;"> <span style="color: blue"> DATA ADMINISTRASI PEMOHON </span></label>
          </div>
          <br/>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Jenis Pemohon <span class="red"> * </span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
             <?= form_dropdown('USER[USER_TYPE_ID]', $USER_TYPE, $this->newsession->userdata('USER_TYPE_ID'), 'id="USER_TYPE_ID" wajib="" style="width:87%;" class="form-control" disabled '); ?>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Pemohon/ Perusahaan <span class="red"> * </span></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input class="form-control" style="width:87%;" type="text" name="USER[USER_NAME]" id="USER_NAME" value="<?= $this->newsession->userdata('USER_NAME') ?>" readonly>
              </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat <span class="red"> * </span></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <textarea class="resizable_textarea form-control" rows="3" placeholder='' style="width:87%;" name="USER[USER_ADDR]" readonly><?= $this->newsession->userdata('USER_ADDR') ?></textarea>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Penanggung Jawab <span class="red"> * </span></label>
              <div class="col-md-8 col-sm-8 col-xs-11 form-group">
                  <input type="hidden" class="form-control" id="userid" name="REQUEST[USER_ID]" value="<?= $arrTxRqs['USER_ID'];?>">
                  <input id="RQS_PIC_NAME" class="form-control alfabet" style="width:87%;" type="text" name="REQUEST[RQS_PIC_NAME]" value="<?= $arrTxRqs['RQS_PIC_NAME'];?>" required readonly>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                <i class="glyphicon glyphicon-edit" style="cursor:pointer; margin-left:-70px" title="Ubah" onclick="ubah('RQS_PIC_NAME')"></i>
              </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No Telp <span class="red"> * </span></label>
              <div class="col-md-8 col-sm-8 col-xs-11 form-group">
                <input id="RQS_PIC_TELP" class="form-control keyup-numeric" style="width:87%;" type="text" name="REQUEST[RQS_PIC_TELP]" value="<?= $arrTxRqs['RQS_PIC_TELP'];?>" required readonly>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                <i class="glyphicon glyphicon-edit" style="cursor:pointer; margin-left:-70px" title="Ubah" onclick="ubah('RQS_PIC_TELP')"></i>
              </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No Fax </label>
               <div class="col-md-8 col-sm-8 col-xs-11 form-group">
                <input id="RQS_PIC_FAX" class="form-control keyup-numeric" style="width:87%;" type="text" name="REQUEST[RQS_PIC_FAX]" value="<?= $arrTxRqs['RQS_PIC_FAX'];?>" readonly>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                <i class="glyphicon glyphicon-edit" style="cursor:pointer; margin-left:-70px" title="Ubah" onclick="ubah('RQS_PIC_FAX')"></i>
              </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat Email <span class="red"> * </span></label>
              <div class="col-md-8 col-sm-8 col-xs-12 form-group">
               <input id="RQS_PIC_MAIL" class="form-control keyup-email" style="width:87%;" type="text" name="REQUEST[RQS_PIC_MAIL]" value="<?= $arrTxRqs['RQS_PIC_MAIL'];?>" required readonly>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                <i class="glyphicon glyphicon-edit" style="cursor:pointer; margin-left:-70px" title="Ubah" onclick="ubah('RQS_PIC_MAIL')"></i>
              </div>
          </div>

<!--
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No. INS <span class="red"> * </span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
            <input type= "text" class="text form-control" placeholder="No INS" name="REQ" value="<?= $arrTxRqs['BTP_TYPE_NO'] ?>" url="<?= site_url() ?>autocomplete/ins" id="pbk_code22" required  style="width:87%;">
            <input type="hidden" name="REQUEST[RQS_BTP_TYPE_ID]" id="noisn" wajib="yes" value="<?= $arrTxRqs['RQS_BTP_TYPE_ID'] ?>"/>
            </div>
          </div>
-->
<!--
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Jenis BTP <span class="red"> * </span></label>
             <div class="col-md-8 col-sm-8 col-xs-12 form-group">
             < form_dropdown('REQUEST[RQS_BTP_TYPE_NAME_ID]', $RQS_BTP_TYPE_NAME_ID, $arrTxRqs['RQS_BTP_TYPE_NAME_ID'], 'style="width:87%;" class="text form-control" style="" onchange="" url="" id="" required'); ?>
             </div>
          </div>
-->
<!--
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Jenis Pangan <span class="red"> * </span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
            <input type= "text" class="text form-control" placeholder="Jenis Pangan" name="REQ1" value="<?= $arrTxRqs['FOOD_TYPE_NAME'] ?>" url="<?= site_url() ?>autocomplete/jenis_pangan" id="food_code22" required  style="width:87%;">
            <input type="hidden" name="REQUEST[RQS_FOOD_TYPE_ID]" id="RQS_FOOD_TYPE_ID" wajib="yes" value="<?= $arrTxRqs['RQS_FOOD_TYPE_ID'] ?>"/>
            </div>
          </div>
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Jenis BTP <span class="red"> * </span></label>
             <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                <input id="jenisBTP" class="form-control" style="width:87%;" type="hidden" name="RQS_BTP_TYPE_NAME" value="<?= $arrTxRqs['BTP_TYPE_NAME'];?>" readonly>
             </div>
          </div>
-->
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No. INS/Nama Jenis BTP <span class="red"> * </span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
             <?= form_dropdown('REQUEST[RQS_BTP_TYPE_ID]', $noisn, $arrTxRqs['RQS_BTP_TYPE_ID'], 'style="width:87%;" class="text form-control chosen-select" data-placeholder="-- Pilih --" tabindex="2" style="" onchange="setnextnoisn(this.value,false);" url="'.site_url().'autocomplete/get_arr_btntype/" id="noisn" required'); ?>
                <input id="jenisBTP" class="form-control" style="width:87%;" type="hidden" name="RQS_BTP_TYPE_NAME" value="<?= $arrTxRqs['BTP_TYPE_NAME'];?>" required>
            </div>
          </div>
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Jenis Pangan <span class="red"> * </span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
             <?= form_dropdown('REQUEST[RQS_FOOD_TYPE_ID]', $foodtype, $arrTxRqs['RQS_FOOD_TYPE_ID'], 'style="width:87%;" class="text form-control chosen-select-pangan" style="" onchange="setnextfoodtype(this.value,false);" url="'.site_url().'autocomplete/get_arr_btnfoodtype/" id="RQS_FOOD_TYPE_ID" required'); ?>
            </div>
          </div>
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Kategori Pangan <span class="red"> * </span></label>
             <div class="col-md-8 col-sm-8 col-xs-12 form-group">
             <?php
             $ex = explode('Kategori Pangan ', $arrTxRqs['RQS_ABOUT']);
             ?>
              <input id="RQS_FOOD_TYPE_NO" class="form-control" style="width:87%;" type="text" name="RQS_FOOD_TYPE_NO" value="<?= $ex[1];?>" readonly required>
             </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3"> Jumlah Penggunaan BTP dalam Produk Pangan <span class="red"> * </span></label>
            <div class="col-md-1 col-sm-2 col-xs-2 ">
              <input class="form-control" type="text" id="RQS_BTP_NETTO"  name="REQUEST[RQS_BTP_NETTO]"  value="<?= $arrTxRqs['RQS_BTP_NETTO'] ?>" required> 
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 ">
              <?= form_dropdown('REQUEST[RQS_BTP_UNIT]', $DENOMINATION, $arrTxRqs['RQS_BTP_UNIT'], ' id="RQS_BTP_UNIT" style="min-width:60px" class="form-control" required'); ?>
            </div>
          </div> 
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Perihal <span class="red"> * </span></label>
             <div class="col-md-8 col-sm-8 col-xs-12 form-group">
              <!--<input class="form-control" style="width:87%;" type="text" name="REQUEST[RQS_ABOUT]" id="RQS_ABOUT" value="<?= $arrTxRqs['RQS_ABOUT'] ?>"  readonly>
              <! placeholder="PERMOHONAN PENGGUNAAN BAHAN TAMBAHAN PANGAN [Nama jenis BTP] ([No. INS]) PADA PRODUK [Jenis Pangan] ([No. Kategori Pangan])" -->
              <textarea class="resizable_textarea form-control" rows="3" id="RQS_ABOUT" style="width:87%;" name="REQUEST[RQS_ABOUT]" readonly><?= $arrTxRqs['RQS_ABOUT'] ?></textarea>
             </div>
          </div>
          <div class="form-group">
            <div id="note" class="col-md-6 col-sm-12 col-xs-12" style="display: none"></div>
          </div> 

          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                <div class="notification hidden mbot-0"><div></div></div>
                <input type="button" id="ya" value="Ya" class="btn btn-info" onclick="lanjut('ya');" style="display: none">
                <input type="button" id="tidak" value="Tidak" class="btn btn-danger" onclick="lanjut('tidak');" style="display: none">

                <!--<button type="button" class="btn btn-danger" id="reset" onclick="reset();"><i class="fa fa-eraser"></i> Reset</button>-->
                <button type="" class="btn btn-success" onclick="emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; if(!emailReg.test($('#RQS_PIC_MAIL').val()) ) { alert('Format Email Salah !'); }else{ cek_perkajinsus(); return false;}" id="simpan" >Lanjut <i class="fa fa-angle-double-right"></i></button>

              </div>
            </div>
         </div>
      </form>
    </div>
  <!-- /.row -->
  </div>
  <!-- /.container -->
 </section>
<script>
    function lanjut(val) {
      if(val== 'ya'){
        alert('Berdasarkan data yang Anda pilih, Anda tidak bisa melanjutkan permohonan. Silakan hubungi standard pangan!');
        location.reload();
      }else if(val== 'tidak'){
        save_post('#fsubmit');
      }
    }
    function cek_perkajinsus(PRODUCT_TYPE) {
        noisn = $("#noisn").val();
        RQS_FOOD_TYPE_ID = $("#RQS_FOOD_TYPE_ID").val();
        RQS_BTP_NETTO = $("#RQS_BTP_NETTO").val();
        RQS_BTP_UNIT = $("#RQS_BTP_UNIT").val();
        $.ajax({
            type: "POST",
            url: "<?= site_url() ?>submission/cek_perkajinsus",
            data: "noisn=" + noisn + "&food_type_id=" + RQS_FOOD_TYPE_ID + "&RQS_BTP_NETTO=" + RQS_BTP_NETTO+ "&RQS_BTP_UNIT=" + RQS_BTP_UNIT,
            beforeSend: function() {
              $("#myModal").modal({backdrop: "static"});
            },
            success: function (msg) {
              $("#myModal").modal('hide');
                msg2 = msg.split('|||');
                $("#RQS_ABOUT").val(msg2[2]);
                console.log(noisn+"="+RQS_FOOD_TYPE_ID+"="+msg2[2]);
                if (msg2[0] == 1) {
                  $("#ya").show();
                  $("#tidak").show();
                  $("#reset").hide();
                  $("#simpan").hide();
                  $("#note").show().html(msg2[3]);
                } else if (msg2[0] == 2) {
                    save_post('#fsubmit');
                } else if (msg2[0] == 0) {
                    alert(msg2[1]);
                }
            }
        });
        return false;
    }
    /*
     $('#fpost').submit(function(){
     //alert(tinyMCE.activeEditor.getContent());
     tinyMCE.triggerSave();
     save_post('#fpost');
     return false;
     });
     */
</script>

  <script src="<?=base_url()?>js/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
     function ubah(val){
        $('#'+val).removeAttr('readonly').focus();
     }


     function selectROW(obj, val){
      var iselect = $("#SELECT_ID").val();
      if ($(obj).hasClass('stdtable_selected_row') ) {
        $(obj).removeClass('stdtable_selected_row');
        $('#userid'+iselect).val('');
        $('#RQS_PIC_NAME'+iselect).val('');
        $('#RQS_PIC_TELP'+iselect).val('');
        $('#RQS_PIC_FAX'+iselect).val('');
        $('#RQS_PIC_MAIL'+iselect).val('');
        
      }else{
        $('.stdtable_selected_row').removeClass('stdtable_selected_row');
        $(obj).addClass('stdtable_selected_row');
        var arrval = val.split(' | ');
        var id     = arrval[0];
        var name   = arrval[1];
        var tlp    = arrval[2];
        var fax    = arrval[3];
        var email  = arrval[4];
        
        $('#userid'+iselect).val(id);
        $('#RQS_PIC_NAME'+iselect).val(name);
        $('#RQS_PIC_TELP'+iselect).val(tlp);
        $('#RQS_PIC_FAX'+iselect).val(fax);
        $('#RQS_PIC_MAIL'+iselect).val(email);
  
      }
    }

    function select_id(n){
     $("#SELECT_ID").val('');
     //$("#SELECT_ID").val(queue);
    }



  $(document).ready(function(e){
    $(".datepicker-input").datepicker({
      autoclose: true
    });
    
    $('.pass').change(function() {
      $('span.error-keyup-7').remove();
      var inputVal = $(this).val();
      var pass = $("#pass").val();
      if(pass != inputVal) {  
        $(this).after('<span style="color:red" class="error error-keyup-7">Password Tidak Sama</span>');
      }
    });
    $('.keyup-email').change(function() {
      $('span.error-keyup-7').remove();
      var inputVal = $(this).val();
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      if(!emailReg.test(inputVal)) {  
        $(this).after('<span style="color:red" class="error error-keyup-7">Format Email Salah</span>');
      }
    });
    
    $('.keyup-numeric').keyup(function() {
      $('span.error-keyup-1').remove();
      var inputVal = $(this).val();
      var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
      if(!numericReg.test(inputVal)) {  
        $(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
      }
    });
    
    $('.keyup-limit-6').keyup(function() {
      $('span.error-keyup-3').remove();
      var inputVal = $(this).val();
      var characterReg = /^([a-zA-Z0-9]{6,100})$/;
      if(!characterReg.test(inputVal)) {
        $(this).after('<span style="color:red" class="error error-keyup-3">Minimal 6 Karakter.</span>');
      }
    });

    $('.alfabet').keyup(function() {
      $('span.error-keyup-3').remove();
      var inputVal = $(this).val();
      var characterReg = /^([a-zA-Z])+$/;
      if(!characterReg.test(inputVal)) {
        $(this).val();
        $(this).after('<span style="color:red" class="error error-keyup-3">Hanya Alfabet.</span>');
      }
    });

  $('.chosen-select').on('chosen:no_results', function(evt, params) {
    alert('Jenis BTP Baru. Silakan hubungi standard pangan');
    do_something(evt, params);
  });
  $('.chosen-select-pangan').on('chosen:no_results', function(evt, params) {
    alert('Jenis Pangan Baru. Silakan hubungi standard pangan');
    do_something(evt, params);
  });

  });

var config = {
  '.chosen-select'           : {search_contains: true},
  '.chosen-select-pangan'    : {search_contains: true},
  '.chosen-select-deselect'  : { allow_single_deselect: true },
  '.chosen-select-no-single' : { disable_search_threshold: 10 },
  '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
  '.chosen-select-rtl'       : { rtl: true },
  '.chosen-select-width'     : { width: '95%' }
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>


<!-- //modal --
  <div id="src-pengguna" class="modal fade">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Data Pengguna</h4>
        </div>

        <div class="modal-body">
          <input type="hidden" name="SELECT_ID" id="SELECT_ID">
          <table id='tableData' class="table datatable-button-init-basic table-bordered table-striped table-hover" cellspacing="0" width="100%">
            <thead>
              <tr style="background-color: #99d6ff;">
                <th>No</th>
                <th>Nama</th>
                <th>No Telp</th>
                <th>No Fax</th>
                <th>Email</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody id="body-pengguna">
              <?php
                $b = count($rowData);
                $no = 1;
                if($b == 0){
              ?>
                <tr style="background: rgb(252, 208, 208) none repeat scroll 0% 0%; font-size:18px;"> <td colspan="3">Tidak ada data auditi</td>
                </tr>
                <?php
                  }else{
                    for($a=0; $a<$b; $a++){
                ?>
                <tr>
                  <td><?= $no;?></t2d>
                  <td><?= $rowData[$a]['USER_PIC_NAME'];?></td>
                  <td><?= $rowData[$a]['USER_PIC_TELP'];?></td>
                  <td><?= $rowData[$a]['USER_PIC_FAX'];?></td>
                  <td><?= $rowData[$a]['USER_PIC_MAIL'];?></td>
                  <td><button type="button"  onclick="selectROW(this, '<?= $rowData[$a]['USER_ID'];?> | <?= $rowData[$a]['USER_PIC_NAME']?> | <?= $rowData[$a]['USER_PIC_TELP'] ?> | <?= $rowData[$a]['USER_PIC_FAX']; ?> | <?= $rowData[$a]['USER_PIC_MAIL']; ?>')" class="btn btn-info btn-rounded btn-xs legitRipple" data-dismiss="modal"><i class="icon-select2"></i> Pilih </button></td>
                </tr>
              <?php
                  $no++;
                  }
                }
              ?>
            </tbody>
          </table>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal"> Simpan </button>
        </div>
      </div>
    </div>
  </div>
 //end modal-->
