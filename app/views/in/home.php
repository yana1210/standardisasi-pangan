<!DOCTYPE html>
<html lang="en">
  {_header_} 

  <body class="nav-md" ><!-- onload="startclock()" -->
    <div class="container body">
      <div class="main_container">
        {_menu_}

        <!-- top navigation -->
        <div class="top_nav">
          <?php
            if($this->newsession->userdata('ROLE_ID') == '601' ){
              $style = 'style="background-color: rgba(216,198,252,1)"';
            }else if($this->newsession->userdata('ROLE_ID') == '602' ){
              $style = 'style="background-color: rgba(198,252,211,1)"';
            }else if($this->newsession->userdata('ROLE_ID') == '603' ){
              $style = 'style="background-color: rgba(182,213,245,1)"';
            }else if($this->newsession->userdata('ROLE_ID') == '605' ){
              $style = 'style="background-color: rgba(237,252,198,1)"';
            }else if($this->newsession->userdata('ROLE_ID') == '606' ){
              $style = 'style="background-color: rgba(204,252,198,1)"';
            }else if($this->newsession->userdata('ROLE_ID') == '607' ){
              $style = 'style="background-color: rgba(198,222,252,1)"';
            }else{
              $style = 'style="background-color: rgba(220,198,252,1)"';
            }
          ?>
          <div class="nav_menu" <?= $style ?>>
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile "  aria-expanded="false">
                    <i class=""></i>
                    <span class="fa fa-sign-out" onclick="window.location = '<?= site_url(); ?>home/logout/<?= $this->newsession->userdata('USER_ID'); ?>';" style="font-weight: bold;">&nbsp; LOGOUT</span>
                  </a> 
                </li>
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >
                    Selamat Datang <b><?= $this->newsession->userdata('USER_NAME'); ?></b> |
                    
                  </a>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class=""><?php //print_r($this->newsession->userdata('MENU')) ?>
            {_content_}
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        {_footer_}
        <!-- /footer content -->
      </div>
    </div>
	
  </body>
</html>

<script type="text/javascript">
  $(document).ready(function(){
      setInterval(function(){
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url(); ?>/autocomplete/cek_sess/'+Math.random() ,
          data: {session:'<?php echo $this->newsession->userdata('LOGGED_IN'); ?>',user_id:'<?php echo $this->newsession->userdata('USER_ID'); ?>'},
          success: function(data){
            if(data!=1){
              alert("Session Login Anda Sudah Habis, Silahkan login ulang.");
              window.location = '<?php echo base_url(); ?>';
/*              
              bootbox.alert("Session Login Anda Sudah Habis, Silahkan login ulang.", function(data) {
                if(data)
                  window.location = '<?php echo base_url(); ?>';
              });   
*/              
            }
          }
        })
      }, 1800000);// 30 menit
  });
</script>
