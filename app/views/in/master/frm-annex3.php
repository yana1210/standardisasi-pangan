<link rel="stylesheet" href="<?=base_url()?>js/chosen.css">
<style>
   .red{
	   color:red;
   }
</style>

<section id="page-header" class="page-section">
	<hr>
</section>

<section id="page-content">
	<div class="container">
		<div class="row">
			<form class="form-horizontal form-label-left " action="<?= $act ?>" id="fAnnex" name="fAnnex" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?= $arrAnnex3['CODEX_ANNEX_THREE_ID'] ?>">
				<div class="col-md-12">
					<h6 class="text-uppercase">MANAGEMENT >> <span style="color: blue">DATA PENGECUALIAN CODEX THREE</span></h6>
					<hr>

					<div class="form-group">
			            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Jenis Pangan <span class="red"> * </span></label>
			            <div class="col-md-8 col-sm-8 col-xs-12">
			             <?= form_dropdown('ANNEX3[FOOD_TYPE_ID]', $foodtype, $arrAnnex3['FOOD_TYPE_ID'], 'style="width:87%;" class="text form-control chosen-select-pangan" style="" id="FOOD_TYPE_ID" required'); ?>
			            </div>
			        </div>
			        
					<!-- <div class="form-group">
			            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Fungsi BTP <span class="red"> * </span></label>
			            <div class="col-md-8 col-sm-8 col-xs-12">
			             <?= form_dropdown('CODEX3[BTP_FUNC_NAME]', $BTPFUNC, $arrCodex3['BTP_FUNC_NAME'], 'style="width:87%;" class="text form-control chosen-select-pangan" style="" id="BTP_FUNC_NAME" required'); ?>
			            </div>
			        </div>

			         <div class="form-group">
			            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Tahun <span class="red"> * </span></label>
			            <div class="col-md-8 col-sm-8 col-xs-12">
			              <input id="CODEX_YEAR" class="form-control" style="width:87%;" type="text" name="CODEX3[CODEX_YEAR]" value="<?= $arrCodex3['CODEX_YEAR'];?>" required>
			            </div>
			        </div> -->

				</div>
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
							<div class="notification hidden mbot-0"><div></div></div>
							<input type="button" value="Reset" class="btn btn-danger" onclick="reset();">
							<input type="button" value="Simpan" id="simpan" class="btn btn-success" onclick="save_post('#fAnnex'); return false;">
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>
<script src="<?=base_url()?>js/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
	function changeImage() {
		document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd="+Math.random();
	}
	function back(){
		document.location = '<?= site_url(); ?>';
	}

	$(document).ready(function(e){
		$(".datepicker-input").datepicker({
			autoclose: true
		});
		
		$('.keyup-numeric').keyup(function() {
			$('span.error-keyup-1').remove();
			var inputVal = $(this).val();
			var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
			if(!numericReg.test(inputVal)) {	
				$(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
			}
		});
			
		$('.keyup-limit-6').keyup(function() {
			$('span.error-keyup-3').remove();
			var inputVal = $(this).val();
			var characterReg = /^([a-zA-Z0-9]{6,100})$/;
			if(!characterReg.test(inputVal)) {
				$(this).after('<span style="color:red" class="error error-keyup-3">Minimal 6 Karakter.</span>');
			}
		});
	});

	var config = {
	  '.chosen-select'           : {search_contains: true},
	  '.chosen-select-pangan'    : {search_contains: true},
	  '.chosen-select-deselect'  : { allow_single_deselect: true },
	  '.chosen-select-no-single' : { disable_search_threshold: 10 },
	  '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
	  '.chosen-select-rtl'       : { rtl: true },
	  '.chosen-select-width'     : { width: '95%' }
	}
	
	for (var selector in config) {
	  $(selector).chosen(config[selector]);
	}

</script>
