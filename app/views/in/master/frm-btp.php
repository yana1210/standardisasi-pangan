<style>
   .red{
	   color:red;
   }
</style>

<section id="page-header" class="page-section">
	<hr>
</section>

<section id="page-content">
	<div class="container">
		<div class="row">
			<form class="form-horizontal form-label-left " action="<?= $act ?>" id="frmBTP" name="frmBTP" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?= $arrBTP['BTP_TYPE_ID'] ?>">
				<div class="col-md-12">
					<h6 class="text-uppercase">MANAGEMENT >> <span style="color: blue">DATA JENIS BTP</span></h6>
					<hr>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">No. INS<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text" required class="form-control" name="BTP[BTP_TYPE_NO]" value="<?= $arrBTP['BTP_TYPE_NO'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Jenis BTP<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text" required class="form-control" name="BTP[BTP_TYPE_NAME]" value="<?= $arrBTP['BTP_TYPE_NAME'] ?>">
						</div>
					</div>

				</div>
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
							<div class="notification hidden mbot-0"><div></div></div>
							<input type="button" value="Reset" class="btn btn-danger" onclick="reset();">
							<input type="button" value="Simpan" id="simpan" class="btn btn-success" onclick="save_post('#frmBTP'); return false;">
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>
<script type="text/javascript">
	function changeImage() {
		document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd="+Math.random();
	}
	function back(){
		document.location = '<?= site_url(); ?>';
	}

	$(document).ready(function(e){
		$(".datepicker-input").datepicker({
			autoclose: true
		});
			
		$('.keyup-numeric').keyup(function() {
			$('span.error-keyup-1').remove();
			var inputVal = $(this).val();
			var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
			if(!numericReg.test(inputVal)) {	
				$(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
			}
		});
			
		$('.keyup-limit-6').keyup(function() {
			$('span.error-keyup-3').remove();
			var inputVal = $(this).val();
			var characterReg = /^([a-zA-Z0-9]{6,100})$/;
			if(!characterReg.test(inputVal)) {
				$(this).after('<span style="color:red" class="error error-keyup-3">Minimal 6 Karakter.</span>');
			}
		});
	});

</script>
