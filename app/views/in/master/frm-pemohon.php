<style>
   .red{
	   color:red;
   }
</style>
<script>
    $(document).ready(function () {
        USER_TYPE_ID = "<?= $arrUser['USER_TYPE_ID']; ?>";
        if (USER_TYPE_ID == 'UTY01') {
            $('.UTY01').show(500);
            $('.UTY02').attr('disabled', 'disabled');
            $('.UTY02').hide();
        } else if(USER_TYPE_ID == 'UTY02'){
            $('.UTY02').show(500).removeAttr('disabled');
            $('.UTY01').attr('disabled', 'disabled');
            $('.UTY01').hide();
        }
        USER_PRSN_IDTYPE = "<?= $arrUser['USER_PRSN_IDTYPE']; ?>";
        if (USER_PRSN_IDTYPE == 'IDT04') {
            $('.lain').removeAttr('disabled').attr('required', 'required');
            $('.etc').show();
            $('#USER_PRSN_IDNO').attr('disabled', 'disabled').hide();
        } else {
            $('.etc').hide();
            $('.lain').attr('disabled', 'disabled');
            $('#USER_PRSN_IDNO').removeAttr('disabled').attr('required', 'required').show();
        }

    });
	function chkkat(kat) {
		if(kat == 'UTY01'){
			$('.UTY02').hide().attr('disabled', 'disabled').removeAttr('required').val('');
		}else if(kat == 'UTY02'){
			$('.UTY01,.lain,.etc').hide().attr('disabled', 'disabled').removeAttr('required').val('');
		}
		$('.'+kat).show(500).removeAttr('disabled');
	}
	function idType(id) {
        if (id == 'IDT04') {
            $('.lain').removeAttr('disabled').attr('required', 'required');
            $('.etc').show();
            $('#USER_PRSN_IDNO').attr('disabled', 'disabled').hide();
        } else {
            $('.etc').hide();
            $('.lain').attr('disabled', 'disabled');
            $('#USER_PRSN_IDNO').removeAttr('disabled').attr('required', 'required').show();
        }
	}

</script>
<section id="page-header" class="page-section">
	<hr>
</section>

<section id="page-content">
	<div class="container">
		<div class="row">
			<form class="form-horizontal form-label-left " action="<?= $act ?>" id="fpemohon" name="fpemohon" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?= $arrUser['USER_ID'] ?>">
				<div class="col-md-12">
					<h6 class="text-uppercase">MANAGEMENT >> <span style="color: blue">DATA PEMOHON</span></h6>
					<hr>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Pemohon<span class="red"> *</span></label>
						<div class="col-md-2 col-sm-6 col-xs-9">
	                        <?= form_dropdown('REG[USER_TYPE_ID]', $USER_TYPE, $arrUser['USER_TYPE_ID'], ' class="text form-control" style="min-width:60px" onchange="chkkat($(this).val());" required '); ?>
						</div>
					</div>

					<div class="form-group UTY01" style="display:none">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Pemohon<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text" required class="form-control UTY01" name="REG[USER_NAME]" value="<?= $arrUser['USER_NAME'] ?>">
						</div>
					</div>

					<div class="form-group UTY01" style="display:none">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">No Identitas Diri<span class="red"> *</span></label>
						<div class="col-md-2 col-sm-3 col-xs-5">
							<?= form_dropdown('REGPRSN[USER_PRSN_IDTYPE]', $ID_TYPE, $arrUser['USER_PRSN_IDTYPE'], ' class="text form-control UTY01" style="" onchange="idType($(this).val())"  '); ?>
						</div>
						<div class="col-md-4 col-sm-3 col-xs-4">
							<input type="text" id="USER_PRSN_IDNO"  class="form-control keyup-numeric UTY01" name="REGPRSN[USER_PRSN_IDNO]" value="<?= $arrUser['USER_PRSN_IDNO'] ?>" placeholder="No Identitas Diri">
						</div>
					</div>

					<div class="form-group etc" style="display:none">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
						<div class="col-md-2 col-sm-3 col-xs-5">
							<input type="text" class="form-control lain" placeholder="Jenis Identitas Diri" name="REGPRSN[USER_TYPE_ETC]" value="<?= $arrUser['USER_TYPE_ETC'] ?>" disabled>
						</div>
						<div class="col-md-4 col-sm-3 col-xs-4">
							<input type="text" class="form-control keyup-numeric lain" name="REGPRSN[USER_PRSN_IDNO]" value="<?= $arrUser['USER_PRSN_IDNO'] ?>" disabled placeholder="No Identitas Diri">
						</div>
					</div>


					<div class="form-group UTY02" style="display:none">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Perusahaan<span class="red"> *</span></label>
						<div class="col-md-1 col-sm-1 col-xs-1">
							<?= form_dropdown('REGCOM[TRADER_TYPE]', array('PT' => 'PT', 'CV' => 'CV', 'UD' => 'UD', 'PJ' => 'PJ', 'FA' => 'FA'), $arrUser['TRADER_TYPE'], 'style="min-width:60px" class="form-control"'); ?> 			
						</div>
						<div class="col-md-5 col-sm-5 col-xs-8">
							<input type="text" required class="form-control UTY02" name="REG[USER_NAME]" value="<?= $arrUser['USER_NAME'] ?>" >
						</div>
					</div>
					<div class="form-group UTY02" style="display:none">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Status Perusahaan<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
	                        <?= form_dropdown('REGCOM[USER_COM_STATUS_ID]', $COMPANY_STATUS, $arrUser['USER_COM_STATUS_ID'], ' class="text form-control UTY02" style="min-width:60px" onchange="" required '); ?>
						</div>
					</div>
					<div class="form-group UTY02" style="display:none">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Skala Industri<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
	                        <?= form_dropdown('REGCOM[USER_COM_SCALE_ID]', $COMPANY_SCALE, $arrUser['USER_COM_SCALE_ID'], ' class="text form-control UTY02" style="min-width:60px" onchange="" required '); ?>
						</div>
					</div>
					<div class="form-group UTY02" style="display:none">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Pimpinan<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text" required class="form-control UTY02" name="REGCOM[USER_COM_LDR_NAME]" value="<?= $arrUser['USER_COM_LDR_NAME'] ?>">
						</div>
					</div>
					<div class="form-group UTY02" style="display:none">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Pimpinan Teknis<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text" required class="form-control UTY02" name="REGCOM[USER_COM_TLDR_NAME]" value="<?= $arrUser['USER_COM_TLDR_NAME'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">NPWP<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text" required class="form-control  keyup-numeric telp" name="REG[USER_NPWP]" value="<?= $arrUser['USER_NPWP'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat <span class="red"> *</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<textarea required class="form-control" name="REG[USER_ADDR]" rows="3" placeholder='Alamat'> <?= $arrUser['USER_ADDR'] ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Propinsi<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<?= form_dropdown('REG[REGION_ID]', $PROVINCE, substr($arrUser['REGION_ID'], 0, 2)."00", ' class="text form-control" style="" onchange="setnextcb($(this));" url="'.site_url().'autocomplete/get_arr_city/" id="province" required'); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Kabupaten/Kota<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<?= form_dropdown('REG[REGION_ID]', $CITY, $arrUser['REGION_ID'], ' class="text form-control" id="city" style="" required'); ?> 
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Kode Pos<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text" required class="form-control keyup-numeric telp" maxlength="5" name="REG[USER_ZIP]"  value="<?= $arrUser['USER_ZIP'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">No Telp<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text" required class="form-control keyup-numeric telp" name="REG[USER_TELP]" maxlength="16" value="<?= $arrUser['USER_TELP'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">No Fax</label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text"  class="form-control keyup-numeric telp" name="REG[USER_FAX]" maxlength="16" value="<?= $arrUser['USER_FAX'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Email Perusahaan<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<input type="text" required id="mail" class="form-control keyup-email" data-parsley-trigger="change" name="REG[USER_MAIL]"  value="<?= $arrUser['USER_MAIL'] ?>" >
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Status User<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<?= form_dropdown('REG[USER_STATUS_ID]', $USER_STATUS_ID, $arrUser['USER_STATUS_ID'], 'style="width:87%;" class="text form-control "'); ?>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
							<div class="notification hidden mbot-0"><div></div></div>
							<input type="button" value="Reset" class="btn btn-danger" onclick="reset();">
							<!--<a href="<?= site_url(); ?>register/new_reg/frm-reg-pic"  class="btn btn-success">Lanjut</a>-->
							<input type="button" value="Simpan" id="simpan" class="btn btn-success" onclick="emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; if(!emailReg.test($('#mail').val()) ) { alert('Format Email Salah !'); }else{ save_post('#fpemohon'); return false;}">
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>
<script type="text/javascript">
	function changeImage() {
		document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd="+Math.random();
	}
	function back(){
		document.location = '<?= site_url(); ?>';
	}
	$(document).ready(function(e){
		$(".datepicker-input").datepicker({
			autoclose: true
		});
		
/*		
		$(".npwp").mask("99.999.999.9-999.999");
		$('#FAC_ZIP').numeric();
		$('#FAC_TELP').numeric();
		$('#FAC_FACTORY_ZIP').numeric();
		$('#FAC_FACTORY_TELP').numeric();
		$('#FAC_PIC_TELP').numeric();
		$('#USER_PHONE').numeric();
		*/		
		$('.pass').change(function() {
			$('span.error-keyup-7').remove();
			var inputVal = $(this).val();
			var pass = $("#pass").val();
			if(pass != inputVal) {	
				$(this).after('<span style="color:red" class="error error-keyup-7">Password Tidak Sama</span>');
			}
		});
		$('.keyup-email').change(function() {
			$('span.error-keyup-7').remove();
			var inputVal = $(this).val();
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if(!emailReg.test(inputVal)) {	
				$(this).after('<span style="color:red" class="error error-keyup-7">Format Email Salah</span>');
			}else{
	            $.ajax({
	                type: "POST",
	                url: "<?= site_url() ?>register/email_exists_check",
	                data: "email=" + inputVal,
	                success: function (msg) {
	                    msg2 = msg.split('|||');
	                    if(msg2[0] == 0){
//	                    	alert(msg2[1]);
	                    }else if(msg2[0] == 1){
	                    	alert(msg2[1]);
	                        $("#mail").val('');
	                    }else{                  
	                    }
	                }
	            });
			}
		});
		
		$('.keyup-numeric').keyup(function() {
			$('span.error-keyup-1').remove();
			var inputVal = $(this).val();
			var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
			if(!numericReg.test(inputVal)) {	
				$(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
			}
		});
		
		$('.keyup-limit-6').keyup(function() {
			$('span.error-keyup-3').remove();
			var inputVal = $(this).val();
			var characterReg = /^([a-zA-Z0-9]{6,100})$/;
			if(!characterReg.test(inputVal)) {
				$(this).after('<span style="color:red" class="error error-keyup-3">Minimal 6 Karakter.</span>');
			}
		});

	});
</script>
