<link rel="stylesheet" href="<?=base_url()?>js/chosen.css">
<style>
   .red{
	   color:red;
   }
</style>

<section id="page-header" class="page-section">
	<hr>
</section>

<section id="page-content">
	<div class="container">
		<div class="row">
			<form class="form-horizontal form-label-left " action="<?= $act ?>" id="fPerka" name="fPerka" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?= $arrPerka['PERKAJINSUS_ID'] ?>">
				<div class="col-md-12">
					<h6 class="text-uppercase">MANAGEMENT >> <span style="color: blue">DATA PERKA DAN IZIN KHUSUS</span></h6>
					<hr>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">No. INS/Nama Jenis BTP<span class="red"> *</span></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<?= form_dropdown('PERKA[BTP_TYPE_ID]', $BTP, $arrPerka['BTP_TYPE_ID'], 'style="width:87%;" class="text form-control chosen-select" style="" id="BTP_TYPE_ID" required'); ?>
						</div>
					</div>

					<div class="form-group">
			            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No. Kategori Pangan/Jenis Pangan <span class="red"> * </span></label>
			            <div class="col-md-8 col-sm-8 col-xs-12">
			             <?= form_dropdown('PERKA[FOOD_TYPE_ID]', $foodtype, $arrPerka['FOOD_TYPE_ID'], 'style="width:87%;" class="text form-control chosen-select-pangan" style="" onchange="setnextfoodtype2(this.value,false);" url="'.site_url().'autocomplete/get_arr_btnfoodtype2/" id="id" required'); ?>
			            </div>
			        </div>

			        <div class="form-group">
			            <label class="control-label col-md-3 col-sm-3 col-xs-3"> Jumlah Penggunaan BTP dalam Produk Pangan <span class="red"> * </span></label>
			            <div class="col-md-1 col-sm-2 col-xs-2 ">
			              <input class="form-control" type="text" id="MAX_LIMIT"  name="PERKA[MAX_LIMIT]"  value="<?= $arrPerka['MAX_LIMIT']?>" required> 
			            </div>
			            <div class="col-md-2 col-sm-2 col-xs-2 ">
			              <?= form_dropdown('PERKA[UNIT]', $DENOMINATION, $arrPerka['UNIT'], ' id="UNIT" style="min-width:60px" class="form-control" required'); ?>
			            </div>
			        </div> 

			        <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Catatan<span class="red"> *</span></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<textarea class="resizable_textarea form-control" rows="3" placeholder='' style="width:87%;" name="PERKA[NOTE]" required><?= $arrPerka['NOTE'];?></textarea>
						</div>
					</div>
 
				</div>
			
				<br/><br/><br/>
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
							<div class="notification hidden mbot-0"><div></div></div>
							<input type="button" value="Reset" class="btn btn-danger" onclick="reset();">
							<input type="button" value="Simpan" id="simpan" class="btn btn-success" onclick="save_post('#fPerka'); return false;">
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>
<script src="<?=base_url()?>js/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
	function changeImage() {
		document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd="+Math.random();
	}

	function back(){
		document.location = '<?= site_url(); ?>';
	}

	$(document).ready(function(e){
		$(".datepicker-input").datepicker({
			autoclose: true
		});
		
		$('.keyup-numeric').keyup(function() {
			$('span.error-keyup-1').remove();
			var inputVal = $(this).val();
			var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
			if(!numericReg.test(inputVal)) {	
				$(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
			}
		});
		
		$('.keyup-limit-6').keyup(function() {
			$('span.error-keyup-3').remove();
			var inputVal = $(this).val();
			var characterReg = /^([a-zA-Z0-9]{6,100})$/;
			if(!characterReg.test(inputVal)) {
				$(this).after('<span style="color:red" class="error error-keyup-3">Minimal 6 Karakter.</span>');
			}
		});

		$('.chosen-select').on('chosen:no_results', function(evt, params) {
	      alert('Jenis BTP Baru. Silakan hubungi standard pangan');
	      do_something(evt, params);
	    });

	    $('.chosen-select-pangan').on('chosen:no_results', function(evt, params) {
	      alert('Jenis Pangan Baru. Silakan hubungi standard pangan');
	      do_something(evt, params);
	    });

	});

	var config = {
	  '.chosen-select'           : {search_contains: true},
	  '.chosen-select-pangan'    : {search_contains: true},
	  '.chosen-select-deselect'  : { allow_single_deselect: true },
	  '.chosen-select-no-single' : { disable_search_threshold: 10 },
	  '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
	  '.chosen-select-rtl'       : { rtl: true },
	  '.chosen-select-width'     : { width: '95%' }
	}
	
	for (var selector in config) {
	  $(selector).chosen(config[selector]);
	}
</script>
