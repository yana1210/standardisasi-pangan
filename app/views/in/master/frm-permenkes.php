<link rel="stylesheet" href="<?=base_url()?>js/chosen.css">
<style>
   .red{
	   color:red;
   }
</style>

<section id="page-header" class="page-section">
	<hr>
</section>

<section id="page-content">
	<div class="container">
		<div class="row">
			<form class="form-horizontal form-label-left " action="<?= $act ?>" id="fpermenkes" name="fpermenkes" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?= $arrPMKS['PERMENKES_ID'] ?>">
				<div class="col-md-12">
					<h6 class="text-uppercase">MANAGEMENT >> <span style="color: blue">DATA PERMENKES</span></h6>
					<hr>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">No. INS/Nama Jenis BTP<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<?= form_dropdown('PMKS[BTP_TYPE_ID]', $BTP, $arrPMKS['BTP_TYPE_ID'], 'style="width:87%;" class="text form-control chosen-select" style="" onchange="setnextfoodtype(this.value,false);" url="'.site_url().'autocomplete/get_arr_btnfoodtype/" id="RQS_FOOD_TYPE_ID" required'); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Fungsi BTP<span class="red"> *</span></label>
						<div class="col-md-6 col-sm-6 col-xs-9">
							<?= form_dropdown('PMKS[BTP_FUNCTION_ID]', $BTPFUNC, $arrPMKS['BTP_FUNCTION_ID'], 'style="width:87%;" class="text form-control chosen-select-pangan" style="" onchange="setnextfoodtype(this.value,false);" url="'.site_url().'autocomplete/get_arr_btnfoodtype/" id="RQS_FOOD_TYPE_ID" required'); ?>
						</div>
					</div>

				</div>
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
							<div class="notification hidden mbot-0"><div></div></div>
							<input type="button" value="Reset" class="btn btn-danger" onclick="reset();">
							<input type="button" value="Simpan" id="simpan" class="btn btn-success" onclick="save_post('#fpermenkes'); return false;">
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>

<script src="<?=base_url()?>js/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
	function changeImage() {
		document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd="+Math.random();
	}

	function back(){
		document.location = '<?= site_url(); ?>';
	}

	$(document).ready(function(e){
		$(".datepicker-input").datepicker({
			autoclose: true
		});
		
		$('.keyup-numeric').keyup(function() {
			$('span.error-keyup-1').remove();
			var inputVal = $(this).val();
			var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
			if(!numericReg.test(inputVal)) {	
				$(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
			}
		});
		
		$('.keyup-limit-6').keyup(function() {
			$('span.error-keyup-3').remove();
			var inputVal = $(this).val();
			var characterReg = /^([a-zA-Z0-9]{6,100})$/;
			if(!characterReg.test(inputVal)) {
				$(this).after('<span style="color:red" class="error error-keyup-3">Minimal 6 Karakter.</span>');
			}
		});

		$('.chosen-select').on('chosen:no_results', function(evt, params) {
	      alert('Jenis BTP Baru. Silakan hubungi standard pangan');
	      do_something(evt, params);
	    });

	    $('.chosen-select-pangan').on('chosen:no_results', function(evt, params) {
	      alert('Jenis Pangan Baru. Silakan hubungi standard pangan');
	      do_something(evt, params);
	    });

	});

	var config = {
	  '.chosen-select'           : {search_contains: true},
	  '.chosen-select-pangan'    : {search_contains: true},
	  '.chosen-select-deselect'  : { allow_single_deselect: true },
	  '.chosen-select-no-single' : { disable_search_threshold: 10 },
	  '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
	  '.chosen-select-rtl'       : { rtl: true },
	  '.chosen-select-width'     : { width: '95%' }
	}
	
	for (var selector in config) {
	  $(selector).chosen(config[selector]);
	}
</script>
