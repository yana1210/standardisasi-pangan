<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>e-Standardisasi Pangan</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="Standardisasi Pangan">
    <meta name="author" content="yana">
    <link rel="shortcut icon" href="<?=base_url()?>img/logo.png">
    <!-- <link rel="manifest" href="manifest.json"> -->
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#27ae60">
    <meta name="theme-color" content="#ffffff">
	
    <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">-->
    <!-- Bootstrap -->
    <link href="<?=base_url()?>css/in/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url()?>css/in/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?=base_url()?>css/in/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?=base_url()?>css/in/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="<?=base_url()?>css/in/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?=base_url()?>css/in/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?=base_url()?>css/in/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="<?=base_url()?>css/in/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?=base_url()?>css/in/daterangepicker.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?=base_url()?>css/in/custom.min.css" rel="stylesheet">


    <link rel="stylesheet" href="<?=base_url()?>css/main.css">
    <script src="<?=base_url()?>js/in/jquery.min.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>js/jquery-upload/css/jquery.fileupload.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>css/newtable-green.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>css/datepicker.css" type="text/css" />
    <script type="text/javascript" src="<?= base_url(); ?>js/clock.js" type="text/javascript"></script>

    <script src="<?=base_url()?>js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css"></script>

<script>

var detik = 1;  
function hitung(){
    var to = setTimeout(hitung,1000);
    detik --;
    if(detik < 0){
        clearTimeout(to);
        detik = 1;
        $(".alert").fadeOut("slow");
    }
}

</script>
    <style type="text/css">
   .red{
       color:red;
   }
.loading_div{
    text-align:center;
    background-color: white;
}
.bb{
    background: url(<?=base_url(); ?>img/trans.png);
}
    </style>

  </head>
  <!-- Modal -->
  <div class="modal fade " id="myModal" role="dialog" >
    <div class="modal-dialog modal-sm " style="background-color: #bababa">
    
      <!-- Modal content-->
      <div class="modal-content " data-backdrop="false" style="background-color: #bababa">
        <!--<div class="modal-header" data-backdrop="false">
          <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>-->
        <div class="modal-body " data-backdrop="false" style="background-color: #bababa">
          <div class='loading_div'><img src='<?=base_url(); ?>img/ajax-loader.gif'></div>
        </div>
        <!--<div class="modal-footer" data-backdrop="false">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
