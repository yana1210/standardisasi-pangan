<script>
var site_url = '<?= site_url(); ?>';
</script>
<style>
  .red{
    color:red;
  }
  #scroll {
    border: 1px solid black;
    height: 400px;
    overflow: scroll;
  }
</style>
<section id="page-header" class="page-section">
  <hr>
</section>

<?php //print_r($_SESSION) ; echo $this->newsession->userdata('USER_TYPE_ID'); ?>
<section id="page-content">
  <div class="container">
    <div class="row">
      <form class="form-horizontal form-label-left " action="<?= site_url('assessment/post/calc-req') ?>" id="fsubmit" name="fsubmit" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $RQS['RQS_ID'] ?>">
        <input type="hidden" name="menu" id="menu" >
        <input type="hidden" name="TX_EXPOSURE[EXPOSURE_AMOUNT]" id="EXPOSURE_AMOUNT" >
        <div class="col-md-12">
          <h6 class="text-uppercase">ASSESSMENT >> ASSIGNMENT >> DAFTAR ASSIGNMENT >> DETAIL ASSIGNMENT >> <span style="color: blue">HITUNG PAPARAN </span> </h6>
          <hr>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3"> Berat Badan (kg)</label>
            <div class="col-md-9 col-sm-9 col-xs-9 form-group">
              <input type="text" id="EXPOSURE_WEIGHT" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_WEIGHT]" required value="<?= $RQS['EXPOSURE_WEIGHT'] ?>">
            </div>
          </div> 
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3"> Asumsi Konsumsi (kg/org/hr)</label>
            <div class="col-md-9 col-sm-9 col-xs-9 form-group">
              <input type="text" id="EXPOSURE_CONSUMPTION" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_CONSUMPTION]" required value="<?= $RQS['EXPOSURE_CONSUMPTION'] ?>">
            </div>
          </div> 
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3"> ADI/PTWI/MTDI (mg/kg BB)</label>
            <div class="col-md-9 col-sm-9 col-xs-9 form-group">
              <input type="text" id="EXPOSURE_ADI" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_ADI]" required value="<?= $RQS['EXPOSURE_ADI'] ?>">
            </div>
          </div> 
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3"> Jumlah Penggunaan/Batas Maksimum (mg/kg)</label>
            <div class="col-md-9 col-sm-9 col-xs-9 form-group">
              <input type="text" id="EXPOSURE_MAX_USAGE" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_MAX_USAGE]" required value="<?= $RQS['EXPOSURE_MAX_USAGE'] ?>">
            </div>
          </div> 
          <br/>
          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                <input type="button" value="Hitung" class="btn btn-success " onclick="hitung();" >
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                <span id='amount'></span>
              </div>
            </div>
          </div>

          <br/> 

          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                <div class="notification hidden mbot-0"><div></div></div>
                <input type="button" value="Simpan" class="btn btn-success " onclick="validate('simpan');" >
                <input type="button" value="Buat Draft Rekomendasi" class="btn btn-info" onclick="validate('draft_rekom');" >
              </div>
            </div>
          </div>


        </div>
      </form>
    </div>
  <!-- /.row -->
  </div>
  <!-- /.container -->
 </section>
<script>
  function validate(menu){
      var menu = $('#menu').val(menu);
      save_post('#fsubmit');
      return false;     
  }
  
  function hitung(){
    EXPOSURE_WEIGHT = $("#EXPOSURE_WEIGHT").val();
    EXPOSURE_CONSUMPTION = $("#EXPOSURE_CONSUMPTION").val();
    EXPOSURE_ADI = $("#EXPOSURE_ADI").val();
    EXPOSURE_MAX_USAGE = $("#EXPOSURE_MAX_USAGE").val();

    amount = (EXPOSURE_MAX_USAGE*EXPOSURE_CONSUMPTION) / (EXPOSURE_ADI*EXPOSURE_WEIGHT);
      //amount = 3.12346;
     amount2 = amount.toString().split(".");
     if(typeof amount2[1] !== 'undefined'){ //alert(amount2[1]);
       amount3 = amount2[1].substr(4,1);
       if(amount3 == 5){
           amount4 = amount2[1].substr(0,4);
          amount = amount2[0]+"."+amount4;
        }else{
          amount = amount.toFixed(4);
        }
     }else{
        amount = amount.toFixed(4);
     }
     $("#amount").html("Paparan = ("+EXPOSURE_MAX_USAGE+" x "+EXPOSURE_CONSUMPTION+")/("+EXPOSURE_ADI+" x "+EXPOSURE_WEIGHT+") = "+amount); //+"=="+amount2[1]+"--"+amount3
     $("#EXPOSURE_AMOUNT").val(amount);

  }

  $(document).ready(function(e){
    $('.keyup-numeric').keyup(function() {
      $('span.error-keyup-1').remove();
      var inputVal = $(this).val();
      var numericReg = /^-{0,1}\d*\.{0,1}\d+$/;
      if(!numericReg.test(inputVal) || inputVal == 0) {  
        //$(this).val('');
        $(this).after('<span style="color:red" class="error error-keyup-1">Input harus bilangan asli atau bilangan desimal, contoh : 1.2 </span>');
      }
    });
  });

</script>
