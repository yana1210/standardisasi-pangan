<script>
var site_url = '<?= site_url(); ?>';
</script>
<style>
  .red{
    color:red;
  }
  #scroll {
    border: 1px solid black;
    height: 400px;
    overflow: scroll;
  }
</style>
<section id="page-header" class="page-section">
  <hr>
</section>

<section id="page-content">
  <div class="container">
    <div class="row">
      <form class="form-horizontal form-label-left " action="<?= site_url('assessment/post/letter_number') ?>" id="fsubmit" name="fsubmit" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $RQS['RQS_ID'] ?>">
        <input type="hidden" name="RQS_ABOUT" value="<?= $RQS['RQS_ABOUT'] ?>">
        <input type="hidden" name="menu" id="menu" >
        <div class="col-md-12">
          <h6 class="text-uppercase">ASSESSMENT >> <span style="color: blue">INPUT NOMOR SURAT </span> </h6>
          <hr>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3"> Nomor Surat</label>
            <div class="col-md-9 col-sm-9 col-xs-9 form-group">
              <input  class="form-control"  type="text" name="RQS_LETTER_NUMBER" value="<?= $RQS['RQS_LETTER_NUMBER'] ?>" required>
            </div>
          </div> 
          <br/>


          <br/> 

          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                <div class="notification hidden mbot-0"><div></div></div>
                <a href="<?= site_url(); ?>assessment/list_assignment/ass"  class="btn " style="background-color: yellow" ><i class="fa fa-angle-double-left"></i> Kembali</a>
                <button type="button" class="btn btn-info" onclick="validate('simpan');"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </div>
          </div>


        </div>
      </form>
    </div>
  <!-- /.row -->
  </div>
  <!-- /.container -->
 </section>
<script>
  function validate(menu){
      var menu = $('#menu').val(menu);
      save_post('#fsubmit');
      return false;     
  }
  
</script>