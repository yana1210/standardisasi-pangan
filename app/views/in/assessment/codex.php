<script>
var site_url = '<?= site_url(); ?>';

  function addberat() { 
    var countTX_RQS_BTP_COMP = $(".TX_RQS_BTP_COMP").length;
    if(countTX_RQS_BTP_COMP == 10){
      alert("Batas Maksimal Komposisi BTP adalah 10");
    }else{
      var nom = $('.addberat').attr('terakhir');
      var idtr = $('.addberat').attr('periksa');
      var cls = idtr + nom;
      $('#hideme').before('<div class="TX_RQS_BTP_COMP form-group row ' + cls + '"><div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-3 col-sm-offset-3 col-xs-offset-3"><input type="hidden" name="TX_RQS_BTP_COMP['+nom+'][RQS_ID]" value="<?= $RQS['RQS_ID'] ?>"><input required class="form-control" type="text" name="TX_RQS_BTP_COMP['+nom+'][XPRT_REC_NOTE]" id="" value=""></div><div class="col-md-3 col-sm-3 col-xs-3"><button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple min_" onClick="$(\'.' + cls + '\').remove();return false;"><i class="fa fa-trash"></i> Hapus</button></div></div>');
      $('.addberat').attr('terakhir', parseInt(nom) + 1);
    }
    return false;

  }

</script>
<style>
  .red{
    color:red;
  }
  #scroll {
    border: 1px solid black;
    height: 400px;
    overflow: scroll;
  }
</style>
<section id="page-header" class="page-section">
  <hr>
</section>

<?php 
$user_id = $RQS['USER_ID'];
$rqs_id = $RQS['RQS_ID']; 
//print_r($_SESSION) ; echo $this->newsession->userdata('USER_TYPE_ID'); ?>
<section id="page-content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h6 class="text-uppercase">ASSESSMENT >> ASSIGNMENT >> DAFTAR ASSIGNMENT >> DETAIL ASSIGNMENT >> CODEX ASSESSMENT </h6>
        <hr>
        <div class="x_panel">
          <div class="x_content">
            <div class="" style="font-size: 14px">
              <label class=" col-md-3 col-sm-3 col-xs-3"> Result : </label>
              <?= $result ?>           
            </div>
          </div>
        </div>
        <br>
        <br>
      </div>

          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                <?= $button ?>
              </div>
            </div>
            <?php
            if($hasil == 'hitung'){
            ?>
              <!--<form class="form-horizontal form-label-left " action="<?= site_url('assessment/post/calc-req') ?>" id="fsubmit" name="fsubmit" enctype="multipart/form-data">-->
              <div class="x_panel">
                <div class="x_content">
                  <div class="col-md-12">

                    <input type="hidden" name="id" value="<?= $RQS['RQS_ID'] ?>">
                    <input type="hidden" name="menu" id="menu" >
                    <input type="hidden" name="TX_EXPOSURE[EXPOSURE_AMOUNT]" id="EXPOSURE_AMOUNT" >
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Berat Badan (kg)</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input type="text" id="EXPOSURE_WEIGHT" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_WEIGHT]" required value="<?= $RQS['EXPOSURE_WEIGHT'] ?>">
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Asumsi Konsumsi (kg/org/hr)</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input type="text" id="EXPOSURE_CONSUMPTION" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_CONSUMPTION]" required value="<?= $RQS['EXPOSURE_CONSUMPTION'] ?>">
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> ADI/PTWI/MTDI (mg/kg BB)</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input type="text" id="EXPOSURE_ADI" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_ADI]" required value="<?= $RQS['EXPOSURE_ADI'] ?>">
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Jumlah Penggunaan/Batas Maksimum (mg/kg)</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input type="text" id="EXPOSURE_MAX_USAGE" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_MAX_USAGE]" required value="<?= $RQS['EXPOSURE_MAX_USAGE'] ?>">
                      </div>
                    </div> 
                    <br/>
                    <br/>
                    <br/>
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                          <span style="color: blue; font-weight: bold; font-size: 14px" id='amount'></span>
                        </div>
                      </div>
                    </div>
                    <br/>
                    <br/>
                    <br/>

                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                          <button type="" class="btn btn-success" onclick="hitung();"><i class="fa fa-calculator"></i> Hitung</button>
                        </div>
                      </div>
                    </div>
                    <br/> 
                    <br/>
                    <br/>
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                          <div class="notification hidden mbot-0"><div></div></div>
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-eye"></i> Lihat Tabel Asumsi Konsumsi</button>
                          <!--<button type="" class="btn btn-success" onclick="validate('draft_rekom');"><i class="fa fa-save"></i> Simpan Hasil Hitung</button>-->
                          <!--<a href="<?= site_url(); ?>cetak/nodin/<?= $RQS['RQS_ID'] ?>"  class="btn  btn-info" ><i class="fa fa-download"></i> Download Nota Dinas & Surat Jawaban</a>-->
                          <a href="<?= site_url(); ?>assessment/ass/form/<?= $RQS['RQS_ID'] ?>"  class="btn  btn-info" ><i class="fa fa-edit"></i> Buat Nodin</a>
                        </div>
                      </div>
                    </div>


                  </div>
                </div>
              </div>
              <!--</form>-->

            <?php
            }else if($hasil == 'bawa pakar'){              
            ?>
              <form class="form-horizontal form-label-left " action="<?= site_url('assessment/post/xprt-an-req') ?>" id="fsubmit" name="fsubmit" enctype="multipart/form-data">
              <div class="x_panel">
                <div class="x_content">
                  <input type="hidden" name="id" value="<?= $RQS['RQS_ID'] ?>">
                  <input type="hidden" name="RQS_ABOUT" value="<?= $RQS['RQS_ABOUT'] ?>">
                  <input type="hidden" name="menu" id="menu" >
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Tanggal</label>
                      <div class="col-md-3 col-sm-3 col-xs-3 form-group">
                        <input required class="form-control datepickerx" type="text" name="RQS_DATE_REC"  value="<?= $RQS['RQS_DATE_REC'] ?>">
                      </div>
                    </div>

                    <?php
                    $jmldata = count($RQS_X)+1;
                    if ($jmldata == 1) {
                      $jmldata = 2;
                      $RQS_X[] = "";
                    }
                    $i = 1;
                    foreach ($RQS_X as $key => $value) {
                      ?>

                      <div class="TX_RQS_BTP_COMP form-group row berat<?= $i; ?>">
                      <?php
                      if ($i == 1) {
                        ?>
                        <label class="control-label col-md-3 col-sm-3 col-xs-3"> Catatan Rekomendasi Pakar </label>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <?php
                          } else {
                            ?>
                        <div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <?php
                      }
                      ?>
                          <input type="hidden" name="TX_RQS_BTP_COMP[<?= $i ?>][RQS_ID]" value="<?= $RQS['RQS_ID'] ?>">
                          <input required class="form-control" type="text" name="TX_RQS_BTP_COMP[<?= $i ?>][XPRT_REC_NOTE]"  value="<?= $value['XPRT_REC_NOTE'] ?>">
                        </div>

                        

                        <div class="col-md-3 col-sm-3 col-xs-3">
                          <?php
                          if ($i == 1) {
                            ?>
                            <button type="button" class="btn btn-info btn-rounded btn-xs legitRipple add_ addberat" onclick="addberat();" periksa="berat" terakhir="<?= $jmldata; ?>"><i class="fa fa-plus-square"></i> Tambah</button>&nbsp;&nbsp;

                            <?php
                          } else {
                            ?>
                            <button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple min_" onclick="$('.berat<?= $i; ?>').remove();return false;"><i class="fa fa-trash"></i> Hapus</button>
                            <?php
                          }
                          ?>
                        </div>
                      </div>

                      <?php
                      $i++;
                    }
                    ?>
                    <div id="hideme"></div>


                    <!--<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3">Catatan Rekomendasi Pakar</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" name="TX_XPRT_NOTE[XPRT_REC_NOTE]" required><?= $RQS['XPRT_REC_NOTE'] ?></textarea>
                      </div>
                    </div>--> 
                    <br/>
                    <br/> 

                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                          <div class="notification hidden mbot-0"><div></div></div>
                          <button type="" class="btn btn-success" onclick="validate('draft'); return false;"><i class="fa fa-save"></i> Simpan Catatan</button>
                          <!--<a href="<?= site_url(); ?>cetak/nodin/<?= $RQS['RQS_ID'] ?>"  class="btn  btn-info" ><i class="fa fa-download"></i> Download Nota Dinas & Surat Jawaban</a>-->
                          <!--<a href="<?= site_url(); ?>assessment/ass/form/<?= $RQS['RQS_ID'] ?>"  class="btn  btn-info" ><i class="fa fa-edit"></i> Buat Nodin</a>-->
<!--
                          <a href="<?= site_url(); ?>assessment/ass/codex/<?= $RQS['RQS_ID'] ?>"  class="btn " style="background-color: yellow" > Kembali</a>
                          <input type="button" value="Simpan" class="btn btn-info" onclick="validate('simpan');" >
                          <input type="button" value="Setujui" class="btn btn-success " onclick="validate('setujui');" >
                          <input type="button" value="Tolak" class="btn btn-danger" onclick="validate('tolak');">
-->
                        </div>
                      </div>
                    </div>

                  </div>

                </div>
              </div>
              </form>

              <div class="x_panel">
                <div class="x_content">
                  <div class="col-md-12">

                    <input type="hidden" name="id" value="<?= $RQS['RQS_ID'] ?>">
                    <input type="hidden" name="menu" id="menu" >
                    <input type="hidden" name="TX_EXPOSURE[EXPOSURE_AMOUNT]" id="EXPOSURE_AMOUNT" >
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Berat Badan (kg)</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input type="text" id="EXPOSURE_WEIGHT" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_WEIGHT]" required value="<?= $RQS['EXPOSURE_WEIGHT'] ?>">
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Asumsi Konsumsi (kg/org/hr)</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input type="text" id="EXPOSURE_CONSUMPTION" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_CONSUMPTION]" required value="<?= $RQS['EXPOSURE_CONSUMPTION'] ?>">
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> ADI/PTWI/MTDI (mg/kg BB)</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input type="text" id="EXPOSURE_ADI" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_ADI]" required value="<?= $RQS['EXPOSURE_ADI'] ?>">
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Jumlah Penggunaan/Batas Maksimum (mg/kg)</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input type="text" id="EXPOSURE_MAX_USAGE" class="form-control keyup-numeric" name="TX_EXPOSURE[EXPOSURE_MAX_USAGE]" required value="<?= $RQS['EXPOSURE_MAX_USAGE'] ?>">
                      </div>
                    </div> 
                    <br/>
                    <br/>
                    <br/>
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                          <span style="color: blue; font-weight: bold; font-size: 14px" id='amount'></span>
                        </div>
                      </div>
                    </div>
                    <br/>
                    <br/>
                    <br/>

                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                          <button type="" class="btn btn-success" onclick="hitung();"><i class="fa fa-calculator"></i> Hitung</button>
                        </div>
                      </div>
                    </div>
                    <br/> 
                    <br/>
                    <br/>
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                          <div class="notification hidden mbot-0"><div></div></div>
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-eye"></i> Lihat Tabel Asumsi Konsumsi</button>
                          <!--<button type="" class="btn btn-success" onclick="validate('draft_rekom');"><i class="fa fa-save"></i> Simpan Hasil Hitung</button>-->
                          <!--<a href="<?= site_url(); ?>cetak/nodin/<?= $RQS['RQS_ID'] ?>"  class="btn  btn-info" ><i class="fa fa-download"></i> Download Nota Dinas & Surat Jawaban</a>-->
                          <a href="<?= site_url(); ?>assessment/ass/form/<?= $RQS['RQS_ID'] ?>"  class="btn  btn-info" ><i class="fa fa-edit"></i> Buat Nodin</a>
                        </div>
                      </div>
                    </div>


                  </div>
                </div>
              </div>


            <?php
            }
            ?>
                  <!-- modals -->
                  <!-- Large modal -->
                  

                  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">TABEL KONSUMSI PANGAN 
UNTUK PENETAPAN BATAS MAKSIMUM CEMARAN & BAHAN TAMBAHAN PANGAN DI INDONESIA</h4>
                        </div>
                        <div class="modal-body">
<table class="table table-bordered table-striped table-hover" >
  <thead>
    <tr>
      <th>No Katpang</th>
      <th>Jenis Pangan</th>
      <th>Sub Jenis Pangan</th>
      <th>Konsumsi Pangan (g/orang/hari)</th>
      <th>Satuan Lain</th>
      <th>Ket</th>
    </tr>
  </thead>
  <tbody>
<?php
foreach ($asumsi as $key => $value) {
echo "<tr>
      <td>".$value['FOOD_TYPE_NO']."</td>
      <td>".$value['FOOD_TYPE_NAME']."</td>
      <td>".$value['FOOD_TYPE_NAME_SUB']."</td>
      <td  style='text-align: right;'>".$value['FOOD_CONSUMPTION']."</td>
      <td>".$value['UNIT']."</td>
      <td>".$value['DESCRIPTION']."</td>
    </tr>";
}
?>
  </tbody>
</table>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                        </div>

                      </div>
                    </div>
                  </div>


          <form class="form-horizontal form-label-left " action="<?= site_url('assessment/post/nodin') ?>" id="fnodin" name="fnodin" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">


                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3">  Nota Dinas & Surat Jawaban </label>
                      <div class="col-md-1 col-sm-1 col-xs-1 form-group">

                        <!--<span class="btn btn-warning btn-sm btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Pdf</span>
                            <input class="form-control" id="fileuploadAlur" type="file" name="files" multiple >
                        </span>-->
                        <span class="btn btn-warning btn-sm btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Doc</span>
                            <input class="form-control" id="fileuploadAlur13" type="file" name="files" multiple >
                        </span>

                      </div>
                      <!--<div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokAlur"> 
                          <?= ($arrTxRqsDoc12['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkAlur" style="padding-top:20px;color:blue;" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc12['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc12['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc12['RQS_DOC_NAME'] . '</a>' : '') ?>
                        </div>    

                      </div>-->
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokAlur13"> 
                          <?= ($arrTxRqsDoc13['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkAlur13" style="padding-top:20px;color:blue;" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc13['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc13['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc13['RQS_DOC_NAME'] . '</a>' : '') ?>
                        </div>    
                      </div>
                </div> 
                <!--<div class="form-group">
                      <div class="col-md-1 col-sm-1 col-xs-1 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 ">

                        <span class="btn btn-warning btn-sm btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Doc</span>
                            <input class="form-control" id="fileuploadAlur13" type="file" name="files" multiple >
                        </span>

                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokAlur13"> 
                          <?= ($arrTxRqsDoc13['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkAlur13" style="padding-top:20px;color:blue;" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc13['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc13['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc13['RQS_DOC_NAME'] . '</a>' : '') ?>
                        </div>    
                      </div>
                </div>--> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3"> 
                  </label>
                      <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                        <button type="" class="btn btn-success" onclick="save_post('#fnodin'); return false;"><i class="fa fa-save"></i> Simpan Hasil Upload</button>
                      </div>
                </div> 
              <input type="hidden" name="DOC[RQS_ID]" value="<?= $RQS['RQS_ID'] ?>">
              <!--<input type="hidden" id="DOC_PATHAlur" name="DOC[RQS_DOC_PATH]" value="<?= $arrTxRqsDoc12['RQS_DOC_PATH'] ?>">
              <input type="hidden" id="DOC_NAMEAlur" name="DOC[RQS_DOC_NAME]" value="<?= $arrTxRqsDoc12['RQS_DOC_NAME'] ?>">
              <input type="hidden" id="DOC_EXTAlur" name="DOC[RQS_DOC_EXT]" value="<?= $arrTxRqsDoc12['RQS_DOC_EXT'] ?>">
              <input type="hidden" id="DOC_SIZEAlur" name="DOC[RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc12['RQS_DOC_SIZE'] ?>">
              <input type="hidden" id="DOC_FILEAlur" name="DOC[RQS_DOC_FILE]" value="<?= $arrTxRqsDoc12['RQS_DOC_FILE'] ?>">
              <input type="hidden" id="DOC_CODEAlur" name="DOC[DOCUMENT_ID]" value="12">-->
            <script>
              $(function () {
                'use strict';
                $('#fileuploadAlur').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '12', format: 'pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokAlur').html('<div id="progressAlur" class="progress" style="margin-top:5px;"><div id="progress-barAlur" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHAlur').val(data.result.path);
                      $('#DOC_NAMEAlur').val(data.result.name);
                      $('#DOC_EXTAlur').val(data.result.ext);
                      $('#DOC_SIZEAlur').val(data.result.size);
                      $('#DOC_FILEAlur').val(data.result.rename);
                      $('#u6').html('');
                      $('#progressAlur').fadeOut('fast', function(){
                        $('#resultDokAlur').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a><span style="color:green; margin-left: 100px">Upload Successful!</span></div>');
                      });
                      hitung();
                    }else{
                      $('#progressAlur').fadeOut('fast', function(){
                        $('#resultDokAlur').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressAlur .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       
              <input type="hidden" name="DOC13[RQS_ID]" value="<?= $RQS['RQS_ID'] ?>">
              <input type="hidden" id="DOC_PATHAlur13" name="DOC13[RQS_DOC_PATH]" value="<?= $arrTxRqsDoc13['RQS_DOC_PATH'] ?>">
              <input type="hidden" id="DOC_NAMEAlur13" name="DOC13[RQS_DOC_NAME]" value="<?= $arrTxRqsDoc13['RQS_DOC_NAME'] ?>">
              <input type="hidden" id="DOC_EXTAlur13" name="DOC13[RQS_DOC_EXT]" value="<?= $arrTxRqsDoc13['RQS_DOC_EXT'] ?>">
              <input type="hidden" id="DOC_SIZEAlur13" name="DOC13[RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc13['RQS_DOC_SIZE'] ?>">
              <input type="hidden" id="DOC_FILEAlur13" name="DOC13[RQS_DOC_FILE]" value="<?= $arrTxRqsDoc13['RQS_DOC_FILE'] ?>">
              <input type="hidden" id="DOC_CODEAlur13" name="DOC13[DOCUMENT_ID]" value="13">
            <script>
              $(function () {
                'use strict';
                $('#fileuploadAlur13').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '13', format: 'docx;doc', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokAlur13').html('<div id="progressAlur13" class="progress" style="margin-top:5px;"><div id="progress-barAlur13" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHAlur13').val(data.result.path);
                      $('#DOC_NAMEAlur13').val(data.result.name);
                      $('#DOC_EXTAlur13').val(data.result.ext);
                      $('#DOC_SIZEAlur13').val(data.result.size);
                      $('#DOC_FILEAlur13').val(data.result.rename);
                      $('#u6').html('');
                      $('#progressAlur13').fadeOut('fast', function(){
                        $('#resultDokAlur13').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a><span style="color:green; margin-left: 100px">Upload Successful!</span></div>');
                      });
                    }else{
                      $('#progressAlur13').fadeOut('fast', function(){
                        $('#resultDokAlur13').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressAlur13 .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       


                  </div>
                </div>
              </div>
            </div>
          </form>








       </div>
    </div>
  <!-- /.row -->
  </div>
  <!-- /.container -->
 </section>
<script>
  function validate(menu){
      var menu = $('#menu').val(menu);
      save_post('#fsubmit');
      return false;     
  }
  
  function hitung(){
    EXPOSURE_WEIGHT = $("#EXPOSURE_WEIGHT").val();
    EXPOSURE_CONSUMPTION = $("#EXPOSURE_CONSUMPTION").val();
    EXPOSURE_ADI = $("#EXPOSURE_ADI").val();
    EXPOSURE_MAX_USAGE = $("#EXPOSURE_MAX_USAGE").val();

    amount = (EXPOSURE_MAX_USAGE*EXPOSURE_CONSUMPTION) / (EXPOSURE_ADI*EXPOSURE_WEIGHT)*100;
      //amount = 3.12346;
     amount2 = amount.toString().split(".");
     if(typeof amount2[1] !== 'undefined'){ //alert(amount2[1]);
       amount3 = amount2[1].substr(4,1);
       if(amount3 == 5){
           amount4 = amount2[1].substr(0,4);
          amount = amount2[0]+"."+amount4;
        }else{
          amount = amount.toFixed(4);
        }
     }else{
        amount = amount.toFixed(4);
     }
     $("#amount").html("Paparan = ("+EXPOSURE_MAX_USAGE+" x "+EXPOSURE_CONSUMPTION+")/("+EXPOSURE_ADI+" x "+EXPOSURE_WEIGHT+")x 100 = "+amount); //+"=="+amount2[1]+"--"+amount3
     $("#EXPOSURE_AMOUNT").val(amount);

  }

  $(document).ready(function(e){
    $('.keyup-numeric').keyup(function() {
      $('span.error-keyup-1').remove();
      var inputVal = $(this).val();
      var numericReg = /^-{0,1}\d*\.{0,1}\d+$/;
      if(!numericReg.test(inputVal) || inputVal == 0) {  
        //$(this).val('');
        $(this).after('<span style="color:red" class="error error-keyup-1">Input harus bilangan asli atau bilangan desimal, contoh : 1.2 </span>');
      }
    });
  });

</script>
