<script>
var site_url = '<?= site_url(); ?>';

    $(document).ready(function () {

        var user_type = "<?= $this->newsession->userdata('USER_TYPE_ID'); ?>";
        if(user_type == 'UTY02'){ //PERORANGAN
          $('#FACTORY_TYPE').attr('disabled', 'disabled').after('<input type="hidden" name="REQUEST[RQS_FAC_TYPE_ID]" value="FTY01">');
        }else{
          RQS_FAC_TYPE_ID = "<?= $arrTxRqs['RQS_FAC_TYPE_ID']; ?>";
          for(i = 1; i <= 4; i++){
            if(RQS_FAC_TYPE_ID == 'FTY0'+i){
              $('.'+RQS_FAC_TYPE_ID).show(500).removeAttr('disabled');
            }else{
              $('.FTY0'+i).hide(500).attr('disabled', 'disabled');
            }
          }        
        }

            var RQS_CTF_STATUS = "<?= $arrTxRqs['RQS_CTF_STATUS'] ?>";
            if(RQS_CTF_STATUS == 'CES01'){
              $('.fileuploadDSAB').show();
              $('#DOC_IDDSAB, #DOC_PATHDSAB, #DOC_NAMEDSAB, #DOC_EXTDSAB, #DOC_SIZEDSAB, #DOC_FILEDSAB, #DOC_CODEDSAB ').removeAttr('disabled');
            }else if(RQS_CTF_STATUS == 'CES02'){
              $('.RQS_UNCTF_RSN').show();
              $('#DOC_IDDSAB, #DOC_PATHDSAB, #DOC_NAMEDSAB, #DOC_EXTDSAB, #DOC_SIZEDSAB, #DOC_FILEDSAB, #DOC_CODEDSAB ').attr('disabled', 'disabled');
            }

  
    });

function verify(val){
  $("#menu").val(val);
  save_post('#fsubmit');
  return false;
}
function verify2(val){
  $("#menu2").val(val);
  save_post('#fsubmit');
  return false;
}
</script>
<style>
  .red{
    color:red;
  }
  #scroll {
    border: 1px solid black;
    height: 400px;
    overflow: scroll;
  }
</style>
<section id="page-header" class="page-section">
  <hr>
</section>

<?php 
$user_id = $arrTxRqs['USER_ID'];
$rqs_id = $arrTxRqs['RQS_ID']; 
//print_r($_SESSION) ; echo $this->newsession->userdata('USER_TYPE_ID'); ?>
<section id="page-content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h6 class="text-uppercase"> ASSESSMENT >> DRAFT REKOMENDASI >> <span style="color: blue"><?= $title ?> </span> </h6>
        <hr>
        <?php 
        if($this->newsession->userdata('ROLE_ID') == '603'){
        ?>

          <div class="row">
            <div class="col-xs-12">
              <div class="x_panel">
                <div class="x_content">
                  <table style="width:100%; text-align: left" >
                    <tbody>
                      <tr>
                        <td style="min-width: 120px"><b>No Permohonan</b></td>
                        <td><?= $arrTxRqs['RQS_NO_AJU'] ?></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td><b>Status Permohonan</b></td>
                        <td style="min-width: 140px"><?= $arrTxRqs['STAT'] ?></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <!--<tr>
                        <td><b>Verified Date</b></td>
                        <td><?= $arrTxRqs['RQS_VER_DATE'] ?></td>
                        <td><b>Verified By</b></td>
                        <td><?= $arrTxRqs['VER_BY'] ?></td>
                      </tr>
                      <tr>
                        <td><b>Assessed Date</b></td>
                        <td><?= $arrTxRqs['RQS_ASS_DATE'] ?></td>
                        <td><b>Assessed By</b></td>
                        <td><?= $arrTxRqs['ASS_BY'] ?></td>
                      </tr>
                      <tr>
                        <td><b>Released Date</b></td>
                        <td>-</td>
                        <td><b>Drafted</b></td>
                        <td>-</td>
                      </tr>-->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>DATA ADMINISTRASI PEMOHON </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Jenis Pemohon</b> </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                       <?= $arrTxRqs['STATUS_DETAIL'] ?>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Pemohon/ Perusahaan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                        <?= $arrTxRqs['TRADER_TYPE']." ".$arrTxRqs['USER_NAME'] ?>
                        </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Alamat</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                        <?= $arrTxRqs['USER_ADDR'] ?>
                        </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Penanggung Jawab</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <?= $arrTxRqs['RQS_PIC_NAME'];?>
                        </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>No Telp</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqs['RQS_PIC_TELP'];?>
                        </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>No Fax</b> </div>
                         <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqs['RQS_PIC_FAX'];?>
                        </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Alamat Email</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                         <?= $arrTxRqs['RQS_PIC_MAIL'];?>
                        </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>DATA UMUM BTP </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Perihal</b> </div>
                       <div class="col-md-8 col-sm-8 col-xs-8">
                        <?= $arrTxRqs['RQS_ABOUT'] ?>
                       </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>No. INS</b> </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                       <?= $arrTxRqs['BTP_TYPE_NO'] ?>
                      
                      </div>
                    </div>

                     <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Jenis BTP</b> </div>
                       <div class="col-md-8 col-sm-8 col-xs-8">
                       <?= $arrTxRqs['BTP_TYPE_NAME'] ?>
                       </div>
                    </div>

                     <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Jenis Pangan</b> </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                       <?= $arrTxRqs['FOOD_TYPE_NAME'] ?>
                      </div>
                    </div>

                     <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Kategori Pangan</b> </div>
                       <div class="col-md-8 col-sm-8 col-xs-8">
                       <?php
                       $ex = explode('Kategori Pangan ', $arrTxRqs['RQS_ABOUT']);
                       ?>
                        <?= $ex[1];//$arrTxRqs['RQS_FOOD_TYPE_NO'];?>
                       </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama/Merk Dagang BTP</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqs['RQS_BTP_MERK'] ?>
                        </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Jumlah Penggunaan BTP dalam Produk Pangan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqs['RQS_BTP_NETTO']." ".$arrTxRqs['RQS_BTP_UNIT'] ?>
                        </div>
                    </div>


                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Jenis Kemasan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqs['RQS_BTP_PCKG_TYPE'] ?>
                        </div>
                    </div>
<?php if($arrTxRqs['JENIS_P'] != ''){ ?>
                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Jenis Perusahaan</b> </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                       <?= $arrTxRqs['JENIS_P'] ?>
                      </div>
                    </div> 
<?php } ?>
                    <!-- Data Pabrik 1 -->
                    <div class="form-group row FTY01" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Pabrik/Perusahaan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsDln['RQS_DLN_FAC_NAME'] ?>
                        </div>
                    </div>

                    <div class="form-group row FTY01" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Alamat Pabrik/Perusahaan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsDln['RQS_DLN_FAC_ADDR'] ?>
                        </div>
                    </div>

                     <div class="form-group row FTY01" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>No. Telp</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsDln['RQS_DLN_FAC_TELP'] ?>
                        </div>
                    </div>
                    <!-- End Data Pabrik 1 -->

                    <!-- Data Pabrik 2 -->
                    <input type="hidden" class="" name="PCKG" value="<?= $arrTxRqsPckg['RQS_PCKG_ID'] ?>">
                    <div class="form-group row FTY02" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Pabrik Pengemas</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsPckg['RQS_PCKG_PACKAGER_NAME'] ?>
                        </div>
                    </div>

                    <div class="form-group row FTY02" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Alamat Pabrik Pengemas</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsPckg['RQS_PCKG_PACKAGER_ADDR'] ?>
                        </div>
                    </div>

                     <div class="form-group row FTY02" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>No. Telp</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsPckg['RQS_PCKG_PACKAGER_TELP'] ?>
                        </div>
                    </div>

                     <div class="form-group row FTY02" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Pabrik Asal</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsPckg['RQS_PCKG_SOURCE_NAME'] ?>
                        </div>
                    </div>

                    <div class="form-group row FTY02" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Alamat Pabrik Asal</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsPckg['RQS_PCKG_SOURCE_ADDR'] ?>
                        </div>
                    </div>
                    <!-- End Data Pabrik 2 -->

                    <!-- Data Pabrik 3 -->
                    <input type="hidden" class="" name="LCNS" value="<?= $arrTxRqsLcns['RQS_LCNS_ID'] ?>">
                    <div class="form-group row FTY03" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Pabrik/Perusahaan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsLcns['RQS_LCNS_FAC_NAME'] ?>
                        </div>
                    </div>

                    <div class="form-group row FTY03" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Alamat Pabrik/Perusahaan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsLcns['RQS_LCNS_FAC_ADDR'] ?>
                        </div>
                    </div>

                     <div class="form-group row FTY03" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>No. Telp</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsLcns['RQS_LCNS_FAC_TELP'] ?>
                        </div>
                    </div>

                     <div class="form-group row FTY03" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Pabrik Pemberi Lisensi</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsLcns['RQS_LCNS_LICENSER_NAME'] ?>
                        </div>
                    </div>

                    <div class="form-group row FTY03" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Alamat Pemberi Lisensi</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsLcns['RQS_LCNS_LICENSER_ADDR'] ?>
                        </div>
                    </div>
                    <!-- End Data Pabrik 3 -->

                     <!-- Data Pabrik 4 -->
                    <input type="hidden" class="" name="IMPR" value="<?= $arrTxRqsImpr['RQS_IMPR_ID'] ?>">
                    <div class="form-group row FTY04" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Pabrik/Perusahaan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsImpr['RQS_IMPR_FAC_NAME'] ?>
                        </div>
                    </div>

                    <div class="form-group row FTY04" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Alamat Pabrik/Perusahaan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsImpr['RQS_IMPR_FAC_ADDR'] ?>
                        </div>
                    </div>

                     <div class="form-group row FTY04" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Importir</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsImpr['RQS_IMPR_IMPORTIR_NAME'] ?>
                        </div>
                    </div>

                    <div class="form-group row FTY04" style="display: none;">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Alamat Importir</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqsImpr['RQS_IMPR_IMPORTIR_ADDR'] ?>
                        </div>
                    </div>

                    <div class="form-group row FTY04" style="display: none;">
                      <div class="col-md-3 col-sm-3 col-xs-3"> <b>No. Telp</b> </div>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <?= $arrTxRqsImpr['RQS_IMPR_IMPORTIR_TELP'] ?>
                        </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>

            <?php 
              if( $arrTxRqs['RQS_PROCESS_ID'] != 'PR02' && $arrTxRqs['RQS_PROCESS_ID'] != 'PR05' || $this->newsession->userdata('ROLE_ID') == '603' ){ $style="style='display:none;'";$disabled="disabled"; }else{ $style="";$disabled=""; }
            ?>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>DATA TEKNIS </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="form-group row">
                      <div class=" col-md-12 col-sm-12 col-xs-12" style="color: black; text-align: left"> <b>Bahan Tambahan Pangan </b></div>
                    </div>

                    <?php
                    $jmldata = count($arrTxRqsBtpComp)+1;
                    if ($jmldata == 1) {
                      $jmldata = 2;
                      $arrTxRqsBtpComp[] = "";
                    }
                    $i = 1;
                    foreach ($arrTxRqsBtpComp as $key => $value) {
                      ?>
                      <div class="form-group row berat<?= $i; ?>">
                      <?php
                      if ($i == 1) {
                        ?>
                        <div class=" col-md-3 col-sm-3 col-xs-3"> <b>Komposisi BTP</b> </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <?php
                          } else {
                            ?>
                        <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <?php
                      }
                      ?>

                          <?= $value['RQS_BTP_COMP_NAME'] ?>
                        </div>                        
                        <div class=" col-md-1 col-sm-1 col-xs-1"> <b>Kadar</b> </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                          <?= $value['RQS_BTP_COMP_QTY'] ?> %
                        </div>
                        <div class="col-md-4 col-sm-1 col-xs-1"></div>
                      </div>

                      <?php
                      $i++;
                    }
                    ?>


                    <!-- Komposisi BTP End -->
                    <div id="KomposisiBTP"></div>
                    <div class="form-group row">
                      <div class="col-md-7 col-sm-7 col-xs-7 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <div id="resultDokBKB"> 
                          <?= ($arrTxRqsDoc05['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a style="color:blue" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc05['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc05['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc05['RQS_DOC_NAME'] . '</a> ' : '') //. $arrTxRqsDoc05['STATUS_DETAIL']?>
                        </div>    
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2" <?= $style ?>>
                          <input type="checkbox" name="TxRqsDoc[]" value="<?= $arrTxRqsDoc05['RQS_DOC_ID'] ?>" > REQUEST REVISI
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class=" col-md-3 col-sm-3 col-xs-3"><b>Spesifikasi Mutu Bahan</b></div>
                      <div class="col-md-7 col-sm-7 col-xs-7">
                        <div id="resultDokDSMB"> 
                          <?= ($arrTxRqsDoc06['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a style="color:blue" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc06['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc06['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc06['RQS_DOC_NAME'] . '</a> ' : '') //. $arrTxRqsDoc06['STATUS_DETAIL']?>
                        </div>    
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2" <?= $style ?>>
                          <input type="checkbox" name="TxRqsDoc[]" value="<?= $arrTxRqsDoc06['RQS_DOC_ID'] ?>"> REQUEST REVISI
                      </div>
                    </div>


                    <!-- Produk Pangan -->          
                    <div class="form-group row">
                      <div class=" col-md-12 col-sm-12 col-xs-12" style="color: black; text-align: left"><b> Produk Pangan </b></div>
                    </div>

                    <?php
                    $jmldata = count($arrTxRqsFoodComp)+1;
                    if ($jmldata == 1) {
                      $jmldata = 2;
                      $arrTxRqsFoodComp[] = "";
                    }
                    $i = 1;
                    foreach ($arrTxRqsFoodComp as $key => $value) {
                      ?>
                      
                      <div class="form-group row row berat<?= $i; ?>">
                      <?php
                      if ($i == 1) {
                        ?>
                        <div class=" col-md-3 col-sm-3 col-xs-3"> <b>Komposisi Produk Pangan</b> </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <?php
                          } else {
                            ?>
                        <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <?php
                      }
                      ?>
                          <?= $value['RQS_FOOD_COMP_NAME'] ?>
                        </div>
                        <div class=" col-md-1 col-sm-1 col-xs-1"> <b>Kadar</b> </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                          <?= $value['RQS_FOOD_COMP_QTY'] ?> %
                        </div>
                        <div class="col-md-4 col-sm-1 col-xs-1"></div>
                      </div>

                      <?php
                      $i++;
                    }
                    ?>

                    <!-- Komposisi BTP End -->

                    <div class="form-group row">
                      <div class="col-md-7 col-sm-7 col-xs-7 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <div id="resultDokBKPP"> 
                          <?= ($arrTxRqsDoc07['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a style="color:blue" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc07['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc07['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc07['RQS_DOC_NAME'] . '</a> ' : '') //. $arrTxRqsDoc07['STATUS_DETAIL']?>
                        </div>    
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2" <?= $style ?>>
                          <input type="checkbox" name="TxRqsDoc[]" value="<?= $arrTxRqsDoc07['RQS_DOC_ID'] ?>"> REQUEST REVISI
                      </div>
                    </div>  


                    <div class="form-group row">
                      <div class=" col-md-3 col-sm-3 col-xs-3"> <b>Fungsi BTP dalam Produk Pangan</b></div>
                      <div class="col-md-9 col-sm-9 col-xs-9">
                       <?= $arrTxRqs['BTP_FUNCTION_NAME'] ?>
                     </div>
                   </div> 
                <div class="form-group row">
                  <div class=" col-md-3 col-sm-3 col-xs-3"> <b>Mekanisme kerja BTP dalam Produk Pangan</b></div>
                  <div class="col-md-9 col-sm-9 col-xs-9">
                    <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" disabled><?= $arrTxRqs['RQS_BTP_MECHANISM'] ?></textarea>
                  </div>
                </div> 


                   <div class="form-group row">
                    <div class=" col-md-3 col-sm-3 col-xs-3"> <b>Sertifikat Analisis BTP</b></div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                     <?= $arrTxRqs['CERTIFICATION_STATUS'] ?>
                   </div>
                 </div>                   

                 <div class="form-group row RQS_UNCTF_RSN" style="display: none;">
                  <div class=" col-md-3 col-sm-3 col-xs-3"> <b>Alasan Tidak Memiliki Sertifikat</b> </div>
                  <div class="col-md-9 col-sm-9 col-xs-9 ">
                    <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" disabled><?= $arrTxRqs['RQS_UNCTF_RSN'] ?></textarea>
                    
                  </div>
                </div>

                <div class="form-group row fileuploadDSAB"  style="display: none;">
                  <div class=" col-md-3 col-sm-3 col-xs-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3"></div>
                      <div class="col-md-7 col-sm-7 col-xs-7">
                        <div id="resultDokDSAB"> 
                          <?= ($arrTxRqsDoc08['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a style="color:blue" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc08['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc08['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc08['RQS_DOC_NAME'] . '</a> ' : '') //. $arrTxRqsDoc08['STATUS_DETAIL']?>
                        </div>    
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2" <?= $style ?>>
                          <input type="checkbox" name="TxRqsDoc[]" value="<?= $arrTxRqsDoc08['RQS_DOC_ID'] ?>"> REQUEST REVISI
                      </div>
                </div> 
                <div class="form-group row">
                  <div class=" col-md-3 col-sm-3 col-xs-3"> <b>Metode pengujian BTP</b> </div>
                  <div class="col-md-9 col-sm-9 col-xs-9">
                    <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" disabled><?= $arrTxRqs['RQS_BTP_TEST_METHOD'] ?></textarea>
                    
                  </div>
                </div> 

                <div class="form-group row">
                      <div class="col-md-7 col-sm-7 col-xs-7 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <div id="resultDokDMPB"> 
                          <?= ($arrTxRqsDoc09['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a style="color:blue" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc09['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc09['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc09['RQS_DOC_NAME'] . '</a> ' : '') //. $arrTxRqsDoc09['STATUS_DETAIL']?>
                        </div>    
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2" <?= $style ?>>
                          <input type="checkbox" name="TxRqsDoc[]" value="<?= $arrTxRqsDoc09['RQS_DOC_ID'] ?>"> REQUEST REVISI
                      </div>
                </div> 

                <div class="form-group row">
                  <div class=" col-md-3 col-sm-3 col-xs-3"> <b>Alur Proses Produksi Produk Pangan</b> </div>
                      <div class="col-md-7 col-sm-7 col-xs-7">
                        <div id="resultDokDMPB"> 
                          <?= ($arrTxRqsDoc11['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a style="color:blue" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc11['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc11['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc11['RQS_DOC_NAME'] . '</a> ' : '') //. $arrTxRqsDoc11['STATUS_DETAIL']?>
                        </div>    
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2" <?= $style ?>>
                          <input type="checkbox" name="TxRqsDoc[]" value="<?= $arrTxRqsDoc11['RQS_DOC_ID'] ?>"> REQUEST REVISI
                      </div>
                </div> 

                <div class="form-group row">
                  <div class=" col-md-3 col-sm-3 col-xs-3"> <b>Cara Penggunaan Produk Pangan</b> </div>
                  <div class="col-md-9 col-sm-9 col-xs-9">
                    <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" disabled><?= $arrTxRqs['RQS_FOOD_HOWTO'] ?></textarea>
                    
                  </div>
                </div>
                <br/>
                <br/>

                <!-- Sandingan Komparasi Negara Lain -->          
                <div class="form-group row">
                  <div><span style="color: black"><b> Sandingan Komparasi Negara Lain </b></span></div>
                </div>

                  <div class="col-md-12 col-sm-12 col-xs-12 " id="line<?= $i; ?>">
                    <table class="table" style="width:100%;">
                      <thead>
                        <th>Negara/Organisasi</th>
                        <th>Jenis Pangan</th>
                        <th>Batas Maksimal (Mg/Kg)</th>
                        <th>File</th>
                        <th>&nbsp;</th>
                      </thead>
                      <tbody>
                <?php
                //echo "<pre>";print_r($arrTxRqsPrdRegDoc);echo "</pre>";
                $jmldata = count($arrTxRqsPrdRegDoc)+1;
                if ($jmldata == 1) {
                  $jmldata = 2;
                  $arrTxRqsPrdRegDoc[] = "";
                }
                $i = 1;
                foreach ($arrTxRqsPrdRegDoc as $key => $value) {
                    ?>
                        <tr>
                          <td style="width:20%;">
                          
                            <?= $value['RQS_PRD_REG_COUNTRY'] ?>
                          </td>
                          <td style="width:25%;">
                            <?= $value['RQS_PRD_REG_FOOD_TYPE'] ?>
                          </td>
                          <td style="width:10%;">
                            <?= $value['RQS_PRD_REG_MAX_QTY'] ?>
                          </td>
                          <td style="width:45%;">

                            <div class="form-group row">
                              <div class="col-md-8 col-sm-9 col-xs-9">
                                <div id="resultDokSKNL1"> 
                                  <?= ($value['RQS_PRD_REG_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a style="color:blue" href="' . site_url() . "download/data/" . base64_encode($value['RQS_PRD_REG_DOC_PATH']) . "/" . $value['RQS_PRD_REG_DOC_NAME'] . '" target="_blank">' . $value['RQS_PRD_REG_DOC_NAME'] . '</a> ' : '') //. $value['STATUS_DETAIL']?>
                                </div>    

                              </div>
                              <div class="col-md-4 col-sm-3 col-xs-3" <?= $style ?>>
                                  <input type="checkbox" name="RQS_PRD_REG_DOC[<?= $value['RQS_PRD_REG_ID'] ?>]" value="<?= $value['RQS_PRD_REG_ID'] ?>"> REQUEST REVISI
                              </div>
                            </div>
                          </td>
                        </tr>


                      
                  <?php
                  $i++;
                }
                ?>
                      </tbody>
                    </table>
                  </div>

                <br/>
                <!-- END -->



                  </div>
                </div>
              </div>
            </div>
        <?php 
        }else{
        ?>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Rangkuman Kajian </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Pemohon/ Perusahaan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                        <?= $arrTxRqs['TRADER_TYPE']." ".$arrTxRqs['USER_NAME'] ?>
                        </div>
                    </div>
                     <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Nama Jenis BTP</b> </div>
                       <div class="col-md-8 col-sm-8 col-xs-8">
                       <?= $arrTxRqs['BTP_TYPE_NAME'] ?>
                       </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>No. INS</b> </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                       <?= $arrTxRqs['BTP_TYPE_NO'] ?>
                      
                      </div>
                    </div>
                     <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>No Kategori Pangan</b> </div>
                       <div class="col-md-8 col-sm-8 col-xs-8">
                        <?= $arrTxRqs['RQS_FOOD_TYPE_NO'];?>
                       </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-4 col-sm-4 col-xs-4"> <b>Jumlah Penggunaan BTP dalam Produk Pangan</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <?= $arrTxRqs['RQS_BTP_NETTO']." ".$arrTxRqs['RQS_BTP_UNIT'] ?>
                        </div>
                    </div>

                  <div class="form-group row">
                    <div class=" col-md-4 col-sm-4 col-xs-3"> <b>Lihat Nota Dinas dan Surat Jawaban</b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <div id="resultDokDMPB"> 
                            <?= ($arrTxRqsDoc12['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a style="color:blue" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc12['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc12['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc12['RQS_DOC_NAME'] . '</a> ' : '') //. $arrTxRqsDoc11['STATUS_DETAIL']?>
                          </div>    
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2" >
                        </div>
                  </div>
                  <div class="form-group row">
                    <div class=" col-md-4 col-sm-4 col-xs-3"> <b>Lihat detail permohonan </b> </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                          <div id=""> 
                            <b><a href="<?= site_url(); ?>assessment/ass/detail/<?= $arrTxRqs['RQS_ID'] ?>" target="_blank" style="color:blue;">Klik di sini</a></b>
                          </div>    
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2" >
                        </div>
                  </div>
                  <?php if($this->newsession->userdata('ROLE_ID') == '605'){ ?> 
                  <div class="form-group row">
                        <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-4 col-xs-offset-3">
                          <div id=""> 
                            <?= ($arrTxRqsDoc13['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a href="' . site_url() . "download/data11/" . base64_encode($arrTxRqsDoc13['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc13['RQS_DOC_NAME'] . '" target="_blank" class="btn  btn-info"><i class="fa fa-download"></i> Download Doc</a></a> ' : '') ?>
                          </div>    
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2" >
                        </div>
                  </div> 
                  <hr>

                  <form class="form-horizontal form-label-left " action="<?= site_url('assessment/post/nodinkasi') ?>" id="fnodin" name="fnodin" enctype="multipart/form-data">
                    <div class="form-group row">
                      <div class=" col-md-4 col-sm-4 col-xs-3"> <b>  Upload Nodin yang sudah diconvert ke pdf</div>
                          <div class="col-md-1 col-sm-1 col-xs-3 form-group">
                            <span class="btn btn-warning btn-sm btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                              <span>Upload Pdf</span>
                                <input class="form-control" id="fileuploadAlur" type="file" name="files" accept="application/pdf" multiple >
                            </span>

                          </div>
                          <div class="col-md-7 col-sm-7 col-xs-7 form-group">
                            <div id="resultDokAlur"> 
                            </div>    

                          </div>
                    </div> 
                    <div class="form-group">
                          <div class="col-md-1 col-sm-1 col-xs-1 col-md-offset-4 col-sm-offset-4 col-xs-offset-3 ">
                            <button type="" class="btn btn-success" onclick="save_post('#fnodin'); return false;"><i class="fa fa-save"></i> Simpan Hasil Upload</button>
                          </div>
                    </div> 
                    <input type="hidden" name="DOC[RQS_ID]" value="<?= $arrTxRqs['RQS_ID'] ?>">
                    <input type="hidden" id="DOC_PATHAlur" name="DOC[RQS_DOC_PATH]" value="<?= $arrTxRqsDoc12['RQS_DOC_PATH'] ?>">
                    <input type="hidden" id="DOC_NAMEAlur" name="DOC[RQS_DOC_NAME]" value="<?= $arrTxRqsDoc12['RQS_DOC_NAME'] ?>">
                    <input type="hidden" id="DOC_EXTAlur" name="DOC[RQS_DOC_EXT]" value="<?= $arrTxRqsDoc12['RQS_DOC_EXT'] ?>">
                    <input type="hidden" id="DOC_SIZEAlur" name="DOC[RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc12['RQS_DOC_SIZE'] ?>">
                    <input type="hidden" id="DOC_FILEAlur" name="DOC[RQS_DOC_FILE]" value="<?= $arrTxRqsDoc12['RQS_DOC_FILE'] ?>">
                    <input type="hidden" id="DOC_CODEAlur" name="DOC[DOCUMENT_ID]" value="12">
                  </form>
            <script>
              $(function () {
                'use strict';
                $('#fileuploadAlur').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '12', format: 'pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokAlur').html('<div id="progressAlur" class="progress" style="margin-top:5px;"><div id="progress-barAlur" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHAlur').val(data.result.path);
                      $('#DOC_NAMEAlur').val(data.result.name);
                      $('#DOC_EXTAlur').val(data.result.ext);
                      $('#DOC_SIZEAlur').val(data.result.size);
                      $('#DOC_FILEAlur').val(data.result.rename);
                      $('#u6').html('');
                      $('#progressAlur').fadeOut('fast', function(){
                        $('#resultDokAlur').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a></div>');
                      });
                    }else{
                      $('#progressAlur').fadeOut('fast', function(){
                        $('#resultDokAlur').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressAlur .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       

                  <?php } ?> 

                  </div>
                </div>
              </div>
            </div>

        <?php 
        }
        ?>


            <div class="row">
                <div class="col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Detail Aktivitas </h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                              <div id="tampillog" style="margin-bottom:6px;">&nbsp;<a href="javascript:;" class="normal" onclick="show_detail('#tampillog', '<?= site_url(); ?>assessment/list_log/<?= $arrTxRqs['RQS_ID']; ?>');"><u>Tampilkan Detail Aktivitas (<?= $jumlahlog ?>)</u></a></div>
                    </div>
                  </div>
                </div>
            </div>
<!--
        <div class="row">
          <div class="col-xs-12">
            <div class="x_panel">
              <div class="x_content">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3"> Draft Rekomendasi</label>
                  <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                    <textarea class="resizable_textarea form-control" rows="5"  style="width:87%;" name="RQS_DRAFT_RECOMMENDATION_LETTER" readonly><?= $arrTxRqs['RQS_DRAFT_RECOMMENDATION_LETTER'] ?></textarea>
                  </div>
                </div> 
                <br/>
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                      <div class="notification hidden mbot-0"><div></div></div>
                      <input type='hidden' id="menu" name='menu' value=''>
                      <?php 
                      if($this->newsession->userdata('ROLE_ID') == '603'){
                      ?>
                        <a href="<?= site_url().$back; ?>"  class="btn " style="background-color: yellow" > Kembali</a>                        
                      <?php
                        if($arrTxRqs['RQS_PROCESS_ID'] == 'PR06' || $arrTxRqs['RQS_PROCESS_ID'] == 'PR07' || $arrTxRqs['RQS_PROCESS_ID'] == 'PR10' || $arrTxRqs['RQS_PROCESS_ID'] == 'PR12' || $arrTxRqs['RQS_PROCESS_ID'] == 'PR14'){ ?>
                          <input type="button" value="Submit" class="btn btn-info " onclick="verify('draft_surat_rekomendasi'); return false;">
                        <?
                        }
                      }
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
-->
       <form class="" action="<?= site_url('assessment/post/draft-rec') ?>" id="fsubmit" name="fsubmit" enctype="multipart/form-data">
          <input type="hidden" name="id" id="RQS_ID" value="<?= $arrTxRqs['RQS_ID'] ?>">
 
                      <?php 
                      if(($this->newsession->userdata('ROLE_ID') == '605' && $arrTxRqs['RQS_PROCESS_ID'] == 'PR06') || ($this->newsession->userdata('ROLE_ID') == '606' && $arrTxRqs['RQS_PROCESS_ID'] == 'PR09') || ($this->newsession->userdata('ROLE_ID') == '607' && $arrTxRqs['RQS_PROCESS_ID'] == 'PR11')){
                      ?>
        <div class="row">
          <div class="col-xs-12">
            <div class="x_panel">
              <div class="x_content">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3"> Catatan Koreksi</label>
                  <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                    <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" name="RQS_LOG_NOTE" ></textarea>
                  </div>
                </div> 
                <br/>
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                      <div class="notification hidden mbot-0"><div></div></div>
                      <input type='hidden' id="menu2" name='menu2' value=''>
                        <button type="" class="btn btn-danger" onclick="verify2('perbaiki'); return false;"><i class="fa fa-eraser"></i> Perbaiki Kajian</button>
                        <button type="" class="btn btn-info" onclick="verify2('setujui'); return false;"><i class="fa fa-check"></i> Setujui Kajian</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
                        <?
                      }
                      ?>

</form> 


      </div>
    </div>
  <!-- /.row -->
  </div>
  <!-- /.container -->
 </section>

