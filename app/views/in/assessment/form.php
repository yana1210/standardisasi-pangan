<script>
  var site_url = '<?= site_url(); ?>';
  
  function addberat() { 
    var countTX_RQS_BTP_COMP = $(".TX_RQS_BTP_COMP").length;
    if(countTX_RQS_BTP_COMP == 10){
      alert("Batas Maksimal Komposisi BTP adalah 10");
    }else{
      var nom = $('.addberat').attr('terakhir');
      var idtr = $('.addberat').attr('periksa');
      var cls = idtr + nom;
      $('#hideme').before('<div class="TX_RQS_BTP_COMP form-group row ' + cls + '"><div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-3 col-sm-offset-3 col-xs-offset-3"><input type="hidden" name="TX_RQS_BTP_COMP['+nom+'][RQS_ID]" value="<?= $RQS['RQS_ID'] ?>"><input  class="form-control" type="text" name="TX_RQS_BTP_COMP['+nom+'][SPEC_DESC]" id="" value=""></div><div class="col-md-3 col-sm-3 col-xs-3"><button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple min_" onClick="$(\'.' + cls + '\').remove();return false;"><i class="fa fa-trash"></i> Hapus</button></div></div>');
      $('.addberat').attr('terakhir', parseInt(nom) + 1);
    }
    return false;

  }

</script>
<style>
  .red{
    color:red;
  }
  #scroll {
    border: 1px solid black;
    height: 400px;
    overflow: scroll;
  }
</style>
<section id="page-header" class="page-section">
  <hr>
</section>
<?php 
$user_id = $RQS['USER_ID']; 
$rqs_id = $RQS['RQS_ID']; 
?>
<section id="page-content">
  <div class="container">
    <div class="row">
        <div class="col-md-12">
          <h6 class="text-uppercase">ASSESSMENT >> <span style="color: blue">INPUT MANUAL  </span> </h6>
          <hr>
          <form class="form-horizontal form-label-left " action="<?= site_url('assessment/post/input_manual')."/".$action ?>" id="fsubmit" name="fsubmit" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                    <input type="hidden" name="id" value="<?= $RQS['RQS_ID'] ?>">
                    <input type="hidden" name="RQS_ABOUT" value="<?= $RQS['RQS_ABOUT'] ?>">
                    <input type="hidden" name="menu" id="menu" >
                    <b style="color:red">NOTA DINAS</b>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Nomor</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[ND_NUMBER]" value="<?= $RQS_L['ND_NUMBER'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Definisi BTP Berdasarkan KMI/JECFA MONOGRAPH</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <textarea class="form-control" name="RQS_L[ND_DEFINITION_BTP]" ><?= $RQS_L['ND_DEFINITION_BTP'] ?></textarea>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Karakteristik Risiko</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                       <?= form_dropdown('RQS_L[ND_RISK_CHARACTERISTICS]', array('' => '', 'ADI' => 'ADI', 'MTDI' => 'MTDI', 'PTWI' => 'PTWI', 'Lain-lain' => 'Lain-lain'), $RQS_L['ND_RISK_CHARACTERISTICS'], ' class="form-control" '); ?>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Nilai ADI/MTDI/PTWI/Lain-lain</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[ND_VALUE_ADI]" value="<?= $RQS_L['ND_VALUE_ADI'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Status BTP pada Permenkes No. 033/2012</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                       <?= form_dropdown('RQS_L[ND_STATUS_PERMENKES]', array('' => '', 'mengatur' => 'mengatur', 'tidak mengatur' => 'tidak mengatur'), $RQS_L['ND_STATUS_PERMENKES'], ' class="form-control" '); ?>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Status BTP pada Perka BTP</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                       <?= form_dropdown('RQS_L[ND_STATUS_PERKA]', array('' => '', 'mengatur' => 'mengatur', 'tidak mengatur' => 'tidak mengatur'), $RQS_L['ND_STATUS_PERKA'], ' class="form-control" '); ?>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Nilai Asumsi Konsumsi (kg/orang/hari)</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[ND_VALUE_ASSUMPTION]" value="<?= $RQS_L['ND_VALUE_ASSUMPTION'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Hasil Perhitungan Paparan Dewasa sesuai jumlah penggunaan</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[ND_ADULT_USE]" value="<?= $RQS_L['ND_ADULT_USE'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Hasil Perhitungan Paparan Anak-anak sesuai jumlah penggunaan</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[ND_CHILDREN_USE]" value="<?= $RQS_L['ND_CHILDREN_USE'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Hasil Perhitungan Paparan Dewasa sesuai BM Codex</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[ND_ADULT_CODEX]" value="<?= $RQS_L['ND_ADULT_CODEX'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Hasil Perhitungan Paparan Anak-anak sesuai BM Codex</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[ND_CHILDREN_CODEX]" value="<?= $RQS_L['ND_CHILDREN_CODEX'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Batas maksimum disetujui</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[ND_MAX_LIMIT]" value="<?= $RQS_L['ND_MAX_LIMIT'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Jenis Surat</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                       <?= form_dropdown('RQS_L[LETTER_TYPE]', array('' => '', 'setuju' => 'Persetujuan', 'tolak' => 'Penolakan'), $RQS_L['LETTER_TYPE'], ' onclick="chkkat($(this).val())" class="form-control" '); ?>
                      </div>
                    </div> 

                    <b style="color:red;display:none" class="setuju">SURAT PERSETUJUAN</b>
                    <div class="form-group setuju" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Nomor Surat</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[S_LETTER_NUMBER]" value="<?= $RQS_L['S_LETTER_NUMBER'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group setuju" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Tanggal Surat</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control datepickerx"  type="text" name="RQS_L[S_DATE]" value="<?= $RQS_L['S_DATE'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group setuju" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Lampiran</label>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[S_ATTACHMENT]" value="<?= $RQS_L['S_ATTACHMENT'] ?>" placeholder="“Jumlah berkas dalam angka” (Jumlah berkas dalam huruf)" >
                      </div>
                      <div class="col-md-1 col-sm-1 col-xs-1"> Berkas
                      </div>
                    </div> 
                    <!--<div class="form-group setuju" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Tujuan Surat</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[S_TO]" value="<?= $RQS_L['S_TO'] ?>" >
                      </div>
                    </div> -->
                    <div class="form-group setuju" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Batas maksimum disetujui</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[S_MAX_LIMIT_APPROVED]" value="<?= $RQS_L['S_MAX_LIMIT_APPROVED'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group setuju" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Nomor Lembar Persetujuan</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[S_NUMBER_APPROVED]" value="<?= $RQS_L['S_NUMBER_APPROVED'] ?>" >
                      </div>
                    </div> 

                    <?php
                    $jmldata = count($arrTxRqsBtpComp)+1;
                    if ($jmldata == 1) {
                      $jmldata = 2;
                      $arrTxRqsBtpComp[] = "";
                    }
                    $i = 1;
                    foreach ($arrTxRqsBtpComp as $key => $value) {
                      ?>

                      <div class="TX_RQS_BTP_COMP form-group row berat<?= $i; ?> setuju">
                      <?php
                      if ($i == 1) {
                        ?>
                        <label class="control-label col-md-3 col-sm-3 col-xs-3"> Spesifikasi berdasarkan KMI atau JECFA </label>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <?php
                          } else {
                            ?>
                        <div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <?php
                      }
                      ?>
                          <input type="hidden" name="TX_RQS_BTP_COMP[<?= $i ?>][RQS_ID]" value="<?= $RQS['RQS_ID'] ?>">
                          <input  class="form-control" type="text" name="TX_RQS_BTP_COMP[<?= $i ?>][SPEC_DESC]"  value="<?= $value['SPEC_DESC'] ?>">
                        </div>

                        

                        <div class="col-md-3 col-sm-3 col-xs-3">
                          <?php
                          if ($i == 1) {
                            ?>
                            <button type="button" class="btn btn-info btn-rounded btn-xs legitRipple add_ addberat" onclick="addberat();" periksa="berat" terakhir="<?= $jmldata; ?>"><i class="fa fa-plus-square"></i> Tambah</button>&nbsp;&nbsp;

                            <?php
                          } else {
                            ?>
                            <button type="button" class="btn btn-danger btn-rounded btn-xs legitRipple min_" onclick="$('.berat<?= $i; ?>').remove();return false;"><i class="fa fa-trash"></i> Hapus</button>
                            <?php
                          }
                          ?>
                        </div>
                      </div>

                      <?php
                      $i++;
                    }
                    ?>
                    <div id="hideme"></div>
<!--
                    <div class="form-group setuju" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Spesifikasi berdasarkan KMI atau JECFA</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[S_SPECIFICATION]" value="<?= $RQS_L['S_SPECIFICATION'] ?>" >
                      </div>
                    </div> 
-->
                    <div class="form-group setuju" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Sumber spesifikasi BTP</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[S_SOURCE_BTP_SPECIFICATION]" value="<?= $RQS_L['S_SOURCE_BTP_SPECIFICATION'] ?>" >
                      </div>
                    </div> 
                    <b style="color:red;display:none" class="tolak" style="display:none">SURAT PENOLAKAN</b>
                    <div class="form-group tolak" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Nomor Surat</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[T_LETTER_NUMBER]" value="<?= $RQS_L['T_LETTER_NUMBER'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group tolak" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Tanggal Surat</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control datepickerx"  type="text" name="RQS_L[T_DATE]" value="<?= $RQS_L['T_DATE'] ?>" >
                      </div>
                    </div> 
                    <div class="form-group tolak" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Lampiran</label>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[T_ATTACHMENT]" value="<?= $RQS_L['T_ATTACHMENT'] ?>" placeholder="“Jumlah berkas dalam angka” (Jumlah berkas dalam huruf)" >
                      </div>
                      <div class="col-md-1 col-sm-1 col-xs-1"> Berkas
                      </div>
                    </div> 
                    <!---<div class="form-group tolak" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Tujuan Surat</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[T_TO]" value="<?= $RQS_L['T_TO'] ?>" >
                      </div>
                    </div> -->
                    <div class="form-group tolak" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-3"> Justifikasi Penolakan</label>
                      <div class="col-md-9 col-sm-9 col-xs-9 form-group">
                        <input  class="form-control"  type="text" name="RQS_L[T_JUSTIFICATION]" value="<?= $RQS_L['T_JUSTIFICATION'] ?>" >
                      </div>
                    </div> 


                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                          <div class="notification hidden mbot-0"><div></div></div>
                          <a href="<?= site_url(); ?>assessment/list_assignment/ass"  class="btn " style="background-color: yellow" ><i class="fa fa-angle-double-left"></i> Kembali</a>
                          <button type="button" class="btn btn-info" onclick="validate('simpan');"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </form>



          <form class="form-horizontal form-label-left " action="<?= site_url('assessment/post/nodin') ?>" id="fnodin" name="fnodin" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

                <div class="form-group row">
                      <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                        <div id=""> 
                          <a href="<?= site_url(); ?>cetak/nodin/<?= $RQS['RQS_ID'] ?>"  class="btn  btn-info" ><i class="fa fa-download"></i> Download Nota Dinas & Surat Jawaban</a>
                        </div>    
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2" >
                      </div>
                </div> 
                <hr>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3">  Nota Dinas & Surat Jawaban </label>
                      <div class="col-md-1 col-sm-1 col-xs-1 form-group">

                        <!--<span class="btn btn-warning btn-sm btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Pdf</span>
                            <input class="form-control" id="fileuploadAlur" type="file" name="files" multiple >
                        </span>-->
                        <span class="btn btn-warning btn-sm btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Doc</span>
                            <input class="form-control" id="fileuploadAlur13" type="file" name="files" multiple >
                        </span>

                      </div>
                      <!--<div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokAlur"> 
                          <?= ($arrTxRqsDoc12['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkAlur" style="padding-top:20px;color:blue;" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc12['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc12['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc12['RQS_DOC_NAME'] . '</a>' : '') ?>
                        </div>    
                      </div>-->
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokAlur13"> 
                          <?= ($arrTxRqsDoc13['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkAlur13" style="padding-top:20px;color:blue;" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc13['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc13['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc13['RQS_DOC_NAME'] . '</a>' : '') ?>
                        </div>    
                      </div>
                </div> 
                <!--<div class="form-group">
                      <div class="col-md-1 col-sm-1 col-xs-1 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 ">

                        <span class="btn btn-warning btn-sm btn-sm fileinput-button" style="" ><i class="fa fa-upload"></i>
                          <span>Upload Doc</span>
                            <input class="form-control" id="fileuploadAlur13" type="file" name="files" multiple >
                        </span>

                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                        <div id="resultDokAlur13"> 
                          <?= ($arrTxRqsDoc13['RQS_DOC_NAME'] != '' ? ' &nbsp; &nbsp; <a id="linkAlur13" style="padding-top:20px;color:blue;" href="' . site_url() . "download/data/" . base64_encode($arrTxRqsDoc13['RQS_DOC_PATH']) . "/" . $arrTxRqsDoc13['RQS_DOC_NAME'] . '" target="_blank">' . $arrTxRqsDoc13['RQS_DOC_NAME'] . '</a>' : '') ?>
                        </div>    
                      </div>
                </div>--> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-3"> 
                  </label>
                      <div class="col-md-1 col-sm-1 col-xs-1 form-group">
                        <button type="" class="btn btn-success" onclick="save_post('#fnodin'); return false;"><i class="fa fa-save"></i> Simpan Hasil Upload</button>
                      </div>
                </div> 
              <input type="hidden" name="DOC[RQS_ID]" value="<?= $RQS['RQS_ID'] ?>">
              <!--<input type="hidden" id="DOC_PATHAlur" name="DOC[RQS_DOC_PATH]" value="<?= $arrTxRqsDoc12['RQS_DOC_PATH'] ?>">
              <input type="hidden" id="DOC_NAMEAlur" name="DOC[RQS_DOC_NAME]" value="<?= $arrTxRqsDoc12['RQS_DOC_NAME'] ?>">
              <input type="hidden" id="DOC_EXTAlur" name="DOC[RQS_DOC_EXT]" value="<?= $arrTxRqsDoc12['RQS_DOC_EXT'] ?>">
              <input type="hidden" id="DOC_SIZEAlur" name="DOC[RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc12['RQS_DOC_SIZE'] ?>">
              <input type="hidden" id="DOC_FILEAlur" name="DOC[RQS_DOC_FILE]" value="<?= $arrTxRqsDoc12['RQS_DOC_FILE'] ?>">
              <input type="hidden" id="DOC_CODEAlur" name="DOC[DOCUMENT_ID]" value="12">-->
            <script>
              $(function () {
                'use strict';
                $('#fileuploadAlur').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '12', format: 'pdf', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokAlur').html('<div id="progressAlur" class="progress" style="margin-top:5px;"><div id="progress-barAlur" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHAlur').val(data.result.path);
                      $('#DOC_NAMEAlur').val(data.result.name);
                      $('#DOC_EXTAlur').val(data.result.ext);
                      $('#DOC_SIZEAlur').val(data.result.size);
                      $('#DOC_FILEAlur').val(data.result.rename);
                      $('#u6').html('');
                      $('#progressAlur').fadeOut('fast', function(){
                        $('#resultDokAlur').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a></div>');
                      });
                    }else{
                      $('#progressAlur').fadeOut('fast', function(){
                        $('#resultDokAlur').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressAlur .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       
              <input type="hidden" name="DOC13[RQS_ID]" value="<?= $RQS['RQS_ID'] ?>">
              <input type="hidden" id="DOC_PATHAlur13" name="DOC13[RQS_DOC_PATH]" value="<?= $arrTxRqsDoc13['RQS_DOC_PATH'] ?>">
              <input type="hidden" id="DOC_NAMEAlur13" name="DOC13[RQS_DOC_NAME]" value="<?= $arrTxRqsDoc13['RQS_DOC_NAME'] ?>">
              <input type="hidden" id="DOC_EXTAlur13" name="DOC13[RQS_DOC_EXT]" value="<?= $arrTxRqsDoc13['RQS_DOC_EXT'] ?>">
              <input type="hidden" id="DOC_SIZEAlur13" name="DOC13[RQS_DOC_SIZE]" value="<?= $arrTxRqsDoc13['RQS_DOC_SIZE'] ?>">
              <input type="hidden" id="DOC_FILEAlur13" name="DOC13[RQS_DOC_FILE]" value="<?= $arrTxRqsDoc13['RQS_DOC_FILE'] ?>">
              <input type="hidden" id="DOC_CODEAlur13" name="DOC13[DOCUMENT_ID]" value="13">
            <script>
              $(function () {
                'use strict';
                $('#fileuploadAlur13').fileupload({
                  url: '<?= site_url() ?>home/upload/<?= $user_id ?>',
                  dataType: 'json',
                  formData: {action: 'upload', kdDok: '13', format: 'docx;doc', maxsize : '5242880', rqs_id : '<?= $rqs_id ?>'},
                  start: function (e) {
                    $('#resultDokAlur13').html('<div id="progressAlur13" class="progress" style="margin-top:5px;"><div id="progress-barAlur13" class="progress-bar progress-bar-success"></div></div>');
                  },
                  done: function (e, data) {
                    if(data.result.status=='true'){
                      $('#DOC_PATHAlur13').val(data.result.path);
                      $('#DOC_NAMEAlur13').val(data.result.name);
                      $('#DOC_EXTAlur13').val(data.result.ext);
                      $('#DOC_SIZEAlur13').val(data.result.size);
                      $('#DOC_FILEAlur13').val(data.result.rename);
                      $('#u6').html('');
                      $('#progressAlur13').fadeOut('fast', function(){
                        $('#resultDokAlur13').html('<div style="padding:0px 0px 20px 0px;margin-left:20px"><a style="color:blue;" href="'+data.result.urll+'" target="_blank">'+data.result.name+'</a></div>');
                      });
                    }else{
                      $('#progressAlur13').fadeOut('fast', function(){
                        $('#resultDokAlur13').html('<span style="color:#FF0000; font-size:11px; padding-top:20px;"><b>'+data.result.error+'</b></span>');
                      });                                
                    }
                  },
                  progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressAlur13 .progress-bar').css(
                      'width',
                      progress + '%'
                      );
                  }
                }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
              });                    
            </script>                                       


                  </div>
                </div>
              </div>
            </div>
          </form>




        </div>
    </div>
  <!-- /.row -->
  </div>
  <!-- /.container -->
 </section>
<script>
    $(document).ready(function () {

        var jenis_surat = "<?= $RQS_L['LETTER_TYPE']; ?>";
        if(jenis_surat == 'setuju'){ 
          $('.setuju').show(500).removeAttr('disabled');
        }else if(jenis_surat == 'tolak'){
          $('.tolak').show(500).removeAttr('disabled');
        }
  
    });


  function validate(menu){
      var menu = $('#menu').val(menu);
      tinyMCE.triggerSave();
      save_post('#fsubmit');
      return false;     
  }
  
  function chkkat(kat) {
      if(kat == 'setuju'){
        $('.setuju').show(500).removeAttr('disabled');
        $('.tolak').hide(500).attr('disabled', 'disabled');
      }else if(kat == 'tolak'){
        $('.tolak').show(500).removeAttr('disabled');
        $('.setuju').hide(500).attr('disabled', 'disabled');
      }else{
        $('.setuju').hide(500).attr('disabled', 'disabled');
        $('.tolak').hide(500).attr('disabled', 'disabled');        
      }
  }
</script>
