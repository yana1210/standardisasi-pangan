<script>
var site_url = '<?= site_url(); ?>';
</script>
<style>
  .red{
    color:red;
  }
  #scroll {
    border: 1px solid black;
    height: 400px;
    overflow: scroll;
  }
</style>
<section id="page-header" class="page-section">
  <hr>
</section>

<?php //print_r($_SESSION) ; echo $this->newsession->userdata('USER_TYPE_ID'); ?>
<section id="page-content">
  <div class="container">
    <div class="row">
      <form class="form-horizontal form-label-left " action="<?= site_url('assessment/post/xprt-an-req') ?>" id="fsubmit" name="fsubmit" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $RQS['RQS_ID'] ?>">
        <input type="hidden" name="RQS_ABOUT" value="<?= $RQS['RQS_ABOUT'] ?>">
        <input type="hidden" name="menu" id="menu" >
        <div class="col-md-12">
          <h6 class="text-uppercase">ASSESSMENT >> ASSIGNMENT >> DAFTAR ASSIGNMENT >> DETAIL ASSIGNMENT >> <span style="color: blue">REKOMENDASI PAKAR </span> </h6>
          <hr>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-3"> Rekomendasi Pakar</label>
            <div class="col-md-9 col-sm-9 col-xs-9 form-group">
              <textarea class="resizable_textarea form-control" rows="3"  style="width:87%;" name="TX_XPRT_NOTE[XPRT_REC_NOTE]" required><?= $RQS['XPRT_REC_NOTE'] ?></textarea>
            </div>
          </div> 
          <br/>


          <br/> 

          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                <div class="notification hidden mbot-0"><div></div></div>
                <a href="<?= site_url(); ?>assessment/ass/codex/<?= $RQS['RQS_ID'] ?>"  class="btn " style="background-color: yellow" > Kembali</a>
                <input type="button" value="Simpan" class="btn btn-info" onclick="validate('simpan');" >
                <input type="button" value="Setujui" class="btn btn-success " onclick="validate('setujui');" >
                <input type="button" value="Tolak" class="btn btn-danger" onclick="validate('tolak');">
              </div>
            </div>
          </div>


        </div>
      </form>
    </div>
  <!-- /.row -->
  </div>
  <!-- /.container -->
 </section>
<script>
  function validate(menu){
      var menu = $('#menu').val(menu);
      save_post('#fsubmit');
      return false;     
  }
  
</script>