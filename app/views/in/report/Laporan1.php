<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); error_reporting(E_ERROR); 
	function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$explo = explode(' ', $tanggal);
		$split = explode('-', $explo[0]);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}

?>
<style>
table.xcel{text-align: left;border:1px solid #000; border-collapse:collapse; color:#000; font-family: "lucida grande", Tahoma, verdana, arial, sans-serif; font-size:10px; direction:ltr;}
table.xcelheader{text-align: left; border-collapse:collapse; color:#000; font-family: "lucida grande", Tahoma, verdana, arial, sans-serif; font-size:10px; direction:ltr;}
</style>
<table width="100%">
	  <tr>
		<td colspan="26" align="center"><h4>Laporan <?= $a ?> </h4></td>
	  </tr>
</table>
<br />
<table class="xcel" width="100%">
  <tr>
    <th colspan="8" style="border:1px solid #000; border-collapse:collapse; background:#fff96c;">Identitas Perusahaan</th>
    <th colspan="7" style="border:1px solid #000; border-collapse:collapse; background:#6cffa7;">Identitas Permohonan</th>
    <th colspan="4" style="border:1px solid #000; border-collapse:collapse; background:#ff6c6c;">Identitas Petugas</th>
    <th rowspan="2" style="border:1px solid #000; border-collapse:collapse; background:#43ff01;">Status Kesulitan</th>
    <th colspan="5" style="border:1px solid #000; border-collapse:collapse; background:#ffd66c;">Status Berkas Pengkajian</th>
	<th rowspan="2" style="border:1px solid #000; border-collapse:collapse; background:#faff01;">Lama Waktu Penyelesaian Permohonan dari Tanggal Verifikasi - Tanggal Surat Jawaban (hari)</th>
  </tr>
  <tr>
    <th style="border:1px solid #000; border-collapse:collapse; background:#fff96c;">ID Perusahaan</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#fff96c;">Nama Perusahaan</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#fff96c;">Alamat Perusahaan</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#fff96c;">No Tlp</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#fff96c;">No Fax</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#fff96c;">Email</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#fff96c;">Status</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#fff96c;">Skala Industri</th>	
	
	<th style="border:1px solid #000; border-collapse:collapse; background:#6cffa7;">No Izin</th> 
    <th style="border:1px solid #000; border-collapse:collapse; background:#6cffa7;">Nama Jenis BTP</th>    
	<th style="border:1px solid #000; border-collapse:collapse; background:#6cffa7;">No. INS</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#6cffa7;">Jenis Pangan</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#6cffa7;">No. Kategori Pangan</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#6cffa7;">Jumlah Penggunaan</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#6cffa7;">Tanggal Submit</th>

    <th style="border:1px solid #000; border-collapse:collapse; background:#ff6c6c;">Nama Verifikator</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#ff6c6c;">Tanggal Selesai Verifikasi</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#ff6c6c;">Nama Asesor</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#ff6c6c;">Tanggal Selesai Pengkajian</th>


    <th style="border:1px solid #000; border-collapse:collapse; background:#ffd66c;">Status</th>
	<th style="border:1px solid #000; border-collapse:collapse; background:#ffd66c;">Batas Maksimum Jika Disetujui</th> 
    <th style="border:1px solid #000; border-collapse:collapse; background:#ffd66c;">Tanggal Surat Jawaban</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#ffd66c;">Nomor Surat Jawaban</th>
    <th style="border:1px solid #000; border-collapse:collapse; background:#ffd66c;">Nomor Lembar Persetujuan</th>

  </tr>
  <?php 
  if(count($baris) > 0){
	  $no = 1;
	  for($i=0; $i<count($baris); $i++){ 
	  
	  $nama = $baris[$i]['TRADER_TYPE']." ".$baris[$i]['USER_NAME'];
	  ?>
	  <tr>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['USER_ID']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $nama; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['USER_ADDR']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['USER_TELP']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['USER_FAX']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['USER_MAIL']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['status_p']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['skala_p']; ?></td>

        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['RQS_NO_AJU']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['BTP_TYPE_NAME']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['BTP_TYPE_NO']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['FOOD_TYPE_NAME']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['FOOD_TYPE_NO']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['RQS_BTP_NETTO']." ".$baris[$i]['RQS_BTP_UNIT']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo tanggal_indo($baris[$i]['RQS_CREATE_DATE']); ?></td>

        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['VERIFIKATOR']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo tanggal_indo($baris[$i]['RQS_VER_DATE']); ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['ASESOR']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo tanggal_indo($baris[$i]['RQS_ASS_DATE']); ?></td>

		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['RQS_LEVEL']; ?></td>
<?php if($baris[$i]['STATUS'] == 'Ditolak'){ $date = $baris[$i]['T_DATE']; $selisih = $baris[$i]['selisih_t']; }else{ $date = $baris[$i]['S_DATE']; $selisih = $baris[$i]['selisih_s']; } ?>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['STATUS']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['S_MAX_LIMIT_APPROVED']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo tanggal_indo($date); ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['S_LETTER_NUMBER']; ?></td>
        <td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $baris[$i]['S_NUMBER_APPROVED']; ?></td>
		<td valign="top" style="border:1px solid #000; border-collapse:collapse;"><?php echo $selisih; ?></td>
	  </tr>
	  <?php
	  $no++;
	  }
  }else{
  ?>
  <tr>
    <td valign="top" style="border:1px solid #000; border-collapse:collapse;" colspan="26">Data Tidak Ditemukan</td>
  </tr>
  <?php } ?>
	  <tr>
	  	<td></td>
	  </tr>
	  <tr>
	  	<td></td>
	  </tr>
	  <tr>
	  	<td></td>
	  </tr>
	  <tr>
	  	<td></td>
	  </tr>
	  <tr>
		<td style="border:1px solid #000;">Jumlah berkas yang ditolak/ disetujui</td>
		<td style="border:1px solid #000;"><?php echo $count; ?></td>
	  </tr>
 
</table>
