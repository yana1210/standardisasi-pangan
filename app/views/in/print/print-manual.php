<html>
	<head>
		<title>Badan Pengawas Obat dan Makanan Republik Indonesia</title>
		<link rel="Shortcut Icon" href="<?= base_url(); ?>assets/in/images/logo_bpom.gif" type="image/x-icon" />
		<style type="text/css" media="print">
		  	.block-temuan{
				page-break-after: always;
			}

			.temuan{
				page-break-inside: avoid;
			}

			.MsoNormalTable{
				border: 1;
			}

			p{
				padding: 5px;
				margin-top: 0px;
			}
		</style>
	</head>
	<body>
		<div style="padding-top: 150px; text-align: center; font-family: 'Times New Roman', Times, serif; margin-bottom: 180px;">
			<p style="line-height: 1.5;">
				<h1>
					TEMUAN
				</h1>
				<h1>
					AUDIT OPERASIONAL
				</h1>
				<h1>
					<?= strtoupper($auditi) . ' DI ' . strtoupper($kota)?>
				</h1>
				<h2> <?=strtoupper($tujuan_audit);?> </h2>
			</p>
		</div>
		<div style="text-align: right; padding-right: 5%;">
			<h2><span style="font-size: 50px;">I</span>NSPEKTORAT BADAN POM </h2>
		</div>

		<pagebreak />

		<div class="page2" style="margin-bottom: 6%;">
			<table align="center" style="width:100%;border:0px solid;">
				<tr>
					<td align="center" style="width:100%; border-bottom:0px solid; vertical-align: top;"> 
						<h3> TEMUAN AUDIT OPERASIONAL PADA <?= strtoupper($auditi) . ' DI ' . strtoupper($kota)?> <br><?=strtoupper($tujuan_audit);?> </h3> 
					</td>
				</tr>
			</table>
		</div>
		
		<?php 
			$b = count($arrTemuan);
			$no = 1;
			for($a=0; $a<$b; $a++){
		?>
			<div class="block-temuan">
				<div class="temuan" style="margin-bottom:2%;">
					<b> Temuan <?=$no;?></b>
					<?= $arrTemuan[$a]['TA_DTL_FINDING']; ?>
				</div> 
			
				<div class="temuan" style="margin-bottom:2%;">
					<b> Kondisi : </b>
					 <?= $arrTemuan[$a]['TA_DTL_CONDITION']; ?>
				</div>
				
				<div class="temuan" style="margin-bottom:2%;">
					<b> Kriteria : </b> 
					<?= $arrTemuan[$a]['TA_DTL_CRITERIA']; ?> 
				</div>
			
				<div class="temuan" style="margin-bottom:2%;">
					<b> Sebab : </b> 
					<?= $arrTemuan[$a]['TA_DTL_CAUSE']; ?> 
				</div>
		
				<div class="temuan" style="margin-bottom:2%;">
					<b> Akibat : </b> 
					<?= $arrTemuan[$a]['TA_DTL_EFFECT']; ?>
				</div>
				
				<div class="temuan" style="margin-bottom:2%;">
					<b>Rekomendasi : </b>
					<?= $arrTemuan[$a]['TA_DTL_RECOMENDATION']; ?>
				</div>

				<div class="temuan" style="margin-bottom:5%;">
					<b> Tanggapan Kepala <?= $auditi . ' di ' . $kota; ?> : </b><br />
					<?= $arrTemuan[$a]['TA_DTL_RESPONSE']; ?>
				</div>
			</div>

		<?php 
			$no++;
			}
		?>	
	
	</body>		
</html>