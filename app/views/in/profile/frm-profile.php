<!-- <?php print_r($arrdata['apptype']);?> -->
<script>
var site_url = '<?= site_url(); ?>';
</script>

<style>
  .red{
    color:red;
  }
  #scroll {
    border: 1px solid black;
    height: 400px;
    overflow: scroll;
  }
</style>
<section id="page-header" class="page-section">
  <hr>
</section>

<?php //print_r($_SESSION) ; echo $this->newsession->userdata('USER_TYPE_ID'); ?>
<section id="page-content">
  <div class="container">
    <div class="row">
      <form class="form-horizontal form-label-left " action="<?= $act ?>" id="fprofile" name="fprofile" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $_SESSION['USER_ID'] ?>">
        <div class="col-md-12">
          <h6 class="text-uppercase"> PROFILE >>  <span style="color: blue"> UBAH DATA PROFILE </span> </h6>
          <hr>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> <span style="color: blue"> FORM UBAH DATA PROFILE </span></label>
          </div>
          <br/>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nama Lengkap </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" wajib="yes" class="form-control" name="PROFILE[USER_NAME]" value="<?= $dtprofile['USER_NAME'] ?>">
              </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> NPWP </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" wajib="yes" class="form-control" disabled="disabled" name="PROFILE[USER_NPWP]" value="<?= $dtprofile['USER_NPWP'] ?>">
              </div>
          </div>
<!--
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Status </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" wajib="yes" class="form-control" name="PROFILE[USER_TYPE_ID]" value="<?= $dtprofile['STATUS'] ?>">
              </div>
          </div>
-->
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Alamat </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" wajib="yes" class="form-control" name="PROFILE[USER_ADDR]" value="<?= $dtprofile['USER_ADDR'] ?>">
              </div>
          </div>

           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No. Telp </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" wajib="yes" class="form-control" name="PROFILE[USER_TELP]" value="<?= $dtprofile['USER_TELP'] ?>">
              </div>
          </div>

           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> No. Fax </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" wajib="yes" class="form-control" name="PROFILE[USER_FAX]" value="<?= $dtprofile['USER_FAX'] ?>">
              </div>
          </div>

           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Email Perusahaan</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" wajib="yes" class="form-control" id="USER_MAIL" name="PROFILE[USER_MAIL]" value="<?= $dtprofile['USER_MAIL'] ?>">
              </div>
          </div>
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Email Penanggung jawab</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" wajib="yes" class="form-control" id="" name="tm_user_pic[USER_PIC_MAIL]" value="<?= $dtprofile['USER_PIC_MAIL'] ?>">
              </div>
          </div>
          <br/>
          <hr/>
          <br/>

          <div class="col-md-12">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                <div class="notification hidden mbot-0"><div></div></div>
                <input type="button" value="Reset" class="btn btn-danger" onclick="reset();">
                <!--<a href="<?= site_url(); ?>register/new_reg/frm-reg-pic"  class="btn btn-success">Lanjut</a>-->
                <input type="button" value="Simpan" id="simpan" class="btn btn-success" onclick="save_post('#fprofile'); return false;">
               <!--   <input type="button" value="Lanjut" id="simpan" class="btn btn-success" onclick="emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; if(!emailReg.test($('#USER_MAIL').val()) ) { alert('Format Email Salah !'); }else{ save_post('#fprofile'); return false;}"> -->
              </div>
            </div>
         </div>
      </form>
    </div>
  <!-- /.row -->
  </div>
  <!-- /.container -->
 </section>


<script type="text/javascript">
       
	$('.keyup-email').change(function() {
      $('span.error-keyup-7').remove();
      var inputVal = $(this).val();
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      if(!emailReg.test(inputVal)) {  
        $(this).after('<span style="color:red" class="error error-keyup-7">Format Email Salah</span>');
      }
    });

     $('.keyup-numeric').keyup(function() {
      $('span.error-keyup-1').remove();
      var inputVal = $(this).val();
      var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
      if(!numericReg.test(inputVal)) {  
        $(this).after('<span style="color:red" class="error error-keyup-1">Hanya Karakter Angka</span>');
      }
    });

</script>


