<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Newtable {
	var $rows				= array();
	var $columns			= array();
	var $hiderows			= array();
	var $keys				= array();
	var $proses				= array();
	var $keycari			= array();
	var $heading			= array();
	var $width				= array();
	var $auto_heading		= TRUE;
	var $show_chk			= TRUE;
	var $use_where			= FALSE;
	var $caption			= NULL;	
	var $template 			= NULL;
	var $newline			= "";
	var $empty_cells		= "&nbsp;";
	var $actions			= "";
	var $detils				= "";
	var $baris				= "AUTO";
	var $db 				= "";
	var $hal 				= "AUTO";
	var $uri				= "";
	var $js_file			= "";
	var $show_search		= TRUE;
	var $use_ajax			= FALSE;
	var $orderby			= 1;
	var $sortby				= "ASC";
	
	function Newtable()
	{
		$this->hiderows[] = 'HAL';
	}
	
	function width($row)
	{
		$this->width = $row;
		return;
	}
	
	function js_file($file)
	{
		$this->js_file = $file;
		return;
	}
	
	function show_search($show)
	{
		$this->show_search = $show;
		return;
	}
	
	function show_chk($show)
	{
		$this->show_chk = $show;
		return;
	}
	
	function use_ajax($use)
	{
		$this->use_ajax = $use;
		return;
	}
	
	function use_where($use)
	{
		$this->use_where = $use;
		return;
	}
	
	function columns($col)
	{
		$this->columns = $col;
		return;
	}
	
	function orderby($order)
	{
		$this->orderby = $order;
		return;
	}
	
	function sortby($sort)
	{
		$this->sortby = $sort;
		return;
	}
	
	function topage($to)
	{
		$this->hal = (int)$to;
		return;
	}
	
	function cidb($db)
	{
		$this->db = $db;
		return;
	}
	
	function rowcount($row)
	{
		$this->baris = $row;
		return;
	}
	
	function ciuri($uri)
	{
		$this->uri = $uri;
		return;
	}
	
	function action($act)
	{
		$this->actions = $act;
		return;
	}
	
	function detail($act)
	{
		$this->detils = $act;
		return;
	}
	
	function hiddens($row)
	{
		if ( ! is_array($row))
		{
			$row = array($row);
		}
		foreach ( $row as $a )
		{
			if ( ! in_array($a, $this->hiderows) ) $this->hiderows[] = $a;
		}
		return;
	}
	
	function keys($row)
	{
		if ( ! is_array($row))
		{
			$row = array($row);
		}
		foreach ( $row as $a )
		{
			if ( ! in_array($a, $this->keys) ) $this->keys[] = $a;
		}
		return;
	}
	
	function menu($row)
	{
		if ( ! is_array($row))
		{
			return FALSE;
		}
		$this->proses = $row;
		return;
	}
	
	function search($row)
	{
		if ( ! is_array($row))
		{
			return FALSE;
		}
		$this->keycari = $row;
		return;
	}
	
	function set_template($template)
	{
		if ( ! is_array($template)) return FALSE;
		$this->template = $template;
	}
	
	function set_heading()
	{
		$args = func_get_args();
		$this->heading = (is_array($args[0])) ? $args[0] : $args;
	}
	
	function make_columns($array = array(), $col_limit = 0)
	{
		if ( ! is_array($array) OR count($array) == 0) return FALSE;
		$this->auto_heading = FALSE;
		if ($col_limit == 0) return $array;
		$new = array();
		while(count($array) > 0)
		{	
			$temp = array_splice($array, 0, $col_limit);
			if (count($temp) < $col_limit)
			{
				for ($i = count($temp); $i < $col_limit; $i++)
				{
					$temp[] = '&nbsp;';
				}
			}
			$new[] = $temp;
		}
		return $new;
	}

	function set_empty($value)
	{
		$this->empty_cells = $value;
	}
	
	function add_row()
	{
		$args = func_get_args();
		$this->rows[] = (is_array($args[0])) ? $args[0] : $args;
	}

	function set_caption($caption)
	{
		$this->caption = $caption;
	}	

	function generate($table_data = NULL)
	{
		if ( ! is_null($table_data))
		{
			if (is_object($table_data))
			{
				$this->_set_from_object($table_data);
			}
			elseif (is_array($table_data))
			{
				$set_heading = (count($this->heading) == 0 AND $this->auto_heading == FALSE) ? FALSE : TRUE;
				$this->_set_from_array($table_data, $set_heading);
			}
			elseif ($table_data!="")
			{
				if ( $this->db == "" || !is_array($this->uri) ) return 'Missing required params (db & uri)';
				if ( ($this->db->dbdriver=='mssql' || $this->db->dbdriver=='sqlsrv') && !is_array($this->columns) )  return 'Missing required params (columns)';
				$kunci = "";
				$terkunci = "";
				$cari = "";
				$tercari = "";
				if ( $key = array_search('search', $this->uri) )
				{
					$kunci = (int)$this->uri[$key+1];
					if ( array_key_exists($kunci, $this->keycari))
					{
						//print_r($this->keycari);
						$terkunci = $this->keycari[$kunci];
						$terkunci = $terkunci[0];
						$cari = $this->uri[$key+2];
						if ($cari != "")
						{
							$cari = str_replace("'", "''", $cari);
							if(substr($terkunci, 0, 4)=="{IN}"){
								$terkunci = substr($terkunci, 4);
								$tercari = str_replace("{LIKE}", "LIKE '%$cari%'", $terkunci);
							}else{
								$tercari = "$terkunci LIKE '%$cari%'";
							}
						}
					}
				}
				if ( $this->baris != "ALL")
				{
					if ( $key = array_search('row', $this->uri) ) $this->baris = (int)$this->uri[$key+1];
					if ( $this->baris<1 ) $this->baris = 15;
					//if ( $this->baris > 100) $this->baris = 100;
				}	
				if ($tercari!="")
				{
					if ( $this->use_where )
					{
						$table_data .= " WHERE $tercari";
					}
					else
					{
						$ada = strpos(strtolower($table_data), "where");
						if ( $ada === false )
							$table_data .= " WHERE $tercari";
						else
							$table_data .= " AND $tercari";
					}
				}
				#die($table_data);
				$total_record = 0;
				$table_count = $this->db->query("SELECT COUNT(*) AS JML FROM ($table_data) AS TBL");
				if ( $table_count )
				{
					$table_count = $table_count->row();
					$total_record = $table_count = $table_count->JML;
				}
				else
				{
					$total_record = 0;
				}
				#print($total_record); die();
				if ( $this->baris != "ALL")
				{
					$table_count = ceil($table_count / $this->baris);
					if ( $this->hal == "AUTO" ) if ( $key = array_search('page', $this->uri) ) $this->hal = (int)$this->uri[$key+1];
					if ( $this->hal < 1 ) $this->hal = 1;
					if ( $this->hal > $table_count ) $this->hal = $table_count;
					if ( $this->hal==1 )
					{
						if ($this->db->dbdriver=='mssql' || $this->db->dbdriver=='sqlsrv')
						{
							$dari = $this->hal;
							$sampai = $this->baris;
						}
						else
						{
							$dari = 0;
							$sampai = $this->baris;
						}
					}
					else
					{
						if ($this->db->dbdriver=='mssql' || $this->db->dbdriver=='sqlsrv')
						{
							$dari = ($this->hal * $this->baris) - $this->baris + 1;
							$sampai = $this->hal * $this->baris;
						}else{
							$dari = $this->hal>0?($this->hal-1) * $this->baris:0;
							$sampai = $this->baris;
						}
					}
					if ($key = array_search('order', $this->uri))
					{
						$this->orderby = (int)$this->uri[$key+1];
						$this->sortby = $this->uri[$key+2];
						#if ($this->show_chk) $orderby = $this->columns[$this->orderby-1];
						#else $orderby = $this->columns[$this->orderby];
						if ($this->db->dbdriver=='mssql' || $this->db->dbdriver=='sqlsrv')
						{
							$orderby = $this->columns[$this->orderby-1];
							if ( is_array($orderby) ) $orderby = $orderby[0];
						}
						else
						{
							$orderby = $this->orderby;
						}
					}
					else
					{
						if ( is_numeric($this->orderby) )
						{
							if ($this->db->dbdriver=='mssql' || $this->db->dbdriver=='sqlsrv')
							{
								$orderby = $this->columns[$this->orderby-1];
								if ( is_array($orderby) ) $orderby = $orderby[0];
							}
							else
							{
								$orderby = $this->orderby;
							}
						}
						else
						{
							$orderby = $this->orderby;
						}
					}
					if ($this->db->dbdriver=='mssql' || $this->db->dbdriver=='sqlsrv')
						$table_data = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY $orderby $this->sortby) AS HAL, ".substr($table_data, 6)." ) AS TBLTMP WHERE HAL >= $dari AND HAL <= $sampai";
					else
						$table_data = "$table_data ORDER BY $orderby $this->sortby LIMIT $dari, $sampai";
				}
				#die($table_data);
				$table_data = $this->db->query($table_data);
				$this->_set_from_object($table_data);
				#print_r($table_data); die();
			}
		}
	
		if (count($this->heading) == 0 AND count($this->rows) == 0)
		{
			return 'Undefined table data';
		}
		$ajax = array_search('row', $this->uri);
		if($ajax){
			if($this->uri[$ajax-1]=="ajax"){
				if($this->use_ajax==TRUE) $ajax = 1;
				else $ajax = 0;
			}else{
				$ajax = 0;
			}
		}else{
			$ajax = 0;
		}
		$this->_compile_template();
		if($ajax==0)
		{
			/*$out = '<div id="divtabelajax">Loading..</div>';
			$out .= "<script>$(document).ready(function(){
setTimeout(function(){ $('#divtabelajax').load('".$this->actions."'); }, 2000);
});</script>";
			return $out;*/
			$out = '<div id="divtabelajax">';
		}
		else
		{
			$out = $this->js_file;
		}
		if ($this->show_search || $this->show_chk)
		{
			$out .= '<form id="tb_form" action="'.$this->actions.'">';
			$out .= $this->template['table_open'].'<tr class="title mtop"><th colspan="'.count($this->heading).'">';
		}
		else
		{
			$out .= '<form id="tb_form" action="'.$this->actions.'">';
			$out .= $this->template['table_open'];
		}
		if (count($this->proses) > 0 && $this->show_chk)
		{
			$out .= '<select id="tb_menu" title="Pilih proses yang akan dijalankan"><option url="">Pilih Proses</option>';
			foreach ($this->proses as $a => $b)
			{
				$out .= '<option met="';
				$out .= $b[0];
				$out .= '" jml="';
				$out .= $b[2];
				$out .= '" url="';
				$out .= $b[1];
				$out .= '">';
				$out .= "- $a";
				$out .= '</option>';
			}
			$out .= '</select> &nbsp;<label id="labelload">Loading..</label>';
		}
		if ($this->show_search)
		{
			if (count($this->proses) > 0 && $this->show_chk) $out .= '<span class="right">Cari <select id="tb_keycari" title="Pilih kategori yang akan dicari">';
			else $out .= '<span>Cari <select id="tb_keycari" title="Pilih kategori yang akan dicari">';
			foreach ($this->keycari as $a => $b)
			{
				if ( $kunci==$a )
					$out .= '<option selected value="';
				else
					$out .= '<option value="';
				$out .= $a;
				$out .= '">';
				$out .= $b[1];
				$out .= '</option>';
			}
			$out .= '</select> <input type="text" class="tb_text" id="tb_cari" title="Masukkan kata kunci yang ingin dicari kemudian tekan Enter" value="'.$cari.'" /></span>';
		}
		if ($this->show_search || $this->show_chk)
		{
			$out .= '</th></tr>';
		}
		if ($this->caption)
		{
			#$out .= $this->newline;
			$out .= '<caption>' . $this->caption . '</caption>';
			#$out .= $this->newline;
		}
		if (count($this->heading) > 0)
		{
			$out .= '<tr class="head">';
			$out .= $this->newline;
			foreach($this->heading as $z => $heading)
			{
				if ($this->db->dbdriver=='mssql' || $this->db->dbdriver=='sqlsrv')
				{
					if( $this->show_chk ) $z--;
				}
				else
				{
					if( !$this->show_chk ) $z++;
				}
				#echo "$z => $heading<hr>";
				if ( ! in_array($heading, $this->hiderows))
				{
					if ( (($this->db->dbdriver=='mssql' || $this->db->dbdriver=='sqlsrv') && $z < 0 && $this->show_chk) || ($z==0 && $this->show_chk) ){
						$out .= '<th width="13">';
						$out .= $heading;
					}else{
						if ( array_key_exists($heading, $this->width) ) $out .= '<th width="'.$this->width[$heading].'">';
						else $out .= "<th>";
						if ( $this->baris != "ALL")
						{
							if ( $z==$this->orderby ){
								if ( $this->sortby=="ASC" )
									$out .= "<span class=\"order\" title=\"Urutkan Data berdasarkan ".$heading." (Z-A)\" orderby=\"$z\" sortby=\"DESC\">$heading</span>";
								else
									$out .= "<span class=\"order\" title=\"Urutkan Data berdasarkan ".$heading." (A-Z)\" orderby=\"$z\" sortby=\"ASC\">$heading</span>";
							}else{
								$out .= "<span class=\"order\" title=\"Urutkan Data berdasarkan ".$heading." (A-Z)\" orderby=\"$z\" sortby=\"ASC\">$heading</span>";
							}
						}
						else
						{
							$out .= "<span class=\"order\" orderby=\"$z\" sortby=\"ASC\">$heading</span>";
						}
					}
					$out .= $this->template['heading_cell_end'];
				}
			}

			$out .= $this->template['heading_row_end'];
			#$out .= $this->newline;
		}
		if (count($this->rows) > 0)
		{
			$i = 1;
			foreach($this->rows as $row)
			{
				if ( ! is_array($row))
				{
					break;
				}
				
				$keyz = "";
				$koma = "";
				foreach ($this->keys as $a)
				{
					$keyz .= $koma.$row[$a];
					$koma = ".";
				}
				$name = (fmod($i++, 2)) ? '' : 'alt_';
			
				//$out .= $this->template['row_'.$name.'start'];
				if($i%2>0) $cls = 'class="odd"';
				else $cls = "";
				if ($this->detils=="")
				{
					$out .= '<tr urldetil="">';
				}
				else
				{
					if ($this->show_chk)
						$out .= '<tr title="Klik Kanan untuk menampilkan detil data" urldetil="/'.$keyz.'">';
					else
						$out .= '<tr title="Klik untuk menampilkan detil data" urldetil="/'.$keyz.'">';
				}
				$out .= $this->newline;
				if ($this->show_chk) $out .= '<td '.$cls.'><input type="checkbox" name="tb_chk[]" class="tb_chk" value="'.$keyz.'"/></td>';
				$seq = -1;
				foreach($row as $rowz => $cell)
				{
					if ( !in_array($rowz, $this->hiderows))
					{
						$out .= "<td $cls>";
						if ($cell === "")
						{
							$out .= $this->empty_cells;
						}
						else
						{
							$url_col = $this->columns[$seq];
							if ( is_array($url_col) )
							{
								$new_url_col = $url_col[1];
								$url_col = explode("{", $new_url_col);
								foreach($url_col as $x){
									$temp_url_col = explode("}", $x);
									$temp_url_col = $temp_url_col[0];
									$new_url_col = str_replace("{".$temp_url_col."}", $row[$temp_url_col], $new_url_col);
								}
								$out .= '<a href="'.$new_url_col.'">'.$cell.'</a>';
							}
							else
							{
								$out .= $cell;
							}
						}
						$out .= $this->template['cell_'.$name.'end'];
					}
					$seq++;
				}
				$out .= $this->template['row_'.$name.'end'];
				#$out .= $this->newline;	
			}
		}
		else
		{
			$out .= '<tr><td colspan="'.count($this->heading).'">Data Tidak Ditemukan</td></tr>';
		}
		if ( $this->baris != "ALL")
		{
			$datast = ($this->hal - 1);
			if ( $datast<1 ) $datast = 1;
			else $datast = $datast * $this->baris + 1;
			$dataen = $datast + $this->baris - 1;
			if ( $total_record < $dataen ) $dataen = $total_record;
			if ( $total_record==0 ) $datast = 0;
			$out .= '<tr class="title mbottom"><th colspan="'.count($this->heading).'"><input type="text" class="tb_text" id="tb_view" title="Masukkan jumlah data yang ingin ditampilkan kemudian tekan Enter" value="'.$this->baris.'" /> Data Per-Halaman. Menampilkan '.$datast.' - '.$dataen.' Dari '.$total_record.' Data.<span class="right"><span class="nav">&laquo;</span>&nbsp; Halaman <input type="text" class="tb_text" id="tb_hal" value="'.$this->hal.'" /> Dari <span id="tb_total">'.$table_count.'</span> &nbsp;<span class="nav">&raquo;</span></span></th></tr>';
		}
		else
		{
			$out .= '<tr class="title mbottom"><th colspan="'.count($this->heading).'">Total '.$total_record.' Data.</th></tr>';
		}
		$out .= $this->template['table_close'];
		$out .= '<input type="hidden" id="orderby" value="'.$this->orderby.'"><input type="hidden" id="sortby" value="'.$this->sortby.'"><input type="hidden" id="useajax" value="'.($this->use_ajax==TRUE?'TRUE':'FALSE').'">';
		if ($this->detils!="") $out .= '<input type="hidden" id="urldtl" value="'.$this->detils.'">';
		if($ajax==0){
			#$out .= '<input type="hidden" id="refresh" value="1"></form>';
			return $out.'</form></div>';
		}else{
			#$out .= '<input type="hidden" id="refresh" value="0"></form>';
			echo $out.'</form>'; die();
		}
	}
	
	function clear()
	{
		$this->rows				= array();
		$this->heading			= array();
		$this->auto_heading		= TRUE;	
	}
	
	function _set_from_object($query)
	{
		if ( ! is_object($query))
		{
			return FALSE;
		}
		
		if (count($this->heading) == 0)
		{
			if ( ! method_exists($query, 'list_fields'))
			{
				return FALSE;
			}
			empty($this->heading);
			if ( $this->show_chk ) $this->heading[] = '<input type="checkbox" id="tb_chkall" />';
			foreach ($query->list_fields() as $a)
			{
				//if ( ! in_array($a, $this->hiderows)) $this->heading[] = $a;
				$this->heading[] = $a;
			}
			//print_r($this->heading);
		}
		
		
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$this->rows[] = $row;
			}
		}
	}

	function _set_from_array($data, $set_heading = TRUE)
	{
		if ( ! is_array($data) OR count($data) == 0)
		{
			return FALSE;
		}
		
		$i = 0;
		foreach ($data as $row)
		{
			if ( ! is_array($row))
			{
				$this->rows[] = $data;
				break;
			}
						
			if ($i == 0 AND count($data) > 1 AND count($this->heading) == 0 AND $set_heading == TRUE)
			{
				$this->heading = $row;
			}
			else
			{
				$this->rows[] = $row;
			}
			
			$i++;
		}
	}

 	function _compile_template()
 	{ 	
 		if ($this->template == NULL)
 		{
 			$this->template = $this->_default_template();
 			return;
 		}
		
		$this->temp = $this->_default_template();
		foreach (array('table_open','heading_row_start', 'heading_row_end', 'heading_cell_start', 'heading_cell_end', 'row_start', 'row_end', 'cell_start', 'cell_end', 'row_alt_start', 'row_alt_end', 'cell_alt_start', 'cell_alt_end', 'table_close') as $val)
		{
			if ( ! isset($this->template[$val]))
			{
				$this->template[$val] = $this->temp[$val];
			}
		} 	
 	}
	
	function _default_template()
	{
		return  array (
						'table_open' 			=> '<table class="tabelajax">',

						'heading_row_start' 	=> '<tr>',
						'heading_row_end' 		=> '</tr>',
						'heading_cell_start'	=> '<th>',
						'heading_cell_end'		=> '</th>',

						'row_start' 			=> '<tr>',
						'row_end' 				=> '</tr>',
						'cell_start'			=> '<td>',
						'cell_end'				=> '</td>',

						'row_alt_start' 		=> '<tr>',
						'row_alt_end' 			=> '</tr>',
						'cell_alt_start'		=> '<td>',
						'cell_alt_end'			=> '</td>',

						'table_close' 			=> '</table>'
					);	
	}
}