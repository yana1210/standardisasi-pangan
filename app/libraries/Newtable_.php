<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Newtable {
	var $rows				= array();
	var $columns			= array();
	var $hiderows			= array();
	var $keys				= array();
	var $proses				= array();
	var $keycari			= array();
	var $heading			= array();
	var $auto_heading		= TRUE;
	var $show_chk			= TRUE;
	var $caption			= NULL;	
	var $template 			= NULL;
	var $newline			= "\n";
	var $empty_cells		= "";
	var $actions			= "";
	var $detils				= "";
	var $baris				= "AUTO";
	var $db 				= "";
	var $hal 				= "AUTO";
	var $uri				= "";
	var $show_search		= TRUE;
	var $orderby			= 1;
	var $sortby				= "ASC";
	
	function Newtable()
	{
		$this->hiderows[] = 'HAL';
	}
	
	function show_search($show)
	{
		$this->show_search = $show;
		return;
	}
	
	function show_chk($show)
	{
		$this->show_chk = $show;
		return;
	}
	
	function columns($col)
	{
		$this->columns = $col;
		return;
	}
	
	function orderby($order)
	{
		$this->orderby = $order;
		return;
	}
	
	function sortby($sort)
	{
		$this->sortby = $sort;
		return;
	}
	
	function topage($to)
	{
		$this->hal = (int)$to;
		return;
	}
	
	function cidb($db)
	{
		$this->db = $db;
		return;
	}
	
	function rowcount($row)
	{
		$this->baris = $row;
		return;
	}
	
	function ciuri($uri)
	{
		$this->uri = $uri;
		return;
	}
	
	function action($act)
	{
		$this->actions = $act;
		return;
	}
	
	function detail($act)
	{
		$this->detils = $act;
		return;
	}
	
	function hiddens($row)
	{
		if ( ! is_array($row))
		{
			$row = array($row);
		}
		foreach ( $row as $a )
		{
			if ( ! in_array($a, $this->hiderows) ) $this->hiderows[] = $a;
		}
		return;
	}
	
	function keys($row)
	{
		if ( ! is_array($row))
		{
			$row = array($row);
		}
		foreach ( $row as $a )
		{
			if ( ! in_array($a, $this->keys) ) $this->keys[] = $a;
		}
		return;
	}
	
	function menu($row)
	{
		if ( ! is_array($row))
		{
			return FALSE;
		}
		$this->proses = $row;
		return;
	}
	
	function search($row)
	{
		if ( ! is_array($row))
		{
			return FALSE;
		}
		$this->keycari = $row;
		return;
	}
	
	function set_template($template)
	{
		if ( ! is_array($template)) return FALSE;
		$this->template = $template;
	}
	
	function set_heading()
	{
		$args = func_get_args();
		$this->heading = (is_array($args[0])) ? $args[0] : $args;
	}
	
	function make_columns($array = array(), $col_limit = 0)
	{
		if ( ! is_array($array) OR count($array) == 0) return FALSE;
		$this->auto_heading = FALSE;
		if ($col_limit == 0) return $array;
		$new = array();
		while(count($array) > 0)
		{	
			$temp = array_splice($array, 0, $col_limit);
			if (count($temp) < $col_limit)
			{
				for ($i = count($temp); $i < $col_limit; $i++)
				{
					$temp[] = '&nbsp;';
				}
			}
			$new[] = $temp;
		}
		return $new;
	}

	function set_empty($value)
	{
		$this->empty_cells = $value;
	}
	
	function add_row()
	{
		$args = func_get_args();
		$this->rows[] = (is_array($args[0])) ? $args[0] : $args;
	}

	function set_caption($caption)
	{
		$this->caption = $caption;
	}	

	function generate($table_data = NULL)
	{
		if ( ! is_null($table_data))
		{
			if (is_object($table_data))
			{
				$this->_set_from_object($table_data);
			}
			elseif (is_array($table_data))
			{
				$set_heading = (count($this->heading) == 0 AND $this->auto_heading == FALSE) ? FALSE : TRUE;
				$this->_set_from_array($table_data, $set_heading);
			}
			elseif ($table_data!="")
			{
				if ( !is_array($this->columns) || $this->db == "" || !is_array($this->uri)) return 'Missing required params';
				$kunci = "";
				$terkunci = "";
				$cari = "";
				$tercari = "";
				if($key = array_search('search', $this->uri))
				{
					$kunci = (int)$this->uri[$key+1];
					if ( array_key_exists($kunci, $this->keycari))
					{
						//print_r($this->keycari);
						$terkunci = $this->keycari[$kunci];
						$terkunci = $terkunci[0];
						$cari = $this->uri[$key+2];
						if ($cari != "")
						{
							$cari = str_replace("'", "''", $cari);
							if(substr($terkunci, 0, 4)=="{IN}"){
								$terkunci = substr($terkunci, 4);
								$tercari = str_replace("{LIKE}", "LIKE '%$cari%'", $terkunci);
							}else{
								$tercari = "$terkunci LIKE '%$cari%'";
							}
						}
					}
				}
				if ( $this->baris == "AUTO")
				{
					if ( $key = array_search('row', $this->uri) ) $this->baris = (int)$this->uri[$key+1];
					if ( $this->baris<1 ) $this->baris = 10;
				}
				if ( $this->baris != "ALL")
				{
					//if ( $this->baris > 100) $this->baris = 100;
				}	
				if ($tercari!="")
				{
					$ada = strpos(strtolower($table_data), "where");
					if ( $ada === false )
						$table_data .= " WHERE $tercari";
					else
						$table_data .= " AND $tercari";
				}
				#echo $table_data;
				$total_record = 0;
				$table_count = $this->db->query("SELECT COUNT(*) AS JML FROM ($table_data) AS TBL");
				if ( $table_count )
				{
					$table_count = $table_count->row();
					$total_record = $table_count = $table_count->JML;
				}
				else
				{
					$total_record = 0;
				}
				#print($total_record); die();
				if ( $this->baris != "ALL")
				{
					$table_count = ceil($table_count / $this->baris);
					if ( $this->hal == "AUTO" ) if ( $key = array_search('page', $this->uri) ) $this->hal = (int)$this->uri[$key+1];
					if ( $this->hal < 1 ) $this->hal = 1;
					if ( $this->hal > $table_count ) $this->hal = $table_count;
					if ( $this->hal==1 )
					{
						$dari = $this->hal;
						$sampai = $this->baris;
					}
					else
					{
						$dari = ($this->hal * $this->baris) - $this->baris + 1;
						$sampai = $this->hal * $this->baris;
					}
					if ($key = array_search('order', $this->uri))
					{
						$this->orderby = (int)$this->uri[$key+1];
						$this->sortby = $this->uri[$key+2];
						$orderby = $this->columns[$this->orderby-1];
						if ( is_array($orderby) ) $orderby = $orderby[0];
					}
					else
					{
						if ( is_numeric($this->orderby) ) $orderby = $this->columns[$this->orderby-1];
						else $orderby = $this->orderby;
					}
					$table_data = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY $orderby $this->sortby) AS HAL, ".substr($table_data, 6)." ) AS TBLTMP WHERE HAL >= $dari AND HAL <= $sampai";
				}
				#die($table_data);
				$table_data = $this->db->query($table_data);
				$this->_set_from_object($table_data);
				#print_r($table_data); die();
			}
		}
	
		if (count($this->heading) == 0 AND count($this->rows) == 0)
		{
			return 'Undefined table data';
		}
	
		$this->_compile_template();
		if ($this->show_search || $this->show_chk)
		{
			$out = $this->template['table_open'].'<form id="tb_form" action="'.$this->actions.'"><tr class="head"><th colspan="'.count($this->heading).'">';
		}
		else
		{
			$out = $this->template['table_open'].'<form id="tb_form" action="'.$this->actions.'">';
		}
		$out .= '<input type="hidden" id="orderby" value="'.$this->orderby.'"><input type="hidden" id="sortby" value="'.$this->sortby.'">';
		if (count($this->proses) > 0 && $this->show_chk )
		{
			$out .= '<select id="tb_menu" title="Pilih proses yang akan dijalankan"><option url="">Pilih Proses</option>';
			foreach ($this->proses as $a => $b)
			{
				$out .= '<option met="';
				$out .= $b[0];
				$out .= '" jml="';
				$out .= $b[2];
				$out .= '" url="';
				$out .= $b[1];
				$out .= '">';
				$out .= "- $a";
				$out .= '</option>';
			}
			$out .= '</select> &nbsp;<label id="labelload">Loading..</label>';
		}
		if ($this->show_search)
		{
			$out .= '<span>Cari <select id="tb_keycari" title="Pilih kategori yang akan dicari">';
			foreach ($this->keycari as $a => $b)
			{
				if ( $kunci==$a )
					$out .= '<option selected value="';
				else
					$out .= '<option value="';
				$out .= $a;
				$out .= '">';
				$out .= $b[1];
				$out .= '</option>';
			}
			$out .= '</select> <input type="text" class="tb_text" id="tb_cari" title="Masukkan kata kunci yang ingin dicari kemudian tekan Enter" value="'.$cari.'" /></span>';
		}
		if ($this->show_search || $this->show_chk)
		{
			$out .= '</th></tr>';
		}
		if ($this->caption)
		{
			$out .= $this->newline;
			$out .= '<caption>' . $this->caption . '</caption>';
			$out .= $this->newline;
		}

		if (count($this->heading) > 0)
		{
			$out .= $this->template['heading_row_start'];
			$out .= $this->newline;
			foreach($this->heading as $z => $heading)
			{
				$z--;
				#echo "$z => $heading<hr>";
				if ( ! in_array($heading, $this->hiderows))
				{
					if ( $z < 0 && $this->show_chk){
						$out .= '<th width="22">';
						$out .= $heading;
					}else{
						$out .= $this->template['heading_cell_start'];
						if ( $this->baris != "ALL")
						{
							if ( $z==$this->orderby ){
								if ( $this->sortby=="ASC" )
									$out .= "<span class=\"order\" title=\"Urutkan Data berdasarkan ".$heading." (Z-A)\" orderby=\"$z\" sortby=\"DESC\">$heading</span>";
								else
									$out .= "<span class=\"order\" title=\"Urutkan Data berdasarkan ".$heading." (A-Z)\" orderby=\"$z\" sortby=\"ASC\">$heading</span>";
							}else{
								$out .= "<span class=\"order\" title=\"Urutkan Data berdasarkan ".$heading." (A-Z)\" orderby=\"$z\" sortby=\"ASC\">$heading</span>";
							}
						}
						else
						{
							$out .= "<span class=\"order\" orderby=\"$z\" sortby=\"ASC\">$heading</span>";
						}
					}
					$out .= $this->template['heading_cell_end'];
				}
			}

			$out .= $this->template['heading_row_end'];
			$out .= $this->newline;				
		}

		if (count($this->rows) > 0)
		{
			$i = 1;
			foreach($this->rows as $row)
			{
				if ( ! is_array($row))
				{
					break;
				}
				
				$keyz = "";
				$koma = "";
				foreach ($this->keys as $a)
				{
					$keyz .= $koma.$row[$a];
					$koma = ".";
				}
				$name = (fmod($i++, 2)) ? '' : 'alt_';
			
				//$out .= $this->template['row_'.$name.'start'];
				if ($this->detils=="")
					$out .= '<tr urldetil="">';
				else
					$out .= '<tr title="Klik Kanan untuk menampilkan detil data" urldetil="'.$this->detils.'/'.$keyz.'">';
				$out .= $this->newline;
				
				if ($this->show_chk) $out .= '<td><input type="checkbox" name="tb_chk[]" class="tb_chk" value="'.$keyz.'" /></td>';
				$seq = -1;
				foreach($row as $rowz => $cell)
				{
					if ( !in_array($rowz, $this->hiderows))
					{
						$out .= $this->template['cell_'.$name.'start'];
						if ($cell === "")
						{
							$out .= $this->empty_cells;
						}
						else
						{
							$url_col = $this->columns[$seq];
							if ( is_array($url_col) )
							{
								$new_url_col = $url_col[1];
								$url_col = explode("{", $new_url_col);
								foreach($url_col as $x){
									$temp_url_col = explode("}", $x);
									$temp_url_col = $temp_url_col[0];
									$new_url_col = str_replace("{".$temp_url_col."}", $row[$temp_url_col], $new_url_col);
								}
								$out .= '<a href="'.$new_url_col.'">'.$cell.'</a>';
							}
							else
							{
								$out .= $cell;
							}
						}
						$out .= $this->template['cell_'.$name.'end'];
					}
					$seq++;
				}
	
				$out .= $this->template['row_'.$name.'end'];
				$out .= $this->newline;	
			}
		}
		else
		{
			$out .= '<tr><td colspan="'.count($this->heading).'">Data Tidak Ditemukan</td></tr>';
		}
		if ( $this->baris != "ALL")
		{
			$datast = ($this->hal - 1);
			if ( $datast<1 ) $datast = 1;
			else $datast = $datast * $this->baris + 1;
			$dataen = $datast + $this->baris - 1;
			if ( $total_record < $dataen ) $dataen = $total_record;
			if ( $total_record==0 ) $datast = 0;
			$out .= '<tr class="head">
				<th colspan="'.count($this->heading).'">
					<input type="text" class="tb_text" id="tb_view" title="Masukkan jumlah data yang ingin ditampilkan kemudian tekan Enter" value="'.$this->baris.'" /> Data Per-Halaman. Menampilkan '.$datast.' - '.$dataen.' Dari '.$total_record.' Data.
					<span>Halaman <input type="text" class="tb_text" id="tb_hal" title="Masukkan nomor halaman yang diinginkan kemudian tekan Enter, atau Double Click untuk ke halaman berikutnya, atau Klik Kanan untuk ke halaman sebelumnya" value="'.$this->hal.'" /> Dari '.$table_count.'</span>
				</th>
			</tr></form>';
		}
		else
		{
			$out .= '<tr class="head">
				<th colspan="'.count($this->heading).'">
					Total '.$total_record.' Data.
				</th>
			</tr></form>';
		}
		$out .= $this->template['table_close'];
	
		return $out;
	}
	
	function clear()
	{
		$this->rows				= array();
		$this->heading			= array();
		$this->auto_heading		= TRUE;	
	}
	
	function _set_from_object($query)
	{
		if ( ! is_object($query))
		{
			return FALSE;
		}
		
		if (count($this->heading) == 0)
		{
			if ( ! method_exists($query, 'list_fields'))
			{
				return FALSE;
			}
			empty($this->heading);
			if ( $this->show_chk ) $this->heading[] = '<input type="checkbox" id="tb_chkall" />';
			foreach ($query->list_fields() as $a)
			{
				//if ( ! in_array($a, $this->hiderows)) $this->heading[] = $a;
				$this->heading[] = $a;
			}
			//print_r($this->heading);
		}
		
		
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$this->rows[] = $row;
			}
		}
	}

	function _set_from_array($data, $set_heading = TRUE)
	{
		if ( ! is_array($data) OR count($data) == 0)
		{
			return FALSE;
		}
		
		$i = 0;
		foreach ($data as $row)
		{
			if ( ! is_array($row))
			{
				$this->rows[] = $data;
				break;
			}
						
			if ($i == 0 AND count($data) > 1 AND count($this->heading) == 0 AND $set_heading == TRUE)
			{
				$this->heading = $row;
			}
			else
			{
				$this->rows[] = $row;
			}
			
			$i++;
		}
	}

 	function _compile_template()
 	{ 	
 		if ($this->template == NULL)
 		{
 			$this->template = $this->_default_template();
 			return;
 		}
		
		$this->temp = $this->_default_template();
		foreach (array('table_open','heading_row_start', 'heading_row_end', 'heading_cell_start', 'heading_cell_end', 'row_start', 'row_end', 'cell_start', 'cell_end', 'row_alt_start', 'row_alt_end', 'cell_alt_start', 'cell_alt_end', 'table_close') as $val)
		{
			if ( ! isset($this->template[$val]))
			{
				$this->template[$val] = $this->temp[$val];
			}
		} 	
 	}
	
	function _default_template()
	{
		return  array (
						'table_open' 			=> '<table class="tabelajax">',

						'heading_row_start' 	=> '<tr>',
						'heading_row_end' 		=> '</tr>',
						'heading_cell_start'	=> '<th>',
						'heading_cell_end'		=> '</th>',

						'row_start' 			=> '<tr>',
						'row_end' 				=> '</tr>',
						'cell_start'			=> '<td>',
						'cell_end'				=> '</td>',

						'row_alt_start' 		=> '<tr>',
						'row_alt_end' 			=> '</tr>',
						'cell_alt_start'		=> '<td>',
						'cell_alt_end'			=> '</td>',

						'table_close' 			=> '</table>'
					);	
	}
}