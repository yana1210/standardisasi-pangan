<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$active_group = "default";
$active_record = TRUE;
/*
$db['default']['hostname'] = "10.1.5.226";
$db['default']['username'] = "bpom";
$db['default']['password'] = "bpom3d11";
$db['default']['database'] = "esp";
$db['default']['dbdriver'] = "mysql";
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

$db['default']['hostname'] = "172.16.1.52";
$db['default']['username'] = "ereg";
$db['default']['password'] = "ereg@2013";
$db['default']['database'] = "test";
$db['default']['dbdriver'] = "mysql";
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

*/
$db['default']['hostname'] = "localhost";
$db['default']['username'] = "root";
$db['default']['password'] = "";
$db['default']['database'] = "estd";
$db['default']['dbdriver'] = "mysqli";
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

$db['simplisia']['hostname'] = "172.16.1.52";
$db['simplisia']['username'] = "ereg";
$db['simplisia']['password'] = "ereg@2013";
$db['simplisia']['database'] = "simplisia";
$db['simplisia']['dbdriver'] = "mysql";
$db['simplisia']['dbprefix'] = "";
$db['simplisia']['pconnect'] = FALSE;
$db['simplisia']['db_debug'] = TRUE;
$db['simplisia']['cache_on'] = FALSE;
$db['simplisia']['cachedir'] = "";
$db['simplisia']['char_set'] = "utf8";
$db['simplisia']['dbcollat'] = "utf8_general_ci";

$db['otsm']['hostname'] = "172.16.1.52";
$db['otsm']['username'] = "ereg";
$db['otsm']['password'] = "ereg@2013";
$db['otsm']['database'] = "otsm";
$db['otsm']['dbdriver'] = "mysql";
$db['otsm']['dbprefix'] = "";
$db['otsm']['pconnect'] = FALSE;
$db['otsm']['db_debug'] = TRUE;
$db['otsm']['cache_on'] = FALSE;
$db['otsm']['cachedir'] = "";
$db['otsm']['char_set'] = "utf8";
$db['otsm']['dbcollat'] = "utf8_general_ci";

/*$db['webreg']['hostname'] = "192.168.4.61";
$db['webreg']['username'] = "sa";
$db['webreg']['password'] = "it@bpom2011";
$db['webreg']['database'] = "webreg";
$db['webreg']['dbdriver'] = "mssql";
$db['webreg']['dbprefix'] = "";
$db['webreg']['pconnect'] = FALSE;
$db['webreg']['db_debug'] = TRUE;
$db['webreg']['cache_on'] = FALSE;
$db['webreg']['cachedir'] = "";
$db['webreg']['char_set'] = "utf8";
$db['webreg']['dbcollat'] = "utf8_general_ci";*/