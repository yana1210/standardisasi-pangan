<?php
error_reporting(E_ERROR);
/* excel
header("Cache-Control: no-stor,no-cache,must-revalidate");
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-control: private");
header("Content-Type: application/force-download");
header("Content-Disposition: inline; attachment;
filename=cimarron.xls");
header("Content-Transfer-Encoding: binary");
header("Pragma: no-cache");
header("Expires: 0");

*/ 

$system_folder = "sys";
$application_folder = "app";
if (strpos($system_folder, '/') === FALSE) {
	if (function_exists('realpath') AND @realpath(dirname(__FILE__)) !== FALSE)
	{
		$system_folder = realpath(dirname(__FILE__)).'/'.$system_folder;
	}
} else {
	$system_folder = str_replace("\\", "/", $system_folder); 
}
define('EXT', '.php');
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('FCPATH', str_replace(SELF, '', __FILE__));
define('BASEPATH', $system_folder.'/');
if (is_dir($application_folder)) {
	define('APPPATH', $application_folder.'/');
} else {
	if ($application_folder == '') $application_folder = 'app';
	define('APPPATH', BASEPATH.$application_folder.'/');
}

require_once BASEPATH.'codeigniter/CodeIgniter'.EXT;